@n = dso_local global i32 0

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @bubblesort(i32* %0){
1:
    %2 = alloca i32*
    store i32* %0, i32** %2
    %3 = alloca i32
    %4 = alloca i32
    store i32 0, i32* %3
    br %5

5:
    %8 = load i32, i32* %3
    %9 = load i32, i32* @n
    %10 = load i32, i32* @n
    %11 = sub i32 %10,1
    %12 = icmp lt i32 %8,i32 %11
    br i32 %12 %6 %7

6:
    store i32 0, i32* %4
    br %13

7:
    ret i32 0

13:
    %16 = load i32, i32* %4
    %17 = load i32, i32* @n
    %18 = load i32, i32* %3
    %19 = load i32, i32* @n
    %20 = load i32, i32* %3
    %21 = sub i32 %19,%20
    %22 = sub i32 %21,1
    %23 = icmp lt i32 %16,i32 %22
    br i32 %23 %14 %15

14:
    %26 = load i32, i32* %4
    %27 = getelementptr i32*, i32** %2, i32 %26
    %28 = load i32*, i32** %27
    %29 = load i32, i32* %4
    %30 = load i32, i32* %4
    %31 = add i32 %30,1
    %32 = getelementptr i32*, i32** %2, i32 %31
    %33 = load i32*, i32** %32
    %34 = icmp gt i32* %28,i32* %33
    br i32* %34 %25 %24

15:
    %54 = load i32, i32* %3
    %55 = load i32, i32* %3
    %56 = add i32 %55,1
    store i32 %56, i32* %3
    br %5

24:
    %51 = load i32, i32* %4
    %52 = load i32, i32* %4
    %53 = add i32 %52,1
    store i32 %53, i32* %4
    br %13

25:
    %35 = alloca i32
    %36 = load i32, i32* %4
    %37 = load i32, i32* %4
    %38 = add i32 %37,1
    %39 = getelementptr i32*, i32** %2, i32 %38
    %40 = load i32*, i32** %39
    store i32* %40, i32* %35
    %41 = load i32, i32* %4
    %42 = load i32, i32* %4
    %43 = add i32 %42,1
    %44 = getelementptr i32*, i32** %2, i32 %43
    %45 = load i32, i32* %4
    %46 = getelementptr i32*, i32** %2, i32 %45
    %47 = load i32*, i32** %46
    store i32* %47, i32** %44
    %48 = load i32, i32* %4
    %49 = getelementptr i32*, i32** %2, i32 %48
    %50 = load i32, i32* %35
    store i32 %50, i32** %49
    br %24
}
define dso_local i32 @insertsort(i32* %0){
1:
    %2 = alloca i32*
    store i32* %0, i32** %2
    %3 = alloca i32
    store i32 1, i32* %3
    br %4

4:
    %7 = load i32, i32* %3
    %8 = load i32, i32* @n
    %9 = icmp lt i32 %7,i32 %8
    br i32 %9 %5 %6

5:
    %10 = alloca i32
    %11 = load i32, i32* %3
    %12 = getelementptr i32*, i32** %2, i32 %11
    %13 = load i32*, i32** %12
    store i32* %13, i32* %10
    %14 = alloca i32
    %15 = load i32, i32* %3
    %16 = load i32, i32* %3
    %17 = sub i32 %16,1
    store i32 %17, i32* %14
    br %18

6:
    ret i32 0

18:
    %21 = load i32, i32* %14
    %22 = sub i32 0,1
    %23 = icmp gt i32 %21,i32 %22
    %24 = load i32, i32* %10
    %25 = load i32, i32* %14
    %26 = getelementptr i32*, i32** %2, i32 %25
    %27 = load i32*, i32** %26
    %28 = icmp lt i32 %24,i32* %27
    %29 = and i32 %23,%28
    br i32 %29 %19 %20

19:
    %30 = load i32, i32* %14
    %31 = load i32, i32* %14
    %32 = add i32 %31,1
    %33 = getelementptr i32*, i32** %2, i32 %32
    %34 = load i32, i32* %14
    %35 = getelementptr i32*, i32** %2, i32 %34
    %36 = load i32*, i32** %35
    store i32* %36, i32** %33
    %37 = load i32, i32* %14
    %38 = load i32, i32* %14
    %39 = sub i32 %38,1
    store i32 %39, i32* %14
    br %18

20:
    %40 = load i32, i32* %14
    %41 = load i32, i32* %14
    %42 = add i32 %41,1
    %43 = getelementptr i32*, i32** %2, i32 %42
    %44 = load i32, i32* %10
    store i32 %44, i32** %43
    %45 = load i32, i32* %3
    %46 = load i32, i32* %3
    %47 = add i32 %46,1
    store i32 %47, i32* %3
    br %4
}
define dso_local i32 @QuickSort(i32* %0, i32 %1, i32 %2){
3:
    %4 = alloca i32*
    store i32* %0, i32** %4
    %5 = alloca i32
    store i32 %1, i32* %5
    %6 = alloca i32
    store i32 %2, i32* %6
    %9 = load i32, i32* %5
    %10 = load i32, i32* %6
    %11 = icmp lt i32 %9,i32 %10
    br i32 %11 %8 %7

7:
    ret i32 0

8:
    %12 = alloca i32
    %13 = load i32, i32* %5
    store i32 %13, i32* %12
    %14 = alloca i32
    %15 = load i32, i32* %6
    store i32 %15, i32* %14
    %16 = alloca i32
    %17 = load i32, i32* %5
    %18 = getelementptr i32*, i32** %4, i32 %17
    %19 = load i32*, i32** %18
    store i32* %19, i32* %16
    br %20

20:
    %23 = load i32, i32* %12
    %24 = load i32, i32* %14
    %25 = icmp lt i32 %23,i32 %24
    br i32 %25 %21 %22

21:
    br %26

22:
    %84 = load i32, i32* %12
    %85 = getelementptr i32*, i32** %4, i32 %84
    %86 = load i32, i32* %16
    store i32 %86, i32** %85
    %87 = alloca i32
    %88 = load i32, i32* %12
    %89 = load i32, i32* %12
    %90 = sub i32 %89,1
    store i32 %90, i32* %87
    %91 = load i32*, i32** %4
    %92 = load i32, i32* %5
    %93 = load i32, i32* %87
    %94 = call i32 @QuickSort(i32* %91,i32 %92,i32 %93)
    store i32 %94, i32* %87
    %95 = load i32, i32* %12
    %96 = load i32, i32* %12
    %97 = add i32 %96,1
    store i32 %97, i32* %87
    %98 = load i32*, i32** %4
    %99 = load i32, i32* %87
    %100 = load i32, i32* %6
    %101 = call i32 @QuickSort(i32* %98,i32 %99,i32 %100)
    store i32 %101, i32* %87

26:
    %29 = load i32, i32* %12
    %30 = load i32, i32* %14
    %31 = icmp lt i32 %29,i32 %30
    %32 = load i32, i32* %14
    %33 = getelementptr i32*, i32** %4, i32 %32
    %34 = load i32*, i32** %33
    %35 = load i32, i32* %16
    %36 = load i32, i32* %16
    %37 = sub i32 %36,1
    %38 = icmp gt i32* %34,i32 %37
    %39 = and i32 %31,%38
    br i32 %39 %27 %28

27:
    %40 = load i32, i32* %14
    %41 = load i32, i32* %14
    %42 = sub i32 %41,1
    store i32 %42, i32* %14
    br %26

28:
    %45 = load i32, i32* %12
    %46 = load i32, i32* %14
    %47 = icmp lt i32 %45,i32 %46
    br i32 %47 %44 %43

43:
    br %56

44:
    %48 = load i32, i32* %12
    %49 = getelementptr i32*, i32** %4, i32 %48
    %50 = load i32, i32* %14
    %51 = getelementptr i32*, i32** %4, i32 %50
    %52 = load i32*, i32** %51
    store i32* %52, i32** %49
    %53 = load i32, i32* %12
    %54 = load i32, i32* %12
    %55 = add i32 %54,1
    store i32 %55, i32* %12
    br %43

56:
    %59 = load i32, i32* %12
    %60 = load i32, i32* %14
    %61 = icmp lt i32 %59,i32 %60
    %62 = load i32, i32* %12
    %63 = getelementptr i32*, i32** %4, i32 %62
    %64 = load i32*, i32** %63
    %65 = load i32, i32* %16
    %66 = icmp lt i32* %64,i32 %65
    %67 = and i32 %61,%66
    br i32 %67 %57 %58

57:
    %68 = load i32, i32* %12
    %69 = load i32, i32* %12
    %70 = add i32 %69,1
    store i32 %70, i32* %12
    br %56

58:
    %73 = load i32, i32* %12
    %74 = load i32, i32* %14
    %75 = icmp lt i32 %73,i32 %74
    br i32 %75 %72 %71

71:
    br %20

72:
    %76 = load i32, i32* %14
    %77 = getelementptr i32*, i32** %4, i32 %76
    %78 = load i32, i32* %12
    %79 = getelementptr i32*, i32** %4, i32 %78
    %80 = load i32*, i32** %79
    store i32* %80, i32** %77
    %81 = load i32, i32* %14
    %82 = load i32, i32* %14
    %83 = sub i32 %82,1
    store i32 %83, i32* %14
    br %71
}
define dso_local i32 @getMid(i32* %0){
1:
    %2 = alloca i32*
    store i32* %0, i32** %2
    %3 = alloca i32
    %7 = load i32, i32* @n
    %8 = srem i32 %7,2
    %9 = icmp eq i32 %8,i32 0
    br i32 %9 %5 %6

4:

5:
    %10 = load i32, i32* @n
    %11 = sdiv i32 %10,2
    store i32 %11, i32* %3
    %12 = load i32, i32* %3
    %13 = getelementptr i32*, i32** %2, i32 %12
    %14 = load i32*, i32** %13
    %15 = load i32, i32* %3
    %16 = load i32, i32* %3
    %17 = sub i32 %16,1
    %18 = getelementptr i32*, i32** %2, i32 %17
    %19 = load i32*, i32** %18
    %20 = load i32, i32* %3
    %21 = getelementptr i32*, i32** %2, i32 %20
    %22 = load i32*, i32** %21
    %23 = load i32, i32* %3
    %24 = load i32, i32* %3
    %25 = sub i32 %24,1
    %26 = getelementptr i32*, i32** %2, i32 %25
    %27 = load i32*, i32** %26
    %28 = add i32 %22,%27
    %29 = sdiv i32 %28,2
    ret i32* %29

6:
    %30 = load i32, i32* @n
    %31 = sdiv i32 %30,2
    store i32 %31, i32* %3
    %32 = load i32, i32* %3
    %33 = getelementptr i32*, i32** %2, i32 %32
    %34 = load i32*, i32** %33
    ret i32* %34
}
define dso_local i32 @getMost(i32* %0){
1:
    %2 = alloca i32*
    store i32* %0, i32** %2
    %3 = alloca [1000 x i32]
    %4 = alloca i32
    store i32 0, i32* %4
    br %5

5:
    %8 = load i32, i32* %4
    %9 = icmp lt i32 %8,i32 1000
    br i32 %9 %6 %7

6:
    %10 = load i32, i32* %4
    %11 = getelementptr [1000 x i32], [1000 x i32]* %3, i32 %10
    store i32 0, i32* %11
    %12 = load i32, i32* %4
    %13 = load i32, i32* %4
    %14 = add i32 %13,1
    store i32 %14, i32* %4
    br %5

7:
    store i32 0, i32* %4
    %15 = alloca i32
    %16 = alloca i32
    store i32 0, i32* %15
    br %17

17:
    %20 = load i32, i32* %4
    %21 = load i32, i32* @n
    %22 = icmp lt i32 %20,i32 %21
    br i32 %22 %18 %19

18:
    %23 = alloca i32
    %24 = load i32, i32* %4
    %25 = getelementptr i32*, i32** %2, i32 %24
    %26 = load i32*, i32** %25
    store i32* %26, i32* %23
    %27 = load i32, i32* %23
    %28 = getelementptr [1000 x i32], [1000 x i32]* %3, i32 %27
    %29 = load i32, i32* %23
    %30 = getelementptr [1000 x i32], [1000 x i32]* %3, i32 %29
    %31 = load i32, i32* %30
    %32 = load i32, i32* %23
    %33 = getelementptr [1000 x i32], [1000 x i32]* %3, i32 %32
    %34 = load i32, i32* %33
    %35 = add i32 %34,1
    store i32 %35, i32* %28
    %38 = load i32, i32* %23
    %39 = getelementptr [1000 x i32], [1000 x i32]* %3, i32 %38
    %40 = load i32, i32* %39
    %41 = load i32, i32* %15
    %42 = icmp gt i32 %40,i32 %41
    br i32 %42 %37 %36

19:
    %50 = load i32, i32* %16
    ret i32 %50

36:
    %47 = load i32, i32* %4
    %48 = load i32, i32* %4
    %49 = add i32 %48,1
    store i32 %49, i32* %4
    br %17

37:
    %43 = load i32, i32* %23
    %44 = getelementptr [1000 x i32], [1000 x i32]* %3, i32 %43
    %45 = load i32, i32* %44
    store i32 %45, i32* %15
    %46 = load i32, i32* %23
    store i32 %46, i32* %16
    br %36
}
define dso_local i32 @revert(i32* %0){
1:
    %2 = alloca i32*
    store i32* %0, i32** %2
    %3 = alloca i32
    %4 = alloca i32
    %5 = alloca i32
    store i32 0, i32* %4
    store i32 0, i32* %5
    br %6

6:
    %9 = load i32, i32* %4
    %10 = load i32, i32* %5
    %11 = icmp lt i32 %9,i32 %10
    br i32 %11 %7 %8

7:
    %12 = load i32, i32* %4
    %13 = getelementptr i32*, i32** %2, i32 %12
    %14 = load i32*, i32** %13
    store i32* %14, i32* %3
    %15 = load i32, i32* %4
    %16 = getelementptr i32*, i32** %2, i32 %15
    %17 = load i32, i32* %5
    %18 = getelementptr i32*, i32** %2, i32 %17
    %19 = load i32*, i32** %18
    store i32* %19, i32** %16
    %20 = load i32, i32* %5
    %21 = getelementptr i32*, i32** %2, i32 %20
    %22 = load i32, i32* %3
    store i32 %22, i32** %21
    %23 = load i32, i32* %4
    %24 = load i32, i32* %4
    %25 = add i32 %24,1
    store i32 %25, i32* %4
    %26 = load i32, i32* %5
    %27 = load i32, i32* %5
    %28 = sub i32 %27,1
    store i32 %28, i32* %5
    br %6

8:
    ret i32 0
}
define dso_local i32 @arrCopy(i32* %0, i32* %1){
2:
    %3 = alloca i32*
    store i32* %0, i32** %3
    %4 = alloca i32*
    store i32* %1, i32** %4
    %5 = alloca i32
    store i32 0, i32* %5
    br %6

6:
    %9 = load i32, i32* %5
    %10 = load i32, i32* @n
    %11 = icmp lt i32 %9,i32 %10
    br i32 %11 %7 %8

7:
    %12 = load i32, i32* %5
    %13 = getelementptr i32*, i32** %4, i32 %12
    %14 = load i32, i32* %5
    %15 = getelementptr i32*, i32** %3, i32 %14
    %16 = load i32*, i32** %15
    store i32* %16, i32** %13
    %17 = load i32, i32* %5
    %18 = load i32, i32* %5
    %19 = add i32 %18,1
    store i32 %19, i32* %5
    br %6

8:
    ret i32 0
}
define dso_local i32 @calSum(i32* %0, i32 %1){
2:
    %3 = alloca i32*
    store i32* %0, i32** %3
    %4 = alloca i32
    store i32 %1, i32* %4
    %5 = alloca i32
    store i32 0, i32* %5
    %6 = alloca i32
    store i32 0, i32* %6
    br %7

7:
    %10 = load i32, i32* %6
    %11 = load i32, i32* @n
    %12 = icmp lt i32 %10,i32 %11
    br i32 %12 %8 %9

8:
    %13 = load i32, i32* %5
    %14 = load i32, i32* %6
    %15 = getelementptr i32*, i32** %3, i32 %14
    %16 = load i32*, i32** %15
    %17 = load i32, i32* %5
    %18 = load i32, i32* %6
    %19 = getelementptr i32*, i32** %3, i32 %18
    %20 = load i32*, i32** %19
    %21 = add i32 %17,%20
    store i32 %21, i32* %5
    %25 = load i32, i32* %6
    %26 = load i32, i32* %4
    %27 = srem i32 %25,%26
    %28 = load i32, i32* %4
    %29 = load i32, i32* %4
    %30 = sub i32 %29,1
    %31 = icmp ne i32 %27,i32 %30
    br i32 %31 %23 %24

9:
    ret i32 0

22:
    %37 = load i32, i32* %6
    %38 = load i32, i32* %6
    %39 = add i32 %38,1
    store i32 %39, i32* %6
    br %7

23:
    %32 = load i32, i32* %6
    %33 = getelementptr i32*, i32** %3, i32 %32
    store i32 0, i32** %33
    br %22

24:
    %34 = load i32, i32* %6
    %35 = getelementptr i32*, i32** %3, i32 %34
    %36 = load i32, i32* %5
    store i32 %36, i32** %35
    store i32 0, i32* %5
    br %22
}
define dso_local i32 @avgPooling(i32* %0, i32 %1){
2:
    %3 = alloca i32*
    store i32* %0, i32** %3
    %4 = alloca i32
    store i32 %1, i32* %4
    %5 = alloca i32
    %6 = alloca i32
    store i32 0, i32* %6
    store i32 0, i32* %5
    %7 = alloca i32
    br %8

8:
    %11 = load i32, i32* %6
    %12 = load i32, i32* @n
    %13 = icmp lt i32 %11,i32 %12
    br i32 %13 %9 %10

9:
    %17 = load i32, i32* %6
    %18 = load i32, i32* %4
    %19 = load i32, i32* %4
    %20 = sub i32 %19,1
    %21 = icmp lt i32 %17,i32 %20
    br i32 %21 %15 %16

10:
    %77 = load i32, i32* @n
    %78 = load i32, i32* %4
    %79 = load i32, i32* @n
    %80 = load i32, i32* %4
    %81 = sub i32 %79,%80
    %82 = add i32 %81,1
    store i32 %82, i32* %6
    br %83

14:
    %74 = load i32, i32* %6
    %75 = load i32, i32* %6
    %76 = add i32 %75,1
    store i32 %76, i32* %6
    br %8

15:
    %22 = load i32, i32* %5
    %23 = load i32, i32* %6
    %24 = getelementptr i32*, i32** %3, i32 %23
    %25 = load i32*, i32** %24
    %26 = load i32, i32* %5
    %27 = load i32, i32* %6
    %28 = getelementptr i32*, i32** %3, i32 %27
    %29 = load i32*, i32** %28
    %30 = add i32 %26,%29
    store i32 %30, i32* %5
    br %14

16:
    %34 = load i32, i32* %6
    %35 = load i32, i32* %4
    %36 = load i32, i32* %4
    %37 = sub i32 %36,1
    %38 = icmp eq i32 %34,i32 %37
    br i32 %38 %32 %33

31:

32:
    %39 = getelementptr i32*, i32** %3, i32 0
    %40 = load i32*, i32** %39
    store i32* %40, i32* %7
    %41 = getelementptr i32*, i32** %3, i32 0
    %42 = load i32, i32* %5
    %43 = load i32, i32* %4
    %44 = sdiv i32 %42,%43
    store i32 %44, i32** %41
    br %31

33:
    %45 = load i32, i32* %5
    %46 = load i32, i32* %6
    %47 = getelementptr i32*, i32** %3, i32 %46
    %48 = load i32*, i32** %47
    %49 = load i32, i32* %5
    %50 = load i32, i32* %6
    %51 = getelementptr i32*, i32** %3, i32 %50
    %52 = load i32*, i32** %51
    %53 = add i32 %49,%52
    %54 = load i32, i32* %7
    %55 = sub i32 %53,%54
    store i32 %55, i32* %5
    %56 = load i32, i32* %6
    %57 = load i32, i32* %4
    %58 = load i32, i32* %6
    %59 = load i32, i32* %4
    %60 = sub i32 %58,%59
    %61 = add i32 %60,1
    %62 = getelementptr i32*, i32** %3, i32 %61
    %63 = load i32*, i32** %62
    store i32* %63, i32* %7
    %64 = load i32, i32* %6
    %65 = load i32, i32* %4
    %66 = load i32, i32* %6
    %67 = load i32, i32* %4
    %68 = sub i32 %66,%67
    %69 = add i32 %68,1
    %70 = getelementptr i32*, i32** %3, i32 %69
    %71 = load i32, i32* %5
    %72 = load i32, i32* %4
    %73 = sdiv i32 %71,%72
    store i32 %73, i32** %70
    br %31

83:
    %86 = load i32, i32* %6
    %87 = load i32, i32* @n
    %88 = icmp lt i32 %86,i32 %87
    br i32 %88 %84 %85

84:
    %89 = load i32, i32* %6
    %90 = getelementptr i32*, i32** %3, i32 %89
    store i32 0, i32** %90
    %91 = load i32, i32* %6
    %92 = load i32, i32* %6
    %93 = add i32 %92,1
    store i32 %93, i32* %6
    br %83

85:
    ret i32 0
}
define dso_local i32 @main(i32* %0, i32 %1){
2:
    store i32 32, i32* @n
    %3 = alloca [32 x i32]
    %4 = alloca [32 x i32]
    %5 = getelementptr [32 x i32], [32 x i32]* %3, i32 0
    store i32 7, i32* %5
    %6 = getelementptr [32 x i32], [32 x i32]* %3, i32 1
    store i32 23, i32* %6
    %7 = getelementptr [32 x i32], [32 x i32]* %3, i32 2
    store i32 89, i32* %7
    %8 = getelementptr [32 x i32], [32 x i32]* %3, i32 3
    store i32 26, i32* %8
    %9 = getelementptr [32 x i32], [32 x i32]* %3, i32 4
    store i32 282, i32* %9
    %10 = getelementptr [32 x i32], [32 x i32]* %3, i32 5
    store i32 254, i32* %10
    %11 = getelementptr [32 x i32], [32 x i32]* %3, i32 6
    store i32 27, i32* %11
    %12 = getelementptr [32 x i32], [32 x i32]* %3, i32 7
    store i32 5, i32* %12
    %13 = getelementptr [32 x i32], [32 x i32]* %3, i32 8
    store i32 83, i32* %13
    %14 = getelementptr [32 x i32], [32 x i32]* %3, i32 9
    store i32 273, i32* %14
    %15 = getelementptr [32 x i32], [32 x i32]* %3, i32 10
    store i32 574, i32* %15
    %16 = getelementptr [32 x i32], [32 x i32]* %3, i32 11
    store i32 905, i32* %16
    %17 = getelementptr [32 x i32], [32 x i32]* %3, i32 12
    store i32 354, i32* %17
    %18 = getelementptr [32 x i32], [32 x i32]* %3, i32 13
    store i32 657, i32* %18
    %19 = getelementptr [32 x i32], [32 x i32]* %3, i32 14
    store i32 935, i32* %19
    %20 = getelementptr [32 x i32], [32 x i32]* %3, i32 15
    store i32 264, i32* %20
    %21 = getelementptr [32 x i32], [32 x i32]* %3, i32 16
    store i32 639, i32* %21
    %22 = getelementptr [32 x i32], [32 x i32]* %3, i32 17
    store i32 459, i32* %22
    %23 = getelementptr [32 x i32], [32 x i32]* %3, i32 18
    store i32 29, i32* %23
    %24 = getelementptr [32 x i32], [32 x i32]* %3, i32 19
    store i32 68, i32* %24
    %25 = getelementptr [32 x i32], [32 x i32]* %3, i32 20
    store i32 929, i32* %25
    %26 = getelementptr [32 x i32], [32 x i32]* %3, i32 21
    store i32 756, i32* %26
    %27 = getelementptr [32 x i32], [32 x i32]* %3, i32 22
    store i32 452, i32* %27
    %28 = getelementptr [32 x i32], [32 x i32]* %3, i32 23
    store i32 279, i32* %28
    %29 = getelementptr [32 x i32], [32 x i32]* %3, i32 24
    store i32 58, i32* %29
    %30 = getelementptr [32 x i32], [32 x i32]* %3, i32 25
    store i32 87, i32* %30
    %31 = getelementptr [32 x i32], [32 x i32]* %3, i32 26
    store i32 96, i32* %31
    %32 = getelementptr [32 x i32], [32 x i32]* %3, i32 27
    store i32 36, i32* %32
    %33 = getelementptr [32 x i32], [32 x i32]* %3, i32 28
    store i32 39, i32* %33
    %34 = getelementptr [32 x i32], [32 x i32]* %3, i32 29
    store i32 28, i32* %34
    %35 = getelementptr [32 x i32], [32 x i32]* %3, i32 30
    store i32 1, i32* %35
    %36 = getelementptr [32 x i32], [32 x i32]* %3, i32 31
    store i32 290, i32* %36
    %37 = alloca i32
    %38 = load [32 x i32], [32 x i32]* %3
    %39 = load [32 x i32], [32 x i32]* %4
    %40 = call i32 @arrCopy([32 x i32] %38,[32 x i32] %39)
    store i32 %40, i32* %37
    %41 = load [32 x i32], [32 x i32]* %4
    %42 = call i32 @revert([32 x i32] %41)
    store i32 %42, i32* %37
    %43 = alloca i32
    store i32 0, i32* %43
    br %44

44:
    %47 = load i32, i32* %43
    %48 = icmp lt i32 %47,i32 32
    br i32 %48 %45 %46

45:
    %49 = load i32, i32* %43
    %50 = getelementptr [32 x i32], [32 x i32]* %4, i32 %49
    %51 = load i32, i32* %50
    store i32 %51, i32* %37
    %52 = load i32, i32* %37
    call void @putint(i32 %52)
    %53 = load i32, i32* %43
    %54 = load i32, i32* %43
    %55 = add i32 %54,1
    store i32 %55, i32* %43
    br %44

46:
    %56 = load [32 x i32], [32 x i32]* %4
    %57 = call i32 @bubblesort([32 x i32] %56)
    store i32 %57, i32* %37
    store i32 0, i32* %43
    br %58

58:
    %61 = load i32, i32* %43
    %62 = icmp lt i32 %61,i32 32
    br i32 %62 %59 %60

59:
    %63 = load i32, i32* %43
    %64 = getelementptr [32 x i32], [32 x i32]* %4, i32 %63
    %65 = load i32, i32* %64
    store i32 %65, i32* %37
    %66 = load i32, i32* %37
    call void @putint(i32 %66)
    %67 = load i32, i32* %43
    %68 = load i32, i32* %43
    %69 = add i32 %68,1
    store i32 %69, i32* %43
    br %58

60:
    %70 = load [32 x i32], [32 x i32]* %4
    %71 = call i32 @getMid([32 x i32] %70)
    store i32 %71, i32* %37
    %72 = load i32, i32* %37
    call void @putint(i32 %72)
    %73 = load [32 x i32], [32 x i32]* %4
    %74 = call i32 @getMost([32 x i32] %73)
    store i32 %74, i32* %37
    %75 = load i32, i32* %37
    call void @putint(i32 %75)
    %76 = load [32 x i32], [32 x i32]* %3
    %77 = load [32 x i32], [32 x i32]* %4
    %78 = call i32 @arrCopy([32 x i32] %76,[32 x i32] %77)
    store i32 %78, i32* %37
    %79 = load [32 x i32], [32 x i32]* %4
    %80 = call i32 @bubblesort([32 x i32] %79)
    store i32 %80, i32* %37
    store i32 0, i32* %43
    br %81

81:
    %84 = load i32, i32* %43
    %85 = icmp lt i32 %84,i32 32
    br i32 %85 %82 %83

82:
    %86 = load i32, i32* %43
    %87 = getelementptr [32 x i32], [32 x i32]* %4, i32 %86
    %88 = load i32, i32* %87
    store i32 %88, i32* %37
    %89 = load i32, i32* %37
    call void @putint(i32 %89)
    %90 = load i32, i32* %43
    %91 = load i32, i32* %43
    %92 = add i32 %91,1
    store i32 %92, i32* %43
    br %81

83:
    %93 = load [32 x i32], [32 x i32]* %3
    %94 = load [32 x i32], [32 x i32]* %4
    %95 = call i32 @arrCopy([32 x i32] %93,[32 x i32] %94)
    store i32 %95, i32* %37
    %96 = load [32 x i32], [32 x i32]* %4
    %97 = call i32 @insertsort([32 x i32] %96)
    store i32 %97, i32* %37
    store i32 0, i32* %43
    br %98

98:
    %101 = load i32, i32* %43
    %102 = icmp lt i32 %101,i32 32
    br i32 %102 %99 %100

99:
    %103 = load i32, i32* %43
    %104 = getelementptr [32 x i32], [32 x i32]* %4, i32 %103
    %105 = load i32, i32* %104
    store i32 %105, i32* %37
    %106 = load i32, i32* %37
    call void @putint(i32 %106)
    %107 = load i32, i32* %43
    %108 = load i32, i32* %43
    %109 = add i32 %108,1
    store i32 %109, i32* %43
    br %98

100:
    %110 = load [32 x i32], [32 x i32]* %3
    %111 = load [32 x i32], [32 x i32]* %4
    %112 = call i32 @arrCopy([32 x i32] %110,[32 x i32] %111)
    store i32 %112, i32* %37
    store i32 0, i32* %43
    store i32 31, i32* %37
    %113 = load [32 x i32], [32 x i32]* %4
    %114 = load i32, i32* %43
    %115 = load i32, i32* %37
    %116 = call i32 @QuickSort([32 x i32] %113,i32 %114,i32 %115)
    store i32 %116, i32* %37
    br %117

117:
    %120 = load i32, i32* %43
    %121 = icmp lt i32 %120,i32 32
    br i32 %121 %118 %119

118:
    %122 = load i32, i32* %43
    %123 = getelementptr [32 x i32], [32 x i32]* %4, i32 %122
    %124 = load i32, i32* %123
    store i32 %124, i32* %37
    %125 = load i32, i32* %37
    call void @putint(i32 %125)
    %126 = load i32, i32* %43
    %127 = load i32, i32* %43
    %128 = add i32 %127,1
    store i32 %128, i32* %43
    br %117

119:
    %129 = load [32 x i32], [32 x i32]* %3
    %130 = load [32 x i32], [32 x i32]* %4
    %131 = call i32 @arrCopy([32 x i32] %129,[32 x i32] %130)
    store i32 %131, i32* %37
    %132 = load [32 x i32], [32 x i32]* %4
    %133 = call i32 @calSum([32 x i32] %132,i32 4)
    store i32 %133, i32* %37
    store i32 0, i32* %43
    br %134

134:
    %137 = load i32, i32* %43
    %138 = icmp lt i32 %137,i32 32
    br i32 %138 %135 %136

135:
    %139 = load i32, i32* %43
    %140 = getelementptr [32 x i32], [32 x i32]* %4, i32 %139
    %141 = load i32, i32* %140
    store i32 %141, i32* %37
    %142 = load i32, i32* %37
    call void @putint(i32 %142)
    %143 = load i32, i32* %43
    %144 = load i32, i32* %43
    %145 = add i32 %144,1
    store i32 %145, i32* %43
    br %134

136:
    %146 = load [32 x i32], [32 x i32]* %3
    %147 = load [32 x i32], [32 x i32]* %4
    %148 = call i32 @arrCopy([32 x i32] %146,[32 x i32] %147)
    store i32 %148, i32* %37
    %149 = load [32 x i32], [32 x i32]* %4
    %150 = call i32 @avgPooling([32 x i32] %149,i32 3)
    store i32 %150, i32* %37
    store i32 0, i32* %43
    br %151

151:
    %154 = load i32, i32* %43
    %155 = icmp lt i32 %154,i32 32
    br i32 %155 %152 %153

152:
    %156 = load i32, i32* %43
    %157 = getelementptr [32 x i32], [32 x i32]* %4, i32 %156
    %158 = load i32, i32* %157
    store i32 %158, i32* %37
    %159 = load i32, i32* %37
    call void @putint(i32 %159)
    %160 = load i32, i32* %43
    %161 = load i32, i32* %43
    %162 = add i32 %161,1
    store i32 %162, i32* %43
    br %151

153:
    ret i32 0
}

