declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @func(i32 %0, [59 x i32]* %1, i32 %2, i32* %3, i32 %4, i32 %5, i32* %6, i32 %7, i32 %8){
9:
    %10 = alloca i32
    store i32 %0, i32* %10
    %11 = alloca [59 x i32]*
    store [59 x i32]* %1, [59 x i32]** %11
    %12 = alloca i32
    store i32 %2, i32* %12
    %13 = alloca i32*
    store i32* %3, i32** %13
    %14 = alloca i32
    store i32 %4, i32* %14
    %15 = alloca i32
    store i32 %5, i32* %15
    %16 = alloca i32*
    store i32* %6, i32** %16
    %17 = alloca i32
    store i32 %7, i32* %17
    %18 = alloca i32
    store i32 %8, i32* %18
    %19 = alloca i32
    store i32 0, i32* %19
    br %20

20:
    %23 = load i32, i32* %19
    %24 = icmp lt i32 %23,i32 10
    br i32 %24 %21 %22

21:
    %25 = load i32, i32* %10
    %26 = load i32, i32* %19
    %27 = getelementptr [59 x i32]*, [59 x i32]** %11, i32 %25, i32 %26
    %28 = load [59 x i32]*, [59 x i32]** %27
    call void @putint([59 x i32]* %28)
    %29 = load i32, i32* %19
    %30 = load i32, i32* %19
    %31 = add i32 %30,1
    store i32 %31, i32* %19
    br %20

22:
    call void @putch(i32 10)
    %32 = load i32, i32* %12
    %33 = getelementptr i32*, i32** %13, i32 %32
    %34 = load i32*, i32** %33
    call void @putint(i32* %34)
    call void @putch(i32 10)
    br %35

35:
    %38 = load i32, i32* %18
    %39 = icmp lt i32 %38,i32 10
    br i32 %39 %36 %37

36:
    %40 = load i32, i32* %18
    %41 = getelementptr i32*, i32** %16, i32 %40
    %42 = load i32, i32* %17
    %43 = mul i32 %42,128875
    %44 = srem i32 %43,3724
    store i32 %44, i32** %41
    %45 = load i32, i32* %18
    %46 = load i32, i32* %18
    %47 = add i32 %46,1
    store i32 %47, i32* %18
    %48 = load i32, i32* %17
    %49 = load i32, i32* %17
    %50 = add i32 %49,7
    store i32 %50, i32* %17
    br %35

37:
    %51 = load i32, i32* %14
    %52 = load i32, i32* %15
    %53 = load i32, i32* %14
    %54 = load i32, i32* %15
    %55 = add i32 %53,%54
    ret i32 %55
}
define dso_local i32 @main(i32 %0, [59 x i32]* %1, i32 %2, i32* %3, i32 %4, i32 %5, i32* %6, i32 %7, i32 %8){
9:
    %10 = alloca [61 x [67 x i32]]
    %11 = alloca [53 x [59 x i32]]
    %12 = getelementptr [61 x [67 x i32]], [61 x [67 x i32]]* %10, i32 17, i32 1
    store i32 6, i32* %12
    %13 = getelementptr [61 x [67 x i32]], [61 x [67 x i32]]* %10, i32 17, i32 3
    store i32 7, i32* %13
    %14 = getelementptr [61 x [67 x i32]], [61 x [67 x i32]]* %10, i32 17, i32 4
    store i32 4, i32* %14
    %15 = getelementptr [61 x [67 x i32]], [61 x [67 x i32]]* %10, i32 17, i32 7
    store i32 9, i32* %15
    %16 = getelementptr [61 x [67 x i32]], [61 x [67 x i32]]* %10, i32 17, i32 11
    store i32 11, i32* %16
    %17 = getelementptr [53 x [59 x i32]], [53 x [59 x i32]]* %11, i32 6, i32 1
    store i32 1, i32* %17
    %18 = getelementptr [53 x [59 x i32]], [53 x [59 x i32]]* %11, i32 6, i32 2
    store i32 2, i32* %18
    %19 = getelementptr [53 x [59 x i32]], [53 x [59 x i32]]* %11, i32 6, i32 3
    store i32 3, i32* %19
    %20 = getelementptr [53 x [59 x i32]], [53 x [59 x i32]]* %11, i32 6, i32 9
    store i32 9, i32* %20
    %21 = alloca i32
    %22 = getelementptr [61 x [67 x i32]], [61 x [67 x i32]]* %10, i32 17, i32 1
    %23 = load i32, i32* %22
    %24 = load [53 x [59 x i32]], [53 x [59 x i32]]* %11
    %25 = getelementptr [61 x [67 x i32]], [61 x [67 x i32]]* %10, i32 17, i32 3
    %26 = load i32, i32* %25
    %27 = getelementptr [61 x [67 x i32]], [61 x [67 x i32]]* %10, i32 17
    %28 = load [67 x i32], [67 x i32]* %27
    %29 = getelementptr [53 x [59 x i32]], [53 x [59 x i32]]* %11, i32 6, i32 3
    %30 = load i32, i32* %29
    %31 = getelementptr [53 x [59 x i32]], [53 x [59 x i32]]* %11, i32 6, i32 0
    %32 = load i32, i32* %31
    %33 = getelementptr [53 x [59 x i32]], [53 x [59 x i32]]* %11, i32 6
    %34 = load [59 x i32], [59 x i32]* %33
    %35 = getelementptr [53 x [59 x i32]], [53 x [59 x i32]]* %11, i32 34, i32 4
    %36 = load i32, i32* %35
    %37 = getelementptr [53 x [59 x i32]], [53 x [59 x i32]]* %11, i32 51, i32 18
    %38 = load i32, i32* %37
    %39 = call i32 @func(i32 %23,[53 x [59 x i32]] %24,i32 %26,[67 x i32] %28,i32 %30,i32 %32,[59 x i32] %34,i32 %36,i32 %38)
    %40 = mul i32 %39,3
    store i32 %40, i32* %21
    br %41

41:
    %44 = load i32, i32* %21
    %45 = icmp ge i32 %44,i32 0
    br i32 %45 %42 %43

42:
    %46 = load i32, i32* %21
    %47 = getelementptr [53 x [59 x i32]], [53 x [59 x i32]]* %11, i32 6, i32 %46
    %48 = load i32, i32* %47
    call void @putint(i32 %48)
    call void @putch(i32 32)
    %49 = load i32, i32* %21
    %50 = load i32, i32* %21
    %51 = sub i32 %50,1
    store i32 %51, i32* %21
    br %41

43:
    call void @putch(i32 10)
    ret i32 0
}

