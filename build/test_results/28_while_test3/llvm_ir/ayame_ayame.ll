@g = dso_local global i32 0

@h = dso_local global i32 0

@f = dso_local global i32 0

@e = dso_local global i32 0

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @EightWhile(){
0:
    %1 = alloca i32
    store i32 5, i32* %1
    %2 = alloca i32
    %3 = alloca i32
    store i32 6, i32* %2
    store i32 7, i32* %3
    %4 = alloca i32
    store i32 10, i32* %4
    br %5

5:
    %8 = load i32, i32* %1
    %9 = icmp lt i32 %8,i32 20
    br i32 %9 %6 %7

6:
    %10 = load i32, i32* %1
    %11 = load i32, i32* %1
    %12 = add i32 %11,3
    store i32 %12, i32* %1
    br %13

7:
    %90 = load i32, i32* %1
    %91 = load i32, i32* %2
    %92 = load i32, i32* %4
    %93 = load i32, i32* %2
    %94 = load i32, i32* %4
    %95 = add i32 %93,%94
    %96 = load i32, i32* %1
    %97 = load i32, i32* %2
    %98 = load i32, i32* %4
    %99 = load i32, i32* %2
    %100 = load i32, i32* %4
    %101 = add i32 %99,%100
    %102 = add i32 %96,%101
    %103 = load i32, i32* %3
    %104 = add i32 %102,%103
    %105 = load i32, i32* @e
    %106 = load i32, i32* %4
    %107 = load i32, i32* @e
    %108 = load i32, i32* %4
    %109 = add i32 %107,%108
    %110 = load i32, i32* @g
    %111 = sub i32 %109,%110
    %112 = load i32, i32* @h
    %113 = add i32 %111,%112
    %114 = load i32, i32* %1
    %115 = load i32, i32* %2
    %116 = load i32, i32* %4
    %117 = load i32, i32* %2
    %118 = load i32, i32* %4
    %119 = add i32 %117,%118
    %120 = load i32, i32* %1
    %121 = load i32, i32* %2
    %122 = load i32, i32* %4
    %123 = load i32, i32* %2
    %124 = load i32, i32* %4
    %125 = add i32 %123,%124
    %126 = add i32 %120,%125
    %127 = load i32, i32* %3
    %128 = add i32 %126,%127
    %129 = load i32, i32* @e
    %130 = load i32, i32* %4
    %131 = load i32, i32* @e
    %132 = load i32, i32* %4
    %133 = add i32 %131,%132
    %134 = load i32, i32* @g
    %135 = sub i32 %133,%134
    %136 = load i32, i32* @h
    %137 = add i32 %135,%136
    %138 = sub i32 %128,%137
    ret i32 %138

13:
    %16 = load i32, i32* %2
    %17 = icmp lt i32 %16,i32 10
    br i32 %17 %14 %15

14:
    %18 = load i32, i32* %2
    %19 = load i32, i32* %2
    %20 = add i32 %19,1
    store i32 %20, i32* %2
    br %21

15:
    %87 = load i32, i32* %2
    %88 = load i32, i32* %2
    %89 = sub i32 %88,2
    store i32 %89, i32* %2
    br %5

21:
    %24 = load i32, i32* %3
    %25 = icmp eq i32 %24,i32 7
    br i32 %25 %22 %23

22:
    %26 = load i32, i32* %3
    %27 = load i32, i32* %3
    %28 = sub i32 %27,1
    store i32 %28, i32* %3
    br %29

23:
    %84 = load i32, i32* %3
    %85 = load i32, i32* %3
    %86 = add i32 %85,1
    store i32 %86, i32* %3
    br %13

29:
    %32 = load i32, i32* %4
    %33 = icmp lt i32 %32,i32 20
    br i32 %33 %30 %31

30:
    %34 = load i32, i32* %4
    %35 = load i32, i32* %4
    %36 = add i32 %35,3
    store i32 %36, i32* %4
    br %37

31:
    %81 = load i32, i32* %4
    %82 = load i32, i32* %4
    %83 = sub i32 %82,1
    store i32 %83, i32* %4
    br %21

37:
    %40 = load i32, i32* @e
    %41 = icmp gt i32 %40,i32 1
    br i32 %41 %38 %39

38:
    %42 = load i32, i32* @e
    %43 = load i32, i32* @e
    %44 = sub i32 %43,1
    store i32 %44, i32* @e
    br %45

39:
    %78 = load i32, i32* @e
    %79 = load i32, i32* @e
    %80 = add i32 %79,1
    store i32 %80, i32* @e
    br %29

45:
    %48 = load i32, i32* @f
    %49 = icmp gt i32 %48,i32 2
    br i32 %49 %46 %47

46:
    %50 = load i32, i32* @f
    %51 = load i32, i32* @f
    %52 = sub i32 %51,2
    store i32 %52, i32* @f
    br %53

47:
    %75 = load i32, i32* @f
    %76 = load i32, i32* @f
    %77 = add i32 %76,1
    store i32 %77, i32* @f
    br %37

53:
    %56 = load i32, i32* @g
    %57 = icmp lt i32 %56,i32 3
    br i32 %57 %54 %55

54:
    %58 = load i32, i32* @g
    %59 = load i32, i32* @g
    %60 = add i32 %59,10
    store i32 %60, i32* @g
    br %61

55:
    %72 = load i32, i32* @g
    %73 = load i32, i32* @g
    %74 = sub i32 %73,8
    store i32 %74, i32* @g
    br %45

61:
    %64 = load i32, i32* @h
    %65 = icmp lt i32 %64,i32 10
    br i32 %65 %62 %63

62:
    %66 = load i32, i32* @h
    %67 = load i32, i32* @h
    %68 = add i32 %67,8
    store i32 %68, i32* @h
    br %61

63:
    %69 = load i32, i32* @h
    %70 = load i32, i32* @h
    %71 = sub i32 %70,1
    store i32 %71, i32* @h
    br %53
}
define dso_local i32 @main(){
0:
    store i32 1, i32* @g
    store i32 2, i32* @h
    store i32 4, i32* @e
    store i32 6, i32* @f
    %1 = call i32 @EightWhile()
    ret i32 %1
}

