declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @main(){
0:
    %1 = alloca [4 x [2 x i32]]
    %2 = getelementptr [4 x [2 x i32]], [4 x [2 x i32]]* %1, i32 0, i32 0
    store i32 1, i32* %2
    %3 = getelementptr [4 x [2 x i32]], [4 x [2 x i32]]* %1, i32 0, i32 1
    store i32 2, i32* %3
    %4 = getelementptr [4 x [2 x i32]], [4 x [2 x i32]]* %1, i32 1, i32 0
    store i32 3, i32* %4
    %5 = getelementptr [4 x [2 x i32]], [4 x [2 x i32]]* %1, i32 1, i32 1
    store i32 4, i32* %5
    %6 = getelementptr [4 x [2 x i32]], [4 x [2 x i32]]* %1, i32 3
    store i32 7, [2 x i32]* %6
    %7 = alloca i32
    store i32 3, i32* %7
    %8 = alloca [4 x [2 x i32]]
    %9 = alloca [4 x [2 x i32]]
    %10 = getelementptr [4 x [2 x i32]], [4 x [2 x i32]]* %9, i32 0
    store i32 1, [2 x i32]* %10
    %11 = getelementptr [4 x [2 x i32]], [4 x [2 x i32]]* %9, i32 1
    store i32 2, [2 x i32]* %11
    %12 = getelementptr [4 x [2 x i32]], [4 x [2 x i32]]* %9, i32 2
    store i32 3, [2 x i32]* %12
    %13 = getelementptr [4 x [2 x i32]], [4 x [2 x i32]]* %9, i32 3
    store i32 4, [2 x i32]* %13
    %14 = getelementptr [4 x [2 x i32]], [4 x [2 x i32]]* %9, i32 4
    store i32 5, [2 x i32]* %14
    %15 = getelementptr [4 x [2 x i32]], [4 x [2 x i32]]* %9, i32 5
    store i32 6, [2 x i32]* %15
    %16 = getelementptr [4 x [2 x i32]], [4 x [2 x i32]]* %9, i32 6
    store i32 7, [2 x i32]* %16
    %17 = getelementptr [4 x [2 x i32]], [4 x [2 x i32]]* %9, i32 7
    store i32 8, [2 x i32]* %17
    %18 = alloca [4 x [2 x i32]]
    %19 = getelementptr [4 x [2 x i32]], [4 x [2 x i32]]* %18, i32 0
    store i32 1, [2 x i32]* %19
    %20 = getelementptr [4 x [2 x i32]], [4 x [2 x i32]]* %18, i32 1
    store i32 2, [2 x i32]* %20
    %21 = getelementptr [4 x [2 x i32]], [4 x [2 x i32]]* %18, i32 2, i32 0
    store i32 3, i32* %21
    %22 = getelementptr [4 x [2 x i32]], [4 x [2 x i32]]* %18, i32 3, i32 0
    store i32 5, i32* %22
    %23 = getelementptr [4 x [2 x i32]], [4 x [2 x i32]]* %1, i32 3, i32 0
    %24 = load i32, i32* %23
    %25 = getelementptr [4 x [2 x i32]], [4 x [2 x i32]]* %18, i32 4
    store i32 %24, [2 x i32]* %25
    %26 = getelementptr [4 x [2 x i32]], [4 x [2 x i32]]* %18, i32 5
    store i32 8, [2 x i32]* %26
    %27 = alloca [4 x [2 x [1 x i32]]]
    %28 = getelementptr [4 x [2 x i32]], [4 x [2 x i32]]* %18, i32 2, i32 1
    %29 = load i32, i32* %28
    %30 = getelementptr [4 x [2 x [1 x i32]]], [4 x [2 x [1 x i32]]]* %27, i32 0, i32 0
    store i32 %29, [1 x i32]* %30
    %31 = getelementptr [4 x [2 x i32]], [4 x [2 x i32]]* %9, i32 2, i32 1
    %32 = load i32, i32* %31
    %33 = getelementptr [4 x [2 x [1 x i32]]], [4 x [2 x [1 x i32]]]* %27, i32 0, i32 1, i32 0
    store i32 %32, i32* %33
    %34 = getelementptr [4 x [2 x [1 x i32]]], [4 x [2 x [1 x i32]]]* %27, i32 1, i32 0
    store i32 3, [1 x i32]* %34
    %35 = getelementptr [4 x [2 x [1 x i32]]], [4 x [2 x [1 x i32]]]* %27, i32 1, i32 1
    store i32 4, [1 x i32]* %35
    %36 = getelementptr [4 x [2 x [1 x i32]]], [4 x [2 x [1 x i32]]]* %27, i32 2, i32 0
    store i32 5, [1 x i32]* %36
    %37 = getelementptr [4 x [2 x [1 x i32]]], [4 x [2 x [1 x i32]]]* %27, i32 2, i32 1
    store i32 6, [1 x i32]* %37
    %38 = getelementptr [4 x [2 x [1 x i32]]], [4 x [2 x [1 x i32]]]* %27, i32 3, i32 0
    store i32 7, [1 x i32]* %38
    %39 = getelementptr [4 x [2 x [1 x i32]]], [4 x [2 x [1 x i32]]]* %27, i32 3, i32 1
    store i32 8, [1 x i32]* %39
    %40 = getelementptr [4 x [2 x [1 x i32]]], [4 x [2 x [1 x i32]]]* %27, i32 3, i32 1, i32 0
    %41 = load i32, i32* %40
    %42 = getelementptr [4 x [2 x [1 x i32]]], [4 x [2 x [1 x i32]]]* %27, i32 0, i32 0, i32 0
    %43 = load i32, i32* %42
    %44 = getelementptr [4 x [2 x [1 x i32]]], [4 x [2 x [1 x i32]]]* %27, i32 3, i32 1, i32 0
    %45 = load i32, i32* %44
    %46 = getelementptr [4 x [2 x [1 x i32]]], [4 x [2 x [1 x i32]]]* %27, i32 0, i32 0, i32 0
    %47 = load i32, i32* %46
    %48 = add i32 %45,%47
    %49 = getelementptr [4 x [2 x [1 x i32]]], [4 x [2 x [1 x i32]]]* %27, i32 0, i32 1, i32 0
    %50 = load i32, i32* %49
    %51 = add i32 %48,%50
    %52 = getelementptr [4 x [2 x i32]], [4 x [2 x i32]]* %18, i32 3, i32 0
    %53 = load i32, i32* %52
    %54 = add i32 %51,%53
    ret i32 %54
}

