@a = dso_local global i32 0

@b = dso_local global i32 0

@d = dso_local global i32 0

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @set_a(i32 %0){
1:
    %2 = alloca i32
    store i32 %0, i32* %2
    %3 = load i32, i32* %2
    store i32 %3, i32* @a
    %4 = load i32, i32* @a
    ret i32 %4
}
define dso_local i32 @set_b(i32 %0){
1:
    %2 = alloca i32
    store i32 %0, i32* %2
    %3 = load i32, i32* %2
    store i32 %3, i32* @b
    %4 = load i32, i32* @b
    ret i32 %4
}
define dso_local i32 @set_d(i32 %0){
1:
    %2 = alloca i32
    store i32 %0, i32* %2
    %3 = load i32, i32* %2
    store i32 %3, i32* @d
    %4 = load i32, i32* @d
    ret i32 %4
}
define dso_local i32 @main(i32 %0){
1:
    store i32 2, i32* @a
    store i32 3, i32* @b
    %4 = call i32 @set_a(i32 0)
    %5 = call i32 @set_b(i32 1)
    %6 = and i32 %4,%5
    br i32 %6 %3 %2

2:
    %7 = load i32, i32* @a
    call void @putint(i32 %7)
    call void @putch(i32 32)
    %8 = load i32, i32* @b
    call void @putint(i32 %8)
    call void @putch(i32 32)
    store i32 2, i32* @a
    store i32 3, i32* @b
    %11 = call i32 @set_a(i32 0)
    %12 = call i32 @set_b(i32 1)
    %13 = and i32 %11,%12
    br i32 %13 %10 %9

3:
    br %2

9:
    %14 = load i32, i32* @a
    call void @putint(i32 %14)
    call void @putch(i32 32)
    %15 = load i32, i32* @b
    call void @putint(i32 %15)
    call void @putch(i32 10)
    %16 = alloca i32
    store i32 1, i32* %16
    store i32 2, i32* @d
    %19 = load i32, i32* %16
    %20 = icmp ge i32 %19,i32 1
    %21 = call i32 @set_d(i32 3)
    %22 = and i32 %20,%21
    br i32 %22 %18 %17

10:
    br %9

17:
    %23 = load i32, i32* @d
    call void @putint(i32 %23)
    call void @putch(i32 32)
    %26 = load i32, i32* %16
    %27 = icmp le i32 %26,i32 1
    %28 = call i32 @set_d(i32 4)
    %29 = or i32 %27,%28
    br i32 %29 %25 %24

18:
    br %17

24:
    %30 = load i32, i32* @d
    call void @putint(i32 %30)
    call void @putch(i32 10)
    %33 = add i32 2,1
    %34 = add i32 2,1
    %35 = sub i32 3,%34
    %36 = icmp ge i32 16,i32 %35
    br i32 %36 %32 %31

25:
    br %24

31:
    %39 = sub i32 25,7
    %40 = mul i32 6,3
    %41 = mul i32 6,3
    %42 = sub i32 36,%41
    %43 = icmp ne i32 %39,i32 %42
    br i32 %43 %38 %37

32:
    call void @putch(i32 65)
    br %31

37:
    %46 = icmp lt i32 1,i32 8
    %47 = srem i32 7,2
    %48 = icmp ne i32 %46,i32 %47
    br i32 %48 %45 %44

38:
    call void @putch(i32 66)
    br %37

44:
    %51 = icmp gt i32 3,i32 4
    %52 = icmp eq i32 %51,i32 0
    br i32 %52 %50 %49

45:
    call void @putch(i32 67)
    br %44

49:
    %55 = icmp le i32 102,i32 77
    %56 = icmp eq i32 1,i32 %55
    br i32 %56 %54 %53

50:
    call void @putch(i32 68)
    br %49

53:
    %59 = sub i32 5,6
    
    %61 = sub i32 0,%60
    %62 = icmp eq i32 %59,i32 %61
    br i32 %62 %58 %57

54:
    call void @putch(i32 69)
    br %53

57:
    call void @putch(i32 10)
    %63 = alloca i32
    store i32 0, i32* %63
    %64 = alloca i32
    store i32 1, i32* %64
    %65 = alloca i32
    store i32 2, i32* %65
    %66 = alloca i32
    store i32 3, i32* %66
    %67 = alloca i32
    store i32 4, i32* %67
    br %68

58:
    call void @putch(i32 70)
    br %57

68:
    %71 = load i32, i32* %63
    %72 = load i32, i32* %64
    %73 = and i32 %71,%72
    br i32 %73 %69 %70

69:
    call void @putch(i32 32)
    br %68

70:
    %76 = load i32, i32* %63
    %77 = load i32, i32* %64
    %78 = or i32 %76,%77
    br i32 %78 %75 %74

74:
    %81 = load i32, i32* %63
    %82 = load i32, i32* %64
    %83 = icmp ge i32 %81,i32 %82
    %84 = load i32, i32* %64
    %85 = load i32, i32* %63
    %86 = icmp le i32 %84,i32 %85
    %87 = or i32 %83,%86
    br i32 %87 %80 %79

75:
    call void @putch(i32 67)
    br %74

79:
    %90 = load i32, i32* %65
    %91 = load i32, i32* %64
    %92 = icmp ge i32 %90,i32 %91
    %93 = load i32, i32* %67
    %94 = load i32, i32* %66
    %95 = icmp ne i32 %93,i32 %94
    %96 = and i32 %92,%95
    br i32 %96 %89 %88

80:
    call void @putch(i32 72)
    br %79

88:
    %99 = load i32, i32* %63
    %100 = load i32, i32* %64
    
    %102 = icmp eq i32 %99,void %101
    %103 = load i32, i32* %66
    %104 = load i32, i32* %66
    %105 = icmp lt i32 %103,i32 %104
    %106 = and i32 %102,%105
    %107 = load i32, i32* %67
    %108 = load i32, i32* %67
    %109 = icmp ge i32 %107,i32 %108
    %110 = or i32 %106,%109
    br i32 %110 %98 %97

89:
    call void @putch(i32 73)
    br %88

97:
    %113 = load i32, i32* %63
    %114 = load i32, i32* %64
    
    %116 = icmp eq i32 %113,void %115
    %117 = load i32, i32* %66
    %118 = load i32, i32* %66
    %119 = icmp lt i32 %117,i32 %118
    %120 = load i32, i32* %67
    %121 = load i32, i32* %67
    %122 = icmp ge i32 %120,i32 %121
    %123 = and i32 %119,%122
    %124 = or i32 %116,%123
    br i32 %124 %112 %111

98:
    call void @putch(i32 74)
    br %97

111:
    call void @putch(i32 10)
    ret i32 0

112:
    call void @putch(i32 75)
    br %111
}

