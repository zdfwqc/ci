declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @main(){
0:
    %1 = alloca i32
    %2 = alloca i32
    %3 = alloca i32
    %4 = alloca i32
    %5 = alloca i32
    store i32 5, i32* %1
    store i32 5, i32* %2
    store i32 1, i32* %3
    %6 = sub i32 0,2
    store i32 %6, i32* %4
    store i32 2, i32* %5
    %9 = load i32, i32* %4
    %10 = mul i32 %9,1
    %11 = sdiv i32 %10,2
    %12 = icmp lt i32 %11,i32 0
    %13 = load i32, i32* %1
    %14 = load i32, i32* %2
    %15 = load i32, i32* %1
    %16 = load i32, i32* %2
    %17 = sub i32 %15,%16
    %18 = icmp ne i32 %17,i32 0
    %19 = load i32, i32* %3
    %20 = load i32, i32* %3
    %21 = add i32 %20,3
    %22 = srem i32 %21,2
    %23 = icmp ne i32 %22,i32 0
    %24 = and i32 %18,%23
    %25 = or i32 %12,%24
    br i32 %25 %8 %7

7:
    %29 = load i32, i32* %4
    %30 = srem i32 %29,2
    %31 = load i32, i32* %4
    %32 = srem i32 %31,2
    %33 = add i32 %32,67
    %34 = icmp lt i32 %33,i32 0
    %35 = load i32, i32* %1
    %36 = load i32, i32* %2
    %37 = load i32, i32* %1
    %38 = load i32, i32* %2
    %39 = sub i32 %37,%38
    %40 = icmp ne i32 %39,i32 0
    %41 = load i32, i32* %3
    %42 = load i32, i32* %3
    %43 = add i32 %42,2
    %44 = srem i32 %43,2
    %45 = icmp ne i32 %44,i32 0
    %46 = and i32 %40,%45
    %47 = or i32 %34,%46
    br i32 %47 %28 %27

8:
    %26 = load i32, i32* %5
    call void @putint(i32 %26)
    br %7

27:
    ret i32 0

28:
    store i32 4, i32* %5
    %48 = load i32, i32* %5
    call void @putint(i32 %48)
    br %27
}

