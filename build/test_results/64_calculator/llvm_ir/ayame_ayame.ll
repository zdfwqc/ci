@ints = dso_local global [10000 x i32] zeroinitializer

@intt = dso_local global i32 10000

@chas = dso_local global [10000 x i32] zeroinitializer

@chat = dso_local global i32 10000

@i = dso_local global i32 0

@ii = dso_local global i32 1

@c = dso_local global i32 1

@get = dso_local global [10000 x i32] zeroinitializer

@get2 = dso_local global [10000 x i32] zeroinitializer

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @isdigit(i32 %0){
1:
    %2 = alloca i32
    store i32 %0, i32* %2
    %5 = load i32, i32* %2
    %6 = icmp ge i32 %5,i32 48
    %7 = load i32, i32* %2
    %8 = icmp le i32 %7,i32 57
    %9 = and i32 %6,%8
    br i32 %9 %4 %3

3:
    ret i32 0

4:
    ret i32 1
}
define dso_local i32 @power(i32 %0, i32 %1){
2:
    %3 = alloca i32
    store i32 %0, i32* %3
    %4 = alloca i32
    store i32 %1, i32* %4
    %5 = alloca i32
    store i32 1, i32* %5
    br %6

6:
    %9 = load i32, i32* %4
    %10 = icmp ne i32 %9,i32 0
    br i32 %10 %7 %8

7:
    %11 = load i32, i32* %5
    %12 = load i32, i32* %3
    %13 = mul i32 %11,%12
    store i32 %13, i32* %5
    %14 = load i32, i32* %4
    %15 = load i32, i32* %4
    %16 = sub i32 %15,1
    store i32 %16, i32* %4
    br %6

8:
    %17 = load i32, i32* %5
    ret i32 %17
}
define dso_local i32 @getstr(i32* %0){
1:
    %2 = alloca i32*
    store i32* %0, i32** %2
    %3 = call i32 @getch()
    %4 = alloca i32
    store i32 %3, i32* %4
    %5 = alloca i32
    store i32 0, i32* %5
    br %6

6:
    %9 = load i32, i32* %4
    %10 = icmp ne i32 %9,i32 13
    %11 = load i32, i32* %4
    %12 = icmp ne i32 %11,i32 10
    %13 = and i32 %10,%12
    br i32 %13 %7 %8

7:
    %14 = load i32, i32* %5
    %15 = getelementptr i32*, i32** %2, i32 %14
    %16 = load i32, i32* %4
    store i32 %16, i32** %15
    %17 = load i32, i32* %5
    %18 = load i32, i32* %5
    %19 = add i32 %18,1
    store i32 %19, i32* %5
    %20 = call i32 @getch()
    store i32 %20, i32* %4
    br %6

8:
    %21 = load i32, i32* %5
    ret i32 %21
}
define dso_local void @intpush(i32 %0){
1:
    %2 = alloca i32
    store i32 %0, i32* %2
    %3 = load i32, i32* @intt
    %4 = load i32, i32* @intt
    %5 = add i32 %4,1
    store i32 %5, i32* @intt
    %6 = load i32, i32* @intt
    %7 = getelementptr [10000 x i32], [10000 x i32]* @ints, i32 %6
    %8 = load i32, i32* %2
    store i32 %8, i32* %7
}
define dso_local void @chapush(i32 %0){
1:
    %2 = alloca i32
    store i32 %0, i32* %2
    %3 = load i32, i32* @chat
    %4 = load i32, i32* @chat
    %5 = add i32 %4,1
    store i32 %5, i32* @chat
    %6 = load i32, i32* @chat
    %7 = getelementptr [10000 x i32], [10000 x i32]* @chas, i32 %6
    %8 = load i32, i32* %2
    store i32 %8, i32* %7
}
define dso_local i32 @intpop(i32 %0){
1:
    %2 = load i32, i32* @intt
    %3 = load i32, i32* @intt
    %4 = sub i32 %3,1
    store i32 %4, i32* @intt
    %5 = load i32, i32* @intt
    %6 = load i32, i32* @intt
    %7 = add i32 %6,1
    %8 = getelementptr [10000 x i32], [10000 x i32]* @ints, i32 %7
    %9 = load i32, i32* %8
    ret i32 %9
}
define dso_local i32 @chapop(i32 %0){
1:
    %2 = load i32, i32* @chat
    %3 = load i32, i32* @chat
    %4 = sub i32 %3,1
    store i32 %4, i32* @chat
    %5 = load i32, i32* @chat
    %6 = load i32, i32* @chat
    %7 = add i32 %6,1
    %8 = getelementptr [10000 x i32], [10000 x i32]* @chas, i32 %7
    %9 = load i32, i32* %8
    ret i32 %9
}
define dso_local void @intadd(i32 %0){
1:
    %2 = alloca i32
    store i32 %0, i32* %2
    %3 = load i32, i32* @intt
    %4 = getelementptr [10000 x i32], [10000 x i32]* @ints, i32 %3
    %5 = load i32, i32* @intt
    %6 = getelementptr [10000 x i32], [10000 x i32]* @ints, i32 %5
    %7 = load i32, i32* %6
    %8 = mul i32 %7,10
    store i32 %8, i32* %4
    %9 = load i32, i32* @intt
    %10 = getelementptr [10000 x i32], [10000 x i32]* @ints, i32 %9
    %11 = load i32, i32* @intt
    %12 = getelementptr [10000 x i32], [10000 x i32]* @ints, i32 %11
    %13 = load i32, i32* %12
    %14 = load i32, i32* %2
    %15 = load i32, i32* @intt
    %16 = getelementptr [10000 x i32], [10000 x i32]* @ints, i32 %15
    %17 = load i32, i32* %16
    %18 = load i32, i32* %2
    %19 = add i32 %17,%18
    store i32 %19, i32* %10
}
define dso_local i32 @find(i32 %0){
1:
    %2 = call i32 @chapop()
    store i32 %2, i32* @c
    %3 = load i32, i32* @ii
    %4 = getelementptr [10000 x i32], [10000 x i32]* @get2, i32 %3
    store i32 32, i32* %4
    %5 = load i32, i32* @ii
    %6 = load i32, i32* @ii
    %7 = add i32 %6,1
    %8 = getelementptr [10000 x i32], [10000 x i32]* @get2, i32 %7
    %9 = load i32, i32* @c
    store i32 %9, i32* %8
    %10 = load i32, i32* @ii
    %11 = load i32, i32* @ii
    %12 = add i32 %11,2
    store i32 %12, i32* @ii
    %15 = load i32, i32* @chat
    %16 = icmp eq i32 %15,i32 0
    br i32 %16 %14 %13

13:
    ret i32 1

14:
    ret i32 0
}
define dso_local i32 @main(i32 %0){
1:
    store i32 0, i32* @intt
    store i32 0, i32* @chat
    %2 = load [10000 x i32], [10000 x i32]* @get
    %3 = call i32 @getstr([10000 x i32] %2)
    %4 = alloca i32
    store i32 %3, i32* %4
    br %5

5:
    %8 = load i32, i32* @i
    %9 = load i32, i32* %4
    %10 = icmp lt i32 %8,i32 %9
    br i32 %10 %6 %7

6:
    %14 = load i32, i32* @i
    %15 = getelementptr [10000 x i32], [10000 x i32]* @get, i32 %14
    %16 = load i32, i32* %15
    %17 = call i32 @isdigit(i32 %16)
    %18 = icmp eq i32 %17,i32 1
    br i32 %18 %12 %13

7:
    br %257

11:
    %254 = load i32, i32* @i
    %255 = load i32, i32* @i
    %256 = add i32 %255,1
    store i32 %256, i32* @i
    br %5

12:
    %19 = load i32, i32* @ii
    %20 = getelementptr [10000 x i32], [10000 x i32]* @get2, i32 %19
    %21 = load i32, i32* @i
    %22 = getelementptr [10000 x i32], [10000 x i32]* @get, i32 %21
    %23 = load i32, i32* %22
    store i32 %23, i32* %20
    %24 = load i32, i32* @ii
    %25 = load i32, i32* @ii
    %26 = add i32 %25,1
    store i32 %26, i32* @ii
    br %11

13:
    %29 = load i32, i32* @i
    %30 = getelementptr [10000 x i32], [10000 x i32]* @get, i32 %29
    %31 = load i32, i32* %30
    %32 = icmp eq i32 %31,i32 40
    br i32 %32 %28 %27

27:
    %36 = load i32, i32* @i
    %37 = getelementptr [10000 x i32], [10000 x i32]* @get, i32 %36
    %38 = load i32, i32* %37
    %39 = icmp eq i32 %38,i32 94
    br i32 %39 %35 %34

28:
    %33 = call void @chapush(i32 40)
    br %27

34:
    %43 = load i32, i32* @i
    %44 = getelementptr [10000 x i32], [10000 x i32]* @get, i32 %43
    %45 = load i32, i32* %44
    %46 = icmp eq i32 %45,i32 41
    br i32 %46 %42 %41

35:
    %40 = call void @chapush(i32 94)
    br %34

41:
    %66 = load i32, i32* @i
    %67 = getelementptr [10000 x i32], [10000 x i32]* @get, i32 %66
    %68 = load i32, i32* %67
    %69 = icmp eq i32 %68,i32 43
    br i32 %69 %65 %64

42:
    %47 = call i32 @chapop()
    store i32 %47, i32* @c
    br %48

48:
    %51 = load i32, i32* @c
    %52 = icmp ne i32 %51,i32 40
    br i32 %52 %49 %50

49:
    %53 = load i32, i32* @ii
    %54 = getelementptr [10000 x i32], [10000 x i32]* @get2, i32 %53
    store i32 32, i32* %54
    %55 = load i32, i32* @ii
    %56 = load i32, i32* @ii
    %57 = add i32 %56,1
    %58 = getelementptr [10000 x i32], [10000 x i32]* @get2, i32 %57
    %59 = load i32, i32* @c
    store i32 %59, i32* %58
    %60 = load i32, i32* @ii
    %61 = load i32, i32* @ii
    %62 = add i32 %61,2
    store i32 %62, i32* @ii
    %63 = call i32 @chapop()
    store i32 %63, i32* @c
    br %48

50:

64:
    %109 = load i32, i32* @i
    %110 = getelementptr [10000 x i32], [10000 x i32]* @get, i32 %109
    %111 = load i32, i32* %110
    %112 = icmp eq i32 %111,i32 45
    br i32 %112 %108 %107

65:
    br %70

70:
    %73 = load i32, i32* @chat
    %74 = getelementptr [10000 x i32], [10000 x i32]* @chas, i32 %73
    %75 = load i32, i32* %74
    %76 = icmp eq i32 %75,i32 43
    %77 = load i32, i32* @chat
    %78 = getelementptr [10000 x i32], [10000 x i32]* @chas, i32 %77
    %79 = load i32, i32* %78
    %80 = icmp eq i32 %79,i32 45
    %81 = or i32 %76,%80
    %82 = load i32, i32* @chat
    %83 = getelementptr [10000 x i32], [10000 x i32]* @chas, i32 %82
    %84 = load i32, i32* %83
    %85 = icmp eq i32 %84,i32 42
    %86 = or i32 %81,%85
    %87 = load i32, i32* @chat
    %88 = getelementptr [10000 x i32], [10000 x i32]* @chas, i32 %87
    %89 = load i32, i32* %88
    %90 = icmp eq i32 %89,i32 47
    %91 = or i32 %86,%90
    %92 = load i32, i32* @chat
    %93 = getelementptr [10000 x i32], [10000 x i32]* @chas, i32 %92
    %94 = load i32, i32* %93
    %95 = icmp eq i32 %94,i32 37
    %96 = or i32 %91,%95
    %97 = load i32, i32* @chat
    %98 = getelementptr [10000 x i32], [10000 x i32]* @chas, i32 %97
    %99 = load i32, i32* %98
    %100 = icmp eq i32 %99,i32 94
    %101 = or i32 %96,%100
    br i32 %101 %71 %72

71:
    %104 = call i32 @find()
    %105 = icmp eq i32 %104,i32 0
    br i32 %105 %103 %102

72:
    %106 = call void @chapush(i32 43)

102:
    br %70

103:
    br %72

107:
    %152 = load i32, i32* @i
    %153 = getelementptr [10000 x i32], [10000 x i32]* @get, i32 %152
    %154 = load i32, i32* %153
    %155 = icmp eq i32 %154,i32 42
    br i32 %155 %151 %150

108:
    br %113

113:
    %116 = load i32, i32* @chat
    %117 = getelementptr [10000 x i32], [10000 x i32]* @chas, i32 %116
    %118 = load i32, i32* %117
    %119 = icmp eq i32 %118,i32 43
    %120 = load i32, i32* @chat
    %121 = getelementptr [10000 x i32], [10000 x i32]* @chas, i32 %120
    %122 = load i32, i32* %121
    %123 = icmp eq i32 %122,i32 45
    %124 = or i32 %119,%123
    %125 = load i32, i32* @chat
    %126 = getelementptr [10000 x i32], [10000 x i32]* @chas, i32 %125
    %127 = load i32, i32* %126
    %128 = icmp eq i32 %127,i32 42
    %129 = or i32 %124,%128
    %130 = load i32, i32* @chat
    %131 = getelementptr [10000 x i32], [10000 x i32]* @chas, i32 %130
    %132 = load i32, i32* %131
    %133 = icmp eq i32 %132,i32 47
    %134 = or i32 %129,%133
    %135 = load i32, i32* @chat
    %136 = getelementptr [10000 x i32], [10000 x i32]* @chas, i32 %135
    %137 = load i32, i32* %136
    %138 = icmp eq i32 %137,i32 37
    %139 = or i32 %134,%138
    %140 = load i32, i32* @chat
    %141 = getelementptr [10000 x i32], [10000 x i32]* @chas, i32 %140
    %142 = load i32, i32* %141
    %143 = icmp eq i32 %142,i32 94
    %144 = or i32 %139,%143
    br i32 %144 %114 %115

114:
    %147 = call i32 @find()
    %148 = icmp eq i32 %147,i32 0
    br i32 %148 %146 %145

115:
    %149 = call void @chapush(i32 45)

145:
    br %113

146:
    br %115

150:
    %185 = load i32, i32* @i
    %186 = getelementptr [10000 x i32], [10000 x i32]* @get, i32 %185
    %187 = load i32, i32* %186
    %188 = icmp eq i32 %187,i32 47
    br i32 %188 %184 %183

151:
    br %156

156:
    %159 = load i32, i32* @chat
    %160 = getelementptr [10000 x i32], [10000 x i32]* @chas, i32 %159
    %161 = load i32, i32* %160
    %162 = icmp eq i32 %161,i32 42
    %163 = load i32, i32* @chat
    %164 = getelementptr [10000 x i32], [10000 x i32]* @chas, i32 %163
    %165 = load i32, i32* %164
    %166 = icmp eq i32 %165,i32 47
    %167 = or i32 %162,%166
    %168 = load i32, i32* @chat
    %169 = getelementptr [10000 x i32], [10000 x i32]* @chas, i32 %168
    %170 = load i32, i32* %169
    %171 = icmp eq i32 %170,i32 37
    %172 = or i32 %167,%171
    %173 = load i32, i32* @chat
    %174 = getelementptr [10000 x i32], [10000 x i32]* @chas, i32 %173
    %175 = load i32, i32* %174
    %176 = icmp eq i32 %175,i32 94
    %177 = or i32 %172,%176
    br i32 %177 %157 %158

157:
    %180 = call i32 @find()
    %181 = icmp eq i32 %180,i32 0
    br i32 %181 %179 %178

158:
    %182 = call void @chapush(i32 42)

178:
    br %156

179:
    br %158

183:
    %218 = load i32, i32* @i
    %219 = getelementptr [10000 x i32], [10000 x i32]* @get, i32 %218
    %220 = load i32, i32* %219
    %221 = icmp eq i32 %220,i32 37
    br i32 %221 %217 %216

184:
    br %189

189:
    %192 = load i32, i32* @chat
    %193 = getelementptr [10000 x i32], [10000 x i32]* @chas, i32 %192
    %194 = load i32, i32* %193
    %195 = icmp eq i32 %194,i32 42
    %196 = load i32, i32* @chat
    %197 = getelementptr [10000 x i32], [10000 x i32]* @chas, i32 %196
    %198 = load i32, i32* %197
    %199 = icmp eq i32 %198,i32 47
    %200 = or i32 %195,%199
    %201 = load i32, i32* @chat
    %202 = getelementptr [10000 x i32], [10000 x i32]* @chas, i32 %201
    %203 = load i32, i32* %202
    %204 = icmp eq i32 %203,i32 37
    %205 = or i32 %200,%204
    %206 = load i32, i32* @chat
    %207 = getelementptr [10000 x i32], [10000 x i32]* @chas, i32 %206
    %208 = load i32, i32* %207
    %209 = icmp eq i32 %208,i32 94
    %210 = or i32 %205,%209
    br i32 %210 %190 %191

190:
    %213 = call i32 @find()
    %214 = icmp eq i32 %213,i32 0
    br i32 %214 %212 %211

191:
    %215 = call void @chapush(i32 47)

211:
    br %189

212:
    br %191

216:
    %249 = load i32, i32* @ii
    %250 = getelementptr [10000 x i32], [10000 x i32]* @get2, i32 %249
    store i32 32, i32* %250
    %251 = load i32, i32* @ii
    %252 = load i32, i32* @ii
    %253 = add i32 %252,1
    store i32 %253, i32* @ii

217:
    br %222

222:
    %225 = load i32, i32* @chat
    %226 = getelementptr [10000 x i32], [10000 x i32]* @chas, i32 %225
    %227 = load i32, i32* %226
    %228 = icmp eq i32 %227,i32 42
    %229 = load i32, i32* @chat
    %230 = getelementptr [10000 x i32], [10000 x i32]* @chas, i32 %229
    %231 = load i32, i32* %230
    %232 = icmp eq i32 %231,i32 47
    %233 = or i32 %228,%232
    %234 = load i32, i32* @chat
    %235 = getelementptr [10000 x i32], [10000 x i32]* @chas, i32 %234
    %236 = load i32, i32* %235
    %237 = icmp eq i32 %236,i32 37
    %238 = or i32 %233,%237
    %239 = load i32, i32* @chat
    %240 = getelementptr [10000 x i32], [10000 x i32]* @chas, i32 %239
    %241 = load i32, i32* %240
    %242 = icmp eq i32 %241,i32 94
    %243 = or i32 %238,%242
    br i32 %243 %223 %224

223:
    %246 = call i32 @find()
    %247 = icmp eq i32 %246,i32 0
    br i32 %247 %245 %244

224:
    %248 = call void @chapush(i32 37)

244:
    br %222

245:
    br %224

257:
    %260 = load i32, i32* @chat
    %261 = icmp gt i32 %260,i32 0
    br i32 %261 %258 %259

258:
    %262 = call i32 @chapop()
    %263 = alloca i32
    store i32 %262, i32* %263
    %264 = load i32, i32* @ii
    %265 = getelementptr [10000 x i32], [10000 x i32]* @get2, i32 %264
    store i32 32, i32* %265
    %266 = load i32, i32* @ii
    %267 = load i32, i32* @ii
    %268 = add i32 %267,1
    %269 = getelementptr [10000 x i32], [10000 x i32]* @get2, i32 %268
    %270 = load i32, i32* %263
    store i32 %270, i32* %269
    %271 = load i32, i32* @ii
    %272 = load i32, i32* @ii
    %273 = add i32 %272,2
    store i32 %273, i32* @ii
    br %257

259:
    %274 = load i32, i32* @ii
    %275 = getelementptr [10000 x i32], [10000 x i32]* @get2, i32 %274
    store i32 64, i32* %275
    store i32 1, i32* @i
    br %276

276:
    %279 = load i32, i32* @i
    %280 = getelementptr [10000 x i32], [10000 x i32]* @get2, i32 %279
    %281 = load i32, i32* %280
    %282 = icmp ne i32 %281,i32 64
    br i32 %282 %277 %278

277:
    %286 = load i32, i32* @i
    %287 = getelementptr [10000 x i32], [10000 x i32]* @get2, i32 %286
    %288 = load i32, i32* %287
    %289 = icmp eq i32 %288,i32 43
    %290 = load i32, i32* @i
    %291 = getelementptr [10000 x i32], [10000 x i32]* @get2, i32 %290
    %292 = load i32, i32* %291
    %293 = icmp eq i32 %292,i32 45
    %294 = or i32 %289,%293
    %295 = load i32, i32* @i
    %296 = getelementptr [10000 x i32], [10000 x i32]* @get2, i32 %295
    %297 = load i32, i32* %296
    %298 = icmp eq i32 %297,i32 42
    %299 = or i32 %294,%298
    %300 = load i32, i32* @i
    %301 = getelementptr [10000 x i32], [10000 x i32]* @get2, i32 %300
    %302 = load i32, i32* %301
    %303 = icmp eq i32 %302,i32 47
    %304 = or i32 %299,%303
    %305 = load i32, i32* @i
    %306 = getelementptr [10000 x i32], [10000 x i32]* @get2, i32 %305
    %307 = load i32, i32* %306
    %308 = icmp eq i32 %307,i32 37
    %309 = or i32 %304,%308
    %310 = load i32, i32* @i
    %311 = getelementptr [10000 x i32], [10000 x i32]* @get2, i32 %310
    %312 = load i32, i32* %311
    %313 = icmp eq i32 %312,i32 94
    %314 = or i32 %309,%313
    br i32 %314 %284 %285

278:
    %433 = getelementptr [10000 x i32], [10000 x i32]* @ints, i32 1
    %434 = load i32, i32* %433
    call void @putint(i32 %434)
    ret i32 0

283:
    %430 = load i32, i32* @i
    %431 = load i32, i32* @i
    %432 = add i32 %431,1
    store i32 %432, i32* @i
    br %276

284:
    %315 = call i32 @intpop()
    %316 = alloca i32
    store i32 %315, i32* %316
    %317 = call i32 @intpop()
    %318 = alloca i32
    store i32 %317, i32* %318
    %319 = alloca i32
    %322 = load i32, i32* @i
    %323 = getelementptr [10000 x i32], [10000 x i32]* @get2, i32 %322
    %324 = load i32, i32* %323
    %325 = icmp eq i32 %324,i32 43
    br i32 %325 %321 %320

285:
    %382 = load i32, i32* @i
    %383 = getelementptr [10000 x i32], [10000 x i32]* @get2, i32 %382
    %384 = load i32, i32* %383
    %385 = icmp ne i32 %384,i32 32
    br i32 %385 %381 %380

320:
    %333 = load i32, i32* @i
    %334 = getelementptr [10000 x i32], [10000 x i32]* @get2, i32 %333
    %335 = load i32, i32* %334
    %336 = icmp eq i32 %335,i32 45
    br i32 %336 %332 %331

321:
    %326 = load i32, i32* %316
    %327 = load i32, i32* %318
    %328 = load i32, i32* %316
    %329 = load i32, i32* %318
    %330 = add i32 %328,%329
    store i32 %330, i32* %319
    br %320

331:
    %344 = load i32, i32* @i
    %345 = getelementptr [10000 x i32], [10000 x i32]* @get2, i32 %344
    %346 = load i32, i32* %345
    %347 = icmp eq i32 %346,i32 42
    br i32 %347 %343 %342

332:
    %337 = load i32, i32* %318
    %338 = load i32, i32* %316
    %339 = load i32, i32* %318
    %340 = load i32, i32* %316
    %341 = sub i32 %339,%340
    store i32 %341, i32* %319
    br %331

342:
    %353 = load i32, i32* @i
    %354 = getelementptr [10000 x i32], [10000 x i32]* @get2, i32 %353
    %355 = load i32, i32* %354
    %356 = icmp eq i32 %355,i32 47
    br i32 %356 %352 %351

343:
    %348 = load i32, i32* %316
    %349 = load i32, i32* %318
    %350 = mul i32 %348,%349
    store i32 %350, i32* %319
    br %342

351:
    %362 = load i32, i32* @i
    %363 = getelementptr [10000 x i32], [10000 x i32]* @get2, i32 %362
    %364 = load i32, i32* %363
    %365 = icmp eq i32 %364,i32 37
    br i32 %365 %361 %360

352:
    %357 = load i32, i32* %318
    %358 = load i32, i32* %316
    %359 = sdiv i32 %357,%358
    store i32 %359, i32* %319
    br %351

360:
    %371 = load i32, i32* @i
    %372 = getelementptr [10000 x i32], [10000 x i32]* @get2, i32 %371
    %373 = load i32, i32* %372
    %374 = icmp eq i32 %373,i32 94
    br i32 %374 %370 %369

361:
    %366 = load i32, i32* %318
    %367 = load i32, i32* %316
    %368 = srem i32 %366,%367
    store i32 %368, i32* %319
    br %360

369:
    %378 = load i32, i32* %319
    %379 = call void @intpush(i32 %378)

370:
    %375 = load i32, i32* %318
    %376 = load i32, i32* %316
    %377 = call i32 @power(i32 %375,i32 %376)
    store i32 %377, i32* %319
    br %369

380:

381:
    %386 = load i32, i32* @i
    %387 = getelementptr [10000 x i32], [10000 x i32]* @get2, i32 %386
    %388 = load i32, i32* %387
    %389 = load i32, i32* @i
    %390 = getelementptr [10000 x i32], [10000 x i32]* @get2, i32 %389
    %391 = load i32, i32* %390
    %392 = sub i32 %391,48
    %393 = call void @intpush(i32 %392)
    store i32 1, i32* @ii
    br %394

394:
    %397 = load i32, i32* @i
    %398 = load i32, i32* @ii
    %399 = load i32, i32* @i
    %400 = load i32, i32* @ii
    %401 = add i32 %399,%400
    %402 = getelementptr [10000 x i32], [10000 x i32]* @get2, i32 %401
    %403 = load i32, i32* %402
    %404 = icmp ne i32 %403,i32 32
    br i32 %404 %395 %396

395:
    %405 = load i32, i32* @i
    %406 = load i32, i32* @ii
    %407 = load i32, i32* @i
    %408 = load i32, i32* @ii
    %409 = add i32 %407,%408
    %410 = getelementptr [10000 x i32], [10000 x i32]* @get2, i32 %409
    %411 = load i32, i32* %410
    %412 = load i32, i32* @i
    %413 = load i32, i32* @ii
    %414 = load i32, i32* @i
    %415 = load i32, i32* @ii
    %416 = add i32 %414,%415
    %417 = getelementptr [10000 x i32], [10000 x i32]* @get2, i32 %416
    %418 = load i32, i32* %417
    %419 = sub i32 %418,48
    %420 = call void @intadd(i32 %419)
    %421 = load i32, i32* @ii
    %422 = load i32, i32* @ii
    %423 = add i32 %422,1
    store i32 %423, i32* @ii
    br %394

396:
    %424 = load i32, i32* @i
    %425 = load i32, i32* @ii
    %426 = load i32, i32* @i
    %427 = load i32, i32* @ii
    %428 = add i32 %426,%427
    %429 = sub i32 %428,1
    store i32 %429, i32* @i
}

