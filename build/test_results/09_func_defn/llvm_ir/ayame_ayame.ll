@a = dso_local global i32 0

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @func(i32 %0){
1:
    %2 = alloca i32
    store i32 %0, i32* %2
    %3 = load i32, i32* %2
    %4 = load i32, i32* %2
    %5 = sub i32 %4,1
    store i32 %5, i32* %2
    %6 = load i32, i32* %2
    ret i32 %6
}
define dso_local i32 @main(i32 %0){
1:
    %2 = alloca i32
    store i32 10, i32* @a
    %3 = load i32, i32* @a
    %4 = call i32 @func(i32 %3)
    store i32 %4, i32* %2
    %5 = load i32, i32* %2
    ret i32 %5
}

