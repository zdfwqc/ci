declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @exgcd(i32 %0, i32 %1, i32* %2, i32* %3){
4:
    %5 = alloca i32
    store i32 %0, i32* %5
    %6 = alloca i32
    store i32 %1, i32* %6
    %7 = alloca i32*
    store i32* %2, i32** %7
    %8 = alloca i32*
    store i32* %3, i32** %8
    %12 = load i32, i32* %6
    %13 = icmp eq i32 %12,i32 0
    br i32 %13 %10 %11

9:

10:
    %14 = getelementptr i32*, i32** %7, i32 0
    store i32 1, i32** %14
    %15 = getelementptr i32*, i32** %8, i32 0
    store i32 0, i32** %15
    %16 = load i32, i32* %5
    ret i32 %16

11:
    %17 = load i32, i32* %6
    %18 = load i32, i32* %5
    %19 = load i32, i32* %6
    %20 = srem i32 %18,%19
    %21 = load i32*, i32** %7
    %22 = load i32*, i32** %8
    %23 = call i32 @exgcd(i32 %17,i32 %20,i32* %21,i32* %22)
    %24 = alloca i32
    store i32 %23, i32* %24
    %25 = getelementptr i32*, i32** %7, i32 0
    %26 = load i32*, i32** %25
    %27 = alloca i32
    store i32* %26, i32* %27
    %28 = getelementptr i32*, i32** %7, i32 0
    %29 = getelementptr i32*, i32** %8, i32 0
    %30 = load i32*, i32** %29
    store i32* %30, i32** %28
    %31 = getelementptr i32*, i32** %8, i32 0
    %32 = load i32, i32* %27
    %33 = load i32, i32* %5
    %34 = load i32, i32* %6
    %35 = sdiv i32 %33,%34
    %36 = getelementptr i32*, i32** %8, i32 0
    %37 = load i32*, i32** %36
    %38 = mul i32 %35,%37
    %39 = load i32, i32* %27
    %40 = load i32, i32* %5
    %41 = load i32, i32* %6
    %42 = sdiv i32 %40,%41
    %43 = getelementptr i32*, i32** %8, i32 0
    %44 = load i32*, i32** %43
    %45 = mul i32 %42,%44
    %46 = sub i32 %39,%45
    store i32 %46, i32** %31
    %47 = load i32, i32* %24
    ret i32 %47
}
define dso_local i32 @main(i32 %0, i32 %1, i32* %2, i32* %3){
4:
    %5 = alloca i32
    store i32 7, i32* %5
    %6 = alloca i32
    store i32 15, i32* %6
    %7 = alloca [1 x i32]
    %8 = getelementptr [1 x i32], [1 x i32]* %7, i32 0
    store i32 1, i32* %8
    %9 = alloca [1 x i32]
    %10 = getelementptr [1 x i32], [1 x i32]* %9, i32 0
    store i32 1, i32* %10
    %11 = load i32, i32* %5
    %12 = load i32, i32* %6
    %13 = load [1 x i32], [1 x i32]* %7
    %14 = load [1 x i32], [1 x i32]* %9
    %15 = call i32 @exgcd(i32 %11,i32 %12,[1 x i32] %13,[1 x i32] %14)
    %16 = getelementptr [1 x i32], [1 x i32]* %7, i32 0
    %17 = getelementptr [1 x i32], [1 x i32]* %7, i32 0
    %18 = load i32, i32* %17
    %19 = load i32, i32* %6
    %20 = srem i32 %18,%19
    %21 = load i32, i32* %6
    %22 = getelementptr [1 x i32], [1 x i32]* %7, i32 0
    %23 = load i32, i32* %22
    %24 = load i32, i32* %6
    %25 = srem i32 %23,%24
    %26 = load i32, i32* %6
    %27 = add i32 %25,%26
    %28 = load i32, i32* %6
    %29 = srem i32 %27,%28
    store i32 %29, i32* %16
    %30 = getelementptr [1 x i32], [1 x i32]* %7, i32 0
    %31 = load i32, i32* %30
    call void @putint(i32 %31)
    ret i32 0
}

