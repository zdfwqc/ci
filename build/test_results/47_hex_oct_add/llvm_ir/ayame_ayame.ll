declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @main(){
0:
    %1 = alloca i32
    %2 = alloca i32
    store i32 15, i32* %1
    store i32 12, i32* %2
    %3 = load i32, i32* %1
    %4 = load i32, i32* %2
    %5 = load i32, i32* %1
    %6 = load i32, i32* %2
    %7 = add i32 %5,%6
    %8 = add i32 %7,75
    ret i32 %8
}

