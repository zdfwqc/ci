@n = dso_local global i32 0

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @select_sort(i32* %0, i32 %1){
2:
    %3 = alloca i32*
    store i32* %0, i32** %3
    %4 = alloca i32
    store i32 %1, i32* %4
    %5 = alloca i32
    %6 = alloca i32
    %7 = alloca i32
    store i32 0, i32* %5
    br %8

8:
    %11 = load i32, i32* %5
    %12 = load i32, i32* %4
    %13 = load i32, i32* %4
    %14 = sub i32 %13,1
    %15 = icmp lt i32 %11,i32 %14
    br i32 %15 %9 %10

9:
    %16 = load i32, i32* %5
    store i32 %16, i32* %7
    %17 = load i32, i32* %5
    %18 = load i32, i32* %5
    %19 = add i32 %18,1
    store i32 %19, i32* %6
    br %20

10:
    ret i32 0

20:
    %23 = load i32, i32* %6
    %24 = load i32, i32* %4
    %25 = icmp lt i32 %23,i32 %24
    br i32 %25 %21 %22

21:
    %28 = load i32, i32* %7
    %29 = getelementptr i32*, i32** %3, i32 %28
    %30 = load i32*, i32** %29
    %31 = load i32, i32* %6
    %32 = getelementptr i32*, i32** %3, i32 %31
    %33 = load i32*, i32** %32
    %34 = icmp gt i32* %30,i32* %33
    br i32* %34 %27 %26

22:
    %41 = load i32, i32* %7
    %42 = load i32, i32* %5
    %43 = icmp ne i32 %41,i32 %42
    br i32 %43 %40 %39

26:
    %36 = load i32, i32* %6
    %37 = load i32, i32* %6
    %38 = add i32 %37,1
    store i32 %38, i32* %6
    br %20

27:
    %35 = load i32, i32* %6
    store i32 %35, i32* %7
    br %26

39:
    %56 = load i32, i32* %5
    %57 = load i32, i32* %5
    %58 = add i32 %57,1
    store i32 %58, i32* %5
    br %8

40:
    %44 = alloca i32
    %45 = load i32, i32* %7
    %46 = getelementptr i32*, i32** %3, i32 %45
    %47 = load i32*, i32** %46
    store i32* %47, i32* %44
    %48 = load i32, i32* %7
    %49 = getelementptr i32*, i32** %3, i32 %48
    %50 = load i32, i32* %5
    %51 = getelementptr i32*, i32** %3, i32 %50
    %52 = load i32*, i32** %51
    store i32* %52, i32** %49
    %53 = load i32, i32* %5
    %54 = getelementptr i32*, i32** %3, i32 %53
    %55 = load i32, i32* %44
    store i32 %55, i32** %54
    br %39
}
define dso_local i32 @main(i32* %0, i32 %1){
2:
    store i32 10, i32* @n
    %3 = alloca [10 x i32]
    %4 = getelementptr [10 x i32], [10 x i32]* %3, i32 0
    store i32 4, i32* %4
    %5 = getelementptr [10 x i32], [10 x i32]* %3, i32 1
    store i32 3, i32* %5
    %6 = getelementptr [10 x i32], [10 x i32]* %3, i32 2
    store i32 9, i32* %6
    %7 = getelementptr [10 x i32], [10 x i32]* %3, i32 3
    store i32 2, i32* %7
    %8 = getelementptr [10 x i32], [10 x i32]* %3, i32 4
    store i32 0, i32* %8
    %9 = getelementptr [10 x i32], [10 x i32]* %3, i32 5
    store i32 1, i32* %9
    %10 = getelementptr [10 x i32], [10 x i32]* %3, i32 6
    store i32 6, i32* %10
    %11 = getelementptr [10 x i32], [10 x i32]* %3, i32 7
    store i32 5, i32* %11
    %12 = getelementptr [10 x i32], [10 x i32]* %3, i32 8
    store i32 7, i32* %12
    %13 = getelementptr [10 x i32], [10 x i32]* %3, i32 9
    store i32 8, i32* %13
    %14 = alloca i32
    store i32 0, i32* %14
    %15 = load [10 x i32], [10 x i32]* %3
    %16 = load i32, i32* @n
    %17 = call i32 @select_sort([10 x i32] %15,i32 %16)
    store i32 %17, i32* %14
    br %18

18:
    %21 = load i32, i32* %14
    %22 = load i32, i32* @n
    %23 = icmp lt i32 %21,i32 %22
    br i32 %23 %19 %20

19:
    %24 = alloca i32
    %25 = load i32, i32* %14
    %26 = getelementptr [10 x i32], [10 x i32]* %3, i32 %25
    %27 = load i32, i32* %26
    store i32 %27, i32* %24
    %28 = load i32, i32* %24
    call void @putint(i32 %28)
    store i32 10, i32* %24
    %29 = load i32, i32* %24
    call void @putch(i32 %29)
    %30 = load i32, i32* %14
    %31 = load i32, i32* %14
    %32 = add i32 %31,1
    store i32 %32, i32* %14
    br %18

20:
    ret i32 0
}

