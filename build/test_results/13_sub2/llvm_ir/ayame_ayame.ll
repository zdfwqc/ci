@a = dso_local constant i32 10

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @main(){
0:
    %1 = alloca i32
    store i32 2, i32* %1
    %2 = load i32, i32* %1
    %3 = load i32, i32* @a
    %4 = load i32, i32* %1
    %5 = load i32, i32* @a
    %6 = sub i32 %4,%5
    ret i32 %6
}

