@a = dso_local global i32 7

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @func(){
0:
    %1 = load i32, i32* @a
    %2 = alloca i32
    store i32 %1, i32* %2
    %3 = alloca i32
    store i32 1, i32* %3
    %7 = load i32, i32* %3
    %8 = load i32, i32* %2
    %9 = icmp eq i32 %7,i32 %8
    br i32 %9 %5 %6

4:

5:
    %10 = load i32, i32* %3
    %11 = load i32, i32* %3
    %12 = add i32 %11,1
    store i32 %12, i32* %3
    ret i32 1

6:
    ret i32 0
}
define dso_local i32 @main(){
0:
    %1 = alloca i32
    store i32 0, i32* %1
    %2 = alloca i32
    store i32 0, i32* %2
    br %3

3:
    %6 = load i32, i32* %2
    %7 = icmp lt i32 %6,i32 100
    br i32 %7 %4 %5

4:
    %10 = call i32 @func()
    %11 = icmp eq i32 %10,i32 1
    br i32 %11 %9 %8

5:
    %21 = load i32, i32* %1
    %22 = icmp lt i32 %21,i32 100
    br i32 %22 %19 %20

8:
    %15 = load i32, i32* %2
    %16 = load i32, i32* %2
    %17 = add i32 %16,1
    store i32 %17, i32* %2
    br %3

9:
    %12 = load i32, i32* %1
    %13 = load i32, i32* %1
    %14 = add i32 %13,1
    store i32 %14, i32* %1
    br %8

18:
    ret i32 0

19:
    call void @putint(i32 1)
    br %18

20:
    call void @putint(i32 0)
    br %18
}

