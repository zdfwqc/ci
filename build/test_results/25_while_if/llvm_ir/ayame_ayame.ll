declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @get_one(i32 %0){
1:
    %2 = alloca i32
    store i32 %0, i32* %2
    ret i32 1
}
define dso_local i32 @deepWhileBr(i32 %0, i32 %1){
2:
    %3 = alloca i32
    store i32 %0, i32* %3
    %4 = alloca i32
    store i32 %1, i32* %4
    %5 = alloca i32
    %6 = load i32, i32* %3
    %7 = load i32, i32* %4
    %8 = load i32, i32* %3
    %9 = load i32, i32* %4
    %10 = add i32 %8,%9
    store i32 %10, i32* %5
    br %11

11:
    %14 = load i32, i32* %5
    %15 = icmp lt i32 %14,i32 75
    br i32 %15 %12 %13

12:
    %16 = alloca i32
    store i32 42, i32* %16
    %19 = load i32, i32* %5
    %20 = icmp lt i32 %19,i32 100
    br i32 %20 %18 %17

13:
    %39 = load i32, i32* %5
    ret i32 %39

17:
    br %11

18:
    %21 = load i32, i32* %5
    %22 = load i32, i32* %16
    %23 = load i32, i32* %5
    %24 = load i32, i32* %16
    %25 = add i32 %23,%24
    store i32 %25, i32* %5
    %28 = load i32, i32* %5
    %29 = icmp gt i32 %28,i32 99
    br i32 %29 %27 %26

26:

27:
    %30 = alloca i32
    %31 = load i32, i32* %16
    %32 = mul i32 %31,2
    store i32 %32, i32* %30
    %35 = call i32 @get_one(i32 0)
    %36 = icmp eq i32 %35,i32 1
    br i32 %36 %34 %33

33:

34:
    %37 = load i32, i32* %30
    %38 = mul i32 %37,2
    store i32 %38, i32* %5
    br %33
}
define dso_local i32 @main(i32 %0, i32 %1){
2:
    %3 = alloca i32
    store i32 2, i32* %3
    %4 = load i32, i32* %3
    %5 = load i32, i32* %3
    %6 = call i32 @deepWhileBr(i32 %4,i32 %5)
    store i32 %6, i32* %3
    %7 = load i32, i32* %3
    call void @putint(i32 %7)
    ret i32 0
}

