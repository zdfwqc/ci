@a = dso_local global i32 0

@b = dso_local global i32 0

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @main(){
0:
    %1 = call i32 @getint()
    store i32 %1, i32* @a
    %2 = call i32 @getint()
    store i32 %2, i32* @b
    %3 = alloca i32
    %7 = load i32, i32* @a
    %8 = load i32, i32* @b
    %9 = icmp eq i32 %7,i32 %8
    %10 = load i32, i32* @a
    %11 = icmp ne i32 %10,i32 3
    %12 = and i32 %9,%11
    br i32 %12 %5 %6

4:
    %13 = load i32, i32* %3
    ret i32 %13

5:
    store i32 1, i32* %3
    br %4

6:
    store i32 0, i32* %3
    br %4
}

