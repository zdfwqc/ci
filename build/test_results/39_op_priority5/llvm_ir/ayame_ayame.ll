@a = dso_local global i32 1

@b = dso_local global i32 0

@c = dso_local global i32 1

@d = dso_local global i32 2

@e = dso_local global i32 4

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @main(){
0:
    %1 = alloca i32
    store i32 0, i32* %1
    %4 = load i32, i32* @a
    %5 = load i32, i32* @b
    %6 = mul i32 %4,%5
    %7 = load i32, i32* @c
    %8 = sdiv i32 %6,%7
    %9 = load i32, i32* @e
    %10 = load i32, i32* @d
    %11 = load i32, i32* @e
    %12 = load i32, i32* @d
    %13 = add i32 %11,%12
    %14 = icmp eq i32 %8,i32 %13
    %15 = load i32, i32* @a
    %16 = load i32, i32* @a
    %17 = load i32, i32* @b
    %18 = load i32, i32* @a
    %19 = load i32, i32* @b
    %20 = add i32 %18,%19
    %21 = mul i32 %15,%20
    %22 = load i32, i32* @c
    %23 = load i32, i32* @a
    %24 = load i32, i32* @a
    %25 = load i32, i32* @b
    %26 = load i32, i32* @a
    %27 = load i32, i32* @b
    %28 = add i32 %26,%27
    %29 = mul i32 %23,%28
    %30 = load i32, i32* @c
    %31 = add i32 %29,%30
    %32 = load i32, i32* @d
    %33 = load i32, i32* @e
    %34 = load i32, i32* @d
    %35 = load i32, i32* @e
    %36 = add i32 %34,%35
    %37 = icmp le i32 %31,i32 %36
    %38 = and i32 %14,%37
    %39 = load i32, i32* @a
    %40 = load i32, i32* @b
    %41 = load i32, i32* @c
    %42 = mul i32 %40,%41
    %43 = load i32, i32* @a
    %44 = load i32, i32* @b
    %45 = load i32, i32* @c
    %46 = mul i32 %44,%45
    %47 = sub i32 %43,%46
    %48 = load i32, i32* @d
    %49 = load i32, i32* @a
    %50 = load i32, i32* @c
    %51 = sdiv i32 %49,%50
    %52 = load i32, i32* @d
    %53 = load i32, i32* @a
    %54 = load i32, i32* @c
    %55 = sdiv i32 %53,%54
    %56 = sub i32 %52,%55
    %57 = icmp eq i32 %47,i32 %56
    %58 = or i32 %38,%57
    br i32 %58 %3 %2

2:
    %59 = load i32, i32* %1
    call void @putint(i32 %59)
    %60 = load i32, i32* %1
    ret i32 %60

3:
    store i32 1, i32* %1
    br %2
}

