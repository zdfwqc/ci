@n = dso_local global i32 0

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @counting_sort(i32* %0, i32* %1, i32 %2){
3:
    %4 = alloca i32*
    store i32* %0, i32** %4
    %5 = alloca i32*
    store i32* %1, i32** %5
    %6 = alloca i32
    store i32 %2, i32* %6
    %7 = alloca [10 x i32]
    %8 = alloca i32
    %9 = alloca i32
    %10 = alloca i32
    store i32 0, i32* %10
    store i32 0, i32* %8
    store i32 0, i32* %9
    br %11

11:
    %14 = load i32, i32* %10
    %15 = icmp lt i32 %14,i32 10
    br i32 %15 %12 %13

12:
    %16 = load i32, i32* %10
    %17 = getelementptr [10 x i32], [10 x i32]* %7, i32 %16
    store i32 0, i32* %17
    %18 = load i32, i32* %10
    %19 = load i32, i32* %10
    %20 = add i32 %19,1
    store i32 %20, i32* %10
    br %11

13:
    br %21

21:
    %24 = load i32, i32* %8
    %25 = load i32, i32* %6
    %26 = icmp lt i32 %24,i32 %25
    br i32 %26 %22 %23

22:
    %27 = load i32, i32* %8
    %28 = getelementptr i32*, i32** %4, i32 %27
    %29 = load i32*, i32** %28
    %30 = getelementptr [10 x i32], [10 x i32]* %7, i32* %29
    %31 = load i32, i32* %8
    %32 = getelementptr i32*, i32** %4, i32 %31
    %33 = load i32*, i32** %32
    %34 = getelementptr [10 x i32], [10 x i32]* %7, i32* %33
    %35 = load i32, i32* %34
    %36 = load i32, i32* %8
    %37 = getelementptr i32*, i32** %4, i32 %36
    %38 = load i32*, i32** %37
    %39 = getelementptr [10 x i32], [10 x i32]* %7, i32* %38
    %40 = load i32, i32* %39
    %41 = add i32 %40,1
    store i32 %41, i32* %30
    %42 = load i32, i32* %8
    %43 = load i32, i32* %8
    %44 = add i32 %43,1
    store i32 %44, i32* %8
    br %21

23:
    store i32 1, i32* %10
    br %45

45:
    %48 = load i32, i32* %10
    %49 = icmp lt i32 %48,i32 10
    br i32 %49 %46 %47

46:
    %50 = load i32, i32* %10
    %51 = getelementptr [10 x i32], [10 x i32]* %7, i32 %50
    %52 = load i32, i32* %10
    %53 = getelementptr [10 x i32], [10 x i32]* %7, i32 %52
    %54 = load i32, i32* %53
    %55 = load i32, i32* %10
    %56 = load i32, i32* %10
    %57 = sub i32 %56,1
    %58 = getelementptr [10 x i32], [10 x i32]* %7, i32 %57
    %59 = load i32, i32* %58
    %60 = load i32, i32* %10
    %61 = getelementptr [10 x i32], [10 x i32]* %7, i32 %60
    %62 = load i32, i32* %61
    %63 = load i32, i32* %10
    %64 = load i32, i32* %10
    %65 = sub i32 %64,1
    %66 = getelementptr [10 x i32], [10 x i32]* %7, i32 %65
    %67 = load i32, i32* %66
    %68 = add i32 %62,%67
    store i32 %68, i32* %51
    %69 = load i32, i32* %10
    %70 = load i32, i32* %10
    %71 = add i32 %70,1
    store i32 %71, i32* %10
    br %45

47:
    %72 = load i32, i32* %6
    store i32 %72, i32* %9
    br %73

73:
    %76 = load i32, i32* %9
    %77 = icmp gt i32 %76,i32 0
    br i32 %77 %74 %75

74:
    %78 = load i32, i32* %9
    %79 = load i32, i32* %9
    %80 = sub i32 %79,1
    %81 = getelementptr i32*, i32** %4, i32 %80
    %82 = load i32*, i32** %81
    %83 = getelementptr [10 x i32], [10 x i32]* %7, i32* %82
    %84 = load i32, i32* %9
    %85 = load i32, i32* %9
    %86 = sub i32 %85,1
    %87 = getelementptr i32*, i32** %4, i32 %86
    %88 = load i32*, i32** %87
    %89 = getelementptr [10 x i32], [10 x i32]* %7, i32* %88
    %90 = load i32, i32* %89
    %91 = load i32, i32* %9
    %92 = load i32, i32* %9
    %93 = sub i32 %92,1
    %94 = getelementptr i32*, i32** %4, i32 %93
    %95 = load i32*, i32** %94
    %96 = getelementptr [10 x i32], [10 x i32]* %7, i32* %95
    %97 = load i32, i32* %96
    %98 = sub i32 %97,1
    store i32 %98, i32* %83
    %99 = load i32, i32* %9
    %100 = load i32, i32* %9
    %101 = sub i32 %100,1
    %102 = getelementptr i32*, i32** %4, i32 %101
    %103 = load i32*, i32** %102
    %104 = getelementptr [10 x i32], [10 x i32]* %7, i32* %103
    %105 = load i32, i32* %104
    %106 = getelementptr i32*, i32** %5, i32 %105
    %107 = load i32, i32* %9
    %108 = load i32, i32* %9
    %109 = sub i32 %108,1
    %110 = getelementptr i32*, i32** %4, i32 %109
    %111 = load i32*, i32** %110
    store i32* %111, i32** %106
    %112 = load i32, i32* %9
    %113 = load i32, i32* %9
    %114 = sub i32 %113,1
    store i32 %114, i32* %9
    br %73

75:
    ret i32 0
}
define dso_local i32 @main(i32* %0, i32* %1, i32 %2){
3:
    store i32 10, i32* @n
    %4 = alloca [10 x i32]
    %5 = getelementptr [10 x i32], [10 x i32]* %4, i32 0
    store i32 4, i32* %5
    %6 = getelementptr [10 x i32], [10 x i32]* %4, i32 1
    store i32 3, i32* %6
    %7 = getelementptr [10 x i32], [10 x i32]* %4, i32 2
    store i32 9, i32* %7
    %8 = getelementptr [10 x i32], [10 x i32]* %4, i32 3
    store i32 2, i32* %8
    %9 = getelementptr [10 x i32], [10 x i32]* %4, i32 4
    store i32 0, i32* %9
    %10 = getelementptr [10 x i32], [10 x i32]* %4, i32 5
    store i32 1, i32* %10
    %11 = getelementptr [10 x i32], [10 x i32]* %4, i32 6
    store i32 6, i32* %11
    %12 = getelementptr [10 x i32], [10 x i32]* %4, i32 7
    store i32 5, i32* %12
    %13 = getelementptr [10 x i32], [10 x i32]* %4, i32 8
    store i32 7, i32* %13
    %14 = getelementptr [10 x i32], [10 x i32]* %4, i32 9
    store i32 8, i32* %14
    %15 = alloca i32
    store i32 0, i32* %15
    %16 = alloca [10 x i32]
    %17 = load [10 x i32], [10 x i32]* %4
    %18 = load [10 x i32], [10 x i32]* %16
    %19 = load i32, i32* @n
    %20 = call i32 @counting_sort([10 x i32] %17,[10 x i32] %18,i32 %19)
    store i32 %20, i32* %15
    br %21

21:
    %24 = load i32, i32* %15
    %25 = load i32, i32* @n
    %26 = icmp lt i32 %24,i32 %25
    br i32 %26 %22 %23

22:
    %27 = alloca i32
    %28 = load i32, i32* %15
    %29 = getelementptr [10 x i32], [10 x i32]* %16, i32 %28
    %30 = load i32, i32* %29
    store i32 %30, i32* %27
    %31 = load i32, i32* %27
    call void @putint(i32 %31)
    store i32 10, i32* %27
    %32 = load i32, i32* %27
    call void @putch(i32 %32)
    %33 = load i32, i32* %15
    %34 = load i32, i32* %15
    %35 = add i32 %34,1
    store i32 %35, i32* %15
    br %21

23:
    ret i32 0
}

