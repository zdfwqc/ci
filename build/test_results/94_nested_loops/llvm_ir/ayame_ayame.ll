@arr1 = dso_local global [10 x [2 x [3 x [4 x [5 x [6 x [2 x i32]]]]]]] zeroinitializer

@arr2 = dso_local global [10 x [2 x [3 x [2 x [4 x [8 x [7 x i32]]]]]]] zeroinitializer

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local void @loop1(i32 %0, i32 %1){
2:
    %3 = alloca i32
    store i32 %0, i32* %3
    %4 = alloca i32
    store i32 %1, i32* %4
    %5 = alloca i32
    %6 = alloca i32
    %7 = alloca i32
    %8 = alloca i32
    %9 = alloca i32
    %10 = alloca i32
    %11 = alloca i32
    store i32 0, i32* %5
    br %12

12:
    %15 = load i32, i32* %5
    %16 = load i32, i32* %3
    %17 = icmp lt i32 %15,i32 %16
    %18 = load i32, i32* %5
    %19 = load i32, i32* %4
    %20 = icmp lt i32 %18,i32 %19
    %21 = and i32 %17,%20
    br i32 %21 %13 %14

13:
    store i32 0, i32* %6
    br %22

14:

22:
    %25 = load i32, i32* %6
    %26 = icmp lt i32 %25,i32 2
    br i32 %26 %23 %24

23:
    store i32 0, i32* %7
    br %27

24:
    %97 = load i32, i32* %5
    %98 = load i32, i32* %5
    %99 = add i32 %98,1
    store i32 %99, i32* %5
    br %12

27:
    %30 = load i32, i32* %7
    %31 = icmp lt i32 %30,i32 3
    br i32 %31 %28 %29

28:
    store i32 0, i32* %8
    br %32

29:
    %94 = load i32, i32* %6
    %95 = load i32, i32* %6
    %96 = add i32 %95,1
    store i32 %96, i32* %6
    br %22

32:
    %35 = load i32, i32* %8
    %36 = icmp lt i32 %35,i32 4
    br i32 %36 %33 %34

33:
    store i32 0, i32* %9
    br %37

34:
    %91 = load i32, i32* %7
    %92 = load i32, i32* %7
    %93 = add i32 %92,1
    store i32 %93, i32* %7
    br %27

37:
    %40 = load i32, i32* %9
    %41 = icmp lt i32 %40,i32 5
    br i32 %41 %38 %39

38:
    store i32 0, i32* %10
    br %42

39:
    %88 = load i32, i32* %8
    %89 = load i32, i32* %8
    %90 = add i32 %89,1
    store i32 %90, i32* %8
    br %32

42:
    %45 = load i32, i32* %10
    %46 = icmp lt i32 %45,i32 6
    br i32 %46 %43 %44

43:
    store i32 0, i32* %11
    br %47

44:
    %85 = load i32, i32* %9
    %86 = load i32, i32* %9
    %87 = add i32 %86,1
    store i32 %87, i32* %9
    br %37

47:
    %50 = load i32, i32* %11
    %51 = icmp lt i32 %50,i32 2
    br i32 %51 %48 %49

48:
    %52 = load i32, i32* %5
    %53 = load i32, i32* %6
    %54 = load i32, i32* %7
    %55 = load i32, i32* %8
    %56 = load i32, i32* %9
    %57 = load i32, i32* %10
    %58 = load i32, i32* %11
    %59 = getelementptr [10 x [2 x [3 x [4 x [5 x [6 x [2 x i32]]]]]]], [10 x [2 x [3 x [4 x [5 x [6 x [2 x i32]]]]]]]* @arr1, i32 %52, i32 %53, i32 %54, i32 %55, i32 %56, i32 %57, i32 %58
    %60 = load i32, i32* %5
    %61 = load i32, i32* %6
    %62 = load i32, i32* %5
    %63 = load i32, i32* %6
    %64 = add i32 %62,%63
    %65 = load i32, i32* %7
    %66 = add i32 %64,%65
    %67 = load i32, i32* %8
    %68 = add i32 %66,%67
    %69 = load i32, i32* %9
    %70 = add i32 %68,%69
    %71 = load i32, i32* %10
    %72 = add i32 %70,%71
    %73 = load i32, i32* %11
    %74 = add i32 %72,%73
    %75 = load i32, i32* %3
    %76 = add i32 %74,%75
    %77 = load i32, i32* %4
    %78 = add i32 %76,%77
    store i32 %78, i32* %59
    %79 = load i32, i32* %11
    %80 = load i32, i32* %11
    %81 = add i32 %80,1
    store i32 %81, i32* %11
    br %47

49:
    %82 = load i32, i32* %10
    %83 = load i32, i32* %10
    %84 = add i32 %83,1
    store i32 %84, i32* %10
    br %42
}
define dso_local void @loop2(i32 %0, i32 %1){
2:
    %3 = alloca i32
    %4 = alloca i32
    %5 = alloca i32
    %6 = alloca i32
    %7 = alloca i32
    %8 = alloca i32
    %9 = alloca i32
    store i32 0, i32* %3
    br %10

10:
    %13 = load i32, i32* %3
    %14 = icmp lt i32 %13,i32 10
    br i32 %14 %11 %12

11:
    store i32 0, i32* %4
    br %15

12:

15:
    %18 = load i32, i32* %4
    %19 = icmp lt i32 %18,i32 2
    br i32 %19 %16 %17

16:
    store i32 0, i32* %5
    br %20

17:
    %80 = load i32, i32* %3
    %81 = load i32, i32* %3
    %82 = add i32 %81,1
    store i32 %82, i32* %3
    br %10

20:
    %23 = load i32, i32* %5
    %24 = icmp lt i32 %23,i32 3
    br i32 %24 %21 %22

21:
    store i32 0, i32* %6
    br %25

22:
    %77 = load i32, i32* %4
    %78 = load i32, i32* %4
    %79 = add i32 %78,1
    store i32 %79, i32* %4
    br %15

25:
    %28 = load i32, i32* %6
    %29 = icmp lt i32 %28,i32 2
    br i32 %29 %26 %27

26:
    store i32 0, i32* %7
    br %30

27:
    %74 = load i32, i32* %5
    %75 = load i32, i32* %5
    %76 = add i32 %75,1
    store i32 %76, i32* %5
    br %20

30:
    %33 = load i32, i32* %7
    %34 = icmp lt i32 %33,i32 4
    br i32 %34 %31 %32

31:
    store i32 0, i32* %8
    br %35

32:
    %71 = load i32, i32* %6
    %72 = load i32, i32* %6
    %73 = add i32 %72,1
    store i32 %73, i32* %6
    br %25

35:
    %38 = load i32, i32* %8
    %39 = icmp lt i32 %38,i32 8
    br i32 %39 %36 %37

36:
    store i32 0, i32* %9
    br %40

37:
    %68 = load i32, i32* %7
    %69 = load i32, i32* %7
    %70 = add i32 %69,1
    store i32 %70, i32* %7
    br %30

40:
    %43 = load i32, i32* %9
    %44 = icmp lt i32 %43,i32 7
    br i32 %44 %41 %42

41:
    %45 = load i32, i32* %3
    %46 = load i32, i32* %4
    %47 = load i32, i32* %5
    %48 = load i32, i32* %6
    %49 = load i32, i32* %7
    %50 = load i32, i32* %8
    %51 = load i32, i32* %9
    %52 = getelementptr [10 x [2 x [3 x [2 x [4 x [8 x [7 x i32]]]]]]], [10 x [2 x [3 x [2 x [4 x [8 x [7 x i32]]]]]]]* @arr2, i32 %45, i32 %46, i32 %47, i32 %48, i32 %49, i32 %50, i32 %51
    %53 = load i32, i32* %3
    %54 = load i32, i32* %4
    %55 = load i32, i32* %3
    %56 = load i32, i32* %4
    %57 = add i32 %55,%56
    %58 = load i32, i32* %6
    %59 = add i32 %57,%58
    %60 = load i32, i32* %9
    %61 = add i32 %59,%60
    store i32 %61, i32* %52
    %62 = load i32, i32* %9
    %63 = load i32, i32* %9
    %64 = add i32 %63,1
    store i32 %64, i32* %9
    br %40

42:
    %65 = load i32, i32* %8
    %66 = load i32, i32* %8
    %67 = add i32 %66,1
    store i32 %67, i32* %8
    br %35
}
define dso_local i32 @loop3(i32 %0, i32 %1, i32 %2, i32 %3, i32 %4, i32 %5, i32 %6){
7:
    %8 = alloca i32
    store i32 %0, i32* %8
    %9 = alloca i32
    store i32 %1, i32* %9
    %10 = alloca i32
    store i32 %2, i32* %10
    %11 = alloca i32
    store i32 %3, i32* %11
    %12 = alloca i32
    store i32 %4, i32* %12
    %13 = alloca i32
    store i32 %5, i32* %13
    %14 = alloca i32
    store i32 %6, i32* %14
    %15 = alloca i32
    %16 = alloca i32
    %17 = alloca i32
    %18 = alloca i32
    %19 = alloca i32
    %20 = alloca i32
    %21 = alloca i32
    %22 = alloca i32
    store i32 0, i32* %22
    store i32 0, i32* %15
    br %23

23:
    %26 = load i32, i32* %15
    %27 = icmp lt i32 %26,i32 10
    br i32 %27 %24 %25

24:
    store i32 0, i32* %16
    br %28

25:
    %147 = load i32, i32* %22
    ret i32 %147

28:
    %31 = load i32, i32* %16
    %32 = icmp lt i32 %31,i32 100
    br i32 %32 %29 %30

29:
    store i32 0, i32* %17
    br %33

30:
    %139 = load i32, i32* %15
    %140 = load i32, i32* %15
    %141 = add i32 %140,1
    store i32 %141, i32* %15
    %144 = load i32, i32* %15
    %145 = load i32, i32* %8
    %146 = icmp ge i32 %144,i32 %145
    br i32 %146 %143 %142

33:
    %36 = load i32, i32* %17
    %37 = icmp lt i32 %36,i32 1000
    br i32 %37 %34 %35

34:
    store i32 0, i32* %18
    br %38

35:
    %131 = load i32, i32* %16
    %132 = load i32, i32* %16
    %133 = add i32 %132,1
    store i32 %133, i32* %16
    %136 = load i32, i32* %16
    %137 = load i32, i32* %9
    %138 = icmp ge i32 %136,i32 %137
    br i32 %138 %135 %134

38:
    %41 = load i32, i32* %18
    %42 = icmp lt i32 %41,i32 10000
    br i32 %42 %39 %40

39:
    store i32 0, i32* %19
    br %43

40:
    %123 = load i32, i32* %17
    %124 = load i32, i32* %17
    %125 = add i32 %124,1
    store i32 %125, i32* %17
    %128 = load i32, i32* %17
    %129 = load i32, i32* %10
    %130 = icmp ge i32 %128,i32 %129
    br i32 %130 %127 %126

43:
    %46 = load i32, i32* %19
    %47 = icmp lt i32 %46,i32 100000
    br i32 %47 %44 %45

44:
    store i32 0, i32* %20
    br %48

45:
    %115 = load i32, i32* %18
    %116 = load i32, i32* %18
    %117 = add i32 %116,1
    store i32 %117, i32* %18
    %120 = load i32, i32* %18
    %121 = load i32, i32* %11
    %122 = icmp ge i32 %120,i32 %121
    br i32 %122 %119 %118

48:
    %51 = load i32, i32* %20
    %52 = icmp lt i32 %51,i32 1000000
    br i32 %52 %49 %50

49:
    store i32 0, i32* %21
    br %53

50:
    %107 = load i32, i32* %19
    %108 = load i32, i32* %19
    %109 = add i32 %108,1
    store i32 %109, i32* %19
    %112 = load i32, i32* %19
    %113 = load i32, i32* %12
    %114 = icmp ge i32 %112,i32 %113
    br i32 %114 %111 %110

53:
    %56 = load i32, i32* %21
    %57 = icmp lt i32 %56,i32 10000000
    br i32 %57 %54 %55

54:
    %58 = load i32, i32* %22
    %59 = srem i32 %58,817
    %60 = load i32, i32* %15
    %61 = load i32, i32* %16
    %62 = load i32, i32* %17
    %63 = load i32, i32* %18
    %64 = load i32, i32* %19
    %65 = load i32, i32* %20
    %66 = load i32, i32* %21
    %67 = getelementptr [10 x [2 x [3 x [4 x [5 x [6 x [2 x i32]]]]]]], [10 x [2 x [3 x [4 x [5 x [6 x [2 x i32]]]]]]]* @arr1, i32 %60, i32 %61, i32 %62, i32 %63, i32 %64, i32 %65, i32 %66
    %68 = load i32, i32* %67
    %69 = load i32, i32* %22
    %70 = srem i32 %69,817
    %71 = load i32, i32* %15
    %72 = load i32, i32* %16
    %73 = load i32, i32* %17
    %74 = load i32, i32* %18
    %75 = load i32, i32* %19
    %76 = load i32, i32* %20
    %77 = load i32, i32* %21
    %78 = getelementptr [10 x [2 x [3 x [4 x [5 x [6 x [2 x i32]]]]]]], [10 x [2 x [3 x [4 x [5 x [6 x [2 x i32]]]]]]]* @arr1, i32 %71, i32 %72, i32 %73, i32 %74, i32 %75, i32 %76, i32 %77
    %79 = load i32, i32* %78
    %80 = add i32 %70,%79
    %81 = load i32, i32* %15
    %82 = load i32, i32* %16
    %83 = load i32, i32* %17
    %84 = load i32, i32* %18
    %85 = load i32, i32* %19
    %86 = load i32, i32* %20
    %87 = load i32, i32* %21
    %88 = getelementptr [10 x [2 x [3 x [2 x [4 x [8 x [7 x i32]]]]]]], [10 x [2 x [3 x [2 x [4 x [8 x [7 x i32]]]]]]]* @arr2, i32 %81, i32 %82, i32 %83, i32 %84, i32 %85, i32 %86, i32 %87
    %89 = load i32, i32* %88
    %90 = add i32 %80,%89
    store i32 %90, i32* %22
    %91 = load i32, i32* %21
    %92 = load i32, i32* %21
    %93 = add i32 %92,1
    store i32 %93, i32* %21
    %96 = load i32, i32* %21
    %97 = load i32, i32* %14
    %98 = icmp ge i32 %96,i32 %97
    br i32 %98 %95 %94

55:
    %99 = load i32, i32* %20
    %100 = load i32, i32* %20
    %101 = add i32 %100,1
    store i32 %101, i32* %20
    %104 = load i32, i32* %20
    %105 = load i32, i32* %13
    %106 = icmp ge i32 %104,i32 %105
    br i32 %106 %103 %102

94:
    br %53

95:
    br %55

102:
    br %48

103:
    br %55

110:
    br %43

111:
    br %55

118:
    br %38

119:
    br %55

126:
    br %33

127:
    br %55

134:
    br %28

135:
    br %55

142:
    br %23

143:
    br %55
}
define dso_local i32 @main(i32 %0, i32 %1, i32 %2, i32 %3, i32 %4, i32 %5, i32 %6){
7:
    %8 = call i32 @getint()
    %9 = alloca i32
    store i32 %8, i32* %9
    %10 = call i32 @getint()
    %11 = alloca i32
    store i32 %10, i32* %11
    %12 = call i32 @getint()
    %13 = alloca i32
    store i32 %12, i32* %13
    %14 = call i32 @getint()
    %15 = alloca i32
    store i32 %14, i32* %15
    %16 = call i32 @getint()
    %17 = alloca i32
    store i32 %16, i32* %17
    %18 = call i32 @getint()
    %19 = alloca i32
    store i32 %18, i32* %19
    %20 = call i32 @getint()
    %21 = alloca i32
    store i32 %20, i32* %21
    %22 = call i32 @getint()
    %23 = alloca i32
    store i32 %22, i32* %23
    %24 = call i32 @getint()
    %25 = alloca i32
    store i32 %24, i32* %25
    %26 = load i32, i32* %9
    %27 = load i32, i32* %11
    %28 = call void @loop1(i32 %26,i32 %27)
    %29 = call void @loop2()
    %30 = load i32, i32* %13
    %31 = load i32, i32* %15
    %32 = load i32, i32* %17
    %33 = load i32, i32* %19
    %34 = load i32, i32* %21
    %35 = load i32, i32* %23
    %36 = load i32, i32* %25
    %37 = call i32 @loop3(i32 %30,i32 %31,i32 %32,i32 %33,i32 %34,i32 %35,i32 %36)
    ret i32 %37
}

