declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @main(){
0:
    %1 = alloca i32
    store i32 10, i32* %1
    %5 = load i32, i32* %1
    
    
    
    %9 = sub i32 0,%8
    br i32 %9 %3 %4

2:
    %13 = load i32, i32* %1
    ret i32 %13

3:
    %10 = sub i32 0,1
    %11 = sub i32 0,%10
    %12 = sub i32 0,%11
    store i32 %12, i32* %1
    br %2

4:
    store i32 0, i32* %1
    br %2
}

