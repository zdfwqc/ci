declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @main(){
0:
    %1 = alloca i32
    %2 = alloca i32
    store i32 70, i32* %1
    store i32 4, i32* %2
    %3 = load i32, i32* %1
    %4 = sub i32 0,4
    %5 = load i32, i32* %1
    %6 = sub i32 0,4
    %7 = sub i32 %5,%6
    %8 = load i32, i32* %2
    %9 = add i32 %7,%8
    store i32 %9, i32* %1
    %13 = load i32, i32* %1
    
    
    
    %17 = sub i32 0,%16
    br i32 %17 %11 %12

10:
    %24 = load i32, i32* %1
    call void @putint(i32 %24)
    ret i32 0

11:
    %18 = sub i32 0,1
    %19 = sub i32 0,%18
    %20 = sub i32 0,%19
    store i32 %20, i32* %1
    br %10

12:
    %21 = load i32, i32* %2
    %22 = load i32, i32* %2
    %23 = add i32 0,%22
    store i32 %23, i32* %1
    br %10
}

