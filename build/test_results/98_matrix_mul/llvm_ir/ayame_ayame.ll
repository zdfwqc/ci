@M = dso_local global i32 0

@L = dso_local global i32 0

@N = dso_local global i32 0

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @mul(float* %0, float* %1, float* %2, float* %3, float* %4, float* %5, float* %6, float* %7, float* %8){
9:
    %10 = alloca float*
    store float* %0, float** %10
    %11 = alloca float*
    store float* %1, float** %11
    %12 = alloca float*
    store float* %2, float** %12
    %13 = alloca float*
    store float* %3, float** %13
    %14 = alloca float*
    store float* %4, float** %14
    %15 = alloca float*
    store float* %5, float** %15
    %16 = alloca float*
    store float* %6, float** %16
    %17 = alloca float*
    store float* %7, float** %17
    %18 = alloca float*
    store float* %8, float** %18
    %19 = alloca i32
    store i32 0, i32* %19
    %20 = getelementptr float*, float** %16, i32 0
    %21 = getelementptr float*, float** %10, i32 0
    %22 = load float*, float** %21
    %23 = getelementptr float*, float** %13, i32 0
    %24 = load float*, float** %23
    %25 = mul i32 %22,%24
    %26 = getelementptr float*, float** %10, i32 1
    %27 = load float*, float** %26
    %28 = getelementptr float*, float** %14, i32 0
    %29 = load float*, float** %28
    %30 = mul i32 %27,%29
    %31 = getelementptr float*, float** %10, i32 0
    %32 = load float*, float** %31
    %33 = getelementptr float*, float** %13, i32 0
    %34 = load float*, float** %33
    %35 = mul i32 %32,%34
    %36 = getelementptr float*, float** %10, i32 1
    %37 = load float*, float** %36
    %38 = getelementptr float*, float** %14, i32 0
    %39 = load float*, float** %38
    %40 = mul i32 %37,%39
    %41 = add i32 %35,%40
    %42 = getelementptr float*, float** %10, i32 2
    %43 = load float*, float** %42
    %44 = getelementptr float*, float** %15, i32 0
    %45 = load float*, float** %44
    %46 = mul i32 %43,%45
    %47 = add i32 %41,%46
    store float* %47, float** %20
    %48 = getelementptr float*, float** %16, i32 1
    %49 = getelementptr float*, float** %10, i32 0
    %50 = load float*, float** %49
    %51 = getelementptr float*, float** %13, i32 1
    %52 = load float*, float** %51
    %53 = mul i32 %50,%52
    %54 = getelementptr float*, float** %10, i32 1
    %55 = load float*, float** %54
    %56 = getelementptr float*, float** %14, i32 1
    %57 = load float*, float** %56
    %58 = mul i32 %55,%57
    %59 = getelementptr float*, float** %10, i32 0
    %60 = load float*, float** %59
    %61 = getelementptr float*, float** %13, i32 1
    %62 = load float*, float** %61
    %63 = mul i32 %60,%62
    %64 = getelementptr float*, float** %10, i32 1
    %65 = load float*, float** %64
    %66 = getelementptr float*, float** %14, i32 1
    %67 = load float*, float** %66
    %68 = mul i32 %65,%67
    %69 = add i32 %63,%68
    %70 = getelementptr float*, float** %10, i32 2
    %71 = load float*, float** %70
    %72 = getelementptr float*, float** %15, i32 1
    %73 = load float*, float** %72
    %74 = mul i32 %71,%73
    %75 = add i32 %69,%74
    store float* %75, float** %48
    %76 = getelementptr float*, float** %16, i32 2
    %77 = getelementptr float*, float** %10, i32 0
    %78 = load float*, float** %77
    %79 = getelementptr float*, float** %13, i32 2
    %80 = load float*, float** %79
    %81 = mul i32 %78,%80
    %82 = getelementptr float*, float** %10, i32 1
    %83 = load float*, float** %82
    %84 = getelementptr float*, float** %14, i32 2
    %85 = load float*, float** %84
    %86 = mul i32 %83,%85
    %87 = getelementptr float*, float** %10, i32 0
    %88 = load float*, float** %87
    %89 = getelementptr float*, float** %13, i32 2
    %90 = load float*, float** %89
    %91 = mul i32 %88,%90
    %92 = getelementptr float*, float** %10, i32 1
    %93 = load float*, float** %92
    %94 = getelementptr float*, float** %14, i32 2
    %95 = load float*, float** %94
    %96 = mul i32 %93,%95
    %97 = add i32 %91,%96
    %98 = getelementptr float*, float** %10, i32 2
    %99 = load float*, float** %98
    %100 = getelementptr float*, float** %15, i32 2
    %101 = load float*, float** %100
    %102 = mul i32 %99,%101
    %103 = add i32 %97,%102
    store float* %103, float** %76
    %104 = getelementptr float*, float** %17, i32 0
    %105 = getelementptr float*, float** %11, i32 0
    %106 = load float*, float** %105
    %107 = getelementptr float*, float** %13, i32 0
    %108 = load float*, float** %107
    %109 = mul i32 %106,%108
    %110 = getelementptr float*, float** %11, i32 1
    %111 = load float*, float** %110
    %112 = getelementptr float*, float** %14, i32 0
    %113 = load float*, float** %112
    %114 = mul i32 %111,%113
    %115 = getelementptr float*, float** %11, i32 0
    %116 = load float*, float** %115
    %117 = getelementptr float*, float** %13, i32 0
    %118 = load float*, float** %117
    %119 = mul i32 %116,%118
    %120 = getelementptr float*, float** %11, i32 1
    %121 = load float*, float** %120
    %122 = getelementptr float*, float** %14, i32 0
    %123 = load float*, float** %122
    %124 = mul i32 %121,%123
    %125 = add i32 %119,%124
    %126 = getelementptr float*, float** %11, i32 2
    %127 = load float*, float** %126
    %128 = getelementptr float*, float** %15, i32 0
    %129 = load float*, float** %128
    %130 = mul i32 %127,%129
    %131 = add i32 %125,%130
    store float* %131, float** %104
    %132 = getelementptr float*, float** %17, i32 1
    %133 = getelementptr float*, float** %11, i32 0
    %134 = load float*, float** %133
    %135 = getelementptr float*, float** %13, i32 1
    %136 = load float*, float** %135
    %137 = mul i32 %134,%136
    %138 = getelementptr float*, float** %11, i32 1
    %139 = load float*, float** %138
    %140 = getelementptr float*, float** %14, i32 1
    %141 = load float*, float** %140
    %142 = mul i32 %139,%141
    %143 = getelementptr float*, float** %11, i32 0
    %144 = load float*, float** %143
    %145 = getelementptr float*, float** %13, i32 1
    %146 = load float*, float** %145
    %147 = mul i32 %144,%146
    %148 = getelementptr float*, float** %11, i32 1
    %149 = load float*, float** %148
    %150 = getelementptr float*, float** %14, i32 1
    %151 = load float*, float** %150
    %152 = mul i32 %149,%151
    %153 = add i32 %147,%152
    %154 = getelementptr float*, float** %11, i32 2
    %155 = load float*, float** %154
    %156 = getelementptr float*, float** %15, i32 1
    %157 = load float*, float** %156
    %158 = mul i32 %155,%157
    %159 = add i32 %153,%158
    store float* %159, float** %132
    %160 = getelementptr float*, float** %17, i32 2
    %161 = getelementptr float*, float** %11, i32 0
    %162 = load float*, float** %161
    %163 = getelementptr float*, float** %13, i32 2
    %164 = load float*, float** %163
    %165 = mul i32 %162,%164
    %166 = getelementptr float*, float** %11, i32 1
    %167 = load float*, float** %166
    %168 = getelementptr float*, float** %14, i32 2
    %169 = load float*, float** %168
    %170 = mul i32 %167,%169
    %171 = getelementptr float*, float** %11, i32 0
    %172 = load float*, float** %171
    %173 = getelementptr float*, float** %13, i32 2
    %174 = load float*, float** %173
    %175 = mul i32 %172,%174
    %176 = getelementptr float*, float** %11, i32 1
    %177 = load float*, float** %176
    %178 = getelementptr float*, float** %14, i32 2
    %179 = load float*, float** %178
    %180 = mul i32 %177,%179
    %181 = add i32 %175,%180
    %182 = getelementptr float*, float** %11, i32 2
    %183 = load float*, float** %182
    %184 = getelementptr float*, float** %15, i32 2
    %185 = load float*, float** %184
    %186 = mul i32 %183,%185
    %187 = add i32 %181,%186
    store float* %187, float** %160
    %188 = getelementptr float*, float** %18, i32 0
    %189 = getelementptr float*, float** %12, i32 0
    %190 = load float*, float** %189
    %191 = getelementptr float*, float** %13, i32 0
    %192 = load float*, float** %191
    %193 = mul i32 %190,%192
    %194 = getelementptr float*, float** %12, i32 1
    %195 = load float*, float** %194
    %196 = getelementptr float*, float** %14, i32 0
    %197 = load float*, float** %196
    %198 = mul i32 %195,%197
    %199 = getelementptr float*, float** %12, i32 0
    %200 = load float*, float** %199
    %201 = getelementptr float*, float** %13, i32 0
    %202 = load float*, float** %201
    %203 = mul i32 %200,%202
    %204 = getelementptr float*, float** %12, i32 1
    %205 = load float*, float** %204
    %206 = getelementptr float*, float** %14, i32 0
    %207 = load float*, float** %206
    %208 = mul i32 %205,%207
    %209 = add i32 %203,%208
    %210 = getelementptr float*, float** %12, i32 2
    %211 = load float*, float** %210
    %212 = getelementptr float*, float** %15, i32 0
    %213 = load float*, float** %212
    %214 = mul i32 %211,%213
    %215 = add i32 %209,%214
    store float* %215, float** %188
    %216 = getelementptr float*, float** %18, i32 1
    %217 = getelementptr float*, float** %12, i32 0
    %218 = load float*, float** %217
    %219 = getelementptr float*, float** %13, i32 1
    %220 = load float*, float** %219
    %221 = mul i32 %218,%220
    %222 = getelementptr float*, float** %12, i32 1
    %223 = load float*, float** %222
    %224 = getelementptr float*, float** %14, i32 1
    %225 = load float*, float** %224
    %226 = mul i32 %223,%225
    %227 = getelementptr float*, float** %12, i32 0
    %228 = load float*, float** %227
    %229 = getelementptr float*, float** %13, i32 1
    %230 = load float*, float** %229
    %231 = mul i32 %228,%230
    %232 = getelementptr float*, float** %12, i32 1
    %233 = load float*, float** %232
    %234 = getelementptr float*, float** %14, i32 1
    %235 = load float*, float** %234
    %236 = mul i32 %233,%235
    %237 = add i32 %231,%236
    %238 = getelementptr float*, float** %12, i32 2
    %239 = load float*, float** %238
    %240 = getelementptr float*, float** %15, i32 1
    %241 = load float*, float** %240
    %242 = mul i32 %239,%241
    %243 = add i32 %237,%242
    store float* %243, float** %216
    %244 = getelementptr float*, float** %18, i32 2
    %245 = getelementptr float*, float** %12, i32 0
    %246 = load float*, float** %245
    %247 = getelementptr float*, float** %13, i32 2
    %248 = load float*, float** %247
    %249 = mul i32 %246,%248
    %250 = getelementptr float*, float** %12, i32 1
    %251 = load float*, float** %250
    %252 = getelementptr float*, float** %14, i32 2
    %253 = load float*, float** %252
    %254 = mul i32 %251,%253
    %255 = getelementptr float*, float** %12, i32 0
    %256 = load float*, float** %255
    %257 = getelementptr float*, float** %13, i32 2
    %258 = load float*, float** %257
    %259 = mul i32 %256,%258
    %260 = getelementptr float*, float** %12, i32 1
    %261 = load float*, float** %260
    %262 = getelementptr float*, float** %14, i32 2
    %263 = load float*, float** %262
    %264 = mul i32 %261,%263
    %265 = add i32 %259,%264
    %266 = getelementptr float*, float** %12, i32 2
    %267 = load float*, float** %266
    %268 = getelementptr float*, float** %15, i32 2
    %269 = load float*, float** %268
    %270 = mul i32 %267,%269
    %271 = add i32 %265,%270
    store float* %271, float** %244
    ret i32 0
}
define dso_local i32 @main(float* %0, float* %1, float* %2, float* %3, float* %4, float* %5, float* %6, float* %7, float* %8){
9:
    store i32 3, i32* @N
    store i32 3, i32* @M
    store i32 3, i32* @L
    %10 = alloca [3 x float]
    %11 = alloca [3 x float]
    %12 = alloca [3 x float]
    %13 = alloca [3 x float]
    %14 = alloca [3 x float]
    %15 = alloca [3 x float]
    %16 = alloca [6 x float]
    %17 = alloca [3 x float]
    %18 = alloca [3 x float]
    %19 = alloca i32
    store i32 0, i32* %19
    br %20

20:
    %23 = load i32, i32* %19
    %24 = load i32, i32* @M
    %25 = icmp lt i32 %23,i32 %24
    br i32 %25 %21 %22

21:
    %26 = load i32, i32* %19
    %27 = getelementptr [3 x float], [3 x float]* %10, i32 %26
    %28 = load i32, i32* %19
    %29 = itof i32 %28 to float
    store float %29, float* %27
    %30 = load i32, i32* %19
    %31 = getelementptr [3 x float], [3 x float]* %11, i32 %30
    %32 = load i32, i32* %19
    %33 = itof i32 %32 to float
    store float %33, float* %31
    %34 = load i32, i32* %19
    %35 = getelementptr [3 x float], [3 x float]* %12, i32 %34
    %36 = load i32, i32* %19
    %37 = itof i32 %36 to float
    store float %37, float* %35
    %38 = load i32, i32* %19
    %39 = getelementptr [3 x float], [3 x float]* %13, i32 %38
    %40 = load i32, i32* %19
    %41 = itof i32 %40 to float
    store float %41, float* %39
    %42 = load i32, i32* %19
    %43 = getelementptr [3 x float], [3 x float]* %14, i32 %42
    %44 = load i32, i32* %19
    %45 = itof i32 %44 to float
    store float %45, float* %43
    %46 = load i32, i32* %19
    %47 = getelementptr [3 x float], [3 x float]* %15, i32 %46
    %48 = load i32, i32* %19
    %49 = itof i32 %48 to float
    store float %49, float* %47
    %50 = load i32, i32* %19
    %51 = load i32, i32* %19
    %52 = add i32 %51,1
    store i32 %52, i32* %19
    br %20

22:
    %53 = load [3 x float], [3 x float]* %10
    %54 = load [3 x float], [3 x float]* %11
    %55 = load [3 x float], [3 x float]* %12
    %56 = load [3 x float], [3 x float]* %13
    %57 = load [3 x float], [3 x float]* %14
    %58 = load [3 x float], [3 x float]* %15
    %59 = load [6 x float], [6 x float]* %16
    %60 = load [3 x float], [3 x float]* %17
    %61 = load [3 x float], [3 x float]* %18
    %62 = call i32 @mul([3 x float] %53,[3 x float] %54,[3 x float] %55,[3 x float] %56,[3 x float] %57,[3 x float] %58,[6 x float] %59,[3 x float] %60,[3 x float] %61)
    store i32 %62, i32* %19
    %63 = alloca i32
    br %64

64:
    %67 = load i32, i32* %19
    %68 = load i32, i32* @N
    %69 = icmp lt i32 %67,i32 %68
    br i32 %69 %65 %66

65:
    %70 = load i32, i32* %19
    %71 = getelementptr [6 x float], [6 x float]* %16, i32 %70
    %72 = load float, float* %71
    %73 = ftoi float %72 to i32
    store i32 %73, i32* %63
    %74 = load i32, i32* %63
    call void @putint(i32 %74)
    %75 = load i32, i32* %19
    %76 = load i32, i32* %19
    %77 = add i32 %76,1
    store i32 %77, i32* %19
    br %64

66:
    store i32 10, i32* %63
    store i32 0, i32* %19
    %78 = load i32, i32* %63
    call void @putch(i32 %78)
    br %79

79:
    %82 = load i32, i32* %19
    %83 = load i32, i32* @N
    %84 = icmp lt i32 %82,i32 %83
    br i32 %84 %80 %81

80:
    %85 = load i32, i32* %19
    %86 = getelementptr [3 x float], [3 x float]* %17, i32 %85
    %87 = load float, float* %86
    %88 = ftoi float %87 to i32
    store i32 %88, i32* %63
    %89 = load i32, i32* %63
    call void @putint(i32 %89)
    %90 = load i32, i32* %19
    %91 = load i32, i32* %19
    %92 = add i32 %91,1
    store i32 %92, i32* %19
    br %79

81:
    store i32 10, i32* %63
    store i32 0, i32* %19
    %93 = load i32, i32* %63
    call void @putch(i32 %93)
    br %94

94:
    %97 = load i32, i32* %19
    %98 = load i32, i32* @N
    %99 = icmp lt i32 %97,i32 %98
    br i32 %99 %95 %96

95:
    %100 = load i32, i32* %19
    %101 = getelementptr [3 x float], [3 x float]* %18, i32 %100
    %102 = load float, float* %101
    %103 = ftoi float %102 to i32
    store i32 %103, i32* %63
    %104 = load i32, i32* %63
    call void @putint(i32 %104)
    %105 = load i32, i32* %19
    %106 = load i32, i32* %19
    %107 = add i32 %106,1
    store i32 %107, i32* %19
    br %94

96:
    store i32 10, i32* %63
    %108 = load i32, i32* %63
    call void @putch(i32 %108)
    ret i32 0
}

