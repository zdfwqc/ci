@k = dso_local global i32 0

@n = dso_local constant i32 10

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @main(){
0:
    %1 = alloca i32
    store i32 0, i32* %1
    store i32 1, i32* @k
    br %2

2:
    %5 = load i32, i32* %1
    %6 = load i32, i32* @n
    %7 = load i32, i32* @n
    %8 = sub i32 %7,1
    %9 = icmp le i32 %5,i32 %8
    br i32 %9 %3 %4

3:
    %10 = load i32, i32* %1
    %11 = load i32, i32* %1
    %12 = add i32 %11,1
    store i32 %12, i32* %1
    %13 = load i32, i32* @k
    %14 = load i32, i32* @k
    %15 = add i32 %14,1
    %16 = load i32, i32* @k
    %17 = load i32, i32* @k
    %18 = load i32, i32* @k
    %19 = load i32, i32* @k
    %20 = add i32 %18,%19
    store i32 %20, i32* @k
    br %2

4:
    %21 = load i32, i32* @k
    call void @putint(i32 %21)
    %22 = load i32, i32* @k
    ret i32 %22
}

