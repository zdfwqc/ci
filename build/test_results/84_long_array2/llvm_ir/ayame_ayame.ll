@a = dso_local global [4096 x i32] zeroinitializer

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @f1(i32* %0){
1:
    %2 = alloca i32*
    store i32* %0, i32** %2
    %3 = getelementptr [4096 x i32], [4096 x i32]* @a, i32 5
    store i32 4000, i32* %3
    %4 = getelementptr [4096 x i32], [4096 x i32]* @a, i32 4000
    store i32 3, i32* %4
    %5 = getelementptr [4096 x i32], [4096 x i32]* @a, i32 4095
    store i32 7, i32* %5
    %6 = getelementptr [4096 x i32], [4096 x i32]* @a, i32 4095
    %7 = load i32, i32* %6
    %8 = getelementptr i32*, i32** %2, i32 %7
    %9 = getelementptr [4096 x i32], [4096 x i32]* @a, i32 2216
    %10 = load i32, i32* %9
    %11 = getelementptr [4096 x i32], [4096 x i32]* @a, i32 2216
    %12 = load i32, i32* %11
    %13 = add i32 %12,9
    store i32 %13, i32** %8
    %14 = getelementptr [4096 x i32], [4096 x i32]* @a, i32 5
    %15 = load i32, i32* %14
    %16 = getelementptr [4096 x i32], [4096 x i32]* @a, i32 %15
    %17 = load i32, i32* %16
    ret i32 %17
}
define dso_local i32 @main(i32* %0){
1:
    %2 = alloca [4 x [1024 x i32]]
    %3 = getelementptr [4 x [1024 x i32]], [4 x [1024 x i32]]* %2, i32 1, i32 0
    store i32 1, i32* %3
    %4 = getelementptr [4 x [1024 x i32]], [4 x [1024 x i32]]* %2, i32 2, i32 0
    store i32 2, i32* %4
    %5 = getelementptr [4 x [1024 x i32]], [4 x [1024 x i32]]* %2, i32 2, i32 1
    store i32 3, i32* %5
    %6 = getelementptr [4 x [1024 x i32]], [4 x [1024 x i32]]* %2, i32 3, i32 0
    store i32 4, i32* %6
    %7 = getelementptr [4 x [1024 x i32]], [4 x [1024 x i32]]* %2, i32 3, i32 1
    store i32 5, i32* %7
    %8 = getelementptr [4 x [1024 x i32]], [4 x [1024 x i32]]* %2, i32 3, i32 2
    store i32 6, i32* %8
    %9 = alloca [1024 x [4 x i32]]
    %10 = getelementptr [1024 x [4 x i32]], [1024 x [4 x i32]]* %9, i32 0, i32 0
    store i32 1, i32* %10
    %11 = getelementptr [1024 x [4 x i32]], [1024 x [4 x i32]]* %9, i32 0, i32 1
    store i32 2, i32* %11
    %12 = getelementptr [1024 x [4 x i32]], [1024 x [4 x i32]]* %9, i32 1, i32 0
    store i32 3, i32* %12
    %13 = getelementptr [1024 x [4 x i32]], [1024 x [4 x i32]]* %9, i32 1, i32 1
    store i32 4, i32* %13
    %14 = getelementptr [1024 x [4 x i32]], [1024 x [4 x i32]]* %9, i32 0
    %15 = load [4 x i32], [4 x i32]* %14
    %16 = call i32 @f1([4 x i32] %15)
    call void @putint([4 x i32] %15,i32 %16)
    call void @putch(i32 10)
    %17 = getelementptr [1024 x [4 x i32]], [1024 x [4 x i32]]* %9, i32 2, i32 0
    %18 = load i32, i32* %17
    ret i32 %18
}

