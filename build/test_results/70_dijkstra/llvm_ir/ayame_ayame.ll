@INF = dso_local constant i32 65535

@e = dso_local global [16 x [16 x i32]] zeroinitializer

@book = dso_local global [16 x i32] zeroinitializer

@dis = dso_local global [16 x i32] zeroinitializer

@n = dso_local global i32 16

@m = dso_local global i32 16

@v1 = dso_local global i32 16

@v2 = dso_local global i32 16

@w = dso_local global i32 16

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local void @Dijkstra(){
0:
    %1 = alloca i32
    %2 = alloca i32
    store i32 1, i32* %1
    br %3

3:
    %6 = load i32, i32* %1
    %7 = load i32, i32* @n
    %8 = icmp le i32 %6,i32 %7
    br i32 %8 %4 %5

4:
    %9 = load i32, i32* %1
    %10 = getelementptr [16 x i32], [16 x i32]* @dis, i32 %9
    %11 = load i32, i32* %1
    %12 = getelementptr [16 x [16 x i32]], [16 x [16 x i32]]* @e, i32 1, i32 %11
    %13 = load i32, i32* %12
    store i32 %13, i32* %10
    %14 = load i32, i32* %1
    %15 = getelementptr [16 x i32], [16 x i32]* @book, i32 %14
    store i32 0, i32* %15
    %16 = load i32, i32* %1
    %17 = load i32, i32* %1
    %18 = add i32 %17,1
    store i32 %18, i32* %1
    br %3

5:
    %19 = getelementptr [16 x i32], [16 x i32]* @book, i32 1
    store i32 1, i32* %19
    store i32 1, i32* %1
    br %20

20:
    %23 = load i32, i32* %1
    %24 = load i32, i32* @n
    %25 = load i32, i32* @n
    %26 = sub i32 %25,1
    %27 = icmp le i32 %23,i32 %26
    br i32 %27 %21 %22

21:
    %28 = load i32, i32* @INF
    %29 = alloca i32
    store i32 %28, i32* %29
    %30 = alloca i32
    store i32 0, i32* %30
    %31 = alloca i32
    store i32 1, i32* %31
    br %32

22:

32:
    %35 = load i32, i32* %31
    %36 = load i32, i32* @n
    %37 = icmp le i32 %35,i32 %36
    br i32 %37 %33 %34

33:
    %40 = load i32, i32* %29
    %41 = load i32, i32* %31
    %42 = getelementptr [16 x i32], [16 x i32]* @dis, i32 %41
    %43 = load i32, i32* %42
    %44 = icmp gt i32 %40,i32 %43
    %45 = load i32, i32* %31
    %46 = getelementptr [16 x i32], [16 x i32]* @book, i32 %45
    %47 = load i32, i32* %46
    %48 = icmp eq i32 %47,i32 0
    %49 = and i32 %44,%48
    br i32 %49 %39 %38

34:
    %57 = load i32, i32* %30
    %58 = getelementptr [16 x i32], [16 x i32]* @book, i32 %57
    store i32 1, i32* %58
    %59 = alloca i32
    store i32 1, i32* %59
    br %60

38:
    %54 = load i32, i32* %31
    %55 = load i32, i32* %31
    %56 = add i32 %55,1
    store i32 %56, i32* %31
    br %32

39:
    %50 = load i32, i32* %31
    %51 = getelementptr [16 x i32], [16 x i32]* @dis, i32 %50
    %52 = load i32, i32* %51
    store i32 %52, i32* %29
    %53 = load i32, i32* %31
    store i32 %53, i32* %30
    br %38

60:
    %63 = load i32, i32* %59
    %64 = load i32, i32* @n
    %65 = icmp le i32 %63,i32 %64
    br i32 %65 %61 %62

61:
    %68 = load i32, i32* %30
    %69 = load i32, i32* %59
    %70 = getelementptr [16 x [16 x i32]], [16 x [16 x i32]]* @e, i32 %68, i32 %69
    %71 = load i32, i32* %70
    %72 = load i32, i32* @INF
    %73 = icmp lt i32 %71,i32 %72
    br i32 %73 %67 %66

62:
    %115 = load i32, i32* %1
    %116 = load i32, i32* %1
    %117 = add i32 %116,1
    store i32 %117, i32* %1
    br %20

66:
    %112 = load i32, i32* %59
    %113 = load i32, i32* %59
    %114 = add i32 %113,1
    store i32 %114, i32* %59
    br %60

67:
    %76 = load i32, i32* %59
    %77 = getelementptr [16 x i32], [16 x i32]* @dis, i32 %76
    %78 = load i32, i32* %77
    %79 = load i32, i32* %30
    %80 = getelementptr [16 x i32], [16 x i32]* @dis, i32 %79
    %81 = load i32, i32* %80
    %82 = load i32, i32* %30
    %83 = load i32, i32* %59
    %84 = getelementptr [16 x [16 x i32]], [16 x [16 x i32]]* @e, i32 %82, i32 %83
    %85 = load i32, i32* %84
    %86 = load i32, i32* %30
    %87 = getelementptr [16 x i32], [16 x i32]* @dis, i32 %86
    %88 = load i32, i32* %87
    %89 = load i32, i32* %30
    %90 = load i32, i32* %59
    %91 = getelementptr [16 x [16 x i32]], [16 x [16 x i32]]* @e, i32 %89, i32 %90
    %92 = load i32, i32* %91
    %93 = add i32 %88,%92
    %94 = icmp gt i32 %78,i32 %93
    br i32 %94 %75 %74

74:

75:
    %95 = load i32, i32* %59
    %96 = getelementptr [16 x i32], [16 x i32]* @dis, i32 %95
    %97 = load i32, i32* %30
    %98 = getelementptr [16 x i32], [16 x i32]* @dis, i32 %97
    %99 = load i32, i32* %98
    %100 = load i32, i32* %30
    %101 = load i32, i32* %59
    %102 = getelementptr [16 x [16 x i32]], [16 x [16 x i32]]* @e, i32 %100, i32 %101
    %103 = load i32, i32* %102
    %104 = load i32, i32* %30
    %105 = getelementptr [16 x i32], [16 x i32]* @dis, i32 %104
    %106 = load i32, i32* %105
    %107 = load i32, i32* %30
    %108 = load i32, i32* %59
    %109 = getelementptr [16 x [16 x i32]], [16 x [16 x i32]]* @e, i32 %107, i32 %108
    %110 = load i32, i32* %109
    %111 = add i32 %106,%110
    store i32 %111, i32* %96
    br %74
}
define dso_local i32 @main(){
0:
    %1 = alloca i32
    %2 = call i32 @getint()
    store i32 %2, i32* @n
    %3 = call i32 @getint()
    store i32 %3, i32* @m
    store i32 1, i32* %1
    br %4

4:
    %7 = load i32, i32* %1
    %8 = load i32, i32* @n
    %9 = icmp le i32 %7,i32 %8
    br i32 %9 %5 %6

5:
    %10 = alloca i32
    store i32 1, i32* %10
    br %11

6:
    store i32 1, i32* %1
    br %36

11:
    %14 = load i32, i32* %10
    %15 = load i32, i32* @n
    %16 = icmp le i32 %14,i32 %15
    br i32 %16 %12 %13

12:
    %20 = load i32, i32* %1
    %21 = load i32, i32* %10
    %22 = icmp eq i32 %20,i32 %21
    br i32 %22 %18 %19

13:
    %33 = load i32, i32* %1
    %34 = load i32, i32* %1
    %35 = add i32 %34,1
    store i32 %35, i32* %1
    br %4

17:
    %30 = load i32, i32* %10
    %31 = load i32, i32* %10
    %32 = add i32 %31,1
    store i32 %32, i32* %10
    br %11

18:
    %23 = load i32, i32* %1
    %24 = load i32, i32* %10
    %25 = getelementptr [16 x [16 x i32]], [16 x [16 x i32]]* @e, i32 %23, i32 %24
    store i32 0, i32* %25
    br %17

19:
    %26 = load i32, i32* %1
    %27 = load i32, i32* %10
    %28 = getelementptr [16 x [16 x i32]], [16 x [16 x i32]]* @e, i32 %26, i32 %27
    %29 = load i32, i32* @INF
    store i32 %29, i32* %28
    br %17

36:
    %39 = load i32, i32* %1
    %40 = load i32, i32* @m
    %41 = icmp le i32 %39,i32 %40
    br i32 %41 %37 %38

37:
    %42 = call i32 @getint()
    %43 = alloca i32
    store i32 %42, i32* %43
    %44 = call i32 @getint()
    %45 = alloca i32
    store i32 %44, i32* %45
    %46 = load i32, i32* %43
    %47 = load i32, i32* %45
    %48 = getelementptr [16 x [16 x i32]], [16 x [16 x i32]]* @e, i32 %46, i32 %47
    %49 = call i32 @getint()
    store i32 %49, i32* %48
    %50 = load i32, i32* %1
    %51 = load i32, i32* %1
    %52 = add i32 %51,1
    store i32 %52, i32* %1
    br %36

38:
    %53 = call void @Dijkstra()
    store i32 1, i32* %1
    br %54

54:
    %57 = load i32, i32* %1
    %58 = load i32, i32* @n
    %59 = icmp le i32 %57,i32 %58
    br i32 %59 %55 %56

55:
    %60 = load i32, i32* %1
    %61 = getelementptr [16 x i32], [16 x i32]* @dis, i32 %60
    %62 = load i32, i32* %61
    call void @putint(i32 %62)
    call void @putch(i32 32)
    %63 = load i32, i32* %1
    %64 = load i32, i32* %1
    %65 = add i32 %64,1
    store i32 %65, i32* %1
    br %54

56:
    call void @putch(i32 10)
    ret i32 0
}

