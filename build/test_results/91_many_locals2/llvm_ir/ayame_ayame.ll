@n = dso_local global i32 0

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @main(){
0:
    %1 = alloca i32
    %2 = alloca i32
    %3 = alloca i32
    %4 = alloca i32
    %5 = alloca i32
    %6 = alloca i32
    %7 = alloca i32
    %8 = alloca i32
    %9 = alloca i32
    %10 = alloca i32
    %11 = alloca i32
    %12 = alloca i32
    %13 = alloca i32
    %14 = alloca i32
    %15 = alloca i32
    %16 = alloca i32
    %17 = alloca i32
    %18 = alloca i32
    %19 = alloca i32
    %20 = alloca i32
    %21 = alloca i32
    %22 = alloca i32
    %23 = alloca i32
    %24 = alloca i32
    %25 = alloca i32
    %26 = alloca i32
    %27 = alloca i32
    %28 = alloca i32
    %29 = alloca i32
    %30 = alloca i32
    %31 = alloca i32
    %32 = call i32 @getint()
    store i32 %32, i32* %31
    br %33

33:
    %36 = load i32, i32* %31
    %37 = icmp eq i32 %36,i32 5
    br i32 %37 %34 %35

34:
    %38 = load i32, i32* %31
    %39 = load i32, i32* %31
    %40 = add i32 %39,1
    store i32 %40, i32* %31
    br %33

35:
    store i32 0, i32* %1
    %41 = load i32, i32* %1
    %42 = load i32, i32* %1
    %43 = add i32 %42,1
    store i32 %43, i32* %2
    %44 = load i32, i32* %2
    %45 = load i32, i32* %2
    %46 = add i32 %45,1
    store i32 %46, i32* %3
    %47 = load i32, i32* %3
    %48 = load i32, i32* %3
    %49 = add i32 %48,1
    store i32 %49, i32* %4
    %50 = load i32, i32* %4
    %51 = load i32, i32* %4
    %52 = add i32 %51,1
    store i32 %52, i32* %5
    %53 = load i32, i32* %5
    %54 = load i32, i32* %5
    %55 = add i32 %54,1
    store i32 %55, i32* %6
    %56 = load i32, i32* %6
    %57 = load i32, i32* %6
    %58 = add i32 %57,1
    store i32 %58, i32* %7
    %59 = load i32, i32* %7
    %60 = load i32, i32* %7
    %61 = add i32 %60,1
    store i32 %61, i32* %8
    %62 = load i32, i32* %8
    %63 = load i32, i32* %8
    %64 = add i32 %63,1
    store i32 %64, i32* %9
    %65 = load i32, i32* %9
    %66 = load i32, i32* %9
    %67 = add i32 %66,1
    store i32 %67, i32* %10
    %68 = load i32, i32* %10
    %69 = load i32, i32* %10
    %70 = add i32 %69,1
    store i32 %70, i32* %11
    %71 = load i32, i32* %11
    %72 = load i32, i32* %11
    %73 = add i32 %72,1
    store i32 %73, i32* %12
    %74 = load i32, i32* %12
    %75 = load i32, i32* %12
    %76 = add i32 %75,1
    store i32 %76, i32* %13
    %77 = load i32, i32* %13
    %78 = load i32, i32* %13
    %79 = add i32 %78,1
    store i32 %79, i32* %14
    %80 = load i32, i32* %14
    %81 = load i32, i32* %14
    %82 = add i32 %81,1
    store i32 %82, i32* %15
    %83 = load i32, i32* %15
    %84 = load i32, i32* %15
    %85 = add i32 %84,1
    store i32 %85, i32* %16
    %86 = load i32, i32* %16
    %87 = load i32, i32* %16
    %88 = add i32 %87,1
    store i32 %88, i32* %17
    %89 = load i32, i32* %17
    %90 = load i32, i32* %17
    %91 = add i32 %90,1
    store i32 %91, i32* %18
    %92 = load i32, i32* %18
    %93 = load i32, i32* %18
    %94 = add i32 %93,1
    store i32 %94, i32* %19
    %95 = load i32, i32* %19
    %96 = load i32, i32* %19
    %97 = add i32 %96,1
    store i32 %97, i32* %20
    %98 = load i32, i32* %20
    %99 = load i32, i32* %20
    %100 = add i32 %99,1
    store i32 %100, i32* %21
    %101 = load i32, i32* %21
    %102 = load i32, i32* %21
    %103 = add i32 %102,1
    store i32 %103, i32* %22
    %104 = load i32, i32* %22
    %105 = load i32, i32* %22
    %106 = add i32 %105,1
    store i32 %106, i32* %23
    %107 = load i32, i32* %23
    %108 = load i32, i32* %23
    %109 = add i32 %108,1
    store i32 %109, i32* %24
    %110 = load i32, i32* %24
    %111 = load i32, i32* %24
    %112 = add i32 %111,1
    store i32 %112, i32* %25
    %113 = load i32, i32* %25
    %114 = load i32, i32* %25
    %115 = add i32 %114,1
    store i32 %115, i32* %26
    %116 = load i32, i32* %26
    %117 = load i32, i32* %26
    %118 = add i32 %117,1
    store i32 %118, i32* %27
    %119 = load i32, i32* %27
    %120 = load i32, i32* %27
    %121 = add i32 %120,1
    store i32 %121, i32* %28
    %122 = load i32, i32* %28
    %123 = load i32, i32* %28
    %124 = add i32 %123,1
    store i32 %124, i32* %29
    %125 = load i32, i32* %29
    %126 = load i32, i32* %29
    %127 = add i32 %126,1
    store i32 %127, i32* %30
    %128 = alloca i32
    %129 = load i32, i32* %1
    call void @putint(i32 %129)
    %130 = load i32, i32* %2
    call void @putint(i32 %130)
    %131 = load i32, i32* %3
    call void @putint(i32 %131)
    %132 = load i32, i32* %4
    call void @putint(i32 %132)
    %133 = load i32, i32* %5
    call void @putint(i32 %133)
    %134 = load i32, i32* %6
    call void @putint(i32 %134)
    %135 = load i32, i32* %7
    call void @putint(i32 %135)
    %136 = load i32, i32* %8
    call void @putint(i32 %136)
    %137 = load i32, i32* %9
    call void @putint(i32 %137)
    %138 = load i32, i32* %10
    call void @putint(i32 %138)
    %139 = load i32, i32* %11
    call void @putint(i32 %139)
    %140 = load i32, i32* %12
    call void @putint(i32 %140)
    %141 = load i32, i32* %13
    call void @putint(i32 %141)
    %142 = load i32, i32* %14
    call void @putint(i32 %142)
    %143 = load i32, i32* %15
    call void @putint(i32 %143)
    %144 = load i32, i32* %16
    call void @putint(i32 %144)
    %145 = load i32, i32* %17
    call void @putint(i32 %145)
    %146 = load i32, i32* %18
    call void @putint(i32 %146)
    %147 = load i32, i32* %19
    call void @putint(i32 %147)
    %148 = load i32, i32* %20
    call void @putint(i32 %148)
    %149 = load i32, i32* %21
    call void @putint(i32 %149)
    %150 = load i32, i32* %22
    call void @putint(i32 %150)
    %151 = load i32, i32* %23
    call void @putint(i32 %151)
    %152 = load i32, i32* %24
    call void @putint(i32 %152)
    %153 = load i32, i32* %25
    call void @putint(i32 %153)
    %154 = load i32, i32* %26
    call void @putint(i32 %154)
    %155 = load i32, i32* %27
    call void @putint(i32 %155)
    %156 = load i32, i32* %28
    call void @putint(i32 %156)
    %157 = load i32, i32* %29
    call void @putint(i32 %157)
    %158 = load i32, i32* %30
    call void @putint(i32 %158)
    %159 = alloca i32
    store i32 10, i32* %159
    %160 = load i32, i32* %159
    call void @putch(i32 %160)
    %161 = load i32, i32* %31
    call void @putint(i32 %161)
    %162 = load i32, i32* %159
    call void @putch(i32 %162)
    %163 = load i32, i32* %26
    ret i32 %163
}

