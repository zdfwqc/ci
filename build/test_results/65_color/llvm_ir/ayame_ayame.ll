@maxn = dso_local constant i32 18

@mod = dso_local constant i32 1000000007

@dp = dso_local global [18 x [18 x [18 x [18 x [18 x [7 x i32]]]]]] zeroinitializer

@list = dso_local global [200 x i32] zeroinitializer

@cns = dso_local global [20 x i32] zeroinitializer

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @equal(i32 %0, i32 %1){
2:
    %3 = alloca i32
    store i32 %0, i32* %3
    %4 = alloca i32
    store i32 %1, i32* %4
    %7 = load i32, i32* %3
    %8 = load i32, i32* %4
    %9 = icmp eq i32 %7,i32 %8
    br i32 %9 %6 %5

5:
    ret i32 0

6:
    ret i32 1
}
define dso_local i32 @dfs(i32 %0, i32 %1, i32 %2, i32 %3, i32 %4, i32 %5){
6:
    %7 = alloca i32
    store i32 %0, i32* %7
    %8 = alloca i32
    store i32 %1, i32* %8
    %9 = alloca i32
    store i32 %2, i32* %9
    %10 = alloca i32
    store i32 %3, i32* %10
    %11 = alloca i32
    store i32 %4, i32* %11
    %12 = alloca i32
    store i32 %5, i32* %12
    %15 = load i32, i32* %7
    %16 = load i32, i32* %8
    %17 = load i32, i32* %9
    %18 = load i32, i32* %10
    %19 = load i32, i32* %11
    %20 = load i32, i32* %12
    %21 = getelementptr [18 x [18 x [18 x [18 x [18 x [7 x i32]]]]]], [18 x [18 x [18 x [18 x [18 x [7 x i32]]]]]]* @dp, i32 %15, i32 %16, i32 %17, i32 %18, i32 %19, i32 %20
    %22 = load i32, i32* %21
    %23 = sub i32 0,1
    %24 = icmp ne i32 %22,i32 %23
    br i32 %24 %14 %13

13:
    %35 = load i32, i32* %7
    %36 = load i32, i32* %8
    %37 = load i32, i32* %7
    %38 = load i32, i32* %8
    %39 = add i32 %37,%38
    %40 = load i32, i32* %9
    %41 = add i32 %39,%40
    %42 = load i32, i32* %10
    %43 = add i32 %41,%42
    %44 = load i32, i32* %11
    %45 = add i32 %43,%44
    %46 = icmp eq i32 %45,i32 0
    br i32 %46 %34 %33

14:
    %25 = load i32, i32* %7
    %26 = load i32, i32* %8
    %27 = load i32, i32* %9
    %28 = load i32, i32* %10
    %29 = load i32, i32* %11
    %30 = load i32, i32* %12
    %31 = getelementptr [18 x [18 x [18 x [18 x [18 x [7 x i32]]]]]], [18 x [18 x [18 x [18 x [18 x [7 x i32]]]]]]* @dp, i32 %25, i32 %26, i32 %27, i32 %28, i32 %29, i32 %30
    %32 = load i32, i32* %31
    ret i32 %32

33:
    %47 = alloca i32
    store i32 0, i32* %47
    %50 = load i32, i32* %7
    br i32 %50 %49 %48

34:
    ret i32 1

48:
    %90 = load i32, i32* %8
    br i32 %90 %89 %88

49:
    %51 = load i32, i32* %47
    %52 = load i32, i32* %7
    %53 = load i32, i32* %12
    %54 = call i32 @equal(i32 %53,i32 2)
    %55 = load i32, i32* %7
    %56 = load i32, i32* %12
    %57 = call i32 @equal(i32 %56,i32 2)
    %58 = sub i32 %55,%57
    %59 = load i32, i32* %7
    %60 = load i32, i32* %7
    %61 = sub i32 %60,1
    %62 = load i32, i32* %8
    %63 = load i32, i32* %9
    %64 = load i32, i32* %10
    %65 = load i32, i32* %11
    %66 = call i32 @dfs(i32 %61,i32 %62,i32 %63,i32 %64,i32 %65,i32 1)
    %67 = mul i32 %58,%66
    %68 = load i32, i32* %47
    %69 = load i32, i32* %7
    %70 = load i32, i32* %12
    %71 = call i32 @equal(i32 %70,i32 2)
    %72 = load i32, i32* %7
    %73 = load i32, i32* %12
    %74 = call i32 @equal(i32 %73,i32 2)
    %75 = sub i32 %72,%74
    %76 = load i32, i32* %7
    %77 = load i32, i32* %7
    %78 = sub i32 %77,1
    %79 = load i32, i32* %8
    %80 = load i32, i32* %9
    %81 = load i32, i32* %10
    %82 = load i32, i32* %11
    %83 = call i32 @dfs(i32 %78,i32 %79,i32 %80,i32 %81,i32 %82,i32 1)
    %84 = mul i32 %75,%83
    %85 = add i32 %68,%84
    %86 = load i32, i32* @mod
    %87 = srem i32 %85,%86
    store i32 %87, i32* %47
    br %48

88:
    %134 = load i32, i32* %9
    br i32 %134 %133 %132

89:
    %91 = load i32, i32* %47
    %92 = load i32, i32* %8
    %93 = load i32, i32* %12
    %94 = call i32 @equal(i32 %93,i32 3)
    %95 = load i32, i32* %8
    %96 = load i32, i32* %12
    %97 = call i32 @equal(i32 %96,i32 3)
    %98 = sub i32 %95,%97
    %99 = load i32, i32* %7
    %100 = load i32, i32* %7
    %101 = add i32 %100,1
    %102 = load i32, i32* %8
    %103 = load i32, i32* %8
    %104 = sub i32 %103,1
    %105 = load i32, i32* %9
    %106 = load i32, i32* %10
    %107 = load i32, i32* %11
    %108 = call i32 @dfs(i32 %101,i32 %104,i32 %105,i32 %106,i32 %107,i32 2)
    %109 = mul i32 %98,%108
    %110 = load i32, i32* %47
    %111 = load i32, i32* %8
    %112 = load i32, i32* %12
    %113 = call i32 @equal(i32 %112,i32 3)
    %114 = load i32, i32* %8
    %115 = load i32, i32* %12
    %116 = call i32 @equal(i32 %115,i32 3)
    %117 = sub i32 %114,%116
    %118 = load i32, i32* %7
    %119 = load i32, i32* %7
    %120 = add i32 %119,1
    %121 = load i32, i32* %8
    %122 = load i32, i32* %8
    %123 = sub i32 %122,1
    %124 = load i32, i32* %9
    %125 = load i32, i32* %10
    %126 = load i32, i32* %11
    %127 = call i32 @dfs(i32 %120,i32 %123,i32 %124,i32 %125,i32 %126,i32 2)
    %128 = mul i32 %117,%127
    %129 = add i32 %110,%128
    %130 = load i32, i32* @mod
    %131 = srem i32 %129,%130
    store i32 %131, i32* %47
    br %88

132:
    %178 = load i32, i32* %10
    br i32 %178 %177 %176

133:
    %135 = load i32, i32* %47
    %136 = load i32, i32* %9
    %137 = load i32, i32* %12
    %138 = call i32 @equal(i32 %137,i32 4)
    %139 = load i32, i32* %9
    %140 = load i32, i32* %12
    %141 = call i32 @equal(i32 %140,i32 4)
    %142 = sub i32 %139,%141
    %143 = load i32, i32* %7
    %144 = load i32, i32* %8
    %145 = load i32, i32* %8
    %146 = add i32 %145,1
    %147 = load i32, i32* %9
    %148 = load i32, i32* %9
    %149 = sub i32 %148,1
    %150 = load i32, i32* %10
    %151 = load i32, i32* %11
    %152 = call i32 @dfs(i32 %143,i32 %146,i32 %149,i32 %150,i32 %151,i32 3)
    %153 = mul i32 %142,%152
    %154 = load i32, i32* %47
    %155 = load i32, i32* %9
    %156 = load i32, i32* %12
    %157 = call i32 @equal(i32 %156,i32 4)
    %158 = load i32, i32* %9
    %159 = load i32, i32* %12
    %160 = call i32 @equal(i32 %159,i32 4)
    %161 = sub i32 %158,%160
    %162 = load i32, i32* %7
    %163 = load i32, i32* %8
    %164 = load i32, i32* %8
    %165 = add i32 %164,1
    %166 = load i32, i32* %9
    %167 = load i32, i32* %9
    %168 = sub i32 %167,1
    %169 = load i32, i32* %10
    %170 = load i32, i32* %11
    %171 = call i32 @dfs(i32 %162,i32 %165,i32 %168,i32 %169,i32 %170,i32 3)
    %172 = mul i32 %161,%171
    %173 = add i32 %154,%172
    %174 = load i32, i32* @mod
    %175 = srem i32 %173,%174
    store i32 %175, i32* %47
    br %132

176:
    %222 = load i32, i32* %11
    br i32 %222 %221 %220

177:
    %179 = load i32, i32* %47
    %180 = load i32, i32* %10
    %181 = load i32, i32* %12
    %182 = call i32 @equal(i32 %181,i32 5)
    %183 = load i32, i32* %10
    %184 = load i32, i32* %12
    %185 = call i32 @equal(i32 %184,i32 5)
    %186 = sub i32 %183,%185
    %187 = load i32, i32* %7
    %188 = load i32, i32* %8
    %189 = load i32, i32* %9
    %190 = load i32, i32* %9
    %191 = add i32 %190,1
    %192 = load i32, i32* %10
    %193 = load i32, i32* %10
    %194 = sub i32 %193,1
    %195 = load i32, i32* %11
    %196 = call i32 @dfs(i32 %187,i32 %188,i32 %191,i32 %194,i32 %195,i32 4)
    %197 = mul i32 %186,%196
    %198 = load i32, i32* %47
    %199 = load i32, i32* %10
    %200 = load i32, i32* %12
    %201 = call i32 @equal(i32 %200,i32 5)
    %202 = load i32, i32* %10
    %203 = load i32, i32* %12
    %204 = call i32 @equal(i32 %203,i32 5)
    %205 = sub i32 %202,%204
    %206 = load i32, i32* %7
    %207 = load i32, i32* %8
    %208 = load i32, i32* %9
    %209 = load i32, i32* %9
    %210 = add i32 %209,1
    %211 = load i32, i32* %10
    %212 = load i32, i32* %10
    %213 = sub i32 %212,1
    %214 = load i32, i32* %11
    %215 = call i32 @dfs(i32 %206,i32 %207,i32 %210,i32 %213,i32 %214,i32 4)
    %216 = mul i32 %205,%215
    %217 = add i32 %198,%216
    %218 = load i32, i32* @mod
    %219 = srem i32 %217,%218
    store i32 %219, i32* %47
    br %176

220:
    %252 = load i32, i32* %7
    %253 = load i32, i32* %8
    %254 = load i32, i32* %9
    %255 = load i32, i32* %10
    %256 = load i32, i32* %11
    %257 = load i32, i32* %12
    %258 = getelementptr [18 x [18 x [18 x [18 x [18 x [7 x i32]]]]]], [18 x [18 x [18 x [18 x [18 x [7 x i32]]]]]]* @dp, i32 %252, i32 %253, i32 %254, i32 %255, i32 %256, i32 %257
    %259 = load i32, i32* %47
    %260 = load i32, i32* @mod
    %261 = srem i32 %259,%260
    store i32 %261, i32* %258
    %262 = load i32, i32* %7
    %263 = load i32, i32* %8
    %264 = load i32, i32* %9
    %265 = load i32, i32* %10
    %266 = load i32, i32* %11
    %267 = load i32, i32* %12
    %268 = getelementptr [18 x [18 x [18 x [18 x [18 x [7 x i32]]]]]], [18 x [18 x [18 x [18 x [18 x [7 x i32]]]]]]* @dp, i32 %262, i32 %263, i32 %264, i32 %265, i32 %266, i32 %267
    %269 = load i32, i32* %268
    ret i32 %269

221:
    %223 = load i32, i32* %47
    %224 = load i32, i32* %11
    %225 = load i32, i32* %7
    %226 = load i32, i32* %8
    %227 = load i32, i32* %9
    %228 = load i32, i32* %10
    %229 = load i32, i32* %10
    %230 = add i32 %229,1
    %231 = load i32, i32* %11
    %232 = load i32, i32* %11
    %233 = sub i32 %232,1
    %234 = call i32 @dfs(i32 %225,i32 %226,i32 %227,i32 %230,i32 %233,i32 5)
    %235 = mul i32 %224,%234
    %236 = load i32, i32* %47
    %237 = load i32, i32* %11
    %238 = load i32, i32* %7
    %239 = load i32, i32* %8
    %240 = load i32, i32* %9
    %241 = load i32, i32* %10
    %242 = load i32, i32* %10
    %243 = add i32 %242,1
    %244 = load i32, i32* %11
    %245 = load i32, i32* %11
    %246 = sub i32 %245,1
    %247 = call i32 @dfs(i32 %238,i32 %239,i32 %240,i32 %243,i32 %246,i32 5)
    %248 = mul i32 %237,%247
    %249 = add i32 %236,%248
    %250 = load i32, i32* @mod
    %251 = srem i32 %249,%250
    store i32 %251, i32* %47
    br %220
}
define dso_local i32 @main(i32 %0, i32 %1, i32 %2, i32 %3, i32 %4, i32 %5){
6:
    %7 = call i32 @getint()
    %8 = alloca i32
    store i32 %7, i32* %8
    %9 = alloca i32
    store i32 0, i32* %9
    br %10

10:
    %13 = load i32, i32* %9
    %14 = load i32, i32* @maxn
    %15 = icmp lt i32 %13,i32 %14
    br i32 %15 %11 %12

11:
    %16 = alloca i32
    store i32 0, i32* %16
    br %17

12:
    store i32 0, i32* %9
    br %76

17:
    %20 = load i32, i32* %16
    %21 = load i32, i32* @maxn
    %22 = icmp lt i32 %20,i32 %21
    br i32 %22 %18 %19

18:
    %23 = alloca i32
    store i32 0, i32* %23
    br %24

19:
    %73 = load i32, i32* %9
    %74 = load i32, i32* %9
    %75 = add i32 %74,1
    store i32 %75, i32* %9
    br %10

24:
    %27 = load i32, i32* %23
    %28 = load i32, i32* @maxn
    %29 = icmp lt i32 %27,i32 %28
    br i32 %29 %25 %26

25:
    %30 = alloca i32
    store i32 0, i32* %30
    br %31

26:
    %70 = load i32, i32* %16
    %71 = load i32, i32* %16
    %72 = add i32 %71,1
    store i32 %72, i32* %16
    br %17

31:
    %34 = load i32, i32* %30
    %35 = load i32, i32* @maxn
    %36 = icmp lt i32 %34,i32 %35
    br i32 %36 %32 %33

32:
    %37 = alloca i32
    store i32 0, i32* %37
    br %38

33:
    %67 = load i32, i32* %23
    %68 = load i32, i32* %23
    %69 = add i32 %68,1
    store i32 %69, i32* %23
    br %24

38:
    %41 = load i32, i32* %37
    %42 = load i32, i32* @maxn
    %43 = icmp lt i32 %41,i32 %42
    br i32 %43 %39 %40

39:
    %44 = alloca i32
    store i32 0, i32* %44
    br %45

40:
    %64 = load i32, i32* %30
    %65 = load i32, i32* %30
    %66 = add i32 %65,1
    store i32 %66, i32* %30
    br %31

45:
    %48 = load i32, i32* %44
    %49 = icmp lt i32 %48,i32 7
    br i32 %49 %46 %47

46:
    %50 = load i32, i32* %9
    %51 = load i32, i32* %16
    %52 = load i32, i32* %23
    %53 = load i32, i32* %30
    %54 = load i32, i32* %37
    %55 = load i32, i32* %44
    %56 = getelementptr [18 x [18 x [18 x [18 x [18 x [7 x i32]]]]]], [18 x [18 x [18 x [18 x [18 x [7 x i32]]]]]]* @dp, i32 %50, i32 %51, i32 %52, i32 %53, i32 %54, i32 %55
    %57 = sub i32 0,1
    store i32 %57, i32* %56
    %58 = load i32, i32* %44
    %59 = load i32, i32* %44
    %60 = add i32 %59,1
    store i32 %60, i32* %44
    br %45

47:
    %61 = load i32, i32* %37
    %62 = load i32, i32* %37
    %63 = add i32 %62,1
    store i32 %63, i32* %37
    br %38

76:
    %79 = load i32, i32* %9
    %80 = load i32, i32* %8
    %81 = icmp lt i32 %79,i32 %80
    br i32 %81 %77 %78

77:
    %82 = load i32, i32* %9
    %83 = getelementptr [200 x i32], [200 x i32]* @list, i32 %82
    %84 = call i32 @getint()
    store i32 %84, i32* %83
    %85 = load i32, i32* %9
    %86 = getelementptr [200 x i32], [200 x i32]* @list, i32 %85
    %87 = load i32, i32* %86
    %88 = getelementptr [20 x i32], [20 x i32]* @cns, i32 %87
    %89 = load i32, i32* %9
    %90 = getelementptr [200 x i32], [200 x i32]* @list, i32 %89
    %91 = load i32, i32* %90
    %92 = getelementptr [20 x i32], [20 x i32]* @cns, i32 %91
    %93 = load i32, i32* %92
    %94 = load i32, i32* %9
    %95 = getelementptr [200 x i32], [200 x i32]* @list, i32 %94
    %96 = load i32, i32* %95
    %97 = getelementptr [20 x i32], [20 x i32]* @cns, i32 %96
    %98 = load i32, i32* %97
    %99 = add i32 %98,1
    store i32 %99, i32* %88
    %100 = load i32, i32* %9
    %101 = load i32, i32* %9
    %102 = add i32 %101,1
    store i32 %102, i32* %9
    br %76

78:
    %103 = getelementptr [20 x i32], [20 x i32]* @cns, i32 1
    %104 = load i32, i32* %103
    %105 = getelementptr [20 x i32], [20 x i32]* @cns, i32 2
    %106 = load i32, i32* %105
    %107 = getelementptr [20 x i32], [20 x i32]* @cns, i32 3
    %108 = load i32, i32* %107
    %109 = getelementptr [20 x i32], [20 x i32]* @cns, i32 4
    %110 = load i32, i32* %109
    %111 = getelementptr [20 x i32], [20 x i32]* @cns, i32 5
    %112 = load i32, i32* %111
    %113 = call i32 @dfs(i32 %104,i32 %106,i32 %108,i32 %110,i32 %112,i32 0)
    %114 = alloca i32
    store i32 %113, i32* %114
    %115 = load i32, i32* %114
    call void @putint(i32 %115)
    %116 = load i32, i32* %114
    ret i32 %116
}

