@n = dso_local global i32 0

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @insertsort(i32* %0){
1:
    %2 = alloca i32*
    store i32* %0, i32** %2
    %3 = alloca i32
    store i32 1, i32* %3
    br %4

4:
    %7 = load i32, i32* %3
    %8 = load i32, i32* @n
    %9 = icmp lt i32 %7,i32 %8
    br i32 %9 %5 %6

5:
    %10 = alloca i32
    %11 = load i32, i32* %3
    %12 = getelementptr i32*, i32** %2, i32 %11
    %13 = load i32*, i32** %12
    store i32* %13, i32* %10
    %14 = alloca i32
    %15 = load i32, i32* %3
    %16 = load i32, i32* %3
    %17 = sub i32 %16,1
    store i32 %17, i32* %14
    br %18

6:
    ret i32 0

18:
    %21 = load i32, i32* %14
    %22 = sub i32 0,1
    %23 = icmp gt i32 %21,i32 %22
    %24 = load i32, i32* %10
    %25 = load i32, i32* %14
    %26 = getelementptr i32*, i32** %2, i32 %25
    %27 = load i32*, i32** %26
    %28 = icmp lt i32 %24,i32* %27
    %29 = and i32 %23,%28
    br i32 %29 %19 %20

19:
    %30 = load i32, i32* %14
    %31 = load i32, i32* %14
    %32 = add i32 %31,1
    %33 = getelementptr i32*, i32** %2, i32 %32
    %34 = load i32, i32* %14
    %35 = getelementptr i32*, i32** %2, i32 %34
    %36 = load i32*, i32** %35
    store i32* %36, i32** %33
    %37 = load i32, i32* %14
    %38 = load i32, i32* %14
    %39 = sub i32 %38,1
    store i32 %39, i32* %14
    br %18

20:
    %40 = load i32, i32* %14
    %41 = load i32, i32* %14
    %42 = add i32 %41,1
    %43 = getelementptr i32*, i32** %2, i32 %42
    %44 = load i32, i32* %10
    store i32 %44, i32** %43
    %45 = load i32, i32* %3
    %46 = load i32, i32* %3
    %47 = add i32 %46,1
    store i32 %47, i32* %3
    br %4
}
define dso_local i32 @main(i32* %0){
1:
    store i32 10, i32* @n
    %2 = alloca [10 x i32]
    %3 = getelementptr [10 x i32], [10 x i32]* %2, i32 0
    store i32 4, i32* %3
    %4 = getelementptr [10 x i32], [10 x i32]* %2, i32 1
    store i32 3, i32* %4
    %5 = getelementptr [10 x i32], [10 x i32]* %2, i32 2
    store i32 9, i32* %5
    %6 = getelementptr [10 x i32], [10 x i32]* %2, i32 3
    store i32 2, i32* %6
    %7 = getelementptr [10 x i32], [10 x i32]* %2, i32 4
    store i32 0, i32* %7
    %8 = getelementptr [10 x i32], [10 x i32]* %2, i32 5
    store i32 1, i32* %8
    %9 = getelementptr [10 x i32], [10 x i32]* %2, i32 6
    store i32 6, i32* %9
    %10 = getelementptr [10 x i32], [10 x i32]* %2, i32 7
    store i32 5, i32* %10
    %11 = getelementptr [10 x i32], [10 x i32]* %2, i32 8
    store i32 7, i32* %11
    %12 = getelementptr [10 x i32], [10 x i32]* %2, i32 9
    store i32 8, i32* %12
    %13 = alloca i32
    %14 = load [10 x i32], [10 x i32]* %2
    %15 = call i32 @insertsort([10 x i32] %14)
    store i32 %15, i32* %13
    br %16

16:
    %19 = load i32, i32* %13
    %20 = load i32, i32* @n
    %21 = icmp lt i32 %19,i32 %20
    br i32 %21 %17 %18

17:
    %22 = alloca i32
    %23 = load i32, i32* %13
    %24 = getelementptr [10 x i32], [10 x i32]* %2, i32 %23
    %25 = load i32, i32* %24
    store i32 %25, i32* %22
    %26 = load i32, i32* %22
    call void @putint(i32 %26)
    store i32 10, i32* %22
    %27 = load i32, i32* %22
    call void @putch(i32 %27)
    %28 = load i32, i32* %13
    %29 = load i32, i32* %13
    %30 = add i32 %29,1
    store i32 %30, i32* %13
    br %16

18:
    ret i32 0
}

