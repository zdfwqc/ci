@TAPE_LEN = dso_local constant i32 65536

@BUFFER_LEN = dso_local constant i32 32768

@tape = dso_local global [65536 x i32] zeroinitializer

@program = dso_local global [32768 x i32] zeroinitializer

@ptr = dso_local global i32 0

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local void @read_program(){
0:
    %1 = alloca i32
    store i32 0, i32* %1
    %2 = call i32 @getint()
    %3 = alloca i32
    store i32 %2, i32* %3
    br %4

4:
    %7 = load i32, i32* %1
    %8 = load i32, i32* %3
    %9 = icmp lt i32 %7,i32 %8
    br i32 %9 %5 %6

5:
    %10 = load i32, i32* %1
    %11 = getelementptr [32768 x i32], [32768 x i32]* @program, i32 %10
    %12 = call i32 @getch()
    store i32 %12, i32* %11
    %13 = load i32, i32* %1
    %14 = load i32, i32* %1
    %15 = add i32 %14,1
    store i32 %15, i32* %1
    br %4

6:
    %16 = load i32, i32* %1
    %17 = getelementptr [32768 x i32], [32768 x i32]* @program, i32 %16
    store i32 0, i32* %17
}
define dso_local void @interpret(i32* %0){
1:
    %2 = alloca i32*
    store i32* %0, i32** %2
    %3 = alloca i32
    %4 = alloca i32
    %5 = alloca i32
    store i32 0, i32* %5
    br %6

6:
    %9 = load i32, i32* %5
    %10 = getelementptr i32*, i32** %2, i32 %9
    %11 = load i32*, i32** %10
    br i32* %11 %7 %8

7:
    %12 = load i32, i32* %5
    %13 = getelementptr i32*, i32** %2, i32 %12
    %14 = load i32*, i32** %13
    store i32* %14, i32* %3
    %18 = load i32, i32* %3
    %19 = icmp eq i32 %18,i32 62
    br i32 %19 %16 %17

8:

15:
    %109 = load i32, i32* %5
    %110 = load i32, i32* %5
    %111 = add i32 %110,1
    store i32 %111, i32* %5
    br %6

16:
    %20 = load i32, i32* @ptr
    %21 = load i32, i32* @ptr
    %22 = add i32 %21,1
    store i32 %22, i32* @ptr
    br %15

17:
    %26 = load i32, i32* %3
    %27 = icmp eq i32 %26,i32 60
    br i32 %27 %24 %25

23:

24:
    %28 = load i32, i32* @ptr
    %29 = load i32, i32* @ptr
    %30 = sub i32 %29,1
    store i32 %30, i32* @ptr
    br %23

25:
    %34 = load i32, i32* %3
    %35 = icmp eq i32 %34,i32 43
    br i32 %35 %32 %33

31:

32:
    %36 = load i32, i32* @ptr
    %37 = getelementptr [65536 x i32], [65536 x i32]* @tape, i32 %36
    %38 = load i32, i32* @ptr
    %39 = getelementptr [65536 x i32], [65536 x i32]* @tape, i32 %38
    %40 = load i32, i32* %39
    %41 = load i32, i32* @ptr
    %42 = getelementptr [65536 x i32], [65536 x i32]* @tape, i32 %41
    %43 = load i32, i32* %42
    %44 = add i32 %43,1
    store i32 %44, i32* %37
    br %31

33:
    %48 = load i32, i32* %3
    %49 = icmp eq i32 %48,i32 45
    br i32 %49 %46 %47

45:

46:
    %50 = load i32, i32* @ptr
    %51 = getelementptr [65536 x i32], [65536 x i32]* @tape, i32 %50
    %52 = load i32, i32* @ptr
    %53 = getelementptr [65536 x i32], [65536 x i32]* @tape, i32 %52
    %54 = load i32, i32* %53
    %55 = load i32, i32* @ptr
    %56 = getelementptr [65536 x i32], [65536 x i32]* @tape, i32 %55
    %57 = load i32, i32* %56
    %58 = sub i32 %57,1
    store i32 %58, i32* %51
    br %45

47:
    %62 = load i32, i32* %3
    %63 = icmp eq i32 %62,i32 46
    br i32 %63 %60 %61

59:

60:
    %64 = load i32, i32* @ptr
    %65 = getelementptr [65536 x i32], [65536 x i32]* @tape, i32 %64
    %66 = load i32, i32* %65
    call void @putch(i32 %66)
    br %59

61:
    %70 = load i32, i32* %3
    %71 = icmp eq i32 %70,i32 44
    br i32 %71 %68 %69

67:

68:
    %72 = load i32, i32* @ptr
    %73 = getelementptr [65536 x i32], [65536 x i32]* @tape, i32 %72
    %74 = call i32 @getch()
    store i32 %74, i32* %73
    br %67

69:
    %77 = load i32, i32* %3
    %78 = icmp eq i32 %77,i32 93
    %79 = load i32, i32* @ptr
    %80 = getelementptr [65536 x i32], [65536 x i32]* @tape, i32 %79
    %81 = load i32, i32* %80
    %82 = and i32 %78,%81
    br i32 %82 %76 %75

75:

76:
    store i32 1, i32* %4
    br %83

83:
    %86 = load i32, i32* %4
    %87 = icmp gt i32 %86,i32 0
    br i32 %87 %84 %85

84:
    %88 = load i32, i32* %5
    %89 = load i32, i32* %5
    %90 = sub i32 %89,1
    store i32 %90, i32* %5
    %91 = load i32, i32* %5
    %92 = getelementptr i32*, i32** %2, i32 %91
    %93 = load i32*, i32** %92
    store i32* %93, i32* %3
    %97 = load i32, i32* %3
    %98 = icmp eq i32 %97,i32 91
    br i32 %98 %95 %96

85:

94:
    br %83

95:
    %99 = load i32, i32* %4
    %100 = load i32, i32* %4
    %101 = sub i32 %100,1
    store i32 %101, i32* %4
    br %94

96:
    %104 = load i32, i32* %3
    %105 = icmp eq i32 %104,i32 93
    br i32 %105 %103 %102

102:

103:
    %106 = load i32, i32* %4
    %107 = load i32, i32* %4
    %108 = add i32 %107,1
    store i32 %108, i32* %4
    br %102
}
define dso_local i32 @main(i32* %0){
1:
    %2 = call void @read_program()
    %3 = load [32768 x i32], [32768 x i32]* @program
    %4 = call void @interpret([32768 x i32] %3)
    ret i32 0
}

