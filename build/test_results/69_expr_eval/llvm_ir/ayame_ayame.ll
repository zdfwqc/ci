@TOKEN_NUM = dso_local constant i32 0

@TOKEN_OTHER = dso_local constant i32 1

@last_char = dso_local global i32 32

@num = dso_local global i32 32

@other = dso_local global i32 32

@cur_token = dso_local global i32 32

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @next_char(){
0:
    %1 = call i32 @getch()
    store i32 %1, i32* @last_char
    %2 = load i32, i32* @last_char
    ret i32 %2
}
define dso_local i32 @is_space(i32 %0){
1:
    %2 = alloca i32
    store i32 %0, i32* %2
    %6 = load i32, i32* %2
    %7 = icmp eq i32 %6,i32 32
    %8 = load i32, i32* %2
    %9 = icmp eq i32 %8,i32 10
    %10 = or i32 %7,%9
    br i32 %10 %4 %5

3:

4:
    ret i32 1

5:
    ret i32 0
}
define dso_local i32 @is_num(i32 %0){
1:
    %2 = alloca i32
    store i32 %0, i32* %2
    %6 = load i32, i32* %2
    %7 = icmp ge i32 %6,i32 48
    %8 = load i32, i32* %2
    %9 = icmp le i32 %8,i32 57
    %10 = and i32 %7,%9
    br i32 %10 %4 %5

3:

4:
    ret i32 1

5:
    ret i32 0
}
define dso_local i32 @next_token(i32 %0){
1:
    br %2

2:
    %5 = load i32, i32* @last_char
    %6 = call i32 @is_space(i32 %5)
    br i32 %6 %3 %4

3:
    %7 = call i32 @next_char()
    br %2

4:
    %11 = load i32, i32* @last_char
    %12 = call i32 @is_num(i32 %11)
    br i32 %12 %9 %10

8:
    %33 = load i32, i32* @cur_token
    ret i32 %33

9:
    %13 = load i32, i32* @last_char
    %14 = load i32, i32* @last_char
    %15 = sub i32 %14,48
    store i32 %15, i32* @num
    br %16

10:
    %30 = load i32, i32* @last_char
    store i32 %30, i32* @other
    %31 = call i32 @next_char()
    %32 = load i32, i32* @TOKEN_OTHER
    store i32 %32, i32* @cur_token
    br %8

16:
    %19 = call i32 @next_char()
    %20 = call i32 @is_num(i32 %19)
    br i32 %20 %17 %18

17:
    %21 = load i32, i32* @num
    %22 = mul i32 %21,10
    %23 = load i32, i32* @last_char
    %24 = load i32, i32* @num
    %25 = mul i32 %24,10
    %26 = load i32, i32* @last_char
    %27 = add i32 %25,%26
    %28 = sub i32 %27,48
    store i32 %28, i32* @num
    br %16

18:
    %29 = load i32, i32* @TOKEN_NUM
    store i32 %29, i32* @cur_token
}
define dso_local i32 @panic(i32 %0){
1:
    call void @putch(i32 112)
    call void @putch(i32 97)
    call void @putch(i32 110)
    call void @putch(i32 105)
    call void @putch(i32 99)
    call void @putch(i32 33)
    call void @putch(i32 10)
    %2 = sub i32 0,1
    ret i32 %2
}
define dso_local i32 @get_op_prec(i32 %0){
1:
    %2 = alloca i32
    store i32 %0, i32* %2
    %5 = load i32, i32* %2
    %6 = icmp eq i32 %5,i32 43
    %7 = load i32, i32* %2
    %8 = icmp eq i32 %7,i32 45
    %9 = or i32 %6,%8
    br i32 %9 %4 %3

3:
    %12 = load i32, i32* %2
    %13 = icmp eq i32 %12,i32 42
    %14 = load i32, i32* %2
    %15 = icmp eq i32 %14,i32 47
    %16 = or i32 %13,%15
    %17 = load i32, i32* %2
    %18 = icmp eq i32 %17,i32 37
    %19 = or i32 %16,%18
    br i32 %19 %11 %10

4:
    ret i32 10

10:
    ret i32 0

11:
    ret i32 20
}
define dso_local void @stack_push(i32* %0, i32 %1){
2:
    %3 = alloca i32*
    store i32* %0, i32** %3
    %4 = alloca i32
    store i32 %1, i32* %4
    %5 = getelementptr i32*, i32** %3, i32 0
    %6 = getelementptr i32*, i32** %3, i32 0
    %7 = load i32*, i32** %6
    %8 = getelementptr i32*, i32** %3, i32 0
    %9 = load i32*, i32** %8
    %10 = add i32 %9,1
    store i32* %10, i32** %5
    %11 = getelementptr i32*, i32** %3, i32 0
    %12 = load i32*, i32** %11
    %13 = getelementptr i32*, i32** %3, i32* %12
    %14 = load i32, i32* %4
    store i32 %14, i32** %13
}
define dso_local i32 @stack_pop(i32* %0){
1:
    %2 = alloca i32*
    store i32* %0, i32** %2
    %3 = getelementptr i32*, i32** %2, i32 0
    %4 = load i32*, i32** %3
    %5 = getelementptr i32*, i32** %2, i32* %4
    %6 = load i32*, i32** %5
    %7 = alloca i32
    store i32* %6, i32* %7
    %8 = getelementptr i32*, i32** %2, i32 0
    %9 = getelementptr i32*, i32** %2, i32 0
    %10 = load i32*, i32** %9
    %11 = getelementptr i32*, i32** %2, i32 0
    %12 = load i32*, i32** %11
    %13 = sub i32 %12,1
    store i32* %13, i32** %8
    %14 = load i32, i32* %7
    ret i32 %14
}
define dso_local i32 @stack_peek(i32* %0){
1:
    %2 = alloca i32*
    store i32* %0, i32** %2
    %3 = getelementptr i32*, i32** %2, i32 0
    %4 = load i32*, i32** %3
    %5 = getelementptr i32*, i32** %2, i32* %4
    %6 = load i32*, i32** %5
    ret i32* %6
}
define dso_local i32 @stack_size(i32* %0){
1:
    %2 = alloca i32*
    store i32* %0, i32** %2
    %3 = getelementptr i32*, i32** %2, i32 0
    %4 = load i32*, i32** %3
    ret i32* %4
}
define dso_local i32 @eval_op(i32 %0, i32 %1, i32 %2){
3:
    %4 = alloca i32
    store i32 %0, i32* %4
    %5 = alloca i32
    store i32 %1, i32* %5
    %6 = alloca i32
    store i32 %2, i32* %6
    %9 = load i32, i32* %4
    %10 = icmp eq i32 %9,i32 43
    br i32 %10 %8 %7

7:
    %18 = load i32, i32* %4
    %19 = icmp eq i32 %18,i32 45
    br i32 %19 %17 %16

8:
    %11 = load i32, i32* %5
    %12 = load i32, i32* %6
    %13 = load i32, i32* %5
    %14 = load i32, i32* %6
    %15 = add i32 %13,%14
    ret i32 %15

16:
    %27 = load i32, i32* %4
    %28 = icmp eq i32 %27,i32 42
    br i32 %28 %26 %25

17:
    %20 = load i32, i32* %5
    %21 = load i32, i32* %6
    %22 = load i32, i32* %5
    %23 = load i32, i32* %6
    %24 = sub i32 %22,%23
    ret i32 %24

25:
    %34 = load i32, i32* %4
    %35 = icmp eq i32 %34,i32 47
    br i32 %35 %33 %32

26:
    %29 = load i32, i32* %5
    %30 = load i32, i32* %6
    %31 = mul i32 %29,%30
    ret i32 %31

32:
    %41 = load i32, i32* %4
    %42 = icmp eq i32 %41,i32 37
    br i32 %42 %40 %39

33:
    %36 = load i32, i32* %5
    %37 = load i32, i32* %6
    %38 = sdiv i32 %36,%37
    ret i32 %38

39:
    ret i32 0

40:
    %43 = load i32, i32* %5
    %44 = load i32, i32* %6
    %45 = srem i32 %43,%44
    ret i32 %45
}
define dso_local i32 @eval(i32 %0, i32 %1, i32 %2){
3:
    %4 = alloca [256 x i32]
    %5 = alloca [256 x i32]
    %8 = load i32, i32* @cur_token
    %9 = load i32, i32* @TOKEN_NUM
    %10 = icmp ne i32 %8,i32 %9
    br i32 %10 %7 %6

6:
    %12 = load [256 x i32], [256 x i32]* %4
    %13 = load i32, i32* @num
    %14 = call void @stack_push([256 x i32] %12,i32 %13)
    %15 = call i32 @next_token()
    br %16

7:
    %11 = call i32 @panic()
    ret i32 %11

16:
    %19 = load i32, i32* @cur_token
    %20 = load i32, i32* @TOKEN_OTHER
    %21 = icmp eq i32 %19,i32 %20
    br i32 %21 %17 %18

17:
    %22 = load i32, i32* @other
    %23 = alloca i32
    store i32 %22, i32* %23
    %26 = load i32, i32* %23
    %27 = call i32 @get_op_prec(i32 %26)
    
    br void %28 %25 %24

18:
    %70 = call i32 @next_token()
    br %71

24:
    %29 = call i32 @next_token()
    br %30

25:
    br %18

30:
    %33 = load [256 x i32], [256 x i32]* %5
    %34 = call i32 @stack_size([256 x i32] %33)
    %35 = load [256 x i32], [256 x i32]* %5
    %36 = call i32 @stack_peek([256 x i32] %35)
    %37 = call i32 @get_op_prec([256 x i32] %35,i32 %36)
    %38 = load i32, i32* %23
    %39 = call i32 @get_op_prec(i32 %38)
    %40 = icmp ge i32 %37,i32 %39
    %41 = and i32 %34,%40
    br i32 %41 %31 %32

31:
    %42 = load [256 x i32], [256 x i32]* %5
    %43 = call i32 @stack_pop([256 x i32] %42)
    %44 = alloca i32
    store i32 %43, i32* %44
    %45 = load [256 x i32], [256 x i32]* %4
    %46 = call i32 @stack_pop([256 x i32] %45)
    %47 = alloca i32
    store i32 %46, i32* %47
    %48 = load [256 x i32], [256 x i32]* %4
    %49 = call i32 @stack_pop([256 x i32] %48)
    %50 = alloca i32
    store i32 %49, i32* %50
    %51 = load [256 x i32], [256 x i32]* %4
    %52 = load i32, i32* %44
    %53 = load i32, i32* %50
    %54 = load i32, i32* %47
    %55 = call i32 @eval_op(i32 %52,i32 %53,i32 %54)
    %56 = call void @stack_push(i32 %52,i32 %53,i32 %54,i32 %55)
    br %30

32:
    %57 = load [256 x i32], [256 x i32]* %5
    %58 = load i32, i32* %23
    %59 = call void @stack_push([256 x i32] %57,i32 %58)
    %62 = load i32, i32* @cur_token
    %63 = load i32, i32* @TOKEN_NUM
    %64 = icmp ne i32 %62,i32 %63
    br i32 %64 %61 %60

60:
    %66 = load [256 x i32], [256 x i32]* %4
    %67 = load i32, i32* @num
    %68 = call void @stack_push([256 x i32] %66,i32 %67)
    %69 = call i32 @next_token()
    br %16

61:
    %65 = call i32 @panic()
    ret i32 %65

71:
    %74 = load [256 x i32], [256 x i32]* %5
    %75 = call i32 @stack_size([256 x i32] %74)
    br i32 %75 %72 %73

72:
    %76 = load [256 x i32], [256 x i32]* %5
    %77 = call i32 @stack_pop([256 x i32] %76)
    %78 = alloca i32
    store i32 %77, i32* %78
    %79 = load [256 x i32], [256 x i32]* %4
    %80 = call i32 @stack_pop([256 x i32] %79)
    %81 = alloca i32
    store i32 %80, i32* %81
    %82 = load [256 x i32], [256 x i32]* %4
    %83 = call i32 @stack_pop([256 x i32] %82)
    %84 = alloca i32
    store i32 %83, i32* %84
    %85 = load [256 x i32], [256 x i32]* %4
    %86 = load i32, i32* %78
    %87 = load i32, i32* %84
    %88 = load i32, i32* %81
    %89 = call i32 @eval_op(i32 %86,i32 %87,i32 %88)
    %90 = call void @stack_push(i32 %86,i32 %87,i32 %88,i32 %89)
    br %71

73:
    %91 = load [256 x i32], [256 x i32]* %4
    %92 = call i32 @stack_peek([256 x i32] %91)
    ret i32 %92
}
define dso_local i32 @main(i32 %0, i32 %1, i32 %2){
3:
    %4 = call i32 @getint()
    %5 = alloca i32
    store i32 %4, i32* %5
    %6 = call i32 @getch()
    %7 = call i32 @next_token()
    br %8

8:
    %11 = load i32, i32* %5
    br i32 %11 %9 %10

9:
    %12 = call i32 @eval()
    call void @putint(i32 %12)
    call void @putch(i32 10)
    %13 = load i32, i32* %5
    %14 = load i32, i32* %5
    %15 = sub i32 %14,1
    store i32 %15, i32* %5
    br %8

10:
    ret i32 0
}

