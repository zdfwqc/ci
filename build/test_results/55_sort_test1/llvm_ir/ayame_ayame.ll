@n = dso_local global i32 0

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @bubblesort(i32* %0){
1:
    %2 = alloca i32*
    store i32* %0, i32** %2
    %3 = alloca i32
    %4 = alloca i32
    store i32 0, i32* %3
    br %5

5:
    %8 = load i32, i32* %3
    %9 = load i32, i32* @n
    %10 = load i32, i32* @n
    %11 = sub i32 %10,1
    %12 = icmp lt i32 %8,i32 %11
    br i32 %12 %6 %7

6:
    store i32 0, i32* %4
    br %13

7:
    ret i32 0

13:
    %16 = load i32, i32* %4
    %17 = load i32, i32* @n
    %18 = load i32, i32* %3
    %19 = load i32, i32* @n
    %20 = load i32, i32* %3
    %21 = sub i32 %19,%20
    %22 = sub i32 %21,1
    %23 = icmp lt i32 %16,i32 %22
    br i32 %23 %14 %15

14:
    %26 = load i32, i32* %4
    %27 = getelementptr i32*, i32** %2, i32 %26
    %28 = load i32*, i32** %27
    %29 = load i32, i32* %4
    %30 = load i32, i32* %4
    %31 = add i32 %30,1
    %32 = getelementptr i32*, i32** %2, i32 %31
    %33 = load i32*, i32** %32
    %34 = icmp gt i32* %28,i32* %33
    br i32* %34 %25 %24

15:
    %54 = load i32, i32* %3
    %55 = load i32, i32* %3
    %56 = add i32 %55,1
    store i32 %56, i32* %3
    br %5

24:
    %51 = load i32, i32* %4
    %52 = load i32, i32* %4
    %53 = add i32 %52,1
    store i32 %53, i32* %4
    br %13

25:
    %35 = alloca i32
    %36 = load i32, i32* %4
    %37 = load i32, i32* %4
    %38 = add i32 %37,1
    %39 = getelementptr i32*, i32** %2, i32 %38
    %40 = load i32*, i32** %39
    store i32* %40, i32* %35
    %41 = load i32, i32* %4
    %42 = load i32, i32* %4
    %43 = add i32 %42,1
    %44 = getelementptr i32*, i32** %2, i32 %43
    %45 = load i32, i32* %4
    %46 = getelementptr i32*, i32** %2, i32 %45
    %47 = load i32*, i32** %46
    store i32* %47, i32** %44
    %48 = load i32, i32* %4
    %49 = getelementptr i32*, i32** %2, i32 %48
    %50 = load i32, i32* %35
    store i32 %50, i32** %49
    br %24
}
define dso_local i32 @main(i32* %0){
1:
    store i32 10, i32* @n
    %2 = alloca [10 x i32]
    %3 = getelementptr [10 x i32], [10 x i32]* %2, i32 0
    store i32 4, i32* %3
    %4 = getelementptr [10 x i32], [10 x i32]* %2, i32 1
    store i32 3, i32* %4
    %5 = getelementptr [10 x i32], [10 x i32]* %2, i32 2
    store i32 9, i32* %5
    %6 = getelementptr [10 x i32], [10 x i32]* %2, i32 3
    store i32 2, i32* %6
    %7 = getelementptr [10 x i32], [10 x i32]* %2, i32 4
    store i32 0, i32* %7
    %8 = getelementptr [10 x i32], [10 x i32]* %2, i32 5
    store i32 1, i32* %8
    %9 = getelementptr [10 x i32], [10 x i32]* %2, i32 6
    store i32 6, i32* %9
    %10 = getelementptr [10 x i32], [10 x i32]* %2, i32 7
    store i32 5, i32* %10
    %11 = getelementptr [10 x i32], [10 x i32]* %2, i32 8
    store i32 7, i32* %11
    %12 = getelementptr [10 x i32], [10 x i32]* %2, i32 9
    store i32 8, i32* %12
    %13 = alloca i32
    %14 = load [10 x i32], [10 x i32]* %2
    %15 = call i32 @bubblesort([10 x i32] %14)
    store i32 %15, i32* %13
    br %16

16:
    %19 = load i32, i32* %13
    %20 = load i32, i32* @n
    %21 = icmp lt i32 %19,i32 %20
    br i32 %21 %17 %18

17:
    %22 = alloca i32
    %23 = load i32, i32* %13
    %24 = getelementptr [10 x i32], [10 x i32]* %2, i32 %23
    %25 = load i32, i32* %24
    store i32 %25, i32* %22
    %26 = load i32, i32* %22
    call void @putint(i32 %26)
    store i32 10, i32* %22
    %27 = load i32, i32* %22
    call void @putch(i32 %27)
    %28 = load i32, i32* %13
    %29 = load i32, i32* %13
    %30 = add i32 %29,1
    store i32 %30, i32* %13
    br %16

18:
    ret i32 0
}

