@a = dso_local global i32 -1

@b = dso_local global i32 1

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @inc_a(){
0:
    %1 = load i32, i32* @a
    %2 = alloca i32
    store i32 %1, i32* %2
    %3 = load i32, i32* %2
    %4 = load i32, i32* %2
    %5 = add i32 %4,1
    store i32 %5, i32* %2
    %6 = load i32, i32* %2
    store i32 %6, i32* @a
    %7 = load i32, i32* @a
    ret i32 %7
}
define dso_local i32 @main(){
0:
    %1 = alloca i32
    store i32 5, i32* %1
    br %2

2:
    %5 = load i32, i32* %1
    %6 = icmp ge i32 %5,i32 0
    br i32 %6 %3 %4

3:
    %9 = call i32 @inc_a()
    %10 = call i32 @inc_a()
    %11 = and i32 %9,%10
    %12 = call i32 @inc_a()
    %13 = and i32 %11,%12
    br i32 %13 %8 %7

4:
    %37 = load i32, i32* @a
    call void @putint(i32 %37)
    call void @putch(i32 32)
    %38 = load i32, i32* @b
    call void @putint(i32 %38)
    call void @putch(i32 10)
    %39 = load i32, i32* @a
    ret i32 %39

7:
    %19 = call i32 @inc_a()
    %20 = icmp lt i32 %19,i32 14
    %21 = call i32 @inc_a()
    %22 = call i32 @inc_a()
    %23 = call i32 @inc_a()
    %24 = call i32 @inc_a()
    %25 = call i32 @inc_a()
    %26 = sub i32 %24,%25
    %27 = add i32 %26,1
    %28 = and i32 %21,%27
    %29 = or i32 %20,%28
    br i32 %29 %17 %18

8:
    %14 = load i32, i32* @a
    call void @putint(i32 %14)
    call void @putch(i32 32)
    %15 = load i32, i32* @b
    call void @putint(i32 %15)
    call void @putch(i32 10)
    br %7

16:
    %34 = load i32, i32* %1
    %35 = load i32, i32* %1
    %36 = sub i32 %35,1
    store i32 %36, i32* %1
    br %2

17:
    %30 = load i32, i32* @a
    call void @putint(i32 %30)
    call void @putch(i32 10)
    %31 = load i32, i32* @b
    %32 = mul i32 %31,2
    store i32 %32, i32* @b
    br %16

18:
    %33 = call i32 @inc_a()
    br %16
}

