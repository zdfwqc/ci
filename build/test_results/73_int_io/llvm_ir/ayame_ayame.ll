@ascii_0 = dso_local constant i32 48

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @my_getint(){
0:
    %1 = alloca i32
    store i32 0, i32* %1
    %2 = alloca i32
    br %3

3:
    br i32 1 %4 %5

4:
    %6 = call i32 @getch()
    %7 = load i32, i32* @ascii_0
    %8 = call i32 @getch()
    %9 = load i32, i32* @ascii_0
    %10 = sub i32 %8,%9
    store i32 %10, i32* %2
    %14 = load i32, i32* %2
    %15 = icmp lt i32 %14,i32 0
    %16 = load i32, i32* %2
    %17 = icmp gt i32 %16,i32 9
    %18 = or i32 %15,%17
    br i32 %18 %12 %13

5:
    %19 = load i32, i32* %2
    store i32 %19, i32* %1
    br %20

11:
    br %3

12:
    br %3

13:
    br %5

20:
    br i32 1 %21 %22

21:
    %23 = call i32 @getch()
    %24 = load i32, i32* @ascii_0
    %25 = call i32 @getch()
    %26 = load i32, i32* @ascii_0
    %27 = sub i32 %25,%26
    store i32 %27, i32* %2
    %31 = load i32, i32* %2
    %32 = icmp ge i32 %31,i32 0
    %33 = load i32, i32* %2
    %34 = icmp le i32 %33,i32 9
    %35 = and i32 %32,%34
    br i32 %35 %29 %30

22:
    %43 = load i32, i32* %1
    ret i32 %43

28:
    br %20

29:
    %36 = load i32, i32* %1
    %37 = mul i32 %36,10
    %38 = load i32, i32* %2
    %39 = load i32, i32* %1
    %40 = mul i32 %39,10
    %41 = load i32, i32* %2
    %42 = add i32 %40,%41
    store i32 %42, i32* %1
    br %28

30:
    br %22
}
define dso_local void @my_putint(i32 %0){
1:
    %2 = alloca i32
    store i32 %0, i32* %2
    %3 = alloca [16 x i32]
    %4 = alloca i32
    store i32 0, i32* %4
    br %5

5:
    %8 = load i32, i32* %2
    %9 = icmp gt i32 %8,i32 0
    br i32 %9 %6 %7

6:
    %10 = load i32, i32* %4
    %11 = getelementptr [16 x i32], [16 x i32]* %3, i32 %10
    %12 = load i32, i32* %2
    %13 = srem i32 %12,10
    %14 = load i32, i32* @ascii_0
    %15 = load i32, i32* %2
    %16 = srem i32 %15,10
    %17 = load i32, i32* @ascii_0
    %18 = add i32 %16,%17
    store i32 %18, i32* %11
    %19 = load i32, i32* %2
    %20 = sdiv i32 %19,10
    store i32 %20, i32* %2
    %21 = load i32, i32* %4
    %22 = load i32, i32* %4
    %23 = add i32 %22,1
    store i32 %23, i32* %4
    br %5

7:
    br %24

24:
    %27 = load i32, i32* %4
    %28 = icmp gt i32 %27,i32 0
    br i32 %28 %25 %26

25:
    %29 = load i32, i32* %4
    %30 = load i32, i32* %4
    %31 = sub i32 %30,1
    store i32 %31, i32* %4
    %32 = load i32, i32* %4
    %33 = getelementptr [16 x i32], [16 x i32]* %3, i32 %32
    %34 = load i32, i32* %33
    call void @putch(i32 %34)
    br %24

26:
}
define dso_local i32 @main(i32 %0){
1:
    %2 = call i32 @my_getint()
    %3 = alloca i32
    store i32 %2, i32* %3
    br %4

4:
    %7 = load i32, i32* %3
    %8 = icmp gt i32 %7,i32 0
    br i32 %8 %5 %6

5:
    %9 = call i32 @my_getint()
    %10 = alloca i32
    store i32 %9, i32* %10
    %11 = load i32, i32* %10
    %12 = call void @my_putint(i32 %11)
    call void @putch(i32 10)
    %13 = load i32, i32* %3
    %14 = load i32, i32* %3
    %15 = sub i32 %14,1
    store i32 %15, i32* %3
    br %4

6:
    ret i32 0
}

