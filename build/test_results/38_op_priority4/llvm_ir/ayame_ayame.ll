@a = dso_local global i32 0

@b = dso_local global i32 0

@c = dso_local global i32 0

@d = dso_local global i32 0

@e = dso_local global i32 0

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @main(){
0:
    %1 = call i32 @getint()
    store i32 %1, i32* @a
    %2 = call i32 @getint()
    store i32 %2, i32* @b
    %3 = call i32 @getint()
    store i32 %3, i32* @c
    %4 = call i32 @getint()
    store i32 %4, i32* @d
    %5 = call i32 @getint()
    store i32 %5, i32* @e
    %6 = alloca i32
    store i32 0, i32* %6
    %9 = load i32, i32* @a
    %10 = load i32, i32* @b
    %11 = load i32, i32* @c
    %12 = mul i32 %10,%11
    %13 = load i32, i32* @a
    %14 = load i32, i32* @b
    %15 = load i32, i32* @c
    %16 = mul i32 %14,%15
    %17 = sub i32 %13,%16
    %18 = load i32, i32* @d
    %19 = load i32, i32* @a
    %20 = load i32, i32* @c
    %21 = sdiv i32 %19,%20
    %22 = load i32, i32* @d
    %23 = load i32, i32* @a
    %24 = load i32, i32* @c
    %25 = sdiv i32 %23,%24
    %26 = sub i32 %22,%25
    %27 = icmp ne i32 %17,i32 %26
    %28 = load i32, i32* @a
    %29 = load i32, i32* @b
    %30 = mul i32 %28,%29
    %31 = load i32, i32* @c
    %32 = sdiv i32 %30,%31
    %33 = load i32, i32* @e
    %34 = load i32, i32* @d
    %35 = load i32, i32* @e
    %36 = load i32, i32* @d
    %37 = add i32 %35,%36
    %38 = icmp eq i32 %32,i32 %37
    %39 = or i32 %27,%38
    %40 = load i32, i32* @a
    %41 = load i32, i32* @b
    %42 = load i32, i32* @a
    %43 = load i32, i32* @b
    %44 = add i32 %42,%43
    %45 = load i32, i32* @c
    %46 = add i32 %44,%45
    %47 = load i32, i32* @d
    %48 = load i32, i32* @e
    %49 = load i32, i32* @d
    %50 = load i32, i32* @e
    %51 = add i32 %49,%50
    %52 = icmp eq i32 %46,i32 %51
    %53 = or i32 %39,%52
    br i32 %53 %8 %7

7:
    %54 = load i32, i32* %6
    ret i32 %54

8:
    store i32 1, i32* %6
    br %7
}

