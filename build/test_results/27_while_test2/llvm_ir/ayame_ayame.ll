declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @FourWhile(){
0:
    %1 = alloca i32
    store i32 5, i32* %1
    %2 = alloca i32
    %3 = alloca i32
    store i32 6, i32* %2
    store i32 7, i32* %3
    %4 = alloca i32
    store i32 10, i32* %4
    br %5

5:
    %8 = load i32, i32* %1
    %9 = icmp lt i32 %8,i32 20
    br i32 %9 %6 %7

6:
    %10 = load i32, i32* %1
    %11 = load i32, i32* %1
    %12 = add i32 %11,3
    store i32 %12, i32* %1
    br %13

7:
    %46 = load i32, i32* %1
    %47 = load i32, i32* %2
    %48 = load i32, i32* %4
    %49 = load i32, i32* %2
    %50 = load i32, i32* %4
    %51 = add i32 %49,%50
    %52 = load i32, i32* %1
    %53 = load i32, i32* %2
    %54 = load i32, i32* %4
    %55 = load i32, i32* %2
    %56 = load i32, i32* %4
    %57 = add i32 %55,%56
    %58 = add i32 %52,%57
    %59 = load i32, i32* %3
    %60 = add i32 %58,%59
    ret i32 %60

13:
    %16 = load i32, i32* %2
    %17 = icmp lt i32 %16,i32 10
    br i32 %17 %14 %15

14:
    %18 = load i32, i32* %2
    %19 = load i32, i32* %2
    %20 = add i32 %19,1
    store i32 %20, i32* %2
    br %21

15:
    %43 = load i32, i32* %2
    %44 = load i32, i32* %2
    %45 = sub i32 %44,2
    store i32 %45, i32* %2
    br %5

21:
    %24 = load i32, i32* %3
    %25 = icmp eq i32 %24,i32 7
    br i32 %25 %22 %23

22:
    %26 = load i32, i32* %3
    %27 = load i32, i32* %3
    %28 = sub i32 %27,1
    store i32 %28, i32* %3
    br %29

23:
    %40 = load i32, i32* %3
    %41 = load i32, i32* %3
    %42 = add i32 %41,1
    store i32 %42, i32* %3
    br %13

29:
    %32 = load i32, i32* %4
    %33 = icmp lt i32 %32,i32 20
    br i32 %33 %30 %31

30:
    %34 = load i32, i32* %4
    %35 = load i32, i32* %4
    %36 = add i32 %35,3
    store i32 %36, i32* %4
    br %29

31:
    %37 = load i32, i32* %4
    %38 = load i32, i32* %4
    %39 = sub i32 %38,1
    store i32 %39, i32* %4
    br %21
}
define dso_local i32 @main(){
0:
    %1 = call i32 @FourWhile()
    ret i32 %1
}

