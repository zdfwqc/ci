@N = dso_local constant i32 10000

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @long_array(i32 %0){
1:
    %2 = alloca i32
    store i32 %0, i32* %2
    %3 = alloca [10000 x i32]
    %4 = alloca [10000 x i32]
    %5 = alloca [10000 x i32]
    %6 = alloca i32
    store i32 0, i32* %6
    br %7

7:
    %10 = load i32, i32* %6
    %11 = load i32, i32* @N
    %12 = icmp lt i32 %10,i32 %11
    br i32 %12 %8 %9

8:
    %13 = load i32, i32* %6
    %14 = getelementptr [10000 x i32], [10000 x i32]* %3, i32 %13
    %15 = load i32, i32* %6
    %16 = load i32, i32* %6
    %17 = mul i32 %15,%16
    %18 = srem i32 %17,10
    store i32 %18, i32* %14
    %19 = load i32, i32* %6
    %20 = load i32, i32* %6
    %21 = add i32 %20,1
    store i32 %21, i32* %6
    br %7

9:
    store i32 0, i32* %6
    br %22

22:
    %25 = load i32, i32* %6
    %26 = load i32, i32* @N
    %27 = icmp lt i32 %25,i32 %26
    br i32 %27 %23 %24

23:
    %28 = load i32, i32* %6
    %29 = getelementptr [10000 x i32], [10000 x i32]* %4, i32 %28
    %30 = load i32, i32* %6
    %31 = getelementptr [10000 x i32], [10000 x i32]* %3, i32 %30
    %32 = load i32, i32* %31
    %33 = load i32, i32* %6
    %34 = getelementptr [10000 x i32], [10000 x i32]* %3, i32 %33
    %35 = load i32, i32* %34
    %36 = mul i32 %32,%35
    %37 = srem i32 %36,10
    store i32 %37, i32* %29
    %38 = load i32, i32* %6
    %39 = load i32, i32* %6
    %40 = add i32 %39,1
    store i32 %40, i32* %6
    br %22

24:
    store i32 0, i32* %6
    br %41

41:
    %44 = load i32, i32* %6
    %45 = load i32, i32* @N
    %46 = icmp lt i32 %44,i32 %45
    br i32 %46 %42 %43

42:
    %47 = load i32, i32* %6
    %48 = getelementptr [10000 x i32], [10000 x i32]* %5, i32 %47
    %49 = load i32, i32* %6
    %50 = getelementptr [10000 x i32], [10000 x i32]* %4, i32 %49
    %51 = load i32, i32* %50
    %52 = load i32, i32* %6
    %53 = getelementptr [10000 x i32], [10000 x i32]* %4, i32 %52
    %54 = load i32, i32* %53
    %55 = mul i32 %51,%54
    %56 = srem i32 %55,100
    %57 = load i32, i32* %6
    %58 = getelementptr [10000 x i32], [10000 x i32]* %3, i32 %57
    %59 = load i32, i32* %58
    %60 = load i32, i32* %6
    %61 = getelementptr [10000 x i32], [10000 x i32]* %4, i32 %60
    %62 = load i32, i32* %61
    %63 = load i32, i32* %6
    %64 = getelementptr [10000 x i32], [10000 x i32]* %4, i32 %63
    %65 = load i32, i32* %64
    %66 = mul i32 %62,%65
    %67 = srem i32 %66,100
    %68 = load i32, i32* %6
    %69 = getelementptr [10000 x i32], [10000 x i32]* %3, i32 %68
    %70 = load i32, i32* %69
    %71 = add i32 %67,%70
    store i32 %71, i32* %48
    %72 = load i32, i32* %6
    %73 = load i32, i32* %6
    %74 = add i32 %73,1
    store i32 %74, i32* %6
    br %41

43:
    %75 = alloca i32
    store i32 0, i32* %75
    store i32 0, i32* %6
    br %76

76:
    %79 = load i32, i32* %6
    %80 = load i32, i32* @N
    %81 = icmp lt i32 %79,i32 %80
    br i32 %81 %77 %78

77:
    %85 = load i32, i32* %6
    %86 = icmp lt i32 %85,i32 10
    br i32 %86 %83 %84

78:
    %199 = load i32, i32* %75
    ret i32 %199

82:
    %196 = load i32, i32* %6
    %197 = load i32, i32* %6
    %198 = add i32 %197,1
    store i32 %198, i32* %6
    br %76

83:
    %87 = load i32, i32* %75
    %88 = load i32, i32* %6
    %89 = getelementptr [10000 x i32], [10000 x i32]* %5, i32 %88
    %90 = load i32, i32* %89
    %91 = load i32, i32* %75
    %92 = load i32, i32* %6
    %93 = getelementptr [10000 x i32], [10000 x i32]* %5, i32 %92
    %94 = load i32, i32* %93
    %95 = add i32 %91,%94
    %96 = srem i32 %95,1333
    store i32 %96, i32* %75
    %97 = load i32, i32* %75
    call void @putint(i32 %97)
    br %82

84:
    %101 = load i32, i32* %6
    %102 = icmp lt i32 %101,i32 20
    br i32 %102 %99 %100

98:

99:
    %103 = load i32, i32* @N
    %104 = sdiv i32 %103,2
    %105 = alloca i32
    store i32 %104, i32* %105
    br %106

100:
    %132 = load i32, i32* %6
    %133 = icmp lt i32 %132,i32 30
    br i32 %133 %130 %131

106:
    %109 = load i32, i32* %105
    %110 = load i32, i32* @N
    %111 = icmp lt i32 %109,i32 %110
    br i32 %111 %107 %108

107:
    %112 = load i32, i32* %75
    %113 = load i32, i32* %6
    %114 = getelementptr [10000 x i32], [10000 x i32]* %5, i32 %113
    %115 = load i32, i32* %114
    %116 = load i32, i32* %75
    %117 = load i32, i32* %6
    %118 = getelementptr [10000 x i32], [10000 x i32]* %5, i32 %117
    %119 = load i32, i32* %118
    %120 = add i32 %116,%119
    %121 = load i32, i32* %105
    %122 = getelementptr [10000 x i32], [10000 x i32]* %3, i32 %121
    %123 = load i32, i32* %122
    %124 = sub i32 %120,%123
    store i32 %124, i32* %75
    %125 = load i32, i32* %105
    %126 = load i32, i32* %105
    %127 = add i32 %126,1
    store i32 %127, i32* %105
    br %106

108:
    %128 = load i32, i32* %75
    call void @putint(i32 %128)

129:

130:
    %134 = load i32, i32* @N
    %135 = sdiv i32 %134,2
    %136 = alloca i32
    store i32 %135, i32* %136
    br %137

131:
    %182 = load i32, i32* %75
    %183 = load i32, i32* %6
    %184 = getelementptr [10000 x i32], [10000 x i32]* %5, i32 %183
    %185 = load i32, i32* %184
    %186 = load i32, i32* %2
    %187 = mul i32 %185,%186
    %188 = load i32, i32* %75
    %189 = load i32, i32* %6
    %190 = getelementptr [10000 x i32], [10000 x i32]* %5, i32 %189
    %191 = load i32, i32* %190
    %192 = load i32, i32* %2
    %193 = mul i32 %191,%192
    %194 = add i32 %188,%193
    %195 = srem i32 %194,99988
    store i32 %195, i32* %75
    br %129

137:
    %140 = load i32, i32* %136
    %141 = load i32, i32* @N
    %142 = icmp lt i32 %140,i32 %141
    br i32 %142 %138 %139

138:
    %146 = load i32, i32* %136
    %147 = icmp gt i32 %146,i32 2233
    br i32 %147 %144 %145

139:
    %181 = load i32, i32* %75
    call void @putint(i32 %181)

143:
    br %137

144:
    %148 = load i32, i32* %75
    %149 = load i32, i32* %6
    %150 = getelementptr [10000 x i32], [10000 x i32]* %4, i32 %149
    %151 = load i32, i32* %150
    %152 = load i32, i32* %75
    %153 = load i32, i32* %6
    %154 = getelementptr [10000 x i32], [10000 x i32]* %4, i32 %153
    %155 = load i32, i32* %154
    %156 = add i32 %152,%155
    %157 = load i32, i32* %136
    %158 = getelementptr [10000 x i32], [10000 x i32]* %3, i32 %157
    %159 = load i32, i32* %158
    %160 = sub i32 %156,%159
    store i32 %160, i32* %75
    %161 = load i32, i32* %136
    %162 = load i32, i32* %136
    %163 = add i32 %162,1
    store i32 %163, i32* %136
    br %143

145:
    %164 = load i32, i32* %75
    %165 = load i32, i32* %6
    %166 = getelementptr [10000 x i32], [10000 x i32]* %3, i32 %165
    %167 = load i32, i32* %166
    %168 = load i32, i32* %75
    %169 = load i32, i32* %6
    %170 = getelementptr [10000 x i32], [10000 x i32]* %3, i32 %169
    %171 = load i32, i32* %170
    %172 = add i32 %168,%171
    %173 = load i32, i32* %136
    %174 = getelementptr [10000 x i32], [10000 x i32]* %5, i32 %173
    %175 = load i32, i32* %174
    %176 = add i32 %172,%175
    %177 = srem i32 %176,13333
    store i32 %177, i32* %75
    %178 = load i32, i32* %136
    %179 = load i32, i32* %136
    %180 = add i32 %179,2
    store i32 %180, i32* %136
    br %143
}
define dso_local i32 @main(i32 %0){
1:
    %2 = call i32 @long_array(i32 9)
    ret i32 %2
}

