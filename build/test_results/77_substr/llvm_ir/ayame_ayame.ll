declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @MAX(i32 %0, i32 %1){
2:
    %3 = alloca i32
    store i32 %0, i32* %3
    %4 = alloca i32
    store i32 %1, i32* %4
    %8 = load i32, i32* %3
    %9 = load i32, i32* %4
    %10 = icmp eq i32 %8,i32 %9
    br i32 %10 %6 %7

5:

6:
    %11 = load i32, i32* %3
    ret i32 %11

7:
    %15 = load i32, i32* %3
    %16 = load i32, i32* %4
    %17 = icmp gt i32 %15,i32 %16
    br i32 %17 %13 %14

12:

13:
    %18 = load i32, i32* %3
    ret i32 %18

14:
    %19 = load i32, i32* %4
    ret i32 %19
}
define dso_local i32 @max_sum_nonadjacent(i32* %0, i32 %1){
2:
    %3 = alloca i32*
    store i32* %0, i32** %3
    %4 = alloca i32
    store i32 %1, i32* %4
    %5 = alloca [16 x i32]
    %6 = getelementptr [16 x i32], [16 x i32]* %5, i32 0
    %7 = getelementptr i32*, i32** %3, i32 0
    %8 = load i32*, i32** %7
    store i32* %8, i32* %6
    %9 = getelementptr [16 x i32], [16 x i32]* %5, i32 1
    %10 = getelementptr i32*, i32** %3, i32 0
    %11 = load i32*, i32** %10
    %12 = getelementptr i32*, i32** %3, i32 1
    %13 = load i32*, i32** %12
    %14 = call i32 @MAX(i32* %11,i32* %13)
    store i32 %14, i32* %9
    %15 = alloca i32
    store i32 2, i32* %15
    br %16

16:
    %19 = load i32, i32* %15
    %20 = load i32, i32* %4
    %21 = icmp lt i32 %19,i32 %20
    br i32 %21 %17 %18

17:
    %22 = load i32, i32* %15
    %23 = getelementptr [16 x i32], [16 x i32]* %5, i32 %22
    %24 = load i32, i32* %15
    %25 = load i32, i32* %15
    %26 = sub i32 %25,2
    %27 = getelementptr [16 x i32], [16 x i32]* %5, i32 %26
    %28 = load i32, i32* %27
    %29 = load i32, i32* %15
    %30 = getelementptr i32*, i32** %3, i32 %29
    %31 = load i32*, i32** %30
    %32 = load i32, i32* %15
    %33 = load i32, i32* %15
    %34 = sub i32 %33,2
    %35 = getelementptr [16 x i32], [16 x i32]* %5, i32 %34
    %36 = load i32, i32* %35
    %37 = load i32, i32* %15
    %38 = getelementptr i32*, i32** %3, i32 %37
    %39 = load i32*, i32** %38
    %40 = add i32 %36,%39
    %41 = load i32, i32* %15
    %42 = load i32, i32* %15
    %43 = sub i32 %42,1
    %44 = getelementptr [16 x i32], [16 x i32]* %5, i32 %43
    %45 = load i32, i32* %44
    %46 = call i32 @MAX(i32 %40,i32 %45)
    store i32 %46, i32* %23
    %47 = load i32, i32* %15
    %48 = load i32, i32* %15
    %49 = add i32 %48,1
    store i32 %49, i32* %15
    br %16

18:
    %50 = load i32, i32* %4
    %51 = load i32, i32* %4
    %52 = sub i32 %51,1
    %53 = getelementptr [16 x i32], [16 x i32]* %5, i32 %52
    %54 = load i32, i32* %53
    ret i32 %54
}
define dso_local i32 @longest_common_subseq(i32* %0, i32 %1, i32* %2, i32 %3){
4:
    %5 = alloca i32*
    store i32* %0, i32** %5
    %6 = alloca i32
    store i32 %1, i32* %6
    %7 = alloca i32*
    store i32* %2, i32** %7
    %8 = alloca i32
    store i32 %3, i32* %8
    %9 = alloca [16 x [16 x i32]]
    %10 = alloca i32
    %11 = alloca i32
    store i32 1, i32* %10
    br %12

12:
    %15 = load i32, i32* %10
    %16 = load i32, i32* %6
    %17 = icmp le i32 %15,i32 %16
    br i32 %17 %13 %14

13:
    store i32 1, i32* %11
    br %18

14:
    %80 = load i32, i32* %6
    %81 = load i32, i32* %8
    %82 = getelementptr [16 x [16 x i32]], [16 x [16 x i32]]* %9, i32 %80, i32 %81
    %83 = load i32, i32* %82
    ret i32 %83

18:
    %21 = load i32, i32* %11
    %22 = load i32, i32* %8
    %23 = icmp le i32 %21,i32 %22
    br i32 %23 %19 %20

19:
    %27 = load i32, i32* %10
    %28 = load i32, i32* %10
    %29 = sub i32 %28,1
    %30 = getelementptr i32*, i32** %5, i32 %29
    %31 = load i32*, i32** %30
    %32 = load i32, i32* %11
    %33 = load i32, i32* %11
    %34 = sub i32 %33,1
    %35 = getelementptr i32*, i32** %7, i32 %34
    %36 = load i32*, i32** %35
    %37 = icmp eq i32* %31,i32* %36
    br i32* %37 %25 %26

20:
    %77 = load i32, i32* %10
    %78 = load i32, i32* %10
    %79 = add i32 %78,1
    store i32 %79, i32* %10
    br %12

24:
    %74 = load i32, i32* %11
    %75 = load i32, i32* %11
    %76 = add i32 %75,1
    store i32 %76, i32* %11
    br %18

25:
    %38 = load i32, i32* %10
    %39 = load i32, i32* %11
    %40 = getelementptr [16 x [16 x i32]], [16 x [16 x i32]]* %9, i32 %38, i32 %39
    %41 = load i32, i32* %10
    %42 = load i32, i32* %10
    %43 = sub i32 %42,1
    %44 = load i32, i32* %11
    %45 = load i32, i32* %11
    %46 = sub i32 %45,1
    %47 = getelementptr [16 x [16 x i32]], [16 x [16 x i32]]* %9, i32 %43, i32 %46
    %48 = load i32, i32* %47
    %49 = load i32, i32* %10
    %50 = load i32, i32* %10
    %51 = sub i32 %50,1
    %52 = load i32, i32* %11
    %53 = load i32, i32* %11
    %54 = sub i32 %53,1
    %55 = getelementptr [16 x [16 x i32]], [16 x [16 x i32]]* %9, i32 %51, i32 %54
    %56 = load i32, i32* %55
    %57 = add i32 %56,1
    store i32 %57, i32* %40
    br %24

26:
    %58 = load i32, i32* %10
    %59 = load i32, i32* %11
    %60 = getelementptr [16 x [16 x i32]], [16 x [16 x i32]]* %9, i32 %58, i32 %59
    %61 = load i32, i32* %10
    %62 = load i32, i32* %10
    %63 = sub i32 %62,1
    %64 = load i32, i32* %11
    %65 = getelementptr [16 x [16 x i32]], [16 x [16 x i32]]* %9, i32 %63, i32 %64
    %66 = load i32, i32* %65
    %67 = load i32, i32* %10
    %68 = load i32, i32* %11
    %69 = load i32, i32* %11
    %70 = sub i32 %69,1
    %71 = getelementptr [16 x [16 x i32]], [16 x [16 x i32]]* %9, i32 %67, i32 %70
    %72 = load i32, i32* %71
    %73 = call i32 @MAX(i32 %66,i32 %72)
    store i32 %73, i32* %60
    br %24
}
define dso_local i32 @main(i32* %0, i32 %1, i32* %2, i32 %3){
4:
    %5 = alloca [15 x i32]
    %6 = getelementptr [15 x i32], [15 x i32]* %5, i32 0
    store i32 8, i32* %6
    %7 = getelementptr [15 x i32], [15 x i32]* %5, i32 1
    store i32 7, i32* %7
    %8 = getelementptr [15 x i32], [15 x i32]* %5, i32 2
    store i32 4, i32* %8
    %9 = getelementptr [15 x i32], [15 x i32]* %5, i32 3
    store i32 1, i32* %9
    %10 = getelementptr [15 x i32], [15 x i32]* %5, i32 4
    store i32 2, i32* %10
    %11 = getelementptr [15 x i32], [15 x i32]* %5, i32 5
    store i32 7, i32* %11
    %12 = getelementptr [15 x i32], [15 x i32]* %5, i32 6
    store i32 0, i32* %12
    %13 = getelementptr [15 x i32], [15 x i32]* %5, i32 7
    store i32 1, i32* %13
    %14 = getelementptr [15 x i32], [15 x i32]* %5, i32 8
    store i32 9, i32* %14
    %15 = getelementptr [15 x i32], [15 x i32]* %5, i32 9
    store i32 3, i32* %15
    %16 = getelementptr [15 x i32], [15 x i32]* %5, i32 10
    store i32 4, i32* %16
    %17 = getelementptr [15 x i32], [15 x i32]* %5, i32 11
    store i32 8, i32* %17
    %18 = getelementptr [15 x i32], [15 x i32]* %5, i32 12
    store i32 3, i32* %18
    %19 = getelementptr [15 x i32], [15 x i32]* %5, i32 13
    store i32 7, i32* %19
    %20 = getelementptr [15 x i32], [15 x i32]* %5, i32 14
    store i32 0, i32* %20
    %21 = alloca [13 x i32]
    %22 = getelementptr [13 x i32], [13 x i32]* %21, i32 0
    store i32 3, i32* %22
    %23 = getelementptr [13 x i32], [13 x i32]* %21, i32 1
    store i32 9, i32* %23
    %24 = getelementptr [13 x i32], [13 x i32]* %21, i32 2
    store i32 7, i32* %24
    %25 = getelementptr [13 x i32], [13 x i32]* %21, i32 3
    store i32 1, i32* %25
    %26 = getelementptr [13 x i32], [13 x i32]* %21, i32 4
    store i32 4, i32* %26
    %27 = getelementptr [13 x i32], [13 x i32]* %21, i32 5
    store i32 2, i32* %27
    %28 = getelementptr [13 x i32], [13 x i32]* %21, i32 6
    store i32 4, i32* %28
    %29 = getelementptr [13 x i32], [13 x i32]* %21, i32 7
    store i32 3, i32* %29
    %30 = getelementptr [13 x i32], [13 x i32]* %21, i32 8
    store i32 6, i32* %30
    %31 = getelementptr [13 x i32], [13 x i32]* %21, i32 9
    store i32 8, i32* %31
    %32 = getelementptr [13 x i32], [13 x i32]* %21, i32 10
    store i32 0, i32* %32
    %33 = getelementptr [13 x i32], [13 x i32]* %21, i32 11
    store i32 1, i32* %33
    %34 = getelementptr [13 x i32], [13 x i32]* %21, i32 12
    store i32 5, i32* %34
    %35 = alloca i32
    %36 = alloca i32
    %37 = load [15 x i32], [15 x i32]* %5
    %38 = call i32 @max_sum_nonadjacent([15 x i32] %37,i32 15)
    call void @putint([15 x i32] %37,i32 15,i32 %38)
    call void @putch(i32 10)
    %39 = load [15 x i32], [15 x i32]* %5
    %40 = load [13 x i32], [13 x i32]* %21
    %41 = call i32 @longest_common_subseq([15 x i32] %39,i32 15,[13 x i32] %40,i32 13)
    call void @putint([15 x i32] %39,i32 15,[13 x i32] %40,i32 13,i32 %41)
    call void @putch(i32 10)
    ret i32 0
}

