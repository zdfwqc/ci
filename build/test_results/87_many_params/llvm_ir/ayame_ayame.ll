declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local void @sort(i32* %0, i32 %1){
2:
    %3 = alloca i32*
    store i32* %0, i32** %3
    %4 = alloca i32
    store i32 %1, i32* %4
    %5 = alloca i32
    store i32 0, i32* %5
    br %6

6:
    %9 = load i32, i32* %5
    %10 = load i32, i32* %4
    %11 = load i32, i32* %4
    %12 = sub i32 %11,1
    %13 = icmp lt i32 %9,i32 %12
    br i32 %13 %7 %8

7:
    %14 = load i32, i32* %5
    %15 = load i32, i32* %5
    %16 = add i32 %15,1
    %17 = alloca i32
    store i32 %16, i32* %17
    br %18

8:

18:
    %21 = load i32, i32* %17
    %22 = load i32, i32* %4
    %23 = icmp lt i32 %21,i32 %22
    br i32 %23 %19 %20

19:
    %26 = load i32, i32* %5
    %27 = getelementptr i32*, i32** %3, i32 %26
    %28 = load i32*, i32** %27
    %29 = load i32, i32* %17
    %30 = getelementptr i32*, i32** %3, i32 %29
    %31 = load i32*, i32** %30
    %32 = icmp lt i32* %28,i32* %31
    br i32* %32 %25 %24

20:
    %48 = load i32, i32* %5
    %49 = load i32, i32* %5
    %50 = add i32 %49,1
    store i32 %50, i32* %5
    br %6

24:
    %45 = load i32, i32* %17
    %46 = load i32, i32* %17
    %47 = add i32 %46,1
    store i32 %47, i32* %17
    br %18

25:
    %33 = load i32, i32* %5
    %34 = getelementptr i32*, i32** %3, i32 %33
    %35 = load i32*, i32** %34
    %36 = alloca i32
    store i32* %35, i32* %36
    %37 = load i32, i32* %5
    %38 = getelementptr i32*, i32** %3, i32 %37
    %39 = load i32, i32* %17
    %40 = getelementptr i32*, i32** %3, i32 %39
    %41 = load i32*, i32** %40
    store i32* %41, i32** %38
    %42 = load i32, i32* %17
    %43 = getelementptr i32*, i32** %3, i32 %42
    %44 = load i32, i32* %36
    store i32 %44, i32** %43
    br %24
}
define dso_local i32 @param32_rec(i32 %0, i32 %1, i32 %2, i32 %3, i32 %4, i32 %5, i32 %6, i32 %7, i32 %8, i32 %9, i32 %10, i32 %11, i32 %12, i32 %13, i32 %14, i32 %15, i32 %16, i32 %17, i32 %18, i32 %19, i32 %20, i32 %21, i32 %22, i32 %23, i32 %24, i32 %25, i32 %26, i32 %27, i32 %28, i32 %29, i32 %30, i32 %31){
32:
    %33 = alloca i32
    store i32 %0, i32* %33
    %34 = alloca i32
    store i32 %1, i32* %34
    %35 = alloca i32
    store i32 %2, i32* %35
    %36 = alloca i32
    store i32 %3, i32* %36
    %37 = alloca i32
    store i32 %4, i32* %37
    %38 = alloca i32
    store i32 %5, i32* %38
    %39 = alloca i32
    store i32 %6, i32* %39
    %40 = alloca i32
    store i32 %7, i32* %40
    %41 = alloca i32
    store i32 %8, i32* %41
    %42 = alloca i32
    store i32 %9, i32* %42
    %43 = alloca i32
    store i32 %10, i32* %43
    %44 = alloca i32
    store i32 %11, i32* %44
    %45 = alloca i32
    store i32 %12, i32* %45
    %46 = alloca i32
    store i32 %13, i32* %46
    %47 = alloca i32
    store i32 %14, i32* %47
    %48 = alloca i32
    store i32 %15, i32* %48
    %49 = alloca i32
    store i32 %16, i32* %49
    %50 = alloca i32
    store i32 %17, i32* %50
    %51 = alloca i32
    store i32 %18, i32* %51
    %52 = alloca i32
    store i32 %19, i32* %52
    %53 = alloca i32
    store i32 %20, i32* %53
    %54 = alloca i32
    store i32 %21, i32* %54
    %55 = alloca i32
    store i32 %22, i32* %55
    %56 = alloca i32
    store i32 %23, i32* %56
    %57 = alloca i32
    store i32 %24, i32* %57
    %58 = alloca i32
    store i32 %25, i32* %58
    %59 = alloca i32
    store i32 %26, i32* %59
    %60 = alloca i32
    store i32 %27, i32* %60
    %61 = alloca i32
    store i32 %28, i32* %61
    %62 = alloca i32
    store i32 %29, i32* %62
    %63 = alloca i32
    store i32 %30, i32* %63
    %64 = alloca i32
    store i32 %31, i32* %64
    %68 = load i32, i32* %33
    %69 = icmp eq i32 %68,i32 0
    br i32 %69 %66 %67

65:

66:
    %70 = load i32, i32* %34
    ret i32 %70

67:
    %71 = load i32, i32* %33
    %72 = load i32, i32* %33
    %73 = sub i32 %72,1
    %74 = load i32, i32* %34
    %75 = load i32, i32* %35
    %76 = load i32, i32* %34
    %77 = load i32, i32* %35
    %78 = add i32 %76,%77
    %79 = srem i32 %78,998244353
    %80 = load i32, i32* %36
    %81 = load i32, i32* %37
    %82 = load i32, i32* %38
    %83 = load i32, i32* %39
    %84 = load i32, i32* %40
    %85 = load i32, i32* %41
    %86 = load i32, i32* %42
    %87 = load i32, i32* %43
    %88 = load i32, i32* %44
    %89 = load i32, i32* %45
    %90 = load i32, i32* %46
    %91 = load i32, i32* %47
    %92 = load i32, i32* %48
    %93 = load i32, i32* %49
    %94 = load i32, i32* %50
    %95 = load i32, i32* %51
    %96 = load i32, i32* %52
    %97 = load i32, i32* %53
    %98 = load i32, i32* %54
    %99 = load i32, i32* %55
    %100 = load i32, i32* %56
    %101 = load i32, i32* %57
    %102 = load i32, i32* %58
    %103 = load i32, i32* %59
    %104 = load i32, i32* %60
    %105 = load i32, i32* %61
    %106 = load i32, i32* %62
    %107 = load i32, i32* %63
    %108 = load i32, i32* %64
    %109 = call i32 @param32_rec(i32 %73,i32 %79,i32 %80,i32 %81,i32 %82,i32 %83,i32 %84,i32 %85,i32 %86,i32 %87,i32 %88,i32 %89,i32 %90,i32 %91,i32 %92,i32 %93,i32 %94,i32 %95,i32 %96,i32 %97,i32 %98,i32 %99,i32 %100,i32 %101,i32 %102,i32 %103,i32 %104,i32 %105,i32 %106,i32 %107,i32 %108,i32 0)
    ret i32 %109
}
define dso_local i32 @param32_arr(i32* %0, i32* %1, i32* %2, i32* %3, i32* %4, i32* %5, i32* %6, i32* %7, i32* %8, i32* %9, i32* %10, i32* %11, i32* %12, i32* %13, i32* %14, i32* %15, i32* %16, i32* %17, i32* %18, i32* %19, i32* %20, i32* %21, i32* %22, i32* %23, i32* %24, i32* %25, i32* %26, i32* %27, i32* %28, i32* %29, i32* %30, i32* %31){
32:
    %33 = alloca i32*
    store i32* %0, i32** %33
    %34 = alloca i32*
    store i32* %1, i32** %34
    %35 = alloca i32*
    store i32* %2, i32** %35
    %36 = alloca i32*
    store i32* %3, i32** %36
    %37 = alloca i32*
    store i32* %4, i32** %37
    %38 = alloca i32*
    store i32* %5, i32** %38
    %39 = alloca i32*
    store i32* %6, i32** %39
    %40 = alloca i32*
    store i32* %7, i32** %40
    %41 = alloca i32*
    store i32* %8, i32** %41
    %42 = alloca i32*
    store i32* %9, i32** %42
    %43 = alloca i32*
    store i32* %10, i32** %43
    %44 = alloca i32*
    store i32* %11, i32** %44
    %45 = alloca i32*
    store i32* %12, i32** %45
    %46 = alloca i32*
    store i32* %13, i32** %46
    %47 = alloca i32*
    store i32* %14, i32** %47
    %48 = alloca i32*
    store i32* %15, i32** %48
    %49 = alloca i32*
    store i32* %16, i32** %49
    %50 = alloca i32*
    store i32* %17, i32** %50
    %51 = alloca i32*
    store i32* %18, i32** %51
    %52 = alloca i32*
    store i32* %19, i32** %52
    %53 = alloca i32*
    store i32* %20, i32** %53
    %54 = alloca i32*
    store i32* %21, i32** %54
    %55 = alloca i32*
    store i32* %22, i32** %55
    %56 = alloca i32*
    store i32* %23, i32** %56
    %57 = alloca i32*
    store i32* %24, i32** %57
    %58 = alloca i32*
    store i32* %25, i32** %58
    %59 = alloca i32*
    store i32* %26, i32** %59
    %60 = alloca i32*
    store i32* %27, i32** %60
    %61 = alloca i32*
    store i32* %28, i32** %61
    %62 = alloca i32*
    store i32* %29, i32** %62
    %63 = alloca i32*
    store i32* %30, i32** %63
    %64 = alloca i32*
    store i32* %31, i32** %64
    %65 = getelementptr i32*, i32** %33, i32 0
    %66 = load i32*, i32** %65
    %67 = getelementptr i32*, i32** %33, i32 1
    %68 = load i32*, i32** %67
    %69 = getelementptr i32*, i32** %33, i32 0
    %70 = load i32*, i32** %69
    %71 = getelementptr i32*, i32** %33, i32 1
    %72 = load i32*, i32** %71
    %73 = add i32 %70,%72
    %74 = alloca i32
    store i32* %73, i32* %74
    %75 = load i32, i32* %74
    %76 = getelementptr i32*, i32** %34, i32 0
    %77 = load i32*, i32** %76
    %78 = load i32, i32* %74
    %79 = getelementptr i32*, i32** %34, i32 0
    %80 = load i32*, i32** %79
    %81 = add i32 %78,%80
    %82 = getelementptr i32*, i32** %34, i32 1
    %83 = load i32*, i32** %82
    %84 = add i32 %81,%83
    store i32 %84, i32* %74
    %85 = load i32, i32* %74
    %86 = getelementptr i32*, i32** %35, i32 0
    %87 = load i32*, i32** %86
    %88 = load i32, i32* %74
    %89 = getelementptr i32*, i32** %35, i32 0
    %90 = load i32*, i32** %89
    %91 = add i32 %88,%90
    %92 = getelementptr i32*, i32** %35, i32 1
    %93 = load i32*, i32** %92
    %94 = add i32 %91,%93
    store i32 %94, i32* %74
    %95 = load i32, i32* %74
    %96 = getelementptr i32*, i32** %36, i32 0
    %97 = load i32*, i32** %96
    %98 = load i32, i32* %74
    %99 = getelementptr i32*, i32** %36, i32 0
    %100 = load i32*, i32** %99
    %101 = add i32 %98,%100
    %102 = getelementptr i32*, i32** %36, i32 1
    %103 = load i32*, i32** %102
    %104 = add i32 %101,%103
    store i32 %104, i32* %74
    %105 = load i32, i32* %74
    %106 = getelementptr i32*, i32** %37, i32 0
    %107 = load i32*, i32** %106
    %108 = load i32, i32* %74
    %109 = getelementptr i32*, i32** %37, i32 0
    %110 = load i32*, i32** %109
    %111 = add i32 %108,%110
    %112 = getelementptr i32*, i32** %37, i32 1
    %113 = load i32*, i32** %112
    %114 = add i32 %111,%113
    store i32 %114, i32* %74
    %115 = load i32, i32* %74
    %116 = getelementptr i32*, i32** %38, i32 0
    %117 = load i32*, i32** %116
    %118 = load i32, i32* %74
    %119 = getelementptr i32*, i32** %38, i32 0
    %120 = load i32*, i32** %119
    %121 = add i32 %118,%120
    %122 = getelementptr i32*, i32** %38, i32 1
    %123 = load i32*, i32** %122
    %124 = add i32 %121,%123
    store i32 %124, i32* %74
    %125 = load i32, i32* %74
    %126 = getelementptr i32*, i32** %39, i32 0
    %127 = load i32*, i32** %126
    %128 = load i32, i32* %74
    %129 = getelementptr i32*, i32** %39, i32 0
    %130 = load i32*, i32** %129
    %131 = add i32 %128,%130
    %132 = getelementptr i32*, i32** %39, i32 1
    %133 = load i32*, i32** %132
    %134 = add i32 %131,%133
    store i32 %134, i32* %74
    %135 = load i32, i32* %74
    %136 = getelementptr i32*, i32** %40, i32 0
    %137 = load i32*, i32** %136
    %138 = load i32, i32* %74
    %139 = getelementptr i32*, i32** %40, i32 0
    %140 = load i32*, i32** %139
    %141 = add i32 %138,%140
    %142 = getelementptr i32*, i32** %40, i32 1
    %143 = load i32*, i32** %142
    %144 = add i32 %141,%143
    store i32 %144, i32* %74
    %145 = load i32, i32* %74
    %146 = getelementptr i32*, i32** %41, i32 0
    %147 = load i32*, i32** %146
    %148 = load i32, i32* %74
    %149 = getelementptr i32*, i32** %41, i32 0
    %150 = load i32*, i32** %149
    %151 = add i32 %148,%150
    %152 = getelementptr i32*, i32** %41, i32 1
    %153 = load i32*, i32** %152
    %154 = add i32 %151,%153
    store i32 %154, i32* %74
    %155 = load i32, i32* %74
    %156 = getelementptr i32*, i32** %42, i32 0
    %157 = load i32*, i32** %156
    %158 = load i32, i32* %74
    %159 = getelementptr i32*, i32** %42, i32 0
    %160 = load i32*, i32** %159
    %161 = add i32 %158,%160
    %162 = getelementptr i32*, i32** %42, i32 1
    %163 = load i32*, i32** %162
    %164 = add i32 %161,%163
    store i32 %164, i32* %74
    %165 = load i32, i32* %74
    %166 = getelementptr i32*, i32** %43, i32 0
    %167 = load i32*, i32** %166
    %168 = load i32, i32* %74
    %169 = getelementptr i32*, i32** %43, i32 0
    %170 = load i32*, i32** %169
    %171 = add i32 %168,%170
    %172 = getelementptr i32*, i32** %43, i32 1
    %173 = load i32*, i32** %172
    %174 = add i32 %171,%173
    store i32 %174, i32* %74
    %175 = load i32, i32* %74
    %176 = getelementptr i32*, i32** %44, i32 0
    %177 = load i32*, i32** %176
    %178 = load i32, i32* %74
    %179 = getelementptr i32*, i32** %44, i32 0
    %180 = load i32*, i32** %179
    %181 = add i32 %178,%180
    %182 = getelementptr i32*, i32** %44, i32 1
    %183 = load i32*, i32** %182
    %184 = add i32 %181,%183
    store i32 %184, i32* %74
    %185 = load i32, i32* %74
    %186 = getelementptr i32*, i32** %45, i32 0
    %187 = load i32*, i32** %186
    %188 = load i32, i32* %74
    %189 = getelementptr i32*, i32** %45, i32 0
    %190 = load i32*, i32** %189
    %191 = add i32 %188,%190
    %192 = getelementptr i32*, i32** %45, i32 1
    %193 = load i32*, i32** %192
    %194 = add i32 %191,%193
    store i32 %194, i32* %74
    %195 = load i32, i32* %74
    %196 = getelementptr i32*, i32** %46, i32 0
    %197 = load i32*, i32** %196
    %198 = load i32, i32* %74
    %199 = getelementptr i32*, i32** %46, i32 0
    %200 = load i32*, i32** %199
    %201 = add i32 %198,%200
    %202 = getelementptr i32*, i32** %46, i32 1
    %203 = load i32*, i32** %202
    %204 = add i32 %201,%203
    store i32 %204, i32* %74
    %205 = load i32, i32* %74
    %206 = getelementptr i32*, i32** %47, i32 0
    %207 = load i32*, i32** %206
    %208 = load i32, i32* %74
    %209 = getelementptr i32*, i32** %47, i32 0
    %210 = load i32*, i32** %209
    %211 = add i32 %208,%210
    %212 = getelementptr i32*, i32** %47, i32 1
    %213 = load i32*, i32** %212
    %214 = add i32 %211,%213
    store i32 %214, i32* %74
    %215 = load i32, i32* %74
    %216 = getelementptr i32*, i32** %48, i32 0
    %217 = load i32*, i32** %216
    %218 = load i32, i32* %74
    %219 = getelementptr i32*, i32** %48, i32 0
    %220 = load i32*, i32** %219
    %221 = add i32 %218,%220
    %222 = getelementptr i32*, i32** %48, i32 1
    %223 = load i32*, i32** %222
    %224 = add i32 %221,%223
    store i32 %224, i32* %74
    %225 = load i32, i32* %74
    %226 = getelementptr i32*, i32** %49, i32 0
    %227 = load i32*, i32** %226
    %228 = load i32, i32* %74
    %229 = getelementptr i32*, i32** %49, i32 0
    %230 = load i32*, i32** %229
    %231 = add i32 %228,%230
    %232 = getelementptr i32*, i32** %49, i32 1
    %233 = load i32*, i32** %232
    %234 = add i32 %231,%233
    store i32 %234, i32* %74
    %235 = load i32, i32* %74
    %236 = getelementptr i32*, i32** %50, i32 0
    %237 = load i32*, i32** %236
    %238 = load i32, i32* %74
    %239 = getelementptr i32*, i32** %50, i32 0
    %240 = load i32*, i32** %239
    %241 = add i32 %238,%240
    %242 = getelementptr i32*, i32** %50, i32 1
    %243 = load i32*, i32** %242
    %244 = add i32 %241,%243
    store i32 %244, i32* %74
    %245 = load i32, i32* %74
    %246 = getelementptr i32*, i32** %51, i32 0
    %247 = load i32*, i32** %246
    %248 = load i32, i32* %74
    %249 = getelementptr i32*, i32** %51, i32 0
    %250 = load i32*, i32** %249
    %251 = add i32 %248,%250
    %252 = getelementptr i32*, i32** %51, i32 1
    %253 = load i32*, i32** %252
    %254 = add i32 %251,%253
    store i32 %254, i32* %74
    %255 = load i32, i32* %74
    %256 = getelementptr i32*, i32** %52, i32 0
    %257 = load i32*, i32** %256
    %258 = load i32, i32* %74
    %259 = getelementptr i32*, i32** %52, i32 0
    %260 = load i32*, i32** %259
    %261 = add i32 %258,%260
    %262 = getelementptr i32*, i32** %52, i32 1
    %263 = load i32*, i32** %262
    %264 = add i32 %261,%263
    store i32 %264, i32* %74
    %265 = load i32, i32* %74
    %266 = getelementptr i32*, i32** %53, i32 0
    %267 = load i32*, i32** %266
    %268 = load i32, i32* %74
    %269 = getelementptr i32*, i32** %53, i32 0
    %270 = load i32*, i32** %269
    %271 = add i32 %268,%270
    %272 = getelementptr i32*, i32** %53, i32 1
    %273 = load i32*, i32** %272
    %274 = add i32 %271,%273
    store i32 %274, i32* %74
    %275 = load i32, i32* %74
    %276 = getelementptr i32*, i32** %54, i32 0
    %277 = load i32*, i32** %276
    %278 = load i32, i32* %74
    %279 = getelementptr i32*, i32** %54, i32 0
    %280 = load i32*, i32** %279
    %281 = add i32 %278,%280
    %282 = getelementptr i32*, i32** %54, i32 1
    %283 = load i32*, i32** %282
    %284 = add i32 %281,%283
    store i32 %284, i32* %74
    %285 = load i32, i32* %74
    %286 = getelementptr i32*, i32** %55, i32 0
    %287 = load i32*, i32** %286
    %288 = load i32, i32* %74
    %289 = getelementptr i32*, i32** %55, i32 0
    %290 = load i32*, i32** %289
    %291 = add i32 %288,%290
    %292 = getelementptr i32*, i32** %55, i32 1
    %293 = load i32*, i32** %292
    %294 = add i32 %291,%293
    store i32 %294, i32* %74
    %295 = load i32, i32* %74
    %296 = getelementptr i32*, i32** %56, i32 0
    %297 = load i32*, i32** %296
    %298 = load i32, i32* %74
    %299 = getelementptr i32*, i32** %56, i32 0
    %300 = load i32*, i32** %299
    %301 = add i32 %298,%300
    %302 = getelementptr i32*, i32** %56, i32 1
    %303 = load i32*, i32** %302
    %304 = add i32 %301,%303
    store i32 %304, i32* %74
    %305 = load i32, i32* %74
    %306 = getelementptr i32*, i32** %57, i32 0
    %307 = load i32*, i32** %306
    %308 = load i32, i32* %74
    %309 = getelementptr i32*, i32** %57, i32 0
    %310 = load i32*, i32** %309
    %311 = add i32 %308,%310
    %312 = getelementptr i32*, i32** %57, i32 1
    %313 = load i32*, i32** %312
    %314 = add i32 %311,%313
    store i32 %314, i32* %74
    %315 = load i32, i32* %74
    %316 = getelementptr i32*, i32** %58, i32 0
    %317 = load i32*, i32** %316
    %318 = load i32, i32* %74
    %319 = getelementptr i32*, i32** %58, i32 0
    %320 = load i32*, i32** %319
    %321 = add i32 %318,%320
    %322 = getelementptr i32*, i32** %58, i32 1
    %323 = load i32*, i32** %322
    %324 = add i32 %321,%323
    store i32 %324, i32* %74
    %325 = load i32, i32* %74
    %326 = getelementptr i32*, i32** %59, i32 0
    %327 = load i32*, i32** %326
    %328 = load i32, i32* %74
    %329 = getelementptr i32*, i32** %59, i32 0
    %330 = load i32*, i32** %329
    %331 = add i32 %328,%330
    %332 = getelementptr i32*, i32** %59, i32 1
    %333 = load i32*, i32** %332
    %334 = add i32 %331,%333
    store i32 %334, i32* %74
    %335 = load i32, i32* %74
    %336 = getelementptr i32*, i32** %60, i32 0
    %337 = load i32*, i32** %336
    %338 = load i32, i32* %74
    %339 = getelementptr i32*, i32** %60, i32 0
    %340 = load i32*, i32** %339
    %341 = add i32 %338,%340
    %342 = getelementptr i32*, i32** %60, i32 1
    %343 = load i32*, i32** %342
    %344 = add i32 %341,%343
    store i32 %344, i32* %74
    %345 = load i32, i32* %74
    %346 = getelementptr i32*, i32** %61, i32 0
    %347 = load i32*, i32** %346
    %348 = load i32, i32* %74
    %349 = getelementptr i32*, i32** %61, i32 0
    %350 = load i32*, i32** %349
    %351 = add i32 %348,%350
    %352 = getelementptr i32*, i32** %61, i32 1
    %353 = load i32*, i32** %352
    %354 = add i32 %351,%353
    store i32 %354, i32* %74
    %355 = load i32, i32* %74
    %356 = getelementptr i32*, i32** %62, i32 0
    %357 = load i32*, i32** %356
    %358 = load i32, i32* %74
    %359 = getelementptr i32*, i32** %62, i32 0
    %360 = load i32*, i32** %359
    %361 = add i32 %358,%360
    %362 = getelementptr i32*, i32** %62, i32 1
    %363 = load i32*, i32** %362
    %364 = add i32 %361,%363
    store i32 %364, i32* %74
    %365 = load i32, i32* %74
    %366 = getelementptr i32*, i32** %63, i32 0
    %367 = load i32*, i32** %366
    %368 = load i32, i32* %74
    %369 = getelementptr i32*, i32** %63, i32 0
    %370 = load i32*, i32** %369
    %371 = add i32 %368,%370
    %372 = getelementptr i32*, i32** %63, i32 1
    %373 = load i32*, i32** %372
    %374 = add i32 %371,%373
    store i32 %374, i32* %74
    %375 = load i32, i32* %74
    %376 = getelementptr i32*, i32** %64, i32 0
    %377 = load i32*, i32** %376
    %378 = load i32, i32* %74
    %379 = getelementptr i32*, i32** %64, i32 0
    %380 = load i32*, i32** %379
    %381 = add i32 %378,%380
    %382 = getelementptr i32*, i32** %64, i32 1
    %383 = load i32*, i32** %382
    %384 = add i32 %381,%383
    store i32 %384, i32* %74
    %385 = load i32, i32* %74
    ret i32 %385
}
define dso_local i32 @param16(i32 %0, i32 %1, i32 %2, i32 %3, i32 %4, i32 %5, i32 %6, i32 %7, i32 %8, i32 %9, i32 %10, i32 %11, i32 %12, i32 %13, i32 %14, i32 %15){
16:
    %17 = alloca i32
    store i32 %0, i32* %17
    %18 = alloca i32
    store i32 %1, i32* %18
    %19 = alloca i32
    store i32 %2, i32* %19
    %20 = alloca i32
    store i32 %3, i32* %20
    %21 = alloca i32
    store i32 %4, i32* %21
    %22 = alloca i32
    store i32 %5, i32* %22
    %23 = alloca i32
    store i32 %6, i32* %23
    %24 = alloca i32
    store i32 %7, i32* %24
    %25 = alloca i32
    store i32 %8, i32* %25
    %26 = alloca i32
    store i32 %9, i32* %26
    %27 = alloca i32
    store i32 %10, i32* %27
    %28 = alloca i32
    store i32 %11, i32* %28
    %29 = alloca i32
    store i32 %12, i32* %29
    %30 = alloca i32
    store i32 %13, i32* %30
    %31 = alloca i32
    store i32 %14, i32* %31
    %32 = alloca i32
    store i32 %15, i32* %32
    %33 = alloca [16 x i32]
    %34 = load i32, i32* %17
    %35 = getelementptr [16 x i32], [16 x i32]* %33, i32 0
    store i32 %34, i32* %35
    %36 = load i32, i32* %18
    %37 = getelementptr [16 x i32], [16 x i32]* %33, i32 1
    store i32 %36, i32* %37
    %38 = load i32, i32* %19
    %39 = getelementptr [16 x i32], [16 x i32]* %33, i32 2
    store i32 %38, i32* %39
    %40 = load i32, i32* %20
    %41 = getelementptr [16 x i32], [16 x i32]* %33, i32 3
    store i32 %40, i32* %41
    %42 = load i32, i32* %21
    %43 = getelementptr [16 x i32], [16 x i32]* %33, i32 4
    store i32 %42, i32* %43
    %44 = load i32, i32* %22
    %45 = getelementptr [16 x i32], [16 x i32]* %33, i32 5
    store i32 %44, i32* %45
    %46 = load i32, i32* %23
    %47 = getelementptr [16 x i32], [16 x i32]* %33, i32 6
    store i32 %46, i32* %47
    %48 = load i32, i32* %24
    %49 = getelementptr [16 x i32], [16 x i32]* %33, i32 7
    store i32 %48, i32* %49
    %50 = load i32, i32* %25
    %51 = getelementptr [16 x i32], [16 x i32]* %33, i32 8
    store i32 %50, i32* %51
    %52 = load i32, i32* %26
    %53 = getelementptr [16 x i32], [16 x i32]* %33, i32 9
    store i32 %52, i32* %53
    %54 = load i32, i32* %27
    %55 = getelementptr [16 x i32], [16 x i32]* %33, i32 10
    store i32 %54, i32* %55
    %56 = load i32, i32* %28
    %57 = getelementptr [16 x i32], [16 x i32]* %33, i32 11
    store i32 %56, i32* %57
    %58 = load i32, i32* %29
    %59 = getelementptr [16 x i32], [16 x i32]* %33, i32 12
    store i32 %58, i32* %59
    %60 = load i32, i32* %30
    %61 = getelementptr [16 x i32], [16 x i32]* %33, i32 13
    store i32 %60, i32* %61
    %62 = load i32, i32* %31
    %63 = getelementptr [16 x i32], [16 x i32]* %33, i32 14
    store i32 %62, i32* %63
    %64 = load i32, i32* %32
    %65 = getelementptr [16 x i32], [16 x i32]* %33, i32 15
    store i32 %64, i32* %65
    %66 = load [16 x i32], [16 x i32]* %33
    %67 = call void @sort([16 x i32] %66,i32 16)
    %68 = getelementptr [16 x i32], [16 x i32]* %33, i32 0
    %69 = load i32, i32* %68
    %70 = getelementptr [16 x i32], [16 x i32]* %33, i32 1
    %71 = load i32, i32* %70
    %72 = getelementptr [16 x i32], [16 x i32]* %33, i32 2
    %73 = load i32, i32* %72
    %74 = getelementptr [16 x i32], [16 x i32]* %33, i32 3
    %75 = load i32, i32* %74
    %76 = getelementptr [16 x i32], [16 x i32]* %33, i32 4
    %77 = load i32, i32* %76
    %78 = getelementptr [16 x i32], [16 x i32]* %33, i32 5
    %79 = load i32, i32* %78
    %80 = getelementptr [16 x i32], [16 x i32]* %33, i32 6
    %81 = load i32, i32* %80
    %82 = getelementptr [16 x i32], [16 x i32]* %33, i32 7
    %83 = load i32, i32* %82
    %84 = getelementptr [16 x i32], [16 x i32]* %33, i32 8
    %85 = load i32, i32* %84
    %86 = getelementptr [16 x i32], [16 x i32]* %33, i32 9
    %87 = load i32, i32* %86
    %88 = getelementptr [16 x i32], [16 x i32]* %33, i32 10
    %89 = load i32, i32* %88
    %90 = getelementptr [16 x i32], [16 x i32]* %33, i32 11
    %91 = load i32, i32* %90
    %92 = getelementptr [16 x i32], [16 x i32]* %33, i32 12
    %93 = load i32, i32* %92
    %94 = getelementptr [16 x i32], [16 x i32]* %33, i32 13
    %95 = load i32, i32* %94
    %96 = getelementptr [16 x i32], [16 x i32]* %33, i32 14
    %97 = load i32, i32* %96
    %98 = getelementptr [16 x i32], [16 x i32]* %33, i32 15
    %99 = load i32, i32* %98
    %100 = load i32, i32* %17
    %101 = load i32, i32* %18
    %102 = load i32, i32* %19
    %103 = load i32, i32* %20
    %104 = load i32, i32* %21
    %105 = load i32, i32* %22
    %106 = load i32, i32* %23
    %107 = load i32, i32* %24
    %108 = load i32, i32* %25
    %109 = load i32, i32* %26
    %110 = load i32, i32* %27
    %111 = load i32, i32* %28
    %112 = load i32, i32* %29
    %113 = load i32, i32* %30
    %114 = load i32, i32* %31
    %115 = load i32, i32* %32
    %116 = call i32 @param32_rec(i32 %69,i32 %71,i32 %73,i32 %75,i32 %77,i32 %79,i32 %81,i32 %83,i32 %85,i32 %87,i32 %89,i32 %91,i32 %93,i32 %95,i32 %97,i32 %99,i32 %100,i32 %101,i32 %102,i32 %103,i32 %104,i32 %105,i32 %106,i32 %107,i32 %108,i32 %109,i32 %110,i32 %111,i32 %112,i32 %113,i32 %114,i32 %115)
    ret i32 %116
}
define dso_local i32 @main(i32 %0, i32 %1, i32 %2, i32 %3, i32 %4, i32 %5, i32 %6, i32 %7, i32 %8, i32 %9, i32 %10, i32 %11, i32 %12, i32 %13, i32 %14, i32 %15){
16:
    %17 = alloca [32 x [2 x i32]]
    %18 = call i32 @getint()
    %19 = call i32 @getint()
    %20 = call i32 @getint()
    %21 = call i32 @getint()
    %22 = call i32 @getint()
    %23 = call i32 @getint()
    %24 = call i32 @getint()
    %25 = call i32 @getint()
    %26 = call i32 @getint()
    %27 = call i32 @getint()
    %28 = call i32 @getint()
    %29 = call i32 @getint()
    %30 = call i32 @getint()
    %31 = call i32 @getint()
    %32 = call i32 @getint()
    %33 = call i32 @getint()
    %34 = call i32 @param16(i32 %33)
    %35 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 0, i32 0
    store i32 %34, i32* %35
    %36 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 0, i32 1
    store i32 8848, i32* %36
    %37 = alloca i32
    store i32 1, i32* %37
    br %38

38:
    %41 = load i32, i32* %37
    %42 = icmp lt i32 %41,i32 32
    br i32 %42 %39 %40

39:
    %43 = load i32, i32* %37
    %44 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 %43, i32 0
    %45 = load i32, i32* %37
    %46 = load i32, i32* %37
    %47 = sub i32 %46,1
    %48 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 %47, i32 1
    %49 = load i32, i32* %48
    %50 = load i32, i32* %37
    %51 = load i32, i32* %37
    %52 = sub i32 %51,1
    %53 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 %52, i32 1
    %54 = load i32, i32* %53
    %55 = sub i32 %54,1
    store i32 %55, i32* %44
    %56 = load i32, i32* %37
    %57 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 %56, i32 1
    %58 = load i32, i32* %37
    %59 = load i32, i32* %37
    %60 = sub i32 %59,1
    %61 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 %60, i32 0
    %62 = load i32, i32* %61
    %63 = load i32, i32* %37
    %64 = load i32, i32* %37
    %65 = sub i32 %64,1
    %66 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 %65, i32 0
    %67 = load i32, i32* %66
    %68 = sub i32 %67,2
    store i32 %68, i32* %57
    %69 = load i32, i32* %37
    %70 = load i32, i32* %37
    %71 = add i32 %70,1
    store i32 %71, i32* %37
    br %38

40:
    %72 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 0
    %73 = load [2 x i32], [2 x i32]* %72
    %74 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 1
    %75 = load [2 x i32], [2 x i32]* %74
    %76 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 2
    %77 = load [2 x i32], [2 x i32]* %76
    %78 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 3
    %79 = load [2 x i32], [2 x i32]* %78
    %80 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 4
    %81 = load [2 x i32], [2 x i32]* %80
    %82 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 5
    %83 = load [2 x i32], [2 x i32]* %82
    %84 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 6
    %85 = load [2 x i32], [2 x i32]* %84
    %86 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 7
    %87 = load [2 x i32], [2 x i32]* %86
    %88 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 8
    %89 = load [2 x i32], [2 x i32]* %88
    %90 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 9
    %91 = load [2 x i32], [2 x i32]* %90
    %92 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 10
    %93 = load [2 x i32], [2 x i32]* %92
    %94 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 11
    %95 = load [2 x i32], [2 x i32]* %94
    %96 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 12
    %97 = load [2 x i32], [2 x i32]* %96
    %98 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 13
    %99 = load [2 x i32], [2 x i32]* %98
    %100 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 14
    %101 = load [2 x i32], [2 x i32]* %100
    %102 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 15
    %103 = load [2 x i32], [2 x i32]* %102
    %104 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 16
    %105 = load [2 x i32], [2 x i32]* %104
    %106 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 17
    %107 = load [2 x i32], [2 x i32]* %106
    %108 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 18
    %109 = load [2 x i32], [2 x i32]* %108
    %110 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 19
    %111 = load [2 x i32], [2 x i32]* %110
    %112 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 20
    %113 = load [2 x i32], [2 x i32]* %112
    %114 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 21
    %115 = load [2 x i32], [2 x i32]* %114
    %116 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 22
    %117 = load [2 x i32], [2 x i32]* %116
    %118 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 23
    %119 = load [2 x i32], [2 x i32]* %118
    %120 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 24
    %121 = load [2 x i32], [2 x i32]* %120
    %122 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 25
    %123 = load [2 x i32], [2 x i32]* %122
    %124 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 26
    %125 = load [2 x i32], [2 x i32]* %124
    %126 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 27
    %127 = load [2 x i32], [2 x i32]* %126
    %128 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 28
    %129 = load [2 x i32], [2 x i32]* %128
    %130 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 29
    %131 = load [2 x i32], [2 x i32]* %130
    %132 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 30
    %133 = load [2 x i32], [2 x i32]* %132
    %134 = getelementptr [32 x [2 x i32]], [32 x [2 x i32]]* %17, i32 31
    %135 = load [2 x i32], [2 x i32]* %134
    %136 = call i32 @param32_arr([2 x i32] %73,[2 x i32] %75,[2 x i32] %77,[2 x i32] %79,[2 x i32] %81,[2 x i32] %83,[2 x i32] %85,[2 x i32] %87,[2 x i32] %89,[2 x i32] %91,[2 x i32] %93,[2 x i32] %95,[2 x i32] %97,[2 x i32] %99,[2 x i32] %101,[2 x i32] %103,[2 x i32] %105,[2 x i32] %107,[2 x i32] %109,[2 x i32] %111,[2 x i32] %113,[2 x i32] %115,[2 x i32] %117,[2 x i32] %119,[2 x i32] %121,[2 x i32] %123,[2 x i32] %125,[2 x i32] %127,[2 x i32] %129,[2 x i32] %131,[2 x i32] %133,[2 x i32] %135)
    call void @putint([2 x i32] %73,[2 x i32] %75,[2 x i32] %77,[2 x i32] %79,[2 x i32] %81,[2 x i32] %83,[2 x i32] %85,[2 x i32] %87,[2 x i32] %89,[2 x i32] %91,[2 x i32] %93,[2 x i32] %95,[2 x i32] %97,[2 x i32] %99,[2 x i32] %101,[2 x i32] %103,[2 x i32] %105,[2 x i32] %107,[2 x i32] %109,[2 x i32] %111,[2 x i32] %113,[2 x i32] %115,[2 x i32] %117,[2 x i32] %119,[2 x i32] %121,[2 x i32] %123,[2 x i32] %125,[2 x i32] %127,[2 x i32] %129,[2 x i32] %131,[2 x i32] %133,[2 x i32] %135,i32 %136)
    call void @putch(i32 10)
    ret i32 0
}

