declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @main(){
0:
    %1 = alloca i32
    store i32 2, i32* %1
    %2 = alloca i32
    store i32 20, i32* %2
    %3 = alloca [20 x i32]
    %4 = getelementptr [20 x i32], [20 x i32]* %3, i32 0
    store i32 1, i32* %4
    %5 = getelementptr [20 x i32], [20 x i32]* %3, i32 1
    store i32 2, i32* %5
    %6 = alloca i32
    store i32 0, i32* %6
    br %7

7:
    %10 = load i32, i32* %1
    %11 = load i32, i32* %2
    %12 = icmp lt i32 %10,i32 %11
    br i32 %12 %8 %9

8:
    %13 = load i32, i32* %1
    %14 = getelementptr [20 x i32], [20 x i32]* %3, i32 %13
    %15 = load i32, i32* %1
    %16 = getelementptr [20 x i32], [20 x i32]* %3, i32 %15
    %17 = load i32, i32* %16
    %18 = load i32, i32* %1
    %19 = load i32, i32* %1
    %20 = sub i32 %19,1
    %21 = getelementptr [20 x i32], [20 x i32]* %3, i32 %20
    %22 = load i32, i32* %21
    %23 = load i32, i32* %1
    %24 = getelementptr [20 x i32], [20 x i32]* %3, i32 %23
    %25 = load i32, i32* %24
    %26 = load i32, i32* %1
    %27 = load i32, i32* %1
    %28 = sub i32 %27,1
    %29 = getelementptr [20 x i32], [20 x i32]* %3, i32 %28
    %30 = load i32, i32* %29
    %31 = add i32 %25,%30
    %32 = load i32, i32* %1
    %33 = load i32, i32* %1
    %34 = sub i32 %33,2
    %35 = getelementptr [20 x i32], [20 x i32]* %3, i32 %34
    %36 = load i32, i32* %35
    %37 = add i32 %31,%36
    store i32 %37, i32* %14
    %38 = load i32, i32* %6
    %39 = load i32, i32* %1
    %40 = getelementptr [20 x i32], [20 x i32]* %3, i32 %39
    %41 = load i32, i32* %40
    %42 = load i32, i32* %6
    %43 = load i32, i32* %1
    %44 = getelementptr [20 x i32], [20 x i32]* %3, i32 %43
    %45 = load i32, i32* %44
    %46 = add i32 %42,%45
    store i32 %46, i32* %6
    %47 = load i32, i32* %1
    %48 = getelementptr [20 x i32], [20 x i32]* %3, i32 %47
    %49 = load i32, i32* %48
    call void @putint(i32 %49)
    call void @putch(i32 10)
    %50 = load i32, i32* %1
    %51 = load i32, i32* %1
    %52 = add i32 %51,1
    store i32 %52, i32* %1
    br %7

9:
    %53 = load i32, i32* %6
    ret i32 %53
}

