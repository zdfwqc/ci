	.text
	.syntax unified
	.eabi_attribute	67, "2.09"	@ Tag_conformance
	.cpu	cortex-a72
	.eabi_attribute	6, 14	@ Tag_CPU_arch
	.eabi_attribute	7, 65	@ Tag_CPU_arch_profile
	.eabi_attribute	8, 1	@ Tag_ARM_ISA_use
	.eabi_attribute	9, 2	@ Tag_THUMB_ISA_use
	.fpu	crypto-neon-fp-armv8
	.eabi_attribute	12, 3	@ Tag_Advanced_SIMD_arch
	.eabi_attribute	36, 1	@ Tag_FP_HP_extension
	.eabi_attribute	42, 1	@ Tag_MPextension_use
	.eabi_attribute	34, 1	@ Tag_CPU_unaligned_access
	.eabi_attribute	68, 3	@ Tag_Virtualization_use
	.eabi_attribute	17, 1	@ Tag_ABI_PCS_GOT_use
	.eabi_attribute	20, 1	@ Tag_ABI_FP_denormal
	.eabi_attribute	21, 1	@ Tag_ABI_FP_exceptions
	.eabi_attribute	23, 3	@ Tag_ABI_FP_number_model
	.eabi_attribute	24, 1	@ Tag_ABI_align_needed
	.eabi_attribute	25, 1	@ Tag_ABI_align_preserved
	.eabi_attribute	28, 1	@ Tag_ABI_VFP_args
	.eabi_attribute	38, 1	@ Tag_ABI_FP_16bit_format
	.eabi_attribute	14, 0	@ Tag_ABI_PCS_R9_use
	.file	"ayame_ayame.ll"
	.globl	testParam8              @ -- Begin function testParam8
	.p2align	2
	.type	testParam8,%function
	.code	32                      @ @testParam8
testParam8:
	.fnstart
@ %bb.0:
	sub	sp, sp, #16
	str	r0, [sp, #12]
	add	r0, r0, r1
	str	r1, [sp, #8]
	ldr	r1, [sp, #16]
	str	r2, [sp, #4]
	str	r3, [sp]
	add	r0, r0, r2
	add	r0, r0, r3
	add	r0, r0, r1
	ldr	r1, [sp, #20]
	add	r0, r0, r1
	ldr	r1, [sp, #24]
	add	r0, r0, r1
	ldr	r1, [sp, #28]
	add	r0, r0, r1
	add	sp, sp, #16
	bx	lr
.Lfunc_end0:
	.size	testParam8, .Lfunc_end0-testParam8
	.fnend
                                        @ -- End function
	.globl	testParam16             @ -- Begin function testParam16
	.p2align	2
	.type	testParam16,%function
	.code	32                      @ @testParam16
testParam16:
	.fnstart
@ %bb.0:
	sub	sp, sp, #16
	str	r0, [sp, #12]
	add	r0, r0, r1
	str	r1, [sp, #8]
	ldr	r1, [sp, #16]
	str	r2, [sp, #4]
	str	r3, [sp]
	add	r0, r0, r2
	sub	r0, r0, r3
	sub	r0, r0, r1
	ldr	r1, [sp, #20]
	sub	r0, r0, r1
	ldr	r1, [sp, #24]
	sub	r0, r0, r1
	ldr	r1, [sp, #28]
	sub	r0, r0, r1
	ldr	r1, [sp, #32]
	add	r0, r0, r1
	ldr	r1, [sp, #36]
	add	r0, r0, r1
	ldr	r1, [sp, #40]
	add	r0, r0, r1
	ldr	r1, [sp, #44]
	add	r0, r0, r1
	ldr	r1, [sp, #48]
	add	r0, r0, r1
	ldr	r1, [sp, #52]
	add	r0, r0, r1
	ldr	r1, [sp, #56]
	add	r0, r0, r1
	ldr	r1, [sp, #60]
	add	r0, r0, r1
	add	sp, sp, #16
	bx	lr
.Lfunc_end1:
	.size	testParam16, .Lfunc_end1-testParam16
	.fnend
                                        @ -- End function
	.globl	testParam32             @ -- Begin function testParam32
	.p2align	2
	.type	testParam32,%function
	.code	32                      @ @testParam32
testParam32:
	.fnstart
@ %bb.0:
	sub	sp, sp, #16
	str	r0, [sp, #12]
	add	r0, r0, r1
	str	r1, [sp, #8]
	ldr	r1, [sp, #16]
	str	r3, [sp]
	str	r2, [sp, #4]
	add	r0, r0, r2
	add	r0, r0, r3
	add	r0, r0, r1
	ldr	r1, [sp, #20]
	add	r0, r0, r1
	ldr	r1, [sp, #24]
	add	r0, r0, r1
	ldr	r1, [sp, #28]
	add	r0, r0, r1
	ldr	r1, [sp, #32]
	add	r0, r0, r1
	ldr	r1, [sp, #36]
	add	r0, r0, r1
	ldr	r1, [sp, #40]
	add	r0, r0, r1
	ldr	r1, [sp, #44]
	add	r0, r0, r1
	ldr	r1, [sp, #48]
	add	r0, r0, r1
	ldr	r1, [sp, #52]
	add	r0, r0, r1
	ldr	r1, [sp, #56]
	add	r0, r0, r1
	ldr	r1, [sp, #60]
	add	r0, r0, r1
	ldr	r1, [sp, #64]
	add	r0, r0, r1
	ldr	r1, [sp, #68]
	add	r0, r0, r1
	ldr	r1, [sp, #72]
	sub	r0, r0, r1
	ldr	r1, [sp, #76]
	sub	r0, r0, r1
	ldr	r1, [sp, #80]
	sub	r0, r0, r1
	ldr	r1, [sp, #84]
	sub	r0, r0, r1
	ldr	r1, [sp, #88]
	sub	r0, r0, r1
	ldr	r1, [sp, #92]
	add	r0, r0, r1
	ldr	r1, [sp, #96]
	add	r0, r0, r1
	ldr	r1, [sp, #100]
	add	r0, r0, r1
	ldr	r1, [sp, #104]
	add	r0, r0, r1
	ldr	r1, [sp, #108]
	add	r0, r0, r1
	ldr	r1, [sp, #112]
	add	r0, r0, r1
	ldr	r1, [sp, #116]
	add	r0, r0, r1
	ldr	r1, [sp, #120]
	add	r0, r0, r1
	ldr	r1, [sp, #124]
	add	r0, r0, r1
	add	sp, sp, #16
	bx	lr
.Lfunc_end2:
	.size	testParam32, .Lfunc_end2-testParam32
	.fnend
                                        @ -- End function
	.globl	main                    @ -- Begin function main
	.p2align	2
	.type	main,%function
	.code	32                      @ @main
main:
	.fnstart
@ %bb.0:
	push	{r4, r5, r6, r7, r8, r9, r10, r11, lr}
	sub	sp, sp, #188
	movw	r4, :lower16:.L_MergedGlobals
	mov	r1, #4
	mov	r0, #0
	mov	r12, #9
	mov	lr, #8
	mov	r8, #7
	mov	r7, #6
	mov	r6, #5
	mov	r5, #3
	mov	r3, #1
	movt	r4, :upper16:.L_MergedGlobals
	add	r2, r4, #96
	add	r9, r4, #56
	stm	r2, {r1, r6, r7, r8, lr}
	mov	r2, #2
	str	r12, [r4, #116]
	str	r0, [r4, #120]
	str	r12, [r4, #76]
	str	r0, [r4, #80]
	str	r3, [r4, #84]
	str	r5, [r4, #92]
	str	r12, [r4, #36]
	str	r0, [r4, #40]
	str	r3, [r4, #44]
	str	r5, [r4, #52]
	str	r2, [r4, #88]
	str	r2, [r4, #48]
	stm	r9, {r1, r6, r7, r8, lr}
	stm	r4, {r0, r3}
	str	r5, [r4, #12]
	movw	r5, :lower16:.L_MergedGlobals.1
	str	r2, [r4, #8]
	add	r2, r4, #16
	movt	r5, :upper16:.L_MergedGlobals.1
	stm	r2, {r1, r6, r7, r8, lr}
	str	r3, [r5, #32]
	str	r3, [r5]
	stmib	r5, {r1, r6, r7, r8, lr}
	add	lr, r4, #20
	str	r12, [r5, #24]
	str	r0, [r5, #28]
	ldm	r4, {r0, r1, r2, r3, r7}
	str	r7, [sp]
	ldm	lr, {r6, r12, lr}
	stmib	sp, {r6, r12, lr}
	bl	testParam8
	str	r0, [r4]
	bl	putint
	ldr	r0, [r5, #4]
	add	r3, r4, #48
	add	lr, r5, #24
	add	r11, r4, #32
	ldr	r7, [r5, #20]
	ldm	lr, {r6, r12, lr}
	ldm	r11, {r8, r9, r10, r11}
	str	r0, [sp, #184]          @ 4-byte Spill
	ldr	r0, [r5, #8]
	str	r7, [sp]
	str	r0, [sp, #180]          @ 4-byte Spill
	ldr	r0, [r5, #12]
	str	r0, [sp, #176]          @ 4-byte Spill
	ldr	r0, [r5, #16]
	str	r0, [sp, #172]          @ 4-byte Spill
	ldm	r3, {r0, r1, r2, r3}
	str	r1, [sp, #36]
	str	r2, [sp, #40]
	str	r3, [sp, #44]
	ldr	r1, [sp, #180]          @ 4-byte Reload
	ldr	r2, [sp, #176]          @ 4-byte Reload
	ldr	r3, [sp, #172]          @ 4-byte Reload
	str	r0, [sp, #32]
	add	r0, sp, #16
	stmib	sp, {r6, r12, lr}
	stm	r0, {r8, r9, r10, r11}
	ldr	r0, [sp, #184]          @ 4-byte Reload
	bl	testParam16
	str	r0, [r4]
	bl	putint
	ldr	r0, [r4]
	add	r2, r4, #112
	add	r9, r4, #80
	ldr	r10, [r5]
	add	lr, r4, #92
	ldr	r11, [r4, #68]
	ldr	r5, [r4, #104]
	ldr	r6, [r4, #108]
	ldm	r9, {r7, r8, r9}
	ldm	lr, {r3, r12, lr}
	str	r0, [sp, #184]          @ 4-byte Spill
	ldr	r0, [r4, #4]
	str	r10, [sp, #108]
	str	r0, [sp, #180]          @ 4-byte Spill
	ldr	r0, [r4, #8]
	str	r0, [sp, #176]          @ 4-byte Spill
	ldr	r0, [r4, #12]
	str	r0, [sp, #172]          @ 4-byte Spill
	ldr	r0, [r4, #16]
	str	r0, [sp, #168]          @ 4-byte Spill
	ldr	r0, [r4, #20]
	str	r0, [sp, #164]          @ 4-byte Spill
	ldr	r0, [r4, #24]
	str	r0, [sp, #160]          @ 4-byte Spill
	ldr	r0, [r4, #28]
	str	r0, [sp, #156]          @ 4-byte Spill
	ldr	r0, [r4, #32]
	str	r0, [sp, #152]          @ 4-byte Spill
	ldr	r0, [r4, #36]
	str	r0, [sp, #148]          @ 4-byte Spill
	ldr	r0, [r4, #40]
	str	r0, [sp, #144]          @ 4-byte Spill
	ldr	r0, [r4, #44]
	str	r0, [sp, #140]          @ 4-byte Spill
	ldr	r0, [r4, #48]
	str	r0, [sp, #136]          @ 4-byte Spill
	ldr	r0, [r4, #52]
	str	r0, [sp, #132]          @ 4-byte Spill
	ldr	r0, [r4, #56]
	str	r0, [sp, #128]          @ 4-byte Spill
	ldr	r0, [r4, #60]
	str	r0, [sp, #124]          @ 4-byte Spill
	ldr	r0, [r4, #64]
	str	r0, [sp, #120]          @ 4-byte Spill
	ldr	r0, [r4, #72]
	str	r0, [sp, #116]          @ 4-byte Spill
	ldr	r0, [r4, #76]
	str	r0, [sp, #112]          @ 4-byte Spill
	ldm	r2, {r0, r1, r2}
	str	r1, [sp, #100]
	str	r2, [sp, #104]
	ldr	r1, [sp, #180]          @ 4-byte Reload
	ldr	r2, [sp, #176]          @ 4-byte Reload
	str	r0, [sp, #96]
	add	r0, sp, #64
	stm	r0, {r7, r8, r9}
	add	r0, sp, #76
	stm	r0, {r3, r12, lr}
	ldr	r0, [sp, #136]          @ 4-byte Reload
	ldr	r3, [sp, #172]          @ 4-byte Reload
	str	r5, [sp, #88]
	str	r6, [sp, #92]
	str	r11, [sp, #52]
	str	r0, [sp, #32]
	ldr	r0, [sp, #132]          @ 4-byte Reload
	str	r0, [sp, #36]
	ldr	r0, [sp, #128]          @ 4-byte Reload
	str	r0, [sp, #40]
	ldr	r0, [sp, #124]          @ 4-byte Reload
	str	r0, [sp, #44]
	ldr	r0, [sp, #120]          @ 4-byte Reload
	str	r0, [sp, #48]
	ldr	r0, [sp, #116]          @ 4-byte Reload
	str	r0, [sp, #56]
	ldr	r0, [sp, #112]          @ 4-byte Reload
	str	r0, [sp, #60]
	ldr	r0, [sp, #168]          @ 4-byte Reload
	str	r0, [sp]
	ldr	r0, [sp, #164]          @ 4-byte Reload
	str	r0, [sp, #4]
	ldr	r0, [sp, #160]          @ 4-byte Reload
	str	r0, [sp, #8]
	ldr	r0, [sp, #156]          @ 4-byte Reload
	str	r0, [sp, #12]
	ldr	r0, [sp, #152]          @ 4-byte Reload
	str	r0, [sp, #16]
	ldr	r0, [sp, #148]          @ 4-byte Reload
	str	r0, [sp, #20]
	ldr	r0, [sp, #144]          @ 4-byte Reload
	str	r0, [sp, #24]
	ldr	r0, [sp, #140]          @ 4-byte Reload
	str	r0, [sp, #28]
	ldr	r0, [sp, #184]          @ 4-byte Reload
	bl	testParam32
	str	r0, [r4]
	bl	putint
	mov	r0, #0
	add	sp, sp, #188
	pop	{r4, r5, r6, r7, r8, r9, r10, r11, pc}
.Lfunc_end3:
	.size	main, .Lfunc_end3-main
	.fnend
                                        @ -- End function
	.type	.L_MergedGlobals,%object @ @_MergedGlobals
	.local	.L_MergedGlobals
	.comm	.L_MergedGlobals,124,4
	.type	.L_MergedGlobals.1,%object @ @_MergedGlobals.1
	.local	.L_MergedGlobals.1
	.comm	.L_MergedGlobals.1,36,4
	.globl	a0
.set a0, .L_MergedGlobals
	.size	a0, 4
	.globl	a1
.set a1, .L_MergedGlobals+4
	.size	a1, 4
	.globl	a2
.set a2, .L_MergedGlobals+8
	.size	a2, 4
	.globl	a3
.set a3, .L_MergedGlobals+12
	.size	a3, 4
	.globl	a4
.set a4, .L_MergedGlobals+16
	.size	a4, 4
	.globl	a5
.set a5, .L_MergedGlobals+20
	.size	a5, 4
	.globl	a6
.set a6, .L_MergedGlobals+24
	.size	a6, 4
	.globl	a7
.set a7, .L_MergedGlobals+28
	.size	a7, 4
	.globl	a8
.set a8, .L_MergedGlobals+32
	.size	a8, 4
	.globl	a9
.set a9, .L_MergedGlobals+36
	.size	a9, 4
	.globl	a10
.set a10, .L_MergedGlobals+40
	.size	a10, 4
	.globl	a11
.set a11, .L_MergedGlobals+44
	.size	a11, 4
	.globl	a12
.set a12, .L_MergedGlobals+48
	.size	a12, 4
	.globl	a13
.set a13, .L_MergedGlobals+52
	.size	a13, 4
	.globl	a14
.set a14, .L_MergedGlobals+56
	.size	a14, 4
	.globl	a15
.set a15, .L_MergedGlobals+60
	.size	a15, 4
	.globl	a16
.set a16, .L_MergedGlobals+64
	.size	a16, 4
	.globl	a17
.set a17, .L_MergedGlobals+68
	.size	a17, 4
	.globl	a18
.set a18, .L_MergedGlobals+72
	.size	a18, 4
	.globl	a19
.set a19, .L_MergedGlobals+76
	.size	a19, 4
	.globl	a20
.set a20, .L_MergedGlobals+80
	.size	a20, 4
	.globl	a21
.set a21, .L_MergedGlobals+84
	.size	a21, 4
	.globl	a22
.set a22, .L_MergedGlobals+88
	.size	a22, 4
	.globl	a23
.set a23, .L_MergedGlobals+92
	.size	a23, 4
	.globl	a24
.set a24, .L_MergedGlobals+96
	.size	a24, 4
	.globl	a25
.set a25, .L_MergedGlobals+100
	.size	a25, 4
	.globl	a26
.set a26, .L_MergedGlobals+104
	.size	a26, 4
	.globl	a27
.set a27, .L_MergedGlobals+108
	.size	a27, 4
	.globl	a28
.set a28, .L_MergedGlobals+112
	.size	a28, 4
	.globl	a29
.set a29, .L_MergedGlobals+116
	.size	a29, 4
	.globl	a30
.set a30, .L_MergedGlobals+120
	.size	a30, 4
	.globl	a31
.set a31, .L_MergedGlobals.1
	.size	a31, 4
	.globl	a32
.set a32, .L_MergedGlobals.1+4
	.size	a32, 4
	.globl	a33
.set a33, .L_MergedGlobals.1+8
	.size	a33, 4
	.globl	a34
.set a34, .L_MergedGlobals.1+12
	.size	a34, 4
	.globl	a35
.set a35, .L_MergedGlobals.1+16
	.size	a35, 4
	.globl	a36
.set a36, .L_MergedGlobals.1+20
	.size	a36, 4
	.globl	a37
.set a37, .L_MergedGlobals.1+24
	.size	a37, 4
	.globl	a38
.set a38, .L_MergedGlobals.1+28
	.size	a38, 4
	.globl	a39
.set a39, .L_MergedGlobals.1+32
	.size	a39, 4
	.section	".note.GNU-stack","",%progbits
