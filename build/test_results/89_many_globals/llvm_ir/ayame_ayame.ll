@a0 = dso_local global i32 0

@a1 = dso_local global i32 0

@a2 = dso_local global i32 0

@a3 = dso_local global i32 0

@a4 = dso_local global i32 0

@a5 = dso_local global i32 0

@a6 = dso_local global i32 0

@a7 = dso_local global i32 0

@a8 = dso_local global i32 0

@a9 = dso_local global i32 0

@a10 = dso_local global i32 0

@a11 = dso_local global i32 0

@a12 = dso_local global i32 0

@a13 = dso_local global i32 0

@a14 = dso_local global i32 0

@a15 = dso_local global i32 0

@a16 = dso_local global i32 0

@a17 = dso_local global i32 0

@a18 = dso_local global i32 0

@a19 = dso_local global i32 0

@a20 = dso_local global i32 0

@a21 = dso_local global i32 0

@a22 = dso_local global i32 0

@a23 = dso_local global i32 0

@a24 = dso_local global i32 0

@a25 = dso_local global i32 0

@a26 = dso_local global i32 0

@a27 = dso_local global i32 0

@a28 = dso_local global i32 0

@a29 = dso_local global i32 0

@a30 = dso_local global i32 0

@a31 = dso_local global i32 0

@a32 = dso_local global i32 0

@a33 = dso_local global i32 0

@a34 = dso_local global i32 0

@a35 = dso_local global i32 0

@a36 = dso_local global i32 0

@a37 = dso_local global i32 0

@a38 = dso_local global i32 0

@a39 = dso_local global i32 0

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @testParam8(i32 %0, i32 %1, i32 %2, i32 %3, i32 %4, i32 %5, i32 %6, i32 %7){
8:
    %9 = alloca i32
    store i32 %0, i32* %9
    %10 = alloca i32
    store i32 %1, i32* %10
    %11 = alloca i32
    store i32 %2, i32* %11
    %12 = alloca i32
    store i32 %3, i32* %12
    %13 = alloca i32
    store i32 %4, i32* %13
    %14 = alloca i32
    store i32 %5, i32* %14
    %15 = alloca i32
    store i32 %6, i32* %15
    %16 = alloca i32
    store i32 %7, i32* %16
    %17 = load i32, i32* %9
    %18 = load i32, i32* %10
    %19 = load i32, i32* %9
    %20 = load i32, i32* %10
    %21 = add i32 %19,%20
    %22 = load i32, i32* %11
    %23 = add i32 %21,%22
    %24 = load i32, i32* %12
    %25 = add i32 %23,%24
    %26 = load i32, i32* %13
    %27 = add i32 %25,%26
    %28 = load i32, i32* %14
    %29 = add i32 %27,%28
    %30 = load i32, i32* %15
    %31 = add i32 %29,%30
    %32 = load i32, i32* %16
    %33 = add i32 %31,%32
    ret i32 %33
}
define dso_local i32 @testParam16(i32 %0, i32 %1, i32 %2, i32 %3, i32 %4, i32 %5, i32 %6, i32 %7, i32 %8, i32 %9, i32 %10, i32 %11, i32 %12, i32 %13, i32 %14, i32 %15){
16:
    %17 = alloca i32
    store i32 %0, i32* %17
    %18 = alloca i32
    store i32 %1, i32* %18
    %19 = alloca i32
    store i32 %2, i32* %19
    %20 = alloca i32
    store i32 %3, i32* %20
    %21 = alloca i32
    store i32 %4, i32* %21
    %22 = alloca i32
    store i32 %5, i32* %22
    %23 = alloca i32
    store i32 %6, i32* %23
    %24 = alloca i32
    store i32 %7, i32* %24
    %25 = alloca i32
    store i32 %8, i32* %25
    %26 = alloca i32
    store i32 %9, i32* %26
    %27 = alloca i32
    store i32 %10, i32* %27
    %28 = alloca i32
    store i32 %11, i32* %28
    %29 = alloca i32
    store i32 %12, i32* %29
    %30 = alloca i32
    store i32 %13, i32* %30
    %31 = alloca i32
    store i32 %14, i32* %31
    %32 = alloca i32
    store i32 %15, i32* %32
    %33 = load i32, i32* %17
    %34 = load i32, i32* %18
    %35 = load i32, i32* %17
    %36 = load i32, i32* %18
    %37 = add i32 %35,%36
    %38 = load i32, i32* %19
    %39 = add i32 %37,%38
    %40 = load i32, i32* %20
    %41 = sub i32 %39,%40
    %42 = load i32, i32* %21
    %43 = sub i32 %41,%42
    %44 = load i32, i32* %22
    %45 = sub i32 %43,%44
    %46 = load i32, i32* %23
    %47 = sub i32 %45,%46
    %48 = load i32, i32* %24
    %49 = sub i32 %47,%48
    %50 = load i32, i32* %25
    %51 = add i32 %49,%50
    %52 = load i32, i32* %26
    %53 = add i32 %51,%52
    %54 = load i32, i32* %27
    %55 = add i32 %53,%54
    %56 = load i32, i32* %28
    %57 = add i32 %55,%56
    %58 = load i32, i32* %29
    %59 = add i32 %57,%58
    %60 = load i32, i32* %30
    %61 = add i32 %59,%60
    %62 = load i32, i32* %31
    %63 = add i32 %61,%62
    %64 = load i32, i32* %32
    %65 = add i32 %63,%64
    ret i32 %65
}
define dso_local i32 @testParam32(i32 %0, i32 %1, i32 %2, i32 %3, i32 %4, i32 %5, i32 %6, i32 %7, i32 %8, i32 %9, i32 %10, i32 %11, i32 %12, i32 %13, i32 %14, i32 %15, i32 %16, i32 %17, i32 %18, i32 %19, i32 %20, i32 %21, i32 %22, i32 %23, i32 %24, i32 %25, i32 %26, i32 %27, i32 %28, i32 %29, i32 %30, i32 %31){
32:
    %33 = alloca i32
    store i32 %0, i32* %33
    %34 = alloca i32
    store i32 %1, i32* %34
    %35 = alloca i32
    store i32 %2, i32* %35
    %36 = alloca i32
    store i32 %3, i32* %36
    %37 = alloca i32
    store i32 %4, i32* %37
    %38 = alloca i32
    store i32 %5, i32* %38
    %39 = alloca i32
    store i32 %6, i32* %39
    %40 = alloca i32
    store i32 %7, i32* %40
    %41 = alloca i32
    store i32 %8, i32* %41
    %42 = alloca i32
    store i32 %9, i32* %42
    %43 = alloca i32
    store i32 %10, i32* %43
    %44 = alloca i32
    store i32 %11, i32* %44
    %45 = alloca i32
    store i32 %12, i32* %45
    %46 = alloca i32
    store i32 %13, i32* %46
    %47 = alloca i32
    store i32 %14, i32* %47
    %48 = alloca i32
    store i32 %15, i32* %48
    %49 = alloca i32
    store i32 %16, i32* %49
    %50 = alloca i32
    store i32 %17, i32* %50
    %51 = alloca i32
    store i32 %18, i32* %51
    %52 = alloca i32
    store i32 %19, i32* %52
    %53 = alloca i32
    store i32 %20, i32* %53
    %54 = alloca i32
    store i32 %21, i32* %54
    %55 = alloca i32
    store i32 %22, i32* %55
    %56 = alloca i32
    store i32 %23, i32* %56
    %57 = alloca i32
    store i32 %24, i32* %57
    %58 = alloca i32
    store i32 %25, i32* %58
    %59 = alloca i32
    store i32 %26, i32* %59
    %60 = alloca i32
    store i32 %27, i32* %60
    %61 = alloca i32
    store i32 %28, i32* %61
    %62 = alloca i32
    store i32 %29, i32* %62
    %63 = alloca i32
    store i32 %30, i32* %63
    %64 = alloca i32
    store i32 %31, i32* %64
    %65 = load i32, i32* %33
    %66 = load i32, i32* %34
    %67 = load i32, i32* %33
    %68 = load i32, i32* %34
    %69 = add i32 %67,%68
    %70 = load i32, i32* %35
    %71 = add i32 %69,%70
    %72 = load i32, i32* %36
    %73 = add i32 %71,%72
    %74 = load i32, i32* %37
    %75 = add i32 %73,%74
    %76 = load i32, i32* %38
    %77 = add i32 %75,%76
    %78 = load i32, i32* %39
    %79 = add i32 %77,%78
    %80 = load i32, i32* %40
    %81 = add i32 %79,%80
    %82 = load i32, i32* %41
    %83 = add i32 %81,%82
    %84 = load i32, i32* %42
    %85 = add i32 %83,%84
    %86 = load i32, i32* %43
    %87 = add i32 %85,%86
    %88 = load i32, i32* %44
    %89 = add i32 %87,%88
    %90 = load i32, i32* %45
    %91 = add i32 %89,%90
    %92 = load i32, i32* %46
    %93 = add i32 %91,%92
    %94 = load i32, i32* %47
    %95 = add i32 %93,%94
    %96 = load i32, i32* %48
    %97 = add i32 %95,%96
    %98 = load i32, i32* %49
    %99 = add i32 %97,%98
    %100 = load i32, i32* %50
    %101 = add i32 %99,%100
    %102 = load i32, i32* %51
    %103 = sub i32 %101,%102
    %104 = load i32, i32* %52
    %105 = sub i32 %103,%104
    %106 = load i32, i32* %53
    %107 = sub i32 %105,%106
    %108 = load i32, i32* %54
    %109 = sub i32 %107,%108
    %110 = load i32, i32* %55
    %111 = sub i32 %109,%110
    %112 = load i32, i32* %56
    %113 = add i32 %111,%112
    %114 = load i32, i32* %57
    %115 = add i32 %113,%114
    %116 = load i32, i32* %58
    %117 = add i32 %115,%116
    %118 = load i32, i32* %59
    %119 = add i32 %117,%118
    %120 = load i32, i32* %60
    %121 = add i32 %119,%120
    %122 = load i32, i32* %61
    %123 = add i32 %121,%122
    %124 = load i32, i32* %62
    %125 = add i32 %123,%124
    %126 = load i32, i32* %63
    %127 = add i32 %125,%126
    %128 = load i32, i32* %64
    %129 = add i32 %127,%128
    ret i32 %129
}
define dso_local i32 @main(i32 %0, i32 %1, i32 %2, i32 %3, i32 %4, i32 %5, i32 %6, i32 %7, i32 %8, i32 %9, i32 %10, i32 %11, i32 %12, i32 %13, i32 %14, i32 %15, i32 %16, i32 %17, i32 %18, i32 %19, i32 %20, i32 %21, i32 %22, i32 %23, i32 %24, i32 %25, i32 %26, i32 %27, i32 %28, i32 %29, i32 %30, i32 %31){
32:
    store i32 0, i32* @a0
    store i32 1, i32* @a1
    store i32 2, i32* @a2
    store i32 3, i32* @a3
    store i32 4, i32* @a4
    store i32 5, i32* @a5
    store i32 6, i32* @a6
    store i32 7, i32* @a7
    store i32 8, i32* @a8
    store i32 9, i32* @a9
    store i32 0, i32* @a10
    store i32 1, i32* @a11
    store i32 2, i32* @a12
    store i32 3, i32* @a13
    store i32 4, i32* @a14
    store i32 5, i32* @a15
    store i32 6, i32* @a16
    store i32 7, i32* @a17
    store i32 8, i32* @a18
    store i32 9, i32* @a19
    store i32 0, i32* @a20
    store i32 1, i32* @a21
    store i32 2, i32* @a22
    store i32 3, i32* @a23
    store i32 4, i32* @a24
    store i32 5, i32* @a25
    store i32 6, i32* @a26
    store i32 7, i32* @a27
    store i32 8, i32* @a28
    store i32 9, i32* @a29
    store i32 0, i32* @a30
    store i32 1, i32* @a31
    store i32 4, i32* @a32
    store i32 5, i32* @a33
    store i32 6, i32* @a34
    store i32 7, i32* @a35
    store i32 8, i32* @a36
    store i32 9, i32* @a37
    store i32 0, i32* @a38
    store i32 1, i32* @a39
    %33 = load i32, i32* @a0
    %34 = load i32, i32* @a1
    %35 = load i32, i32* @a2
    %36 = load i32, i32* @a3
    %37 = load i32, i32* @a4
    %38 = load i32, i32* @a5
    %39 = load i32, i32* @a6
    %40 = load i32, i32* @a7
    %41 = call i32 @testParam8(i32 %33,i32 %34,i32 %35,i32 %36,i32 %37,i32 %38,i32 %39,i32 %40)
    store i32 %41, i32* @a0
    %42 = load i32, i32* @a0
    call void @putint(i32 %42)
    %43 = load i32, i32* @a32
    %44 = load i32, i32* @a33
    %45 = load i32, i32* @a34
    %46 = load i32, i32* @a35
    %47 = load i32, i32* @a36
    %48 = load i32, i32* @a37
    %49 = load i32, i32* @a38
    %50 = load i32, i32* @a39
    %51 = load i32, i32* @a8
    %52 = load i32, i32* @a9
    %53 = load i32, i32* @a10
    %54 = load i32, i32* @a11
    %55 = load i32, i32* @a12
    %56 = load i32, i32* @a13
    %57 = load i32, i32* @a14
    %58 = load i32, i32* @a15
    %59 = call i32 @testParam16(i32 %43,i32 %44,i32 %45,i32 %46,i32 %47,i32 %48,i32 %49,i32 %50,i32 %51,i32 %52,i32 %53,i32 %54,i32 %55,i32 %56,i32 %57,i32 %58)
    store i32 %59, i32* @a0
    %60 = load i32, i32* @a0
    call void @putint(i32 %60)
    %61 = load i32, i32* @a0
    %62 = load i32, i32* @a1
    %63 = load i32, i32* @a2
    %64 = load i32, i32* @a3
    %65 = load i32, i32* @a4
    %66 = load i32, i32* @a5
    %67 = load i32, i32* @a6
    %68 = load i32, i32* @a7
    %69 = load i32, i32* @a8
    %70 = load i32, i32* @a9
    %71 = load i32, i32* @a10
    %72 = load i32, i32* @a11
    %73 = load i32, i32* @a12
    %74 = load i32, i32* @a13
    %75 = load i32, i32* @a14
    %76 = load i32, i32* @a15
    %77 = load i32, i32* @a16
    %78 = load i32, i32* @a17
    %79 = load i32, i32* @a18
    %80 = load i32, i32* @a19
    %81 = load i32, i32* @a20
    %82 = load i32, i32* @a21
    %83 = load i32, i32* @a22
    %84 = load i32, i32* @a23
    %85 = load i32, i32* @a24
    %86 = load i32, i32* @a25
    %87 = load i32, i32* @a26
    %88 = load i32, i32* @a27
    %89 = load i32, i32* @a28
    %90 = load i32, i32* @a29
    %91 = load i32, i32* @a30
    %92 = load i32, i32* @a31
    %93 = call i32 @testParam32(i32 %61,i32 %62,i32 %63,i32 %64,i32 %65,i32 %66,i32 %67,i32 %68,i32 %69,i32 %70,i32 %71,i32 %72,i32 %73,i32 %74,i32 %75,i32 %76,i32 %77,i32 %78,i32 %79,i32 %80,i32 %81,i32 %82,i32 %83,i32 %84,i32 %85,i32 %86,i32 %87,i32 %88,i32 %89,i32 %90,i32 %91,i32 %92)
    store i32 %93, i32* @a0
    %94 = load i32, i32* @a0
    call void @putint(i32 %94)
    ret i32 0
}

