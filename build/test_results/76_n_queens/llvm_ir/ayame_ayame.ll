@ans = dso_local global [50 x i32] zeroinitializer

@sum = dso_local global i32 0

@n = dso_local global i32 0

@row = dso_local global [50 x i32] zeroinitializer

@line1 = dso_local global [50 x i32] zeroinitializer

@line2 = dso_local global [100 x i32] zeroinitializer

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local void @printans(){
0:
    %1 = load i32, i32* @sum
    %2 = load i32, i32* @sum
    %3 = add i32 %2,1
    store i32 %3, i32* @sum
    %4 = alloca i32
    store i32 1, i32* %4
    br %5

5:
    %8 = load i32, i32* %4
    %9 = load i32, i32* @n
    %10 = icmp le i32 %8,i32 %9
    br i32 %10 %6 %7

6:
    %11 = load i32, i32* %4
    %12 = getelementptr [50 x i32], [50 x i32]* @ans, i32 %11
    %13 = load i32, i32* %12
    call void @putint(i32 %13)
    %17 = load i32, i32* %4
    %18 = load i32, i32* @n
    %19 = icmp eq i32 %17,i32 %18
    br i32 %19 %15 %16

7:

14:
    %20 = load i32, i32* %4
    %21 = load i32, i32* %4
    %22 = add i32 %21,1
    store i32 %22, i32* %4
    br %5

15:
    call void @putch(i32 10)
    br %14

16:
    call void @putch(i32 32)
    br %14
}
define dso_local void @f(i32 %0){
1:
    %2 = alloca i32
    store i32 %0, i32* %2
    %3 = alloca i32
    store i32 1, i32* %3
    br %4

4:
    %7 = load i32, i32* %3
    %8 = load i32, i32* @n
    %9 = icmp le i32 %7,i32 %8
    br i32 %9 %5 %6

5:
    %12 = load i32, i32* %3
    %13 = getelementptr [50 x i32], [50 x i32]* @row, i32 %12
    %14 = load i32, i32* %13
    %15 = icmp ne i32 %14,i32 1
    %16 = load i32, i32* %2
    %17 = load i32, i32* %3
    %18 = load i32, i32* %2
    %19 = load i32, i32* %3
    %20 = add i32 %18,%19
    %21 = getelementptr [50 x i32], [50 x i32]* @line1, i32 %20
    %22 = load i32, i32* %21
    %23 = icmp eq i32 %22,i32 0
    %24 = and i32 %15,%23
    %25 = load i32, i32* @n
    %26 = load i32, i32* %2
    %27 = load i32, i32* @n
    %28 = load i32, i32* %2
    %29 = add i32 %27,%28
    %30 = load i32, i32* %3
    %31 = sub i32 %29,%30
    %32 = getelementptr [100 x i32], [100 x i32]* @line2, i32 %31
    %33 = load i32, i32* %32
    
    %35 = and i32 %24,%34
    br i32 %35 %11 %10

6:

10:
    %81 = load i32, i32* %3
    %82 = load i32, i32* %3
    %83 = add i32 %82,1
    store i32 %83, i32* %3
    br %4

11:
    %36 = load i32, i32* %2
    %37 = getelementptr [50 x i32], [50 x i32]* @ans, i32 %36
    %38 = load i32, i32* %3
    store i32 %38, i32* %37
    %41 = load i32, i32* %2
    %42 = load i32, i32* @n
    %43 = icmp eq i32 %41,i32 %42
    br i32 %43 %40 %39

39:
    %45 = load i32, i32* %3
    %46 = getelementptr [50 x i32], [50 x i32]* @row, i32 %45
    store i32 1, i32* %46
    %47 = load i32, i32* %2
    %48 = load i32, i32* %3
    %49 = load i32, i32* %2
    %50 = load i32, i32* %3
    %51 = add i32 %49,%50
    %52 = getelementptr [50 x i32], [50 x i32]* @line1, i32 %51
    store i32 1, i32* %52
    %53 = load i32, i32* @n
    %54 = load i32, i32* %2
    %55 = load i32, i32* @n
    %56 = load i32, i32* %2
    %57 = add i32 %55,%56
    %58 = load i32, i32* %3
    %59 = sub i32 %57,%58
    %60 = getelementptr [100 x i32], [100 x i32]* @line2, i32 %59
    store i32 1, i32* %60
    %61 = load i32, i32* %2
    %62 = load i32, i32* %2
    %63 = add i32 %62,1
    %64 = call void @f(i32 %63)
    %65 = load i32, i32* %3
    %66 = getelementptr [50 x i32], [50 x i32]* @row, i32 %65
    store i32 0, i32* %66
    %67 = load i32, i32* %2
    %68 = load i32, i32* %3
    %69 = load i32, i32* %2
    %70 = load i32, i32* %3
    %71 = add i32 %69,%70
    %72 = getelementptr [50 x i32], [50 x i32]* @line1, i32 %71
    store i32 0, i32* %72
    %73 = load i32, i32* @n
    %74 = load i32, i32* %2
    %75 = load i32, i32* @n
    %76 = load i32, i32* %2
    %77 = add i32 %75,%76
    %78 = load i32, i32* %3
    %79 = sub i32 %77,%78
    %80 = getelementptr [100 x i32], [100 x i32]* @line2, i32 %79
    store i32 0, i32* %80

40:
    %44 = call void @printans()
    br %39
}
define dso_local i32 @main(i32 %0){
1:
    %2 = call i32 @getint()
    %3 = alloca i32
    store i32 %2, i32* %3
    br %4

4:
    %7 = load i32, i32* %3
    %8 = icmp gt i32 %7,i32 0
    br i32 %8 %5 %6

5:
    %9 = call i32 @getint()
    store i32 %9, i32* @n
    %10 = call void @f(i32 1)
    %11 = load i32, i32* %3
    %12 = load i32, i32* %3
    %13 = sub i32 %12,1
    store i32 %13, i32* %3
    br %4

6:
    %14 = load i32, i32* @sum
    ret i32 %14
}

