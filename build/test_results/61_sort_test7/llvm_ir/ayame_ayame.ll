@buf = dso_local global [2 x [100 x i32]] zeroinitializer

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local void @merge_sort(i32 %0, i32 %1){
2:
    %3 = alloca i32
    store i32 %0, i32* %3
    %4 = alloca i32
    store i32 %1, i32* %4
    %7 = load i32, i32* %3
    %8 = load i32, i32* %3
    %9 = add i32 %8,1
    %10 = load i32, i32* %4
    %11 = icmp ge i32 %9,i32 %10
    br i32 %11 %6 %5

5:
    %12 = load i32, i32* %3
    %13 = load i32, i32* %4
    %14 = load i32, i32* %3
    %15 = load i32, i32* %4
    %16 = add i32 %14,%15
    %17 = sdiv i32 %16,2
    %18 = alloca i32
    store i32 %17, i32* %18
    %19 = load i32, i32* %3
    %20 = load i32, i32* %18
    %21 = call void @merge_sort(i32 %19,i32 %20)
    %22 = load i32, i32* %18
    %23 = load i32, i32* %4
    %24 = call void @merge_sort(i32 %22,i32 %23)
    %25 = load i32, i32* %3
    %26 = alloca i32
    store i32 %25, i32* %26
    %27 = load i32, i32* %18
    %28 = alloca i32
    store i32 %27, i32* %28
    %29 = load i32, i32* %3
    %30 = alloca i32
    store i32 %29, i32* %30
    br %31

6:
    br %5

31:
    %34 = load i32, i32* %26
    %35 = load i32, i32* %18
    %36 = icmp lt i32 %34,i32 %35
    %37 = load i32, i32* %28
    %38 = load i32, i32* %4
    %39 = icmp lt i32 %37,i32 %38
    %40 = and i32 %36,%39
    br i32 %40 %32 %33

32:
    %44 = load i32, i32* %26
    %45 = getelementptr [2 x [100 x i32]], [2 x [100 x i32]]* @buf, i32 0, i32 %44
    %46 = load i32, i32* %45
    %47 = load i32, i32* %28
    %48 = getelementptr [2 x [100 x i32]], [2 x [100 x i32]]* @buf, i32 0, i32 %47
    %49 = load i32, i32* %48
    %50 = icmp lt i32 %46,i32 %49
    br i32 %50 %42 %43

33:
    br %70

41:
    %67 = load i32, i32* %30
    %68 = load i32, i32* %30
    %69 = add i32 %68,1
    store i32 %69, i32* %30
    br %31

42:
    %51 = load i32, i32* %30
    %52 = getelementptr [2 x [100 x i32]], [2 x [100 x i32]]* @buf, i32 1, i32 %51
    %53 = load i32, i32* %26
    %54 = getelementptr [2 x [100 x i32]], [2 x [100 x i32]]* @buf, i32 0, i32 %53
    %55 = load i32, i32* %54
    store i32 %55, i32* %52
    %56 = load i32, i32* %26
    %57 = load i32, i32* %26
    %58 = add i32 %57,1
    store i32 %58, i32* %26
    br %41

43:
    %59 = load i32, i32* %30
    %60 = getelementptr [2 x [100 x i32]], [2 x [100 x i32]]* @buf, i32 1, i32 %59
    %61 = load i32, i32* %28
    %62 = getelementptr [2 x [100 x i32]], [2 x [100 x i32]]* @buf, i32 0, i32 %61
    %63 = load i32, i32* %62
    store i32 %63, i32* %60
    %64 = load i32, i32* %28
    %65 = load i32, i32* %28
    %66 = add i32 %65,1
    store i32 %66, i32* %28
    br %41

70:
    %73 = load i32, i32* %26
    %74 = load i32, i32* %18
    %75 = icmp lt i32 %73,i32 %74
    br i32 %75 %71 %72

71:
    %76 = load i32, i32* %30
    %77 = getelementptr [2 x [100 x i32]], [2 x [100 x i32]]* @buf, i32 1, i32 %76
    %78 = load i32, i32* %26
    %79 = getelementptr [2 x [100 x i32]], [2 x [100 x i32]]* @buf, i32 0, i32 %78
    %80 = load i32, i32* %79
    store i32 %80, i32* %77
    %81 = load i32, i32* %26
    %82 = load i32, i32* %26
    %83 = add i32 %82,1
    store i32 %83, i32* %26
    %84 = load i32, i32* %30
    %85 = load i32, i32* %30
    %86 = add i32 %85,1
    store i32 %86, i32* %30
    br %70

72:
    br %87

87:
    %90 = load i32, i32* %28
    %91 = load i32, i32* %4
    %92 = icmp lt i32 %90,i32 %91
    br i32 %92 %88 %89

88:
    %93 = load i32, i32* %30
    %94 = getelementptr [2 x [100 x i32]], [2 x [100 x i32]]* @buf, i32 1, i32 %93
    %95 = load i32, i32* %28
    %96 = getelementptr [2 x [100 x i32]], [2 x [100 x i32]]* @buf, i32 0, i32 %95
    %97 = load i32, i32* %96
    store i32 %97, i32* %94
    %98 = load i32, i32* %28
    %99 = load i32, i32* %28
    %100 = add i32 %99,1
    store i32 %100, i32* %28
    %101 = load i32, i32* %30
    %102 = load i32, i32* %30
    %103 = add i32 %102,1
    store i32 %103, i32* %30
    br %87

89:
    br %104

104:
    %107 = load i32, i32* %3
    %108 = load i32, i32* %4
    %109 = icmp lt i32 %107,i32 %108
    br i32 %109 %105 %106

105:
    %110 = load i32, i32* %3
    %111 = getelementptr [2 x [100 x i32]], [2 x [100 x i32]]* @buf, i32 0, i32 %110
    %112 = load i32, i32* %3
    %113 = getelementptr [2 x [100 x i32]], [2 x [100 x i32]]* @buf, i32 1, i32 %112
    %114 = load i32, i32* %113
    store i32 %114, i32* %111
    %115 = load i32, i32* %3
    %116 = load i32, i32* %3
    %117 = add i32 %116,1
    store i32 %117, i32* %3
    br %104

106:
}
define dso_local i32 @main(i32 %0, i32 %1){
2:
    %3 = getelementptr [2 x [100 x i32]], [2 x [100 x i32]]* @buf, i32 0
    %4 = load [100 x i32], [100 x i32]* %3
    %5 = call i32 @getarray([100 x i32] %4)
    %6 = alloca i32
    store i32 %5, i32* %6
    %7 = load i32, i32* %6
    %8 = call void @merge_sort(i32 0,i32 %7)
    %9 = load i32, i32* %6
    %10 = getelementptr [2 x [100 x i32]], [2 x [100 x i32]]* @buf, i32 0
    %11 = load [100 x i32], [100 x i32]* %10
    call void @putarray(i32 %9,[100 x i32] %11)
    ret i32 0
}

