declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local void @move(i32 %0, i32 %1){
2:
    %3 = alloca i32
    store i32 %0, i32* %3
    %4 = alloca i32
    store i32 %1, i32* %4
    %5 = load i32, i32* %3
    call void @putint(i32 %5)
    call void @putch(i32 32)
    %6 = load i32, i32* %4
    call void @putint(i32 %6)
    call void @putch(i32 44)
    call void @putch(i32 32)
}
define dso_local void @hanoi(i32 %0, i32 %1, i32 %2, i32 %3){
4:
    %5 = alloca i32
    store i32 %0, i32* %5
    %6 = alloca i32
    store i32 %1, i32* %6
    %7 = alloca i32
    store i32 %2, i32* %7
    %8 = alloca i32
    store i32 %3, i32* %8
    %12 = load i32, i32* %5
    %13 = icmp eq i32 %12,i32 1
    br i32 %13 %10 %11

9:

10:
    %14 = load i32, i32* %6
    %15 = load i32, i32* %8
    %16 = call void @move(i32 %14,i32 %15)
    br %9

11:
    %17 = load i32, i32* %5
    %18 = load i32, i32* %5
    %19 = sub i32 %18,1
    %20 = load i32, i32* %6
    %21 = load i32, i32* %8
    %22 = load i32, i32* %7
    %23 = call void @hanoi(i32 %19,i32 %20,i32 %21,i32 %22)
    %24 = load i32, i32* %6
    %25 = load i32, i32* %8
    %26 = call void @move(i32 %24,i32 %25)
    %27 = load i32, i32* %5
    %28 = load i32, i32* %5
    %29 = sub i32 %28,1
    %30 = load i32, i32* %7
    %31 = load i32, i32* %6
    %32 = load i32, i32* %8
    %33 = call void @hanoi(i32 %29,i32 %30,i32 %31,i32 %32)
    br %9
}
define dso_local i32 @main(i32 %0, i32 %1, i32 %2, i32 %3){
4:
    %5 = call i32 @getint()
    %6 = alloca i32
    store i32 %5, i32* %6
    br %7

7:
    %10 = load i32, i32* %6
    %11 = icmp gt i32 %10,i32 0
    br i32 %11 %8 %9

8:
    %12 = call i32 @getint()
    %13 = call void @hanoi(i32 %12,i32 1,i32 2,i32 3)
    call void @putch(i32 10)
    %14 = load i32, i32* %6
    %15 = load i32, i32* %6
    %16 = sub i32 %15,1
    store i32 %16, i32* %6
    br %7

9:
    ret i32 0
}

