declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @whileIf(){
0:
    %1 = alloca i32
    store i32 0, i32* %1
    %2 = alloca i32
    store i32 0, i32* %2
    br %3

3:
    %6 = load i32, i32* %1
    %7 = icmp lt i32 %6,i32 100
    br i32 %7 %4 %5

4:
    %11 = load i32, i32* %1
    %12 = icmp eq i32 %11,i32 5
    br i32 %12 %9 %10

5:
    %23 = load i32, i32* %2
    ret i32 %23

8:
    %20 = load i32, i32* %1
    %21 = load i32, i32* %1
    %22 = add i32 %21,1
    store i32 %22, i32* %1
    br %3

9:
    store i32 25, i32* %2
    br %8

10:
    %16 = load i32, i32* %1
    %17 = icmp eq i32 %16,i32 10
    br i32 %17 %14 %15

13:

14:
    store i32 42, i32* %2
    br %13

15:
    %18 = load i32, i32* %1
    %19 = mul i32 %18,2
    store i32 %19, i32* %2
    br %13
}
define dso_local i32 @main(){
0:
    %1 = call i32 @whileIf()
    ret i32 %1
}

