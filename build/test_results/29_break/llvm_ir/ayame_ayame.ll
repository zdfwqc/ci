declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @main(){
0:
    %1 = alloca i32
    store i32 0, i32* %1
    %2 = alloca i32
    store i32 0, i32* %2
    br %3

3:
    %6 = load i32, i32* %1
    %7 = icmp lt i32 %6,i32 100
    br i32 %7 %4 %5

4:
    %10 = load i32, i32* %1
    %11 = icmp eq i32 %10,i32 50
    br i32 %11 %9 %8

5:
    %20 = load i32, i32* %2
    ret i32 %20

8:
    %12 = load i32, i32* %2
    %13 = load i32, i32* %1
    %14 = load i32, i32* %2
    %15 = load i32, i32* %1
    %16 = add i32 %14,%15
    store i32 %16, i32* %2
    %17 = load i32, i32* %1
    %18 = load i32, i32* %1
    %19 = add i32 %18,1
    store i32 %19, i32* %1
    br %3

9:
    br %5
}

