declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @main(){
0:
    %1 = alloca [100 x i32]
    %2 = alloca i32
    store i32 0, i32* %2
    %3 = alloca i32
    store i32 0, i32* %3
    br %4

4:
    %7 = call i32 @getint()
    br i32 %7 %5 %6

5:
    %8 = load i32, i32* %2
    %9 = getelementptr [100 x i32], [100 x i32]* %1, i32 %8
    %10 = call i32 @getint()
    store i32 %10, i32* %9
    %11 = load i32, i32* %2
    %12 = load i32, i32* %2
    %13 = add i32 %12,1
    store i32 %13, i32* %2
    br %4

6:
    br %14

14:
    %17 = load i32, i32* %2
    br i32 %17 %15 %16

15:
    %18 = load i32, i32* %2
    %19 = load i32, i32* %2
    %20 = sub i32 %19,1
    store i32 %20, i32* %2
    %21 = load i32, i32* %3
    %22 = load i32, i32* %2
    %23 = getelementptr [100 x i32], [100 x i32]* %1, i32 %22
    %24 = load i32, i32* %23
    %25 = load i32, i32* %3
    %26 = load i32, i32* %2
    %27 = getelementptr [100 x i32], [100 x i32]* %1, i32 %26
    %28 = load i32, i32* %27
    %29 = add i32 %25,%28
    store i32 %29, i32* %3
    br %14

16:
    %30 = load i32, i32* %3
    %31 = srem i32 %30,79
    ret i32 %31
}

