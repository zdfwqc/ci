declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @main(){
0:
    %1 = alloca i32
    %2 = alloca i32
    %3 = alloca i32
    %4 = alloca i32
    %5 = alloca i32
    store i32 5, i32* %1
    store i32 5, i32* %2
    store i32 1, i32* %3
    %6 = sub i32 0,2
    store i32 %6, i32* %4
    %7 = load i32, i32* %4
    %8 = mul i32 %7,1
    %9 = sdiv i32 %8,2
    %10 = load i32, i32* %1
    %11 = load i32, i32* %2
    %12 = load i32, i32* %1
    %13 = load i32, i32* %2
    %14 = sub i32 %12,%13
    %15 = load i32, i32* %4
    %16 = mul i32 %15,1
    %17 = sdiv i32 %16,2
    %18 = load i32, i32* %1
    %19 = load i32, i32* %2
    %20 = load i32, i32* %1
    %21 = load i32, i32* %2
    %22 = sub i32 %20,%21
    %23 = add i32 %17,%22
    %24 = load i32, i32* %3
    %25 = load i32, i32* %3
    %26 = add i32 %25,3
    %27 = sub i32 0,%26
    %28 = srem i32 %27,2
    %29 = sub i32 %23,%28
    store i32 %29, i32* %5
    %30 = load i32, i32* %5
    call void @putint(i32 %30)
    %31 = load i32, i32* %4
    %32 = srem i32 %31,2
    %33 = load i32, i32* %4
    %34 = srem i32 %33,2
    %35 = add i32 %34,67
    %36 = load i32, i32* %1
    %37 = load i32, i32* %2
    %38 = load i32, i32* %1
    %39 = load i32, i32* %2
    %40 = sub i32 %38,%39
    %41 = sub i32 0,%40
    %42 = load i32, i32* %4
    %43 = srem i32 %42,2
    %44 = load i32, i32* %4
    %45 = srem i32 %44,2
    %46 = add i32 %45,67
    %47 = load i32, i32* %1
    %48 = load i32, i32* %2
    %49 = load i32, i32* %1
    %50 = load i32, i32* %2
    %51 = sub i32 %49,%50
    %52 = sub i32 0,%51
    %53 = add i32 %46,%52
    %54 = load i32, i32* %3
    %55 = load i32, i32* %3
    %56 = add i32 %55,2
    %57 = srem i32 %56,2
    %58 = sub i32 0,%57
    %59 = sub i32 %53,%58
    store i32 %59, i32* %5
    %60 = load i32, i32* %5
    %61 = load i32, i32* %5
    %62 = add i32 %61,3
    store i32 %62, i32* %5
    %63 = load i32, i32* %5
    call void @putint(i32 %63)
    ret i32 0
}

