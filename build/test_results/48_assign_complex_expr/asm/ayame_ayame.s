	.text
	.syntax unified
	.eabi_attribute	67, "2.09"	@ Tag_conformance
	.cpu	cortex-a72
	.eabi_attribute	6, 14	@ Tag_CPU_arch
	.eabi_attribute	7, 65	@ Tag_CPU_arch_profile
	.eabi_attribute	8, 1	@ Tag_ARM_ISA_use
	.eabi_attribute	9, 2	@ Tag_THUMB_ISA_use
	.fpu	crypto-neon-fp-armv8
	.eabi_attribute	12, 3	@ Tag_Advanced_SIMD_arch
	.eabi_attribute	36, 1	@ Tag_FP_HP_extension
	.eabi_attribute	42, 1	@ Tag_MPextension_use
	.eabi_attribute	34, 1	@ Tag_CPU_unaligned_access
	.eabi_attribute	68, 3	@ Tag_Virtualization_use
	.eabi_attribute	17, 1	@ Tag_ABI_PCS_GOT_use
	.eabi_attribute	20, 1	@ Tag_ABI_FP_denormal
	.eabi_attribute	21, 1	@ Tag_ABI_FP_exceptions
	.eabi_attribute	23, 3	@ Tag_ABI_FP_number_model
	.eabi_attribute	24, 1	@ Tag_ABI_align_needed
	.eabi_attribute	25, 1	@ Tag_ABI_align_preserved
	.eabi_attribute	28, 1	@ Tag_ABI_VFP_args
	.eabi_attribute	38, 1	@ Tag_ABI_FP_16bit_format
	.eabi_attribute	14, 0	@ Tag_ABI_PCS_R9_use
	.file	"ayame_ayame.ll"
	.globl	main                    @ -- Begin function main
	.p2align	2
	.type	main,%function
	.code	32                      @ @main
main:
	.fnstart
@ %bb.0:
	push	{r11, lr}
	sub	sp, sp, #24
	mov	r0, #5
	str	r0, [sp, #16]
	str	r0, [sp, #20]
	mov	r0, #1
	str	r0, [sp, #12]
	mvn	r0, #1
	str	r0, [sp, #8]
	mvn	r0, #0
	str	r0, [sp, #4]
	mvn	r0, #0
	bl	putint
	ldr	r0, [sp, #20]
	ldr	r1, [sp, #16]
	sub	r0, r1, r0
	ldr	r1, [sp, #8]
	add	r2, r1, r1, lsr #31
	bic	r2, r2, #1
	sub	r1, r1, r2
	add	r0, r1, r0
	ldr	r1, [sp, #12]
	add	r1, r1, #2
	add	r2, r1, r1, lsr #31
	bic	r2, r2, #1
	sub	r1, r1, r2
	add	r0, r0, r1
	add	r0, r0, #70
	str	r0, [sp, #4]
	bl	putint
	mov	r0, #0
	add	sp, sp, #24
	pop	{r11, pc}
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.fnend
                                        @ -- End function
	.section	".note.GNU-stack","",%progbits
