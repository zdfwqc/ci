@b = dso_local global i32 5

@c = dso_local global [i32 6, i32 7, i32 8, i32 9]

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @main(){
0:
    %1 = alloca i32
    store i32 1, i32* %1
    %2 = alloca i32
    store i32 2, i32* %2
    store i32 3, i32* %2
    %3 = load i32, i32* %2
    call void @putint(i32 %3)
    %4 = load i32, i32* %2
    call void @putint(i32 %4)
    %5 = load i32, i32* %1
    call void @putint(i32 %5)
    call void @putch(i32 10)
    br %6

6:
    %9 = load i32, i32* %1
    %10 = icmp lt i32 %9,i32 5
    br i32 %10 %7 %8

7:
    %11 = alloca i32
    store i32 0, i32* %11
    %12 = load i32, i32* %11
    %13 = load i32, i32* %11
    %14 = add i32 %13,1
    store i32 %14, i32* %11
    %17 = load i32, i32* %11
    br i32 %17 %16 %15

8:
    %18 = load i32, i32* %1
    call void @putint(i32 %18)
    call void @putch(i32 10)
    %19 = getelementptr [4 x i32], [4 x i32]* @c, i32 2
    store i32 1, i32* %19
    %20 = alloca [2 x [8 x i32]]
    %21 = getelementptr [2 x [8 x i32]], [2 x [8 x i32]]* %20, i32 0, i32 0
    store i32 0, i32* %21
    %22 = getelementptr [2 x [8 x i32]], [2 x [8 x i32]]* %20, i32 0, i32 1
    store i32 9, i32* %22
    %23 = getelementptr [2 x [8 x i32]], [2 x [8 x i32]]* %20, i32 1
    store i32 8, [8 x i32]* %23
    %24 = getelementptr [2 x [8 x i32]], [2 x [8 x i32]]* %20, i32 2
    store i32 3, [8 x i32]* %24
    %25 = alloca i32
    store i32 2, i32* %25
    %28 = getelementptr [4 x i32], [4 x i32]* @c, i32 2
    %29 = load i32, i32* %28
    br i32 %29 %27 %26

15:
    br %6

16:
    br %8

26:
    call void @putch(i32 10)
    %43 = load i32, i32* @b
    call void @putint(i32 %43)
    call void @putch(i32 10)
    %44 = getelementptr [4 x i32], [4 x i32]* @c, i32 0
    %45 = load i32, i32* %44
    call void @putint(i32 %45)
    %46 = getelementptr [4 x i32], [4 x i32]* @c, i32 1
    %47 = load i32, i32* %46
    call void @putint(i32 %47)
    %48 = getelementptr [4 x i32], [4 x i32]* @c, i32 2
    %49 = load i32, i32* %48
    call void @putint(i32 %49)
    %50 = getelementptr [4 x i32], [4 x i32]* @c, i32 3
    %51 = load i32, i32* %50
    call void @putint(i32 %51)
    call void @putch(i32 10)
    ret i32 0

27:
    %30 = alloca [7 x [1 x [5 x i32]]]
    %31 = getelementptr [7 x [1 x [5 x i32]]], [7 x [1 x [5 x i32]]]* %30, i32 2, i32 0
    store i32 2, [5 x i32]* %31
    %32 = getelementptr [7 x [1 x [5 x i32]]], [7 x [1 x [5 x i32]]]* %30, i32 2, i32 1
    store i32 1, [5 x i32]* %32
    %33 = getelementptr [7 x [1 x [5 x i32]]], [7 x [1 x [5 x i32]]]* %30, i32 2, i32 2
    store i32 8, [5 x i32]* %33
    %34 = load i32, i32* %25
    %35 = getelementptr [7 x [1 x [5 x i32]]], [7 x [1 x [5 x i32]]]* %30, i32 %34, i32 0, i32 0
    %36 = load i32, i32* %35
    call void @putint(i32 %36)
    %37 = load i32, i32* %25
    %38 = getelementptr [7 x [1 x [5 x i32]]], [7 x [1 x [5 x i32]]]* %30, i32 %37, i32 0, i32 1
    %39 = load i32, i32* %38
    call void @putint(i32 %39)
    %40 = load i32, i32* %25
    %41 = getelementptr [7 x [1 x [5 x i32]]], [7 x [1 x [5 x i32]]]* %30, i32 %40, i32 0, i32 2
    %42 = load i32, i32* %41
    call void @putint(i32 %42)
    br %26
}

