declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local void @reverse(i32 %0){
1:
    %2 = alloca i32
    store i32 %0, i32* %2
    %3 = alloca i32
    %7 = load i32, i32* %2
    %8 = icmp le i32 %7,i32 1
    br i32 %8 %5 %6

4:

5:
    %9 = call i32 @getint()
    store i32 %9, i32* %3
    %10 = load i32, i32* %3
    call void @putint(i32 %10)
    br %4

6:
    %11 = call i32 @getint()
    store i32 %11, i32* %3
    %12 = load i32, i32* %2
    %13 = load i32, i32* %2
    %14 = sub i32 %13,1
    %15 = call void @reverse(i32 %14)
    %16 = load i32, i32* %3
    call void @putint(i32 %16)
    br %4
}
define dso_local i32 @main(i32 %0){
1:
    %2 = alloca i32
    store i32 200, i32* %2
    %3 = load i32, i32* %2
    %4 = call void @reverse(i32 %3)
    ret i32 0
}

