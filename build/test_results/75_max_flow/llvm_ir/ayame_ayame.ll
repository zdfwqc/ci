@INF = dso_local constant i32 1879048192

@size = dso_local global [10 x i32] zeroinitializer

@to = dso_local global [10 x [10 x i32]] zeroinitializer

@cap = dso_local global [10 x [10 x i32]] zeroinitializer

@rev = dso_local global [10 x [10 x i32]] zeroinitializer

@used = dso_local global [10 x i32] zeroinitializer

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local void @my_memset(i32* %0, i32 %1, i32 %2){
3:
    %4 = alloca i32*
    store i32* %0, i32** %4
    %5 = alloca i32
    store i32 %1, i32* %5
    %6 = alloca i32
    store i32 %2, i32* %6
    %7 = alloca i32
    store i32 0, i32* %7
    br %8

8:
    %11 = load i32, i32* %7
    %12 = load i32, i32* %6
    %13 = icmp lt i32 %11,i32 %12
    br i32 %13 %9 %10

9:
    %14 = load i32, i32* %7
    %15 = getelementptr i32*, i32** %4, i32 %14
    %16 = load i32, i32* %5
    store i32 %16, i32** %15
    %17 = load i32, i32* %7
    %18 = load i32, i32* %7
    %19 = add i32 %18,1
    store i32 %19, i32* %7
    br %8

10:
}
define dso_local void @add_node(i32 %0, i32 %1, i32 %2){
3:
    %4 = alloca i32
    store i32 %0, i32* %4
    %5 = alloca i32
    store i32 %1, i32* %5
    %6 = alloca i32
    store i32 %2, i32* %6
    %7 = load i32, i32* %4
    %8 = load i32, i32* %4
    %9 = getelementptr [10 x i32], [10 x i32]* @size, i32 %8
    %10 = load i32, i32* %9
    %11 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* @to, i32 %7, i32 %10
    %12 = load i32, i32* %5
    store i32 %12, i32* %11
    %13 = load i32, i32* %4
    %14 = load i32, i32* %4
    %15 = getelementptr [10 x i32], [10 x i32]* @size, i32 %14
    %16 = load i32, i32* %15
    %17 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* @cap, i32 %13, i32 %16
    %18 = load i32, i32* %6
    store i32 %18, i32* %17
    %19 = load i32, i32* %4
    %20 = load i32, i32* %4
    %21 = getelementptr [10 x i32], [10 x i32]* @size, i32 %20
    %22 = load i32, i32* %21
    %23 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* @rev, i32 %19, i32 %22
    %24 = load i32, i32* %5
    %25 = getelementptr [10 x i32], [10 x i32]* @size, i32 %24
    %26 = load i32, i32* %25
    store i32 %26, i32* %23
    %27 = load i32, i32* %5
    %28 = load i32, i32* %5
    %29 = getelementptr [10 x i32], [10 x i32]* @size, i32 %28
    %30 = load i32, i32* %29
    %31 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* @to, i32 %27, i32 %30
    %32 = load i32, i32* %4
    store i32 %32, i32* %31
    %33 = load i32, i32* %5
    %34 = load i32, i32* %5
    %35 = getelementptr [10 x i32], [10 x i32]* @size, i32 %34
    %36 = load i32, i32* %35
    %37 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* @cap, i32 %33, i32 %36
    store i32 0, i32* %37
    %38 = load i32, i32* %5
    %39 = load i32, i32* %5
    %40 = getelementptr [10 x i32], [10 x i32]* @size, i32 %39
    %41 = load i32, i32* %40
    %42 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* @rev, i32 %38, i32 %41
    %43 = load i32, i32* %4
    %44 = getelementptr [10 x i32], [10 x i32]* @size, i32 %43
    %45 = load i32, i32* %44
    store i32 %45, i32* %42
    %46 = load i32, i32* %4
    %47 = getelementptr [10 x i32], [10 x i32]* @size, i32 %46
    %48 = load i32, i32* %4
    %49 = getelementptr [10 x i32], [10 x i32]* @size, i32 %48
    %50 = load i32, i32* %49
    %51 = load i32, i32* %4
    %52 = getelementptr [10 x i32], [10 x i32]* @size, i32 %51
    %53 = load i32, i32* %52
    %54 = add i32 %53,1
    store i32 %54, i32* %47
    %55 = load i32, i32* %5
    %56 = getelementptr [10 x i32], [10 x i32]* @size, i32 %55
    %57 = load i32, i32* %5
    %58 = getelementptr [10 x i32], [10 x i32]* @size, i32 %57
    %59 = load i32, i32* %58
    %60 = load i32, i32* %5
    %61 = getelementptr [10 x i32], [10 x i32]* @size, i32 %60
    %62 = load i32, i32* %61
    %63 = add i32 %62,1
    store i32 %63, i32* %56
}
define dso_local i32 @dfs(i32 %0, i32 %1, i32 %2){
3:
    %4 = alloca i32
    store i32 %0, i32* %4
    %5 = alloca i32
    store i32 %1, i32* %5
    %6 = alloca i32
    store i32 %2, i32* %6
    %9 = load i32, i32* %4
    %10 = load i32, i32* %5
    %11 = icmp eq i32 %9,i32 %10
    br i32 %11 %8 %7

7:
    %13 = load i32, i32* %4
    %14 = getelementptr [10 x i32], [10 x i32]* @used, i32 %13
    store i32 1, i32* %14
    %15 = alloca i32
    store i32 0, i32* %15
    br %16

8:
    %12 = load i32, i32* %6
    ret i32 %12

16:
    %19 = load i32, i32* %15
    %20 = load i32, i32* %4
    %21 = getelementptr [10 x i32], [10 x i32]* @size, i32 %20
    %22 = load i32, i32* %21
    %23 = icmp lt i32 %19,i32 %22
    br i32 %23 %17 %18

17:
    %26 = load i32, i32* %4
    %27 = load i32, i32* %15
    %28 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* @to, i32 %26, i32 %27
    %29 = load i32, i32* %28
    %30 = getelementptr [10 x i32], [10 x i32]* @used, i32 %29
    %31 = load i32, i32* %30
    br i32 %31 %25 %24

18:
    ret i32 0

24:
    %37 = load i32, i32* %4
    %38 = load i32, i32* %15
    %39 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* @cap, i32 %37, i32 %38
    %40 = load i32, i32* %39
    %41 = icmp le i32 %40,i32 0
    br i32 %41 %36 %35

25:
    %32 = load i32, i32* %15
    %33 = load i32, i32* %15
    %34 = add i32 %33,1
    store i32 %34, i32* %15
    br %16

35:
    %45 = alloca i32
    %49 = load i32, i32* %6
    %50 = load i32, i32* %4
    %51 = load i32, i32* %15
    %52 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* @cap, i32 %50, i32 %51
    %53 = load i32, i32* %52
    %54 = icmp lt i32 %49,i32 %53
    br i32 %54 %47 %48

36:
    %42 = load i32, i32* %15
    %43 = load i32, i32* %15
    %44 = add i32 %43,1
    store i32 %44, i32* %15
    br %16

46:
    %60 = load i32, i32* %4
    %61 = load i32, i32* %15
    %62 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* @to, i32 %60, i32 %61
    %63 = load i32, i32* %62
    %64 = load i32, i32* %5
    %65 = load i32, i32* %45
    %66 = call i32 @dfs(i32 %63,i32 %64,i32 %65)
    %67 = alloca i32
    store i32 %66, i32* %67
    %70 = load i32, i32* %67
    %71 = icmp gt i32 %70,i32 0
    br i32 %71 %69 %68

47:
    %55 = load i32, i32* %6
    store i32 %55, i32* %45
    br %46

48:
    %56 = load i32, i32* %4
    %57 = load i32, i32* %15
    %58 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* @cap, i32 %56, i32 %57
    %59 = load i32, i32* %58
    store i32 %59, i32* %45
    br %46

68:
    %119 = load i32, i32* %15
    %120 = load i32, i32* %15
    %121 = add i32 %120,1
    store i32 %121, i32* %15
    br %16

69:
    %72 = load i32, i32* %4
    %73 = load i32, i32* %15
    %74 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* @cap, i32 %72, i32 %73
    %75 = load i32, i32* %4
    %76 = load i32, i32* %15
    %77 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* @cap, i32 %75, i32 %76
    %78 = load i32, i32* %77
    %79 = load i32, i32* %67
    %80 = load i32, i32* %4
    %81 = load i32, i32* %15
    %82 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* @cap, i32 %80, i32 %81
    %83 = load i32, i32* %82
    %84 = load i32, i32* %67
    %85 = sub i32 %83,%84
    store i32 %85, i32* %74
    %86 = load i32, i32* %4
    %87 = load i32, i32* %15
    %88 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* @to, i32 %86, i32 %87
    %89 = load i32, i32* %88
    %90 = load i32, i32* %4
    %91 = load i32, i32* %15
    %92 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* @rev, i32 %90, i32 %91
    %93 = load i32, i32* %92
    %94 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* @cap, i32 %89, i32 %93
    %95 = load i32, i32* %4
    %96 = load i32, i32* %15
    %97 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* @to, i32 %95, i32 %96
    %98 = load i32, i32* %97
    %99 = load i32, i32* %4
    %100 = load i32, i32* %15
    %101 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* @rev, i32 %99, i32 %100
    %102 = load i32, i32* %101
    %103 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* @cap, i32 %98, i32 %102
    %104 = load i32, i32* %103
    %105 = load i32, i32* %67
    %106 = load i32, i32* %4
    %107 = load i32, i32* %15
    %108 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* @to, i32 %106, i32 %107
    %109 = load i32, i32* %108
    %110 = load i32, i32* %4
    %111 = load i32, i32* %15
    %112 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* @rev, i32 %110, i32 %111
    %113 = load i32, i32* %112
    %114 = getelementptr [10 x [10 x i32]], [10 x [10 x i32]]* @cap, i32 %109, i32 %113
    %115 = load i32, i32* %114
    %116 = load i32, i32* %67
    %117 = add i32 %115,%116
    store i32 %117, i32* %94
    %118 = load i32, i32* %67
    ret i32 %118
}
define dso_local i32 @max_flow(i32 %0, i32 %1){
2:
    %3 = alloca i32
    store i32 %0, i32* %3
    %4 = alloca i32
    store i32 %1, i32* %4
    %5 = alloca i32
    store i32 0, i32* %5
    br %6

6:
    br i32 1 %7 %8

7:
    %9 = load [10 x i32], [10 x i32]* @used
    %10 = call void @my_memset([10 x i32] %9,i32 0,i32 10)
    %11 = load i32, i32* %3
    %12 = load i32, i32* %4
    %13 = load i32, i32* @INF
    %14 = call i32 @dfs(i32 %11,i32 %12,i32 %13)
    %15 = alloca i32
    store i32 %14, i32* %15
    %18 = load i32, i32* %15
    %19 = icmp eq i32 %18,i32 0
    br i32 %19 %17 %16

8:

16:
    %21 = load i32, i32* %5
    %22 = load i32, i32* %15
    %23 = load i32, i32* %5
    %24 = load i32, i32* %15
    %25 = add i32 %23,%24
    store i32 %25, i32* %5
    br %6

17:
    %20 = load i32, i32* %5
    ret i32 %20
}
define dso_local i32 @main(i32 %0, i32 %1){
2:
    %3 = alloca i32
    %4 = alloca i32
    %5 = call i32 @getint()
    store i32 %5, i32* %3
    %6 = call i32 @getint()
    store i32 %6, i32* %4
    %7 = load [10 x i32], [10 x i32]* @size
    %8 = call void @my_memset([10 x i32] %7,i32 0,i32 10)
    br %9

9:
    %12 = load i32, i32* %4
    %13 = icmp gt i32 %12,i32 0
    br i32 %13 %10 %11

10:
    %14 = alloca i32
    %15 = alloca i32
    %16 = call i32 @getint()
    store i32 %16, i32* %14
    %17 = call i32 @getint()
    store i32 %17, i32* %15
    %18 = call i32 @getint()
    %19 = alloca i32
    store i32 %18, i32* %19
    %20 = load i32, i32* %14
    %21 = load i32, i32* %15
    %22 = load i32, i32* %19
    %23 = call void @add_node(i32 %20,i32 %21,i32 %22)
    %24 = load i32, i32* %4
    %25 = load i32, i32* %4
    %26 = sub i32 %25,1
    store i32 %26, i32* %4
    br %9

11:
    %27 = load i32, i32* %3
    %28 = call i32 @max_flow(i32 1,i32 %27)
    call void @putint(i32 1,i32 %27,i32 %28)
    call void @putch(i32 10)
    ret i32 0
}

