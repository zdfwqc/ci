declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @if_if_Else(){
0:
    %1 = alloca i32
    store i32 5, i32* %1
    %2 = alloca i32
    store i32 10, i32* %2
    %6 = load i32, i32* %1
    %7 = icmp eq i32 %6,i32 5
    br i32 %7 %4 %5

3:
    %15 = load i32, i32* %1
    ret i32 %15

4:
    %10 = load i32, i32* %2
    %11 = icmp eq i32 %10,i32 10
    br i32 %11 %9 %8

5:
    %12 = load i32, i32* %1
    %13 = load i32, i32* %1
    %14 = add i32 %13,15
    store i32 %14, i32* %1
    br %3

8:

9:
    store i32 25, i32* %1
    br %8
}
define dso_local i32 @main(){
0:
    %1 = call i32 @if_if_Else()
    ret i32 %1
}

