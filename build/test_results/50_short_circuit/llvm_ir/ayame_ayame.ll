@g = dso_local global i32 0

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @func(i32 %0){
1:
    %2 = alloca i32
    store i32 %0, i32* %2
    %3 = load i32, i32* @g
    %4 = load i32, i32* %2
    %5 = load i32, i32* @g
    %6 = load i32, i32* %2
    %7 = add i32 %5,%6
    store i32 %7, i32* @g
    %8 = load i32, i32* @g
    call void @putint(i32 %8)
    %9 = load i32, i32* @g
    ret i32 %9
}
define dso_local i32 @main(i32 %0){
1:
    %2 = alloca i32
    %3 = call i32 @getint()
    store i32 %3, i32* %2
    %7 = load i32, i32* %2
    %8 = icmp gt i32 %7,i32 10
    %9 = load i32, i32* %2
    %10 = call i32 @func(i32 %9)
    %11 = and i32 %8,%10
    br i32 %11 %5 %6

4:
    %12 = call i32 @getint()
    store i32 %12, i32* %2
    %16 = load i32, i32* %2
    %17 = icmp gt i32 %16,i32 11
    %18 = load i32, i32* %2
    %19 = call i32 @func(i32 %18)
    %20 = and i32 %17,%19
    br i32 %20 %14 %15

5:
    store i32 1, i32* %2
    br %4

6:
    store i32 0, i32* %2
    br %4

13:
    %21 = call i32 @getint()
    store i32 %21, i32* %2
    %25 = load i32, i32* %2
    %26 = icmp le i32 %25,i32 99
    %27 = load i32, i32* %2
    %28 = call i32 @func(i32 %27)
    %29 = or i32 %26,%28
    br i32 %29 %23 %24

14:
    store i32 1, i32* %2
    br %13

15:
    store i32 0, i32* %2
    br %13

22:
    %30 = call i32 @getint()
    store i32 %30, i32* %2
    %34 = load i32, i32* %2
    %35 = icmp le i32 %34,i32 100
    %36 = load i32, i32* %2
    %37 = call i32 @func(i32 %36)
    %38 = or i32 %35,%37
    br i32 %38 %32 %33

23:
    store i32 1, i32* %2
    br %22

24:
    store i32 0, i32* %2
    br %22

31:
    %42 = call i32 @func(i32 99)
    
    %44 = call i32 @func(i32 100)
    %45 = and i32 %43,%44
    br void %45 %40 %41

32:
    store i32 1, i32* %2
    br %31

33:
    store i32 0, i32* %2
    br %31

39:
    ret i32 0

40:
    store i32 1, i32* %2
    br %39

41:
    store i32 0, i32* %2
    br %39
}

