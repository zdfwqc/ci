declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @main(){
0:
    %1 = alloca i32
    %2 = alloca i32
    store i32 10, i32* %1
    store i32 30, i32* %2
    %3 = load i32, i32* %1
    %4 = sub i32 0,5
    %5 = load i32, i32* %1
    %6 = sub i32 0,5
    %7 = sub i32 %5,%6
    %8 = load i32, i32* %2
    %9 = add i32 %7,%8
    %10 = sub i32 0,5
    %11 = add i32 %9,%10
    ret i32 %11
}

