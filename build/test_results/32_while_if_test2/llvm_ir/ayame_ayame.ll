declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @ifWhile(){
0:
    %1 = alloca i32
    store i32 0, i32* %1
    %2 = alloca i32
    store i32 3, i32* %2
    %6 = load i32, i32* %1
    %7 = icmp eq i32 %6,i32 5
    br i32 %7 %4 %5

3:
    %29 = load i32, i32* %2
    ret i32 %29

4:
    br %8

5:
    br %19

8:
    %11 = load i32, i32* %2
    %12 = icmp eq i32 %11,i32 2
    br i32 %12 %9 %10

9:
    %13 = load i32, i32* %2
    %14 = load i32, i32* %2
    %15 = add i32 %14,2
    store i32 %15, i32* %2
    br %8

10:
    %16 = load i32, i32* %2
    %17 = load i32, i32* %2
    %18 = add i32 %17,25
    store i32 %18, i32* %2

19:
    %22 = load i32, i32* %1
    %23 = icmp lt i32 %22,i32 5
    br i32 %23 %20 %21

20:
    %24 = load i32, i32* %2
    %25 = mul i32 %24,2
    store i32 %25, i32* %2
    %26 = load i32, i32* %1
    %27 = load i32, i32* %1
    %28 = add i32 %27,1
    store i32 %28, i32* %1
    br %19

21:
}
define dso_local i32 @main(){
0:
    %1 = call i32 @ifWhile()
    ret i32 %1
}

