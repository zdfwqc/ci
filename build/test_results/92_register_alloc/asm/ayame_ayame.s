	.text
	.syntax unified
	.eabi_attribute	67, "2.09"	@ Tag_conformance
	.cpu	cortex-a72
	.eabi_attribute	6, 14	@ Tag_CPU_arch
	.eabi_attribute	7, 65	@ Tag_CPU_arch_profile
	.eabi_attribute	8, 1	@ Tag_ARM_ISA_use
	.eabi_attribute	9, 2	@ Tag_THUMB_ISA_use
	.fpu	crypto-neon-fp-armv8
	.eabi_attribute	12, 3	@ Tag_Advanced_SIMD_arch
	.eabi_attribute	36, 1	@ Tag_FP_HP_extension
	.eabi_attribute	42, 1	@ Tag_MPextension_use
	.eabi_attribute	34, 1	@ Tag_CPU_unaligned_access
	.eabi_attribute	68, 3	@ Tag_Virtualization_use
	.eabi_attribute	17, 1	@ Tag_ABI_PCS_GOT_use
	.eabi_attribute	20, 1	@ Tag_ABI_FP_denormal
	.eabi_attribute	21, 1	@ Tag_ABI_FP_exceptions
	.eabi_attribute	23, 3	@ Tag_ABI_FP_number_model
	.eabi_attribute	24, 1	@ Tag_ABI_align_needed
	.eabi_attribute	25, 1	@ Tag_ABI_align_preserved
	.eabi_attribute	28, 1	@ Tag_ABI_VFP_args
	.eabi_attribute	38, 1	@ Tag_ABI_FP_16bit_format
	.eabi_attribute	14, 0	@ Tag_ABI_PCS_R9_use
	.file	"ayame_ayame.ll"
	.globl	func                    @ -- Begin function func
	.p2align	2
	.type	func,%function
	.code	32                      @ @func
func:
	.fnstart
@ %bb.0:
	push	{r4, r5, r6, r7, r8, lr}
	sub	sp, sp, #160
	str	r0, [sp, #156]
	add	r0, r0, r1
	str	r1, [sp, #152]
	str	r0, [sp, #148]
	bl	getint
	str	r0, [sp, #144]
	bl	getint
	str	r0, [sp, #140]
	bl	getint
	str	r0, [sp, #136]
	bl	getint
	movw	r1, :lower16:.L_MergedGlobals
	ldr	r4, [sp, #144]
	str	r0, [sp, #132]
	movt	r1, :upper16:.L_MergedGlobals
	ldm	r1, {r2, r3, r12, lr}
	add	r0, r0, lr
	ldr	r5, [r1, #36]
	ldr	lr, [r1, #32]
	ldr	r6, [r1, #40]
	add	r8, r1, #20
	add	r2, r4, r2
	ldr	r4, [sp, #140]
	add	r2, r2, #1
	str	r2, [sp, #128]
	add	r3, r4, r3
	ldr	r4, [sp, #136]
	add	r3, r3, #2
	str	r3, [sp, #124]
	add	r4, r4, r12
	add	r12, r0, #4
	ldr	r0, [r1, #16]
	add	r4, r4, #3
	str	r12, [sp, #116]
	str	r4, [sp, #120]
	add	r0, r2, r0
	ldr	r2, [r1, #20]
	add	r0, r0, #1
	str	r0, [sp, #112]
	add	r0, r0, lr
	add	r0, r0, #1
	add	r2, r3, r2
	ldr	r3, [r1, #24]
	str	r0, [sp, #96]
	add	r2, r2, #2
	str	r2, [sp, #108]
	add	r2, r2, r5
	ldr	r5, [r1, #48]
	add	r2, r2, #2
	add	r3, r4, r3
	ldr	r4, [r1, #28]
	str	r2, [sp, #92]
	add	r3, r3, #3
	add	r0, r0, r5
	ldr	r5, [r1, #52]
	str	r3, [sp, #104]
	add	r3, r3, r6
	ldr	r6, [r1, #44]
	add	r3, r3, #3
	add	r0, r0, #1
	add	r4, r12, r4
	str	r3, [sp, #88]
	str	r0, [sp, #80]
	movw	r12, :lower16:a32
	add	r4, r4, #4
	movt	r12, :upper16:a32
	add	r2, r2, r5
	ldr	r5, [r1, #56]
	add	r6, r4, r6
	str	r4, [sp, #100]
	add	r6, r6, #4
	add	r2, r2, #2
	str	r6, [sp, #84]
	str	r2, [sp, #76]
	add	r3, r3, r5
	ldr	r5, [r1, #60]
	add	r3, r3, #3
	str	r3, [sp, #72]
	add	r6, r6, r5
	ldr	r5, [r1, #64]
	add	r6, r6, #4
	str	r6, [sp, #68]
	add	r0, r0, r5
	ldr	r5, [r1, #68]
	add	r0, r0, #1
	str	r0, [sp, #64]
	add	r2, r2, r5
	ldr	r5, [r1, #72]
	add	r2, r2, #2
	str	r2, [sp, #60]
	add	r3, r3, r5
	ldr	r5, [r1, #76]
	add	r3, r3, #3
	str	r3, [sp, #56]
	add	r6, r6, r5
	ldr	r5, [r1, #80]
	add	r6, r6, #4
	str	r6, [sp, #52]
	add	r0, r0, r5
	ldr	r5, [r1, #84]
	add	r0, r0, #1
	str	r0, [sp, #48]
	add	r2, r2, r5
	add	r5, r2, #2
	ldr	r2, [r1, #88]
	str	r5, [sp, #44]
	add	r2, r3, r2
	add	r4, r2, #3
	ldr	r2, [r1, #92]
	str	r4, [sp, #40]
	add	r2, r6, r2
	add	r6, r2, #4
	ldr	r2, [r1, #96]
	str	r6, [sp, #36]
	add	r0, r0, r2
	add	r2, r0, #1
	ldr	r0, [r1, #100]
	str	r2, [sp, #32]
	add	r0, r5, r0
	ldr	r5, [r1, #108]
	add	r3, r0, #2
	ldr	r0, [r1, #104]
	str	r3, [sp, #28]
	add	r6, r6, r5
	ldr	r5, [r1, #112]
	add	r0, r4, r0
	add	r6, r6, #4
	ldr	r4, [sp, #144]
	add	r0, r0, #3
	str	r6, [sp, #20]
	str	r0, [sp, #24]
	add	r5, r2, r5
	add	r5, r5, #1
	str	r5, [sp, #16]
	ldr	r5, [r1, #116]
	add	r5, r3, r5
	add	r5, r5, #2
	str	r5, [sp, #12]
	ldr	r5, [r1, #120]
	add	r0, r0, r5
	ldr	r5, [r12]
	add	r0, r0, #3
	str	r0, [sp, #8]
	add	r6, r6, r5
	ldr	r5, [sp, #156]
	add	r6, r6, #4
	str	r6, [sp, #4]
	ldr	r6, [sp, #152]
	sub	r6, r5, r6
	ldr	r5, [r1, #12]
	add	r6, r6, #10
	str	r6, [sp, #148]
	ldr	r6, [r1, #112]
	ldr	r0, [sp, #148]
	add	r2, r2, r6
	add	r2, r2, #1
	str	r2, [sp, #16]
	ldr	r2, [r1, #116]
	add	r2, r3, r2
	ldr	r3, [sp, #24]
	add	r2, r2, #2
	str	r2, [sp, #12]
	ldr	r2, [r1, #120]
	add	r2, r3, r2
	ldr	r3, [sp, #20]
	add	r2, r2, #3
	str	r2, [sp, #8]
	ldr	r2, [r12]
	add	r2, r3, r2
	ldr	r3, [sp, #48]
	add	r2, r2, #4
	str	r2, [sp, #4]
	ldr	r2, [r1, #96]
	add	r2, r3, r2
	ldr	r3, [sp, #44]
	add	r2, r2, #1
	str	r2, [sp, #32]
	ldr	r2, [r1, #100]
	add	r2, r3, r2
	ldr	r3, [sp, #40]
	add	r2, r2, #2
	str	r2, [sp, #28]
	ldr	r2, [r1, #104]
	add	r2, r3, r2
	ldr	r3, [sp, #36]
	add	r2, r2, #3
	str	r2, [sp, #24]
	ldr	r2, [r1, #108]
	add	r2, r3, r2
	ldr	r3, [sp, #64]
	add	r2, r2, #4
	str	r2, [sp, #20]
	ldr	r2, [r1, #80]
	add	r2, r3, r2
	ldr	r3, [sp, #60]
	add	r2, r2, #1
	str	r2, [sp, #48]
	ldr	r2, [r1, #84]
	add	r2, r3, r2
	ldr	r3, [sp, #56]
	add	r2, r2, #2
	str	r2, [sp, #44]
	ldr	r2, [r1, #88]
	add	r2, r3, r2
	ldr	r3, [sp, #52]
	add	r2, r2, #3
	str	r2, [sp, #40]
	ldr	r2, [r1, #92]
	add	r2, r3, r2
	ldr	r3, [sp, #80]
	add	r2, r2, #4
	str	r2, [sp, #36]
	ldr	r2, [r1, #64]
	add	r2, r3, r2
	ldr	r3, [sp, #76]
	add	r2, r2, #1
	str	r2, [sp, #64]
	ldr	r2, [r1, #68]
	add	r2, r3, r2
	ldr	r3, [sp, #72]
	add	r2, r2, #2
	str	r2, [sp, #60]
	ldr	r2, [r1, #72]
	add	r2, r3, r2
	ldr	r3, [sp, #68]
	add	r2, r2, #3
	str	r2, [sp, #56]
	ldr	r2, [r1, #76]
	add	r2, r3, r2
	ldr	r3, [sp, #96]
	add	r2, r2, #4
	str	r2, [sp, #52]
	ldr	r2, [r1, #48]
	add	r2, r3, r2
	ldr	r3, [sp, #92]
	add	r2, r2, #1
	str	r2, [sp, #80]
	ldr	r2, [r1, #52]
	add	r2, r3, r2
	ldr	r3, [sp, #88]
	add	r2, r2, #2
	str	r2, [sp, #76]
	ldr	r2, [r1, #56]
	add	r2, r3, r2
	ldr	r3, [sp, #84]
	add	r2, r2, #3
	str	r2, [sp, #72]
	ldr	r2, [r1, #60]
	add	r2, r3, r2
	ldr	r3, [sp, #112]
	add	r2, r2, #4
	str	r2, [sp, #68]
	ldr	r2, [r1, #32]
	add	r2, r3, r2
	ldr	r3, [sp, #108]
	add	r2, r2, #1
	str	r2, [sp, #96]
	ldr	r2, [r1, #36]
	add	r2, r3, r2
	ldr	r3, [sp, #104]
	add	r2, r2, #2
	str	r2, [sp, #92]
	ldr	r2, [r1, #40]
	add	r2, r3, r2
	ldr	r3, [sp, #100]
	add	r2, r2, #3
	str	r2, [sp, #88]
	ldr	r2, [r1, #44]
	add	r2, r3, r2
	ldr	r3, [sp, #128]
	add	r2, r2, #4
	str	r2, [sp, #84]
	ldr	r2, [r1, #16]
	add	r2, r3, r2
	ldr	r3, [sp, #124]
	add	r2, r2, #1
	str	r2, [sp, #112]
	ldr	r2, [r1, #20]
	add	r2, r3, r2
	ldr	r3, [sp, #120]
	add	r2, r2, #2
	str	r2, [sp, #108]
	ldr	r2, [r1, #24]
	add	r2, r3, r2
	ldr	r3, [sp, #116]
	add	r2, r2, #3
	str	r2, [sp, #104]
	ldr	r2, [r1, #28]
	add	r2, r3, r2
	ldr	r3, [sp, #144]
	add	r2, r2, #4
	str	r2, [sp, #100]
	ldr	r2, [r1]
	add	r2, r3, r2
	ldr	r3, [sp, #140]
	add	r2, r2, #1
	str	r2, [sp, #128]
	ldr	r2, [r1, #4]
	add	r2, r3, r2
	ldr	r3, [sp, #136]
	add	r2, r2, #2
	str	r2, [sp, #124]
	ldr	r2, [r1, #8]
	add	r2, r3, r2
	ldr	r3, [sp, #132]
	add	r2, r2, #3
	str	r2, [sp, #120]
	ldr	r2, [r1, #12]
	add	r2, r3, r2
	add	r2, r2, #4
	str	r2, [sp, #116]
	ldm	r1, {r2, r3, r6}
	add	r2, r4, r2
	ldr	r4, [sp, #140]
	add	r2, r2, #1
	str	r2, [sp, #128]
	add	r3, r4, r3
	ldr	r4, [sp, #136]
	add	r3, r3, #2
	str	r3, [sp, #124]
	add	r6, r4, r6
	ldr	r4, [sp, #132]
	add	r6, r6, #3
	str	r6, [sp, #120]
	add	r5, r4, r5
	ldr	r4, [sp, #144]
	add	r5, r5, #4
	str	r5, [sp, #116]
	add	r0, r0, r4
	ldr	r4, [sp, #140]
	add	r0, r0, r4
	ldr	r4, [sp, #136]
	add	r0, r0, r4
	ldr	r4, [sp, #132]
	add	r0, r0, r4
	ldr	r4, [r1, #16]
	sub	r0, r0, r2
	ldr	r2, [sp, #112]
	sub	r0, r0, r3
	sub	r0, r0, r6
	sub	r0, r0, r5
	ldr	r5, [r1, #12]
	add	r0, r0, r2
	ldr	r2, [sp, #108]
	add	r0, r0, r2
	ldr	r2, [sp, #104]
	add	r0, r0, r2
	ldr	r2, [sp, #100]
	add	r0, r0, r2
	ldr	r2, [sp, #96]
	sub	r0, r0, r2
	ldr	r2, [sp, #92]
	sub	r0, r0, r2
	ldr	r2, [sp, #88]
	sub	r0, r0, r2
	ldr	r2, [sp, #84]
	sub	r0, r0, r2
	ldr	r2, [sp, #80]
	add	r0, r0, r2
	ldr	r2, [sp, #76]
	add	r0, r0, r2
	ldr	r2, [sp, #72]
	add	r0, r0, r2
	ldr	r2, [sp, #68]
	add	r0, r0, r2
	ldr	r2, [sp, #64]
	sub	r0, r0, r2
	ldr	r2, [sp, #60]
	sub	r0, r0, r2
	ldr	r2, [sp, #56]
	sub	r0, r0, r2
	ldr	r2, [sp, #52]
	sub	r0, r0, r2
	ldr	r2, [sp, #48]
	add	r0, r0, r2
	ldr	r2, [sp, #44]
	add	r0, r0, r2
	ldr	r2, [sp, #40]
	add	r0, r0, r2
	ldr	r2, [sp, #36]
	add	r0, r0, r2
	ldr	r2, [sp, #32]
	sub	r0, r0, r2
	ldr	r2, [sp, #28]
	sub	r0, r0, r2
	ldr	r2, [sp, #24]
	sub	r0, r0, r2
	ldr	r2, [sp, #20]
	sub	r0, r0, r2
	ldr	r2, [sp, #16]
	add	r0, r0, r2
	ldr	r2, [sp, #12]
	add	r0, r0, r2
	ldr	r2, [sp, #8]
	add	r0, r0, r2
	ldr	r2, [sp, #4]
	add	lr, r0, r2
	ldm	r1, {r2, r3, r6}
	ldm	r8, {r0, r7, r8}
	add	r2, lr, r2
	sub	r2, r2, r3
	add	r2, r2, r6
	ldr	r6, [r1, #44]
	sub	r2, r2, r5
	ldr	r5, [r1, #48]
	add	r2, r2, r4
	ldr	r4, [r1, #52]
	sub	r0, r2, r0
	add	r0, r0, r7
	add	r7, r1, #32
	ldm	r7, {r2, r3, r7}
	sub	lr, r0, r8
	ldr	r0, [r1, #56]
	ldr	r8, [r1, #60]
	add	r2, lr, r2
	sub	r2, r2, r3
	add	r2, r2, r7
	add	r7, r1, #64
	sub	r2, r2, r6
	ldr	r6, [r1, #76]
	add	r2, r2, r5
	ldr	r5, [r1, #80]
	sub	r2, r2, r4
	ldr	r4, [r1, #84]
	add	r0, r2, r0
	ldm	r7, {r2, r3, r7}
	sub	lr, r0, r8
	ldr	r0, [r1, #88]
	ldr	r8, [r1, #92]
	add	r2, lr, r2
	sub	r2, r2, r3
	add	r2, r2, r7
	add	r7, r1, #96
	sub	r2, r2, r6
	ldr	r6, [r1, #108]
	add	r2, r2, r5
	ldr	r5, [r1, #112]
	sub	r2, r2, r4
	ldr	r4, [r1, #116]
	ldr	r1, [r1, #120]
	add	r0, r2, r0
	ldm	r7, {r2, r3, r7}
	sub	r0, r0, r8
	add	r0, r0, r2
	sub	r0, r0, r3
	add	r0, r0, r7
	sub	r0, r0, r6
	add	r0, r0, r5
	sub	r0, r0, r4
	add	r0, r0, r1
	ldr	r1, [r12]
	sub	r0, r0, r1
	add	sp, sp, #160
	pop	{r4, r5, r6, r7, r8, pc}
.Lfunc_end0:
	.size	func, .Lfunc_end0-func
	.fnend
                                        @ -- End function
	.globl	main                    @ -- Begin function main
	.p2align	2
	.type	main,%function
	.code	32                      @ @main
main:
	.fnstart
@ %bb.0:
	push	{r11, lr}
	sub	sp, sp, #8
	bl	getint
	add	r1, r0, #18
	str	r0, [sp, #4]
	str	r1, [sp]
	bl	func
	str	r0, [sp, #4]
	bl	putint
	ldr	r0, [sp, #4]
	add	sp, sp, #8
	pop	{r11, pc}
.Lfunc_end1:
	.size	main, .Lfunc_end1-main
	.fnend
                                        @ -- End function
	.type	a32,%object             @ @a32
	.data
	.globl	a32
	.p2align	2
a32:
	.long	16                      @ 0x10
	.size	a32, 4

	.type	.L_MergedGlobals,%object @ @_MergedGlobals
	.p2align	2
.L_MergedGlobals:
	.long	1                       @ 0x1
	.long	2                       @ 0x2
	.long	3                       @ 0x3
	.long	4                       @ 0x4
	.long	5                       @ 0x5
	.long	6                       @ 0x6
	.long	7                       @ 0x7
	.long	8                       @ 0x8
	.long	9                       @ 0x9
	.long	10                      @ 0xa
	.long	11                      @ 0xb
	.long	12                      @ 0xc
	.long	13                      @ 0xd
	.long	14                      @ 0xe
	.long	15                      @ 0xf
	.long	16                      @ 0x10
	.long	1                       @ 0x1
	.long	2                       @ 0x2
	.long	3                       @ 0x3
	.long	4                       @ 0x4
	.long	5                       @ 0x5
	.long	6                       @ 0x6
	.long	7                       @ 0x7
	.long	8                       @ 0x8
	.long	9                       @ 0x9
	.long	10                      @ 0xa
	.long	11                      @ 0xb
	.long	12                      @ 0xc
	.long	13                      @ 0xd
	.long	14                      @ 0xe
	.long	15                      @ 0xf
	.size	.L_MergedGlobals, 124

	.globl	a1
.set a1, .L_MergedGlobals
	.size	a1, 4
	.globl	a2
.set a2, .L_MergedGlobals+4
	.size	a2, 4
	.globl	a3
.set a3, .L_MergedGlobals+8
	.size	a3, 4
	.globl	a4
.set a4, .L_MergedGlobals+12
	.size	a4, 4
	.globl	a5
.set a5, .L_MergedGlobals+16
	.size	a5, 4
	.globl	a6
.set a6, .L_MergedGlobals+20
	.size	a6, 4
	.globl	a7
.set a7, .L_MergedGlobals+24
	.size	a7, 4
	.globl	a8
.set a8, .L_MergedGlobals+28
	.size	a8, 4
	.globl	a9
.set a9, .L_MergedGlobals+32
	.size	a9, 4
	.globl	a10
.set a10, .L_MergedGlobals+36
	.size	a10, 4
	.globl	a11
.set a11, .L_MergedGlobals+40
	.size	a11, 4
	.globl	a12
.set a12, .L_MergedGlobals+44
	.size	a12, 4
	.globl	a13
.set a13, .L_MergedGlobals+48
	.size	a13, 4
	.globl	a14
.set a14, .L_MergedGlobals+52
	.size	a14, 4
	.globl	a15
.set a15, .L_MergedGlobals+56
	.size	a15, 4
	.globl	a16
.set a16, .L_MergedGlobals+60
	.size	a16, 4
	.globl	a17
.set a17, .L_MergedGlobals+64
	.size	a17, 4
	.globl	a18
.set a18, .L_MergedGlobals+68
	.size	a18, 4
	.globl	a19
.set a19, .L_MergedGlobals+72
	.size	a19, 4
	.globl	a20
.set a20, .L_MergedGlobals+76
	.size	a20, 4
	.globl	a21
.set a21, .L_MergedGlobals+80
	.size	a21, 4
	.globl	a22
.set a22, .L_MergedGlobals+84
	.size	a22, 4
	.globl	a23
.set a23, .L_MergedGlobals+88
	.size	a23, 4
	.globl	a24
.set a24, .L_MergedGlobals+92
	.size	a24, 4
	.globl	a25
.set a25, .L_MergedGlobals+96
	.size	a25, 4
	.globl	a26
.set a26, .L_MergedGlobals+100
	.size	a26, 4
	.globl	a27
.set a27, .L_MergedGlobals+104
	.size	a27, 4
	.globl	a28
.set a28, .L_MergedGlobals+108
	.size	a28, 4
	.globl	a29
.set a29, .L_MergedGlobals+112
	.size	a29, 4
	.globl	a30
.set a30, .L_MergedGlobals+116
	.size	a30, 4
	.globl	a31
.set a31, .L_MergedGlobals+120
	.size	a31, 4
	.section	".note.GNU-stack","",%progbits
