@a1 = dso_local global i32 1

@a2 = dso_local global i32 2

@a3 = dso_local global i32 3

@a4 = dso_local global i32 4

@a5 = dso_local global i32 5

@a6 = dso_local global i32 6

@a7 = dso_local global i32 7

@a8 = dso_local global i32 8

@a9 = dso_local global i32 9

@a10 = dso_local global i32 10

@a11 = dso_local global i32 11

@a12 = dso_local global i32 12

@a13 = dso_local global i32 13

@a14 = dso_local global i32 14

@a15 = dso_local global i32 15

@a16 = dso_local global i32 16

@a17 = dso_local global i32 1

@a18 = dso_local global i32 2

@a19 = dso_local global i32 3

@a20 = dso_local global i32 4

@a21 = dso_local global i32 5

@a22 = dso_local global i32 6

@a23 = dso_local global i32 7

@a24 = dso_local global i32 8

@a25 = dso_local global i32 9

@a26 = dso_local global i32 10

@a27 = dso_local global i32 11

@a28 = dso_local global i32 12

@a29 = dso_local global i32 13

@a30 = dso_local global i32 14

@a31 = dso_local global i32 15

@a32 = dso_local global i32 16

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @func(i32 %0, i32 %1){
2:
    %3 = alloca i32
    store i32 %0, i32* %3
    %4 = alloca i32
    store i32 %1, i32* %4
    %5 = alloca i32
    %6 = load i32, i32* %3
    %7 = load i32, i32* %4
    %8 = load i32, i32* %3
    %9 = load i32, i32* %4
    %10 = add i32 %8,%9
    store i32 %10, i32* %5
    %11 = alloca i32
    %12 = alloca i32
    %13 = alloca i32
    %14 = alloca i32
    %15 = alloca i32
    %16 = alloca i32
    %17 = alloca i32
    %18 = alloca i32
    %19 = alloca i32
    %20 = alloca i32
    %21 = alloca i32
    %22 = alloca i32
    %23 = alloca i32
    %24 = alloca i32
    %25 = alloca i32
    %26 = alloca i32
    %27 = alloca i32
    %28 = alloca i32
    %29 = alloca i32
    %30 = alloca i32
    %31 = alloca i32
    %32 = alloca i32
    %33 = alloca i32
    %34 = alloca i32
    %35 = alloca i32
    %36 = alloca i32
    %37 = alloca i32
    %38 = alloca i32
    %39 = alloca i32
    %40 = alloca i32
    %41 = alloca i32
    %42 = alloca i32
    %43 = alloca i32
    %44 = alloca i32
    %45 = alloca i32
    %46 = alloca i32
    %47 = call i32 @getint()
    store i32 %47, i32* %11
    %48 = call i32 @getint()
    store i32 %48, i32* %12
    %49 = call i32 @getint()
    store i32 %49, i32* %13
    %50 = call i32 @getint()
    store i32 %50, i32* %14
    %51 = load i32, i32* %11
    %52 = load i32, i32* %11
    %53 = add i32 1,%52
    %54 = load i32, i32* @a1
    %55 = add i32 %53,%54
    store i32 %55, i32* %15
    %56 = load i32, i32* %12
    %57 = load i32, i32* %12
    %58 = add i32 2,%57
    %59 = load i32, i32* @a2
    %60 = add i32 %58,%59
    store i32 %60, i32* %16
    %61 = load i32, i32* %13
    %62 = load i32, i32* %13
    %63 = add i32 3,%62
    %64 = load i32, i32* @a3
    %65 = add i32 %63,%64
    store i32 %65, i32* %17
    %66 = load i32, i32* %14
    %67 = load i32, i32* %14
    %68 = add i32 4,%67
    %69 = load i32, i32* @a4
    %70 = add i32 %68,%69
    store i32 %70, i32* %18
    %71 = load i32, i32* %15
    %72 = load i32, i32* %15
    %73 = add i32 1,%72
    %74 = load i32, i32* @a5
    %75 = add i32 %73,%74
    store i32 %75, i32* %19
    %76 = load i32, i32* %16
    %77 = load i32, i32* %16
    %78 = add i32 2,%77
    %79 = load i32, i32* @a6
    %80 = add i32 %78,%79
    store i32 %80, i32* %20
    %81 = load i32, i32* %17
    %82 = load i32, i32* %17
    %83 = add i32 3,%82
    %84 = load i32, i32* @a7
    %85 = add i32 %83,%84
    store i32 %85, i32* %21
    %86 = load i32, i32* %18
    %87 = load i32, i32* %18
    %88 = add i32 4,%87
    %89 = load i32, i32* @a8
    %90 = add i32 %88,%89
    store i32 %90, i32* %22
    %91 = load i32, i32* %19
    %92 = load i32, i32* %19
    %93 = add i32 1,%92
    %94 = load i32, i32* @a9
    %95 = add i32 %93,%94
    store i32 %95, i32* %23
    %96 = load i32, i32* %20
    %97 = load i32, i32* %20
    %98 = add i32 2,%97
    %99 = load i32, i32* @a10
    %100 = add i32 %98,%99
    store i32 %100, i32* %24
    %101 = load i32, i32* %21
    %102 = load i32, i32* %21
    %103 = add i32 3,%102
    %104 = load i32, i32* @a11
    %105 = add i32 %103,%104
    store i32 %105, i32* %25
    %106 = load i32, i32* %22
    %107 = load i32, i32* %22
    %108 = add i32 4,%107
    %109 = load i32, i32* @a12
    %110 = add i32 %108,%109
    store i32 %110, i32* %26
    %111 = load i32, i32* %23
    %112 = load i32, i32* %23
    %113 = add i32 1,%112
    %114 = load i32, i32* @a13
    %115 = add i32 %113,%114
    store i32 %115, i32* %27
    %116 = load i32, i32* %24
    %117 = load i32, i32* %24
    %118 = add i32 2,%117
    %119 = load i32, i32* @a14
    %120 = add i32 %118,%119
    store i32 %120, i32* %28
    %121 = load i32, i32* %25
    %122 = load i32, i32* %25
    %123 = add i32 3,%122
    %124 = load i32, i32* @a15
    %125 = add i32 %123,%124
    store i32 %125, i32* %29
    %126 = load i32, i32* %26
    %127 = load i32, i32* %26
    %128 = add i32 4,%127
    %129 = load i32, i32* @a16
    %130 = add i32 %128,%129
    store i32 %130, i32* %30
    %131 = load i32, i32* %27
    %132 = load i32, i32* %27
    %133 = add i32 1,%132
    %134 = load i32, i32* @a17
    %135 = add i32 %133,%134
    store i32 %135, i32* %31
    %136 = load i32, i32* %28
    %137 = load i32, i32* %28
    %138 = add i32 2,%137
    %139 = load i32, i32* @a18
    %140 = add i32 %138,%139
    store i32 %140, i32* %32
    %141 = load i32, i32* %29
    %142 = load i32, i32* %29
    %143 = add i32 3,%142
    %144 = load i32, i32* @a19
    %145 = add i32 %143,%144
    store i32 %145, i32* %33
    %146 = load i32, i32* %30
    %147 = load i32, i32* %30
    %148 = add i32 4,%147
    %149 = load i32, i32* @a20
    %150 = add i32 %148,%149
    store i32 %150, i32* %34
    %151 = load i32, i32* %31
    %152 = load i32, i32* %31
    %153 = add i32 1,%152
    %154 = load i32, i32* @a21
    %155 = add i32 %153,%154
    store i32 %155, i32* %35
    %156 = load i32, i32* %32
    %157 = load i32, i32* %32
    %158 = add i32 2,%157
    %159 = load i32, i32* @a22
    %160 = add i32 %158,%159
    store i32 %160, i32* %36
    %161 = load i32, i32* %33
    %162 = load i32, i32* %33
    %163 = add i32 3,%162
    %164 = load i32, i32* @a23
    %165 = add i32 %163,%164
    store i32 %165, i32* %37
    %166 = load i32, i32* %34
    %167 = load i32, i32* %34
    %168 = add i32 4,%167
    %169 = load i32, i32* @a24
    %170 = add i32 %168,%169
    store i32 %170, i32* %38
    %171 = load i32, i32* %35
    %172 = load i32, i32* %35
    %173 = add i32 1,%172
    %174 = load i32, i32* @a25
    %175 = add i32 %173,%174
    store i32 %175, i32* %39
    %176 = load i32, i32* %36
    %177 = load i32, i32* %36
    %178 = add i32 2,%177
    %179 = load i32, i32* @a26
    %180 = add i32 %178,%179
    store i32 %180, i32* %40
    %181 = load i32, i32* %37
    %182 = load i32, i32* %37
    %183 = add i32 3,%182
    %184 = load i32, i32* @a27
    %185 = add i32 %183,%184
    store i32 %185, i32* %41
    %186 = load i32, i32* %38
    %187 = load i32, i32* %38
    %188 = add i32 4,%187
    %189 = load i32, i32* @a28
    %190 = add i32 %188,%189
    store i32 %190, i32* %42
    %191 = load i32, i32* %39
    %192 = load i32, i32* %39
    %193 = add i32 1,%192
    %194 = load i32, i32* @a29
    %195 = add i32 %193,%194
    store i32 %195, i32* %43
    %196 = load i32, i32* %40
    %197 = load i32, i32* %40
    %198 = add i32 2,%197
    %199 = load i32, i32* @a30
    %200 = add i32 %198,%199
    store i32 %200, i32* %44
    %201 = load i32, i32* %41
    %202 = load i32, i32* %41
    %203 = add i32 3,%202
    %204 = load i32, i32* @a31
    %205 = add i32 %203,%204
    store i32 %205, i32* %45
    %206 = load i32, i32* %42
    %207 = load i32, i32* %42
    %208 = add i32 4,%207
    %209 = load i32, i32* @a32
    %210 = add i32 %208,%209
    store i32 %210, i32* %46
    %211 = load i32, i32* %3
    %212 = load i32, i32* %4
    %213 = load i32, i32* %3
    %214 = load i32, i32* %4
    %215 = sub i32 %213,%214
    %216 = add i32 %215,10
    store i32 %216, i32* %5
    %217 = load i32, i32* %39
    %218 = load i32, i32* %39
    %219 = add i32 1,%218
    %220 = load i32, i32* @a29
    %221 = add i32 %219,%220
    store i32 %221, i32* %43
    %222 = load i32, i32* %40
    %223 = load i32, i32* %40
    %224 = add i32 2,%223
    %225 = load i32, i32* @a30
    %226 = add i32 %224,%225
    store i32 %226, i32* %44
    %227 = load i32, i32* %41
    %228 = load i32, i32* %41
    %229 = add i32 3,%228
    %230 = load i32, i32* @a31
    %231 = add i32 %229,%230
    store i32 %231, i32* %45
    %232 = load i32, i32* %42
    %233 = load i32, i32* %42
    %234 = add i32 4,%233
    %235 = load i32, i32* @a32
    %236 = add i32 %234,%235
    store i32 %236, i32* %46
    %237 = load i32, i32* %35
    %238 = load i32, i32* %35
    %239 = add i32 1,%238
    %240 = load i32, i32* @a25
    %241 = add i32 %239,%240
    store i32 %241, i32* %39
    %242 = load i32, i32* %36
    %243 = load i32, i32* %36
    %244 = add i32 2,%243
    %245 = load i32, i32* @a26
    %246 = add i32 %244,%245
    store i32 %246, i32* %40
    %247 = load i32, i32* %37
    %248 = load i32, i32* %37
    %249 = add i32 3,%248
    %250 = load i32, i32* @a27
    %251 = add i32 %249,%250
    store i32 %251, i32* %41
    %252 = load i32, i32* %38
    %253 = load i32, i32* %38
    %254 = add i32 4,%253
    %255 = load i32, i32* @a28
    %256 = add i32 %254,%255
    store i32 %256, i32* %42
    %257 = load i32, i32* %31
    %258 = load i32, i32* %31
    %259 = add i32 1,%258
    %260 = load i32, i32* @a21
    %261 = add i32 %259,%260
    store i32 %261, i32* %35
    %262 = load i32, i32* %32
    %263 = load i32, i32* %32
    %264 = add i32 2,%263
    %265 = load i32, i32* @a22
    %266 = add i32 %264,%265
    store i32 %266, i32* %36
    %267 = load i32, i32* %33
    %268 = load i32, i32* %33
    %269 = add i32 3,%268
    %270 = load i32, i32* @a23
    %271 = add i32 %269,%270
    store i32 %271, i32* %37
    %272 = load i32, i32* %34
    %273 = load i32, i32* %34
    %274 = add i32 4,%273
    %275 = load i32, i32* @a24
    %276 = add i32 %274,%275
    store i32 %276, i32* %38
    %277 = load i32, i32* %27
    %278 = load i32, i32* %27
    %279 = add i32 1,%278
    %280 = load i32, i32* @a17
    %281 = add i32 %279,%280
    store i32 %281, i32* %31
    %282 = load i32, i32* %28
    %283 = load i32, i32* %28
    %284 = add i32 2,%283
    %285 = load i32, i32* @a18
    %286 = add i32 %284,%285
    store i32 %286, i32* %32
    %287 = load i32, i32* %29
    %288 = load i32, i32* %29
    %289 = add i32 3,%288
    %290 = load i32, i32* @a19
    %291 = add i32 %289,%290
    store i32 %291, i32* %33
    %292 = load i32, i32* %30
    %293 = load i32, i32* %30
    %294 = add i32 4,%293
    %295 = load i32, i32* @a20
    %296 = add i32 %294,%295
    store i32 %296, i32* %34
    %297 = load i32, i32* %23
    %298 = load i32, i32* %23
    %299 = add i32 1,%298
    %300 = load i32, i32* @a13
    %301 = add i32 %299,%300
    store i32 %301, i32* %27
    %302 = load i32, i32* %24
    %303 = load i32, i32* %24
    %304 = add i32 2,%303
    %305 = load i32, i32* @a14
    %306 = add i32 %304,%305
    store i32 %306, i32* %28
    %307 = load i32, i32* %25
    %308 = load i32, i32* %25
    %309 = add i32 3,%308
    %310 = load i32, i32* @a15
    %311 = add i32 %309,%310
    store i32 %311, i32* %29
    %312 = load i32, i32* %26
    %313 = load i32, i32* %26
    %314 = add i32 4,%313
    %315 = load i32, i32* @a16
    %316 = add i32 %314,%315
    store i32 %316, i32* %30
    %317 = load i32, i32* %19
    %318 = load i32, i32* %19
    %319 = add i32 1,%318
    %320 = load i32, i32* @a9
    %321 = add i32 %319,%320
    store i32 %321, i32* %23
    %322 = load i32, i32* %20
    %323 = load i32, i32* %20
    %324 = add i32 2,%323
    %325 = load i32, i32* @a10
    %326 = add i32 %324,%325
    store i32 %326, i32* %24
    %327 = load i32, i32* %21
    %328 = load i32, i32* %21
    %329 = add i32 3,%328
    %330 = load i32, i32* @a11
    %331 = add i32 %329,%330
    store i32 %331, i32* %25
    %332 = load i32, i32* %22
    %333 = load i32, i32* %22
    %334 = add i32 4,%333
    %335 = load i32, i32* @a12
    %336 = add i32 %334,%335
    store i32 %336, i32* %26
    %337 = load i32, i32* %15
    %338 = load i32, i32* %15
    %339 = add i32 1,%338
    %340 = load i32, i32* @a5
    %341 = add i32 %339,%340
    store i32 %341, i32* %19
    %342 = load i32, i32* %16
    %343 = load i32, i32* %16
    %344 = add i32 2,%343
    %345 = load i32, i32* @a6
    %346 = add i32 %344,%345
    store i32 %346, i32* %20
    %347 = load i32, i32* %17
    %348 = load i32, i32* %17
    %349 = add i32 3,%348
    %350 = load i32, i32* @a7
    %351 = add i32 %349,%350
    store i32 %351, i32* %21
    %352 = load i32, i32* %18
    %353 = load i32, i32* %18
    %354 = add i32 4,%353
    %355 = load i32, i32* @a8
    %356 = add i32 %354,%355
    store i32 %356, i32* %22
    %357 = load i32, i32* %11
    %358 = load i32, i32* %11
    %359 = add i32 1,%358
    %360 = load i32, i32* @a1
    %361 = add i32 %359,%360
    store i32 %361, i32* %15
    %362 = load i32, i32* %12
    %363 = load i32, i32* %12
    %364 = add i32 2,%363
    %365 = load i32, i32* @a2
    %366 = add i32 %364,%365
    store i32 %366, i32* %16
    %367 = load i32, i32* %13
    %368 = load i32, i32* %13
    %369 = add i32 3,%368
    %370 = load i32, i32* @a3
    %371 = add i32 %369,%370
    store i32 %371, i32* %17
    %372 = load i32, i32* %14
    %373 = load i32, i32* %14
    %374 = add i32 4,%373
    %375 = load i32, i32* @a4
    %376 = add i32 %374,%375
    store i32 %376, i32* %18
    %377 = load i32, i32* %11
    %378 = load i32, i32* %11
    %379 = add i32 1,%378
    %380 = load i32, i32* @a1
    %381 = add i32 %379,%380
    store i32 %381, i32* %15
    %382 = load i32, i32* %12
    %383 = load i32, i32* %12
    %384 = add i32 2,%383
    %385 = load i32, i32* @a2
    %386 = add i32 %384,%385
    store i32 %386, i32* %16
    %387 = load i32, i32* %13
    %388 = load i32, i32* %13
    %389 = add i32 3,%388
    %390 = load i32, i32* @a3
    %391 = add i32 %389,%390
    store i32 %391, i32* %17
    %392 = load i32, i32* %14
    %393 = load i32, i32* %14
    %394 = add i32 4,%393
    %395 = load i32, i32* @a4
    %396 = add i32 %394,%395
    store i32 %396, i32* %18
    %397 = load i32, i32* %5
    %398 = load i32, i32* %11
    %399 = load i32, i32* %5
    %400 = load i32, i32* %11
    %401 = add i32 %399,%400
    %402 = load i32, i32* %12
    %403 = add i32 %401,%402
    %404 = load i32, i32* %13
    %405 = add i32 %403,%404
    %406 = load i32, i32* %14
    %407 = add i32 %405,%406
    %408 = load i32, i32* %15
    %409 = sub i32 %407,%408
    %410 = load i32, i32* %16
    %411 = sub i32 %409,%410
    %412 = load i32, i32* %17
    %413 = sub i32 %411,%412
    %414 = load i32, i32* %18
    %415 = sub i32 %413,%414
    %416 = load i32, i32* %19
    %417 = add i32 %415,%416
    %418 = load i32, i32* %20
    %419 = add i32 %417,%418
    %420 = load i32, i32* %21
    %421 = add i32 %419,%420
    %422 = load i32, i32* %22
    %423 = add i32 %421,%422
    %424 = load i32, i32* %23
    %425 = sub i32 %423,%424
    %426 = load i32, i32* %24
    %427 = sub i32 %425,%426
    %428 = load i32, i32* %25
    %429 = sub i32 %427,%428
    %430 = load i32, i32* %26
    %431 = sub i32 %429,%430
    %432 = load i32, i32* %27
    %433 = add i32 %431,%432
    %434 = load i32, i32* %28
    %435 = add i32 %433,%434
    %436 = load i32, i32* %29
    %437 = add i32 %435,%436
    %438 = load i32, i32* %30
    %439 = add i32 %437,%438
    %440 = load i32, i32* %31
    %441 = sub i32 %439,%440
    %442 = load i32, i32* %32
    %443 = sub i32 %441,%442
    %444 = load i32, i32* %33
    %445 = sub i32 %443,%444
    %446 = load i32, i32* %34
    %447 = sub i32 %445,%446
    %448 = load i32, i32* %35
    %449 = add i32 %447,%448
    %450 = load i32, i32* %36
    %451 = add i32 %449,%450
    %452 = load i32, i32* %37
    %453 = add i32 %451,%452
    %454 = load i32, i32* %38
    %455 = add i32 %453,%454
    %456 = load i32, i32* %39
    %457 = sub i32 %455,%456
    %458 = load i32, i32* %40
    %459 = sub i32 %457,%458
    %460 = load i32, i32* %41
    %461 = sub i32 %459,%460
    %462 = load i32, i32* %42
    %463 = sub i32 %461,%462
    %464 = load i32, i32* %43
    %465 = add i32 %463,%464
    %466 = load i32, i32* %44
    %467 = add i32 %465,%466
    %468 = load i32, i32* %45
    %469 = add i32 %467,%468
    %470 = load i32, i32* %46
    %471 = add i32 %469,%470
    %472 = load i32, i32* @a1
    %473 = add i32 %471,%472
    %474 = load i32, i32* @a2
    %475 = sub i32 %473,%474
    %476 = load i32, i32* @a3
    %477 = add i32 %475,%476
    %478 = load i32, i32* @a4
    %479 = sub i32 %477,%478
    %480 = load i32, i32* @a5
    %481 = add i32 %479,%480
    %482 = load i32, i32* @a6
    %483 = sub i32 %481,%482
    %484 = load i32, i32* @a7
    %485 = add i32 %483,%484
    %486 = load i32, i32* @a8
    %487 = sub i32 %485,%486
    %488 = load i32, i32* @a9
    %489 = add i32 %487,%488
    %490 = load i32, i32* @a10
    %491 = sub i32 %489,%490
    %492 = load i32, i32* @a11
    %493 = add i32 %491,%492
    %494 = load i32, i32* @a12
    %495 = sub i32 %493,%494
    %496 = load i32, i32* @a13
    %497 = add i32 %495,%496
    %498 = load i32, i32* @a14
    %499 = sub i32 %497,%498
    %500 = load i32, i32* @a15
    %501 = add i32 %499,%500
    %502 = load i32, i32* @a16
    %503 = sub i32 %501,%502
    %504 = load i32, i32* @a17
    %505 = add i32 %503,%504
    %506 = load i32, i32* @a18
    %507 = sub i32 %505,%506
    %508 = load i32, i32* @a19
    %509 = add i32 %507,%508
    %510 = load i32, i32* @a20
    %511 = sub i32 %509,%510
    %512 = load i32, i32* @a21
    %513 = add i32 %511,%512
    %514 = load i32, i32* @a22
    %515 = sub i32 %513,%514
    %516 = load i32, i32* @a23
    %517 = add i32 %515,%516
    %518 = load i32, i32* @a24
    %519 = sub i32 %517,%518
    %520 = load i32, i32* @a25
    %521 = add i32 %519,%520
    %522 = load i32, i32* @a26
    %523 = sub i32 %521,%522
    %524 = load i32, i32* @a27
    %525 = add i32 %523,%524
    %526 = load i32, i32* @a28
    %527 = sub i32 %525,%526
    %528 = load i32, i32* @a29
    %529 = add i32 %527,%528
    %530 = load i32, i32* @a30
    %531 = sub i32 %529,%530
    %532 = load i32, i32* @a31
    %533 = add i32 %531,%532
    %534 = load i32, i32* @a32
    %535 = sub i32 %533,%534
    ret i32 %535
}
define dso_local i32 @main(i32 %0, i32 %1){
2:
    %3 = alloca i32
    %4 = alloca i32
    %5 = call i32 @getint()
    store i32 %5, i32* %3
    %6 = load i32, i32* %3
    %7 = mul i32 2,9
    %8 = load i32, i32* %3
    %9 = mul i32 2,9
    %10 = add i32 %8,%9
    store i32 %10, i32* %4
    %11 = load i32, i32* %3
    %12 = load i32, i32* %4
    %13 = call i32 @func(i32 %11,i32 %12)
    store i32 %13, i32* %3
    %14 = load i32, i32* %3
    call void @putint(i32 %14)
    %15 = load i32, i32* %3
    ret i32 %15
}

