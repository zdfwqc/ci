@a = dso_local constant [i32 0, i32 1, i32 2, i32 3, i32 4]

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @main(){
0:
    %1 = getelementptr [5 x i32], [5 x i32]* @a, i32 4
    %2 = load i32, i32* %1
    ret i32 %2
}

