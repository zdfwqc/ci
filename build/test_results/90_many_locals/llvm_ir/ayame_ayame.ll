declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @foo(){
0:
    %1 = alloca [16 x i32]
    %2 = getelementptr [16 x i32], [16 x i32]* %1, i32 0
    store i32 0, i32* %2
    %3 = getelementptr [16 x i32], [16 x i32]* %1, i32 1
    store i32 1, i32* %3
    %4 = getelementptr [16 x i32], [16 x i32]* %1, i32 2
    store i32 2, i32* %4
    %5 = getelementptr [16 x i32], [16 x i32]* %1, i32 3
    store i32 3, i32* %5
    %6 = getelementptr [16 x i32], [16 x i32]* %1, i32 4
    store i32 0, i32* %6
    %7 = getelementptr [16 x i32], [16 x i32]* %1, i32 5
    store i32 1, i32* %7
    %8 = getelementptr [16 x i32], [16 x i32]* %1, i32 6
    store i32 2, i32* %8
    %9 = getelementptr [16 x i32], [16 x i32]* %1, i32 7
    store i32 3, i32* %9
    %10 = getelementptr [16 x i32], [16 x i32]* %1, i32 8
    store i32 0, i32* %10
    %11 = getelementptr [16 x i32], [16 x i32]* %1, i32 9
    store i32 1, i32* %11
    %12 = getelementptr [16 x i32], [16 x i32]* %1, i32 10
    store i32 2, i32* %12
    %13 = getelementptr [16 x i32], [16 x i32]* %1, i32 11
    store i32 3, i32* %13
    %14 = getelementptr [16 x i32], [16 x i32]* %1, i32 12
    store i32 0, i32* %14
    %15 = getelementptr [16 x i32], [16 x i32]* %1, i32 13
    store i32 1, i32* %15
    %16 = getelementptr [16 x i32], [16 x i32]* %1, i32 14
    store i32 2, i32* %16
    %17 = getelementptr [16 x i32], [16 x i32]* %1, i32 15
    store i32 3, i32* %17
    %18 = alloca i32
    store i32 3, i32* %18
    %19 = alloca i32
    store i32 7, i32* %19
    %20 = alloca i32
    store i32 5, i32* %20
    %21 = alloca i32
    store i32 6, i32* %21
    %22 = alloca i32
    store i32 1, i32* %22
    %23 = alloca i32
    store i32 0, i32* %23
    %24 = alloca i32
    store i32 3, i32* %24
    %25 = alloca i32
    store i32 5, i32* %25
    %26 = alloca i32
    store i32 4, i32* %26
    %27 = alloca i32
    store i32 2, i32* %27
    %28 = alloca i32
    store i32 7, i32* %28
    %29 = alloca i32
    store i32 9, i32* %29
    %30 = alloca i32
    store i32 8, i32* %30
    %31 = alloca i32
    store i32 1, i32* %31
    %32 = alloca i32
    store i32 4, i32* %32
    %33 = alloca i32
    store i32 6, i32* %33
    %34 = load i32, i32* %18
    %35 = load i32, i32* %19
    %36 = load i32, i32* %18
    %37 = load i32, i32* %19
    %38 = add i32 %36,%37
    %39 = load i32, i32* %20
    %40 = add i32 %38,%39
    %41 = load i32, i32* %21
    %42 = add i32 %40,%41
    %43 = load i32, i32* %22
    %44 = add i32 %42,%43
    %45 = load i32, i32* %23
    %46 = add i32 %44,%45
    %47 = load i32, i32* %24
    %48 = add i32 %46,%47
    %49 = load i32, i32* %25
    %50 = add i32 %48,%49
    %51 = alloca i32
    store i32 %50, i32* %51
    %52 = load i32, i32* %26
    %53 = load i32, i32* %27
    %54 = load i32, i32* %26
    %55 = load i32, i32* %27
    %56 = add i32 %54,%55
    %57 = load i32, i32* %28
    %58 = add i32 %56,%57
    %59 = load i32, i32* %29
    %60 = add i32 %58,%59
    %61 = load i32, i32* %30
    %62 = add i32 %60,%61
    %63 = load i32, i32* %31
    %64 = add i32 %62,%63
    %65 = load i32, i32* %32
    %66 = add i32 %64,%65
    %67 = load i32, i32* %33
    %68 = add i32 %66,%67
    %69 = alloca i32
    store i32 %68, i32* %69
    %70 = load i32, i32* %51
    %71 = load i32, i32* %69
    %72 = load i32, i32* %51
    %73 = load i32, i32* %69
    %74 = add i32 %72,%73
    %75 = load i32, i32* %18
    %76 = getelementptr [16 x i32], [16 x i32]* %1, i32 %75
    %77 = load i32, i32* %76
    %78 = add i32 %74,%77
    ret i32 %78
}
define dso_local i32 @main(){
0:
    %1 = alloca i32
    store i32 3, i32* %1
    %2 = alloca i32
    store i32 7, i32* %2
    %3 = alloca i32
    store i32 5, i32* %3
    %4 = alloca i32
    store i32 6, i32* %4
    %5 = alloca i32
    store i32 1, i32* %5
    %6 = alloca i32
    store i32 0, i32* %6
    %7 = alloca i32
    store i32 3, i32* %7
    %8 = alloca i32
    store i32 5, i32* %8
    %9 = alloca i32
    store i32 4, i32* %9
    %10 = alloca i32
    store i32 2, i32* %10
    %11 = alloca i32
    store i32 7, i32* %11
    %12 = alloca i32
    store i32 9, i32* %12
    %13 = alloca i32
    store i32 8, i32* %13
    %14 = alloca i32
    store i32 1, i32* %14
    %15 = alloca i32
    store i32 4, i32* %15
    %16 = alloca i32
    store i32 6, i32* %16
    %17 = load i32, i32* %1
    %18 = load i32, i32* %2
    %19 = load i32, i32* %1
    %20 = load i32, i32* %2
    %21 = add i32 %19,%20
    %22 = load i32, i32* %3
    %23 = add i32 %21,%22
    %24 = load i32, i32* %4
    %25 = add i32 %23,%24
    %26 = load i32, i32* %5
    %27 = add i32 %25,%26
    %28 = load i32, i32* %6
    %29 = add i32 %27,%28
    %30 = load i32, i32* %7
    %31 = add i32 %29,%30
    %32 = load i32, i32* %8
    %33 = add i32 %31,%32
    %34 = alloca i32
    store i32 %33, i32* %34
    %35 = load i32, i32* %9
    %36 = load i32, i32* %10
    %37 = load i32, i32* %9
    %38 = load i32, i32* %10
    %39 = add i32 %37,%38
    %40 = load i32, i32* %11
    %41 = add i32 %39,%40
    %42 = load i32, i32* %12
    %43 = add i32 %41,%42
    %44 = load i32, i32* %13
    %45 = add i32 %43,%44
    %46 = load i32, i32* %14
    %47 = add i32 %45,%46
    %48 = load i32, i32* %15
    %49 = add i32 %47,%48
    %50 = load i32, i32* %16
    %51 = add i32 %49,%50
    %52 = alloca i32
    store i32 %51, i32* %52
    %53 = load i32, i32* %34
    %54 = call i32 @foo()
    %55 = load i32, i32* %34
    %56 = call i32 @foo()
    %57 = add i32 %55,%56
    store i32 %57, i32* %34
    %58 = alloca i32
    store i32 4, i32* %58
    %59 = alloca i32
    store i32 7, i32* %59
    %60 = alloca i32
    store i32 2, i32* %60
    %61 = alloca i32
    store i32 5, i32* %61
    %62 = alloca i32
    store i32 8, i32* %62
    %63 = alloca i32
    store i32 0, i32* %63
    %64 = alloca i32
    store i32 6, i32* %64
    %65 = alloca i32
    store i32 3, i32* %65
    %66 = load i32, i32* %52
    %67 = call i32 @foo()
    %68 = load i32, i32* %52
    %69 = call i32 @foo()
    %70 = add i32 %68,%69
    store i32 %70, i32* %52
    %71 = load i32, i32* %9
    store i32 %71, i32* %1
    %72 = load i32, i32* %10
    store i32 %72, i32* %2
    %73 = load i32, i32* %11
    store i32 %73, i32* %3
    %74 = load i32, i32* %12
    store i32 %74, i32* %4
    %75 = load i32, i32* %13
    store i32 %75, i32* %5
    %76 = load i32, i32* %14
    store i32 %76, i32* %6
    %77 = load i32, i32* %15
    store i32 %77, i32* %7
    %78 = load i32, i32* %16
    store i32 %78, i32* %8
    %79 = load i32, i32* %58
    %80 = load i32, i32* %59
    %81 = load i32, i32* %58
    %82 = load i32, i32* %59
    %83 = add i32 %81,%82
    %84 = load i32, i32* %60
    %85 = add i32 %83,%84
    %86 = load i32, i32* %61
    %87 = add i32 %85,%86
    %88 = load i32, i32* %62
    %89 = add i32 %87,%88
    %90 = load i32, i32* %63
    %91 = add i32 %89,%90
    %92 = load i32, i32* %64
    %93 = add i32 %91,%92
    %94 = load i32, i32* %65
    %95 = add i32 %93,%94
    %96 = alloca i32
    store i32 %95, i32* %96
    %97 = load i32, i32* %34
    %98 = load i32, i32* %52
    %99 = load i32, i32* %34
    %100 = load i32, i32* %52
    %101 = add i32 %99,%100
    %102 = load i32, i32* %96
    %103 = add i32 %101,%102
    %104 = alloca i32
    store i32 %103, i32* %104
    %105 = load i32, i32* %104
    call void @putint(i32 %105)
    call void @putch(i32 10)
    ret i32 0
}

