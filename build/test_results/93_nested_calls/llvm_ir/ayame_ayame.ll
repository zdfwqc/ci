declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @func1(i32 %0, i32 %1, i32 %2){
3:
    %4 = alloca i32
    store i32 %0, i32* %4
    %5 = alloca i32
    store i32 %1, i32* %5
    %6 = alloca i32
    store i32 %2, i32* %6
    %10 = load i32, i32* %6
    %11 = icmp eq i32 %10,i32 0
    br i32 %11 %8 %9

7:

8:
    %12 = load i32, i32* %4
    %13 = load i32, i32* %5
    %14 = mul i32 %12,%13
    ret i32 %14

9:
    %15 = load i32, i32* %4
    %16 = load i32, i32* %5
    %17 = load i32, i32* %6
    %18 = load i32, i32* %5
    %19 = load i32, i32* %6
    %20 = sub i32 %18,%19
    %21 = call i32 @func1(i32 %15,i32 %20,i32 0)
    ret i32 %21
}
define dso_local i32 @func2(i32 %0, i32 %1){
2:
    %3 = alloca i32
    store i32 %0, i32* %3
    %4 = alloca i32
    store i32 %1, i32* %4
    %8 = load i32, i32* %4
    br i32 %8 %6 %7

5:

6:
    %9 = load i32, i32* %3
    %10 = load i32, i32* %4
    %11 = srem i32 %9,%10
    %12 = call i32 @func2(i32 %11,i32 0)
    ret i32 %12

7:
    %13 = load i32, i32* %3
    ret i32 %13
}
define dso_local i32 @func3(i32 %0, i32 %1){
2:
    %3 = alloca i32
    store i32 %0, i32* %3
    %4 = alloca i32
    store i32 %1, i32* %4
    %8 = load i32, i32* %4
    %9 = icmp eq i32 %8,i32 0
    br i32 %9 %6 %7

5:

6:
    %10 = load i32, i32* %3
    %11 = load i32, i32* %3
    %12 = add i32 %11,1
    ret i32 %12

7:
    %13 = load i32, i32* %3
    %14 = load i32, i32* %4
    %15 = load i32, i32* %3
    %16 = load i32, i32* %4
    %17 = add i32 %15,%16
    %18 = call i32 @func3(i32 %17,i32 0)
    ret i32 %18
}
define dso_local i32 @func4(i32 %0, i32 %1, i32 %2){
3:
    %4 = alloca i32
    store i32 %0, i32* %4
    %5 = alloca i32
    store i32 %1, i32* %5
    %6 = alloca i32
    store i32 %2, i32* %6
    %10 = load i32, i32* %4
    br i32 %10 %8 %9

7:

8:
    %11 = load i32, i32* %5
    ret i32 %11

9:
    %12 = load i32, i32* %6
    ret i32 %12
}
define dso_local i32 @func5(i32 %0){
1:
    %2 = alloca i32
    store i32 %0, i32* %2
    %3 = load i32, i32* %2
    %4 = sub i32 0,%3
    ret i32 %4
}
define dso_local i32 @func6(i32 %0, i32 %1){
2:
    %3 = alloca i32
    store i32 %0, i32* %3
    %4 = alloca i32
    store i32 %1, i32* %4
    %8 = load i32, i32* %3
    %9 = load i32, i32* %4
    %10 = and i32 %8,%9
    br i32 %10 %6 %7

5:

6:
    ret i32 1

7:
    ret i32 0
}
define dso_local i32 @func7(i32 %0){
1:
    %2 = alloca i32
    store i32 %0, i32* %2
    %6 = load i32, i32* %2
    
    br void %7 %4 %5

3:

4:
    ret i32 1

5:
    ret i32 0
}
define dso_local i32 @main(i32 %0){
1:
    %2 = call i32 @getint()
    %3 = alloca i32
    store i32 %2, i32* %3
    %4 = call i32 @getint()
    %5 = alloca i32
    store i32 %4, i32* %5
    %6 = call i32 @getint()
    %7 = alloca i32
    store i32 %6, i32* %7
    %8 = call i32 @getint()
    %9 = alloca i32
    store i32 %8, i32* %9
    %10 = alloca [10 x i32]
    %11 = alloca i32
    store i32 0, i32* %11
    br %12

12:
    %15 = load i32, i32* %11
    %16 = icmp lt i32 %15,i32 10
    br i32 %16 %13 %14

13:
    %17 = load i32, i32* %11
    %18 = getelementptr [10 x i32], [10 x i32]* %10, i32 %17
    %19 = call i32 @getint()
    store i32 %19, i32* %18
    %20 = load i32, i32* %11
    %21 = load i32, i32* %11
    %22 = add i32 %21,1
    store i32 %22, i32* %11
    br %12

14:
    %23 = load i32, i32* %3
    %24 = call i32 @func7(i32 %23)
    %25 = load i32, i32* %5
    %26 = call i32 @func5(i32 %25)
    %27 = call i32 @func6(i32 %25,i32 %26)
    %28 = load i32, i32* %7
    %29 = call i32 @func2(i32 %25,i32 %26,i32 %27,i32 %28)
    %30 = load i32, i32* %9
    %31 = call i32 @func3(i32 %25,i32 %26,i32 %27,i32 %28,i32 %29,i32 %30)
    %32 = call i32 @func5(i32 %25,i32 %26,i32 %27,i32 %28,i32 %29,i32 %30,i32 %31)
    %33 = getelementptr [10 x i32], [10 x i32]* %10, i32 0
    %34 = load i32, i32* %33
    %35 = getelementptr [10 x i32], [10 x i32]* %10, i32 1
    %36 = load i32, i32* %35
    %37 = call i32 @func5(i32 %36)
    %38 = getelementptr [10 x i32], [10 x i32]* %10, i32 2
    %39 = load i32, i32* %38
    %40 = getelementptr [10 x i32], [10 x i32]* %10, i32 3
    %41 = load i32, i32* %40
    %42 = call i32 @func7(i32 %41)
    %43 = call i32 @func6(i32 %41,i32 %42)
    %44 = getelementptr [10 x i32], [10 x i32]* %10, i32 4
    %45 = load i32, i32* %44
    %46 = getelementptr [10 x i32], [10 x i32]* %10, i32 5
    %47 = load i32, i32* %46
    %48 = call i32 @func7(i32 %47)
    %49 = call i32 @func2(i32 %47,i32 %48)
    %50 = call i32 @func4(i32 %47,i32 %48,i32 %49)
    %51 = getelementptr [10 x i32], [10 x i32]* %10, i32 6
    %52 = load i32, i32* %51
    %53 = call i32 @func3(i32 %47,i32 %48,i32 %49,i32 %50,i32 %52)
    %54 = getelementptr [10 x i32], [10 x i32]* %10, i32 7
    %55 = load i32, i32* %54
    %56 = call i32 @func2(i32 %47,i32 %48,i32 %49,i32 %50,i32 %52,i32 %53,i32 %55)
    %57 = getelementptr [10 x i32], [10 x i32]* %10, i32 8
    %58 = load i32, i32* %57
    %59 = getelementptr [10 x i32], [10 x i32]* %10, i32 9
    %60 = load i32, i32* %59
    %61 = call i32 @func7(i32 %60)
    %62 = call i32 @func3(i32 %60,i32 %61)
    %63 = load i32, i32* %3
    %64 = call i32 @func1(i32 %60,i32 %61,i32 %62,i32 %63)
    %65 = call i32 @func4(i32 %60,i32 %61,i32 %62,i32 %63,i32 %64)
    %66 = load i32, i32* %5
    %67 = load i32, i32* %7
    %68 = call i32 @func7(i32 %67)
    %69 = load i32, i32* %9
    %70 = call i32 @func3(i32 %67,i32 %68,i32 %69)
    %71 = call i32 @func2(i32 %67,i32 %68,i32 %69,i32 %70)
    %72 = call i32 @func3(i32 %67,i32 %68,i32 %69,i32 %70,i32 %71)
    %73 = getelementptr [10 x i32], [10 x i32]* %10, i32 0
    %74 = load i32, i32* %73
    %75 = getelementptr [10 x i32], [10 x i32]* %10, i32 1
    %76 = load i32, i32* %75
    %77 = call i32 @func1(i32 %67,i32 %68,i32 %69,i32 %70,i32 %71,i32 %72,i32 %74,i32 %76)
    %78 = getelementptr [10 x i32], [10 x i32]* %10, i32 2
    %79 = load i32, i32* %78
    %80 = call i32 @func2(i32 %67,i32 %68,i32 %69,i32 %70,i32 %71,i32 %72,i32 %74,i32 %76,i32 %77,i32 %79)
    %81 = getelementptr [10 x i32], [10 x i32]* %10, i32 3
    %82 = load i32, i32* %81
    %83 = getelementptr [10 x i32], [10 x i32]* %10, i32 4
    %84 = load i32, i32* %83
    %85 = getelementptr [10 x i32], [10 x i32]* %10, i32 5
    %86 = load i32, i32* %85
    %87 = call i32 @func5(i32 %86)
    %88 = call i32 @func3(i32 %86,i32 %87)
    %89 = getelementptr [10 x i32], [10 x i32]* %10, i32 6
    %90 = load i32, i32* %89
    %91 = call i32 @func5(i32 %90)
    %92 = call i32 @func2(i32 %90,i32 %91)
    %93 = getelementptr [10 x i32], [10 x i32]* %10, i32 7
    %94 = load i32, i32* %93
    %95 = getelementptr [10 x i32], [10 x i32]* %10, i32 8
    %96 = load i32, i32* %95
    %97 = call i32 @func7(i32 %96)
    %98 = call i32 @func1(i32 %96,i32 %97)
    %99 = getelementptr [10 x i32], [10 x i32]* %10, i32 9
    %100 = load i32, i32* %99
    %101 = call i32 @func5(i32 %100)
    %102 = call i32 @func2(i32 %100,i32 %101)
    %103 = load i32, i32* %3
    %104 = call i32 @func3(i32 %100,i32 %101,i32 %102,i32 %103)
    %105 = call i32 @func1(i32 %100,i32 %101,i32 %102,i32 %103,i32 %104)
    %106 = alloca i32
    store i32 %105, i32* %106
    %107 = load i32, i32* %106
    ret i32 %107
}

