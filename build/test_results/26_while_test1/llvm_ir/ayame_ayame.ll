declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @doubleWhile(){
0:
    %1 = alloca i32
    store i32 5, i32* %1
    %2 = alloca i32
    store i32 7, i32* %2
    br %3

3:
    %6 = load i32, i32* %1
    %7 = icmp lt i32 %6,i32 100
    br i32 %7 %4 %5

4:
    %8 = load i32, i32* %1
    %9 = load i32, i32* %1
    %10 = add i32 %9,30
    store i32 %10, i32* %1
    br %11

5:
    %22 = load i32, i32* %2
    ret i32 %22

11:
    %14 = load i32, i32* %2
    %15 = icmp lt i32 %14,i32 100
    br i32 %15 %12 %13

12:
    %16 = load i32, i32* %2
    %17 = load i32, i32* %2
    %18 = add i32 %17,6
    store i32 %18, i32* %2
    br %11

13:
    %19 = load i32, i32* %2
    %20 = load i32, i32* %2
    %21 = sub i32 %20,100
    store i32 %21, i32* %2
    br %3
}
define dso_local i32 @main(){
0:
    %1 = call i32 @doubleWhile()
    ret i32 %1
}

