@n = dso_local global i32 0

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @QuickSort(i32* %0, i32 %1, i32 %2){
3:
    %4 = alloca i32*
    store i32* %0, i32** %4
    %5 = alloca i32
    store i32 %1, i32* %5
    %6 = alloca i32
    store i32 %2, i32* %6
    %9 = load i32, i32* %5
    %10 = load i32, i32* %6
    %11 = icmp lt i32 %9,i32 %10
    br i32 %11 %8 %7

7:
    ret i32 0

8:
    %12 = alloca i32
    %13 = load i32, i32* %5
    store i32 %13, i32* %12
    %14 = alloca i32
    %15 = load i32, i32* %6
    store i32 %15, i32* %14
    %16 = alloca i32
    %17 = load i32, i32* %5
    %18 = getelementptr i32*, i32** %4, i32 %17
    %19 = load i32*, i32** %18
    store i32* %19, i32* %16
    br %20

20:
    %23 = load i32, i32* %12
    %24 = load i32, i32* %14
    %25 = icmp lt i32 %23,i32 %24
    br i32 %25 %21 %22

21:
    br %26

22:
    %84 = load i32, i32* %12
    %85 = getelementptr i32*, i32** %4, i32 %84
    %86 = load i32, i32* %16
    store i32 %86, i32** %85
    %87 = alloca i32
    %88 = load i32, i32* %12
    %89 = load i32, i32* %12
    %90 = sub i32 %89,1
    store i32 %90, i32* %87
    %91 = load i32*, i32** %4
    %92 = load i32, i32* %5
    %93 = load i32, i32* %87
    %94 = call i32 @QuickSort(i32* %91,i32 %92,i32 %93)
    store i32 %94, i32* %87
    %95 = load i32, i32* %12
    %96 = load i32, i32* %12
    %97 = add i32 %96,1
    store i32 %97, i32* %87
    %98 = load i32*, i32** %4
    %99 = load i32, i32* %87
    %100 = load i32, i32* %6
    %101 = call i32 @QuickSort(i32* %98,i32 %99,i32 %100)
    store i32 %101, i32* %87

26:
    %29 = load i32, i32* %12
    %30 = load i32, i32* %14
    %31 = icmp lt i32 %29,i32 %30
    %32 = load i32, i32* %14
    %33 = getelementptr i32*, i32** %4, i32 %32
    %34 = load i32*, i32** %33
    %35 = load i32, i32* %16
    %36 = load i32, i32* %16
    %37 = sub i32 %36,1
    %38 = icmp gt i32* %34,i32 %37
    %39 = and i32 %31,%38
    br i32 %39 %27 %28

27:
    %40 = load i32, i32* %14
    %41 = load i32, i32* %14
    %42 = sub i32 %41,1
    store i32 %42, i32* %14
    br %26

28:
    %45 = load i32, i32* %12
    %46 = load i32, i32* %14
    %47 = icmp lt i32 %45,i32 %46
    br i32 %47 %44 %43

43:
    br %56

44:
    %48 = load i32, i32* %12
    %49 = getelementptr i32*, i32** %4, i32 %48
    %50 = load i32, i32* %14
    %51 = getelementptr i32*, i32** %4, i32 %50
    %52 = load i32*, i32** %51
    store i32* %52, i32** %49
    %53 = load i32, i32* %12
    %54 = load i32, i32* %12
    %55 = add i32 %54,1
    store i32 %55, i32* %12
    br %43

56:
    %59 = load i32, i32* %12
    %60 = load i32, i32* %14
    %61 = icmp lt i32 %59,i32 %60
    %62 = load i32, i32* %12
    %63 = getelementptr i32*, i32** %4, i32 %62
    %64 = load i32*, i32** %63
    %65 = load i32, i32* %16
    %66 = icmp lt i32* %64,i32 %65
    %67 = and i32 %61,%66
    br i32 %67 %57 %58

57:
    %68 = load i32, i32* %12
    %69 = load i32, i32* %12
    %70 = add i32 %69,1
    store i32 %70, i32* %12
    br %56

58:
    %73 = load i32, i32* %12
    %74 = load i32, i32* %14
    %75 = icmp lt i32 %73,i32 %74
    br i32 %75 %72 %71

71:
    br %20

72:
    %76 = load i32, i32* %14
    %77 = getelementptr i32*, i32** %4, i32 %76
    %78 = load i32, i32* %12
    %79 = getelementptr i32*, i32** %4, i32 %78
    %80 = load i32*, i32** %79
    store i32* %80, i32** %77
    %81 = load i32, i32* %14
    %82 = load i32, i32* %14
    %83 = sub i32 %82,1
    store i32 %83, i32* %14
    br %71
}
define dso_local i32 @main(i32* %0, i32 %1, i32 %2){
3:
    store i32 10, i32* @n
    %4 = alloca [10 x i32]
    %5 = getelementptr [10 x i32], [10 x i32]* %4, i32 0
    store i32 4, i32* %5
    %6 = getelementptr [10 x i32], [10 x i32]* %4, i32 1
    store i32 3, i32* %6
    %7 = getelementptr [10 x i32], [10 x i32]* %4, i32 2
    store i32 9, i32* %7
    %8 = getelementptr [10 x i32], [10 x i32]* %4, i32 3
    store i32 2, i32* %8
    %9 = getelementptr [10 x i32], [10 x i32]* %4, i32 4
    store i32 0, i32* %9
    %10 = getelementptr [10 x i32], [10 x i32]* %4, i32 5
    store i32 1, i32* %10
    %11 = getelementptr [10 x i32], [10 x i32]* %4, i32 6
    store i32 6, i32* %11
    %12 = getelementptr [10 x i32], [10 x i32]* %4, i32 7
    store i32 5, i32* %12
    %13 = getelementptr [10 x i32], [10 x i32]* %4, i32 8
    store i32 7, i32* %13
    %14 = getelementptr [10 x i32], [10 x i32]* %4, i32 9
    store i32 8, i32* %14
    %15 = alloca i32
    store i32 0, i32* %15
    %16 = alloca i32
    store i32 9, i32* %16
    %17 = load [10 x i32], [10 x i32]* %4
    %18 = load i32, i32* %15
    %19 = load i32, i32* %16
    %20 = call i32 @QuickSort([10 x i32] %17,i32 %18,i32 %19)
    store i32 %20, i32* %15
    br %21

21:
    %24 = load i32, i32* %15
    %25 = load i32, i32* @n
    %26 = icmp lt i32 %24,i32 %25
    br i32 %26 %22 %23

22:
    %27 = alloca i32
    %28 = load i32, i32* %15
    %29 = getelementptr [10 x i32], [10 x i32]* %4, i32 %28
    %30 = load i32, i32* %29
    store i32 %30, i32* %27
    %31 = load i32, i32* %27
    call void @putint(i32 %31)
    store i32 10, i32* %27
    %32 = load i32, i32* %27
    call void @putch(i32 %32)
    %33 = load i32, i32* %15
    %34 = load i32, i32* %15
    %35 = add i32 %34,1
    store i32 %35, i32* %15
    br %21

23:
    ret i32 0
}

