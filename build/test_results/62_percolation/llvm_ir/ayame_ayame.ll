@array = dso_local global [110 x i32] zeroinitializer

@n = dso_local global i32 110

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local void @init(i32 %0){
1:
    %2 = alloca i32
    store i32 %0, i32* %2
    %3 = alloca i32
    store i32 1, i32* %3
    br %4

4:
    %7 = load i32, i32* %3
    %8 = load i32, i32* %2
    %9 = load i32, i32* %2
    %10 = mul i32 %8,%9
    %11 = load i32, i32* %2
    %12 = load i32, i32* %2
    %13 = mul i32 %11,%12
    %14 = add i32 %13,1
    %15 = icmp le i32 %7,i32 %14
    br i32 %15 %5 %6

5:
    %16 = load i32, i32* %3
    %17 = getelementptr [110 x i32], [110 x i32]* @array, i32 %16
    %18 = sub i32 0,1
    store i32 %18, i32* %17
    %19 = load i32, i32* %3
    %20 = load i32, i32* %3
    %21 = add i32 %20,1
    store i32 %21, i32* %3
    br %4

6:
}
define dso_local i32 @findfa(i32 %0){
1:
    %2 = alloca i32
    store i32 %0, i32* %2
    %6 = load i32, i32* %2
    %7 = getelementptr [110 x i32], [110 x i32]* @array, i32 %6
    %8 = load i32, i32* %7
    %9 = load i32, i32* %2
    %10 = icmp eq i32 %8,i32 %9
    br i32 %10 %4 %5

3:

4:
    %11 = load i32, i32* %2
    ret i32 %11

5:
    %12 = load i32, i32* %2
    %13 = getelementptr [110 x i32], [110 x i32]* @array, i32 %12
    %14 = load i32, i32* %2
    %15 = getelementptr [110 x i32], [110 x i32]* @array, i32 %14
    %16 = load i32, i32* %15
    %17 = call i32 @findfa(i32 %16)
    store i32 %17, i32* %13
    %18 = load i32, i32* %2
    %19 = getelementptr [110 x i32], [110 x i32]* @array, i32 %18
    %20 = load i32, i32* %19
    ret i32 %20
}
define dso_local void @mmerge(i32 %0, i32 %1){
2:
    %3 = alloca i32
    store i32 %0, i32* %3
    %4 = alloca i32
    store i32 %1, i32* %4
    %5 = load i32, i32* %3
    %6 = call i32 @findfa(i32 %5)
    %7 = alloca i32
    store i32 %6, i32* %7
    %8 = load i32, i32* %4
    %9 = call i32 @findfa(i32 %8)
    %10 = alloca i32
    store i32 %9, i32* %10
    %13 = load i32, i32* %7
    %14 = load i32, i32* %10
    %15 = icmp ne i32 %13,i32 %14
    br i32 %15 %12 %11

11:

12:
    %16 = load i32, i32* %7
    %17 = getelementptr [110 x i32], [110 x i32]* @array, i32 %16
    %18 = load i32, i32* %10
    store i32 %18, i32* %17
    br %11
}
define dso_local i32 @main(i32 %0, i32 %1){
2:
    %3 = alloca i32
    %4 = alloca i32
    %5 = alloca i32
    %6 = alloca i32
    store i32 1, i32* %3
    br %7

7:
    %10 = load i32, i32* %3
    br i32 %10 %8 %9

8:
    %11 = load i32, i32* %3
    %12 = load i32, i32* %3
    %13 = sub i32 %12,1
    store i32 %13, i32* %3
    store i32 4, i32* @n
    store i32 10, i32* %4
    %14 = alloca i32
    store i32 0, i32* %14
    %15 = alloca i32
    store i32 0, i32* %15
    %16 = load i32, i32* @n
    %17 = call void @init(i32 %16)
    %18 = load i32, i32* @n
    %19 = load i32, i32* @n
    %20 = mul i32 %18,%19
    %21 = load i32, i32* @n
    %22 = load i32, i32* @n
    %23 = mul i32 %21,%22
    %24 = add i32 %23,1
    %25 = alloca i32
    store i32 %24, i32* %25
    br %26

9:
    ret i32 0

26:
    %29 = load i32, i32* %14
    %30 = load i32, i32* %4
    %31 = icmp lt i32 %29,i32 %30
    br i32 %31 %27 %28

27:
    %32 = call i32 @getint()
    store i32 %32, i32* %5
    %33 = call i32 @getint()
    store i32 %33, i32* %6
    %36 = load i32, i32* %15
    
    br void %37 %35 %34

28:
    %178 = load i32, i32* %15
    
    br void %179 %177 %176

34:
    %173 = load i32, i32* %14
    %174 = load i32, i32* %14
    %175 = add i32 %174,1
    store i32 %175, i32* %14
    br %26

35:
    %38 = load i32, i32* @n
    %39 = load i32, i32* %5
    %40 = load i32, i32* %5
    %41 = sub i32 %40,1
    %42 = mul i32 %38,%41
    %43 = load i32, i32* %6
    %44 = load i32, i32* @n
    %45 = load i32, i32* %5
    %46 = load i32, i32* %5
    %47 = sub i32 %46,1
    %48 = mul i32 %44,%47
    %49 = load i32, i32* %6
    %50 = add i32 %48,%49
    %51 = alloca i32
    store i32 %50, i32* %51
    %52 = load i32, i32* %51
    %53 = getelementptr [110 x i32], [110 x i32]* @array, i32 %52
    %54 = load i32, i32* %51
    store i32 %54, i32* %53
    %57 = load i32, i32* %5
    %58 = icmp eq i32 %57,i32 1
    br i32 %58 %56 %55

55:
    %64 = load i32, i32* %5
    %65 = load i32, i32* @n
    %66 = icmp eq i32 %64,i32 %65
    br i32 %66 %63 %62

56:
    %59 = getelementptr [110 x i32], [110 x i32]* @array, i32 0
    store i32 0, i32* %59
    %60 = load i32, i32* %51
    %61 = call void @mmerge(i32 %60,i32 0)
    br %55

62:
    %75 = load i32, i32* %6
    %76 = load i32, i32* @n
    %77 = icmp lt i32 %75,i32 %76
    %78 = load i32, i32* %51
    %79 = load i32, i32* %51
    %80 = add i32 %79,1
    %81 = getelementptr [110 x i32], [110 x i32]* @array, i32 %80
    %82 = load i32, i32* %81
    %83 = sub i32 0,1
    %84 = icmp ne i32 %82,i32 %83
    %85 = and i32 %77,%84
    br i32 %85 %74 %73

63:
    %67 = load i32, i32* %25
    %68 = getelementptr [110 x i32], [110 x i32]* @array, i32 %67
    %69 = load i32, i32* %25
    store i32 %69, i32* %68
    %70 = load i32, i32* %51
    %71 = load i32, i32* %25
    %72 = call void @mmerge(i32 %70,i32 %71)
    br %62

73:
    %93 = load i32, i32* %6
    %94 = icmp gt i32 %93,i32 1
    %95 = load i32, i32* %51
    %96 = load i32, i32* %51
    %97 = sub i32 %96,1
    %98 = getelementptr [110 x i32], [110 x i32]* @array, i32 %97
    %99 = load i32, i32* %98
    %100 = sub i32 0,1
    %101 = icmp ne i32 %99,i32 %100
    %102 = and i32 %94,%101
    br i32 %102 %92 %91

74:
    %86 = load i32, i32* %51
    %87 = load i32, i32* %51
    %88 = load i32, i32* %51
    %89 = add i32 %88,1
    %90 = call void @mmerge(i32 %86,i32 %89)
    br %73

91:
    %110 = load i32, i32* %5
    %111 = load i32, i32* @n
    %112 = icmp lt i32 %110,i32 %111
    %113 = load i32, i32* %51
    %114 = load i32, i32* @n
    %115 = load i32, i32* %51
    %116 = load i32, i32* @n
    %117 = add i32 %115,%116
    %118 = getelementptr [110 x i32], [110 x i32]* @array, i32 %117
    %119 = load i32, i32* %118
    %120 = sub i32 0,1
    %121 = icmp ne i32 %119,i32 %120
    %122 = and i32 %112,%121
    br i32 %122 %109 %108

92:
    %103 = load i32, i32* %51
    %104 = load i32, i32* %51
    %105 = load i32, i32* %51
    %106 = sub i32 %105,1
    %107 = call void @mmerge(i32 %103,i32 %106)
    br %91

108:
    %132 = load i32, i32* %5
    %133 = icmp gt i32 %132,i32 1
    %134 = load i32, i32* %51
    %135 = load i32, i32* @n
    %136 = load i32, i32* %51
    %137 = load i32, i32* @n
    %138 = sub i32 %136,%137
    %139 = getelementptr [110 x i32], [110 x i32]* @array, i32 %138
    %140 = load i32, i32* %139
    %141 = sub i32 0,1
    %142 = icmp ne i32 %140,i32 %141
    %143 = and i32 %133,%142
    br i32 %143 %131 %130

109:
    %123 = load i32, i32* %51
    %124 = load i32, i32* %51
    %125 = load i32, i32* @n
    %126 = load i32, i32* %51
    %127 = load i32, i32* @n
    %128 = add i32 %126,%127
    %129 = call void @mmerge(i32 %123,i32 %128)
    br %108

130:
    %153 = getelementptr [110 x i32], [110 x i32]* @array, i32 0
    %154 = load i32, i32* %153
    %155 = sub i32 0,1
    %156 = icmp ne i32 %154,i32 %155
    %157 = load i32, i32* %25
    %158 = getelementptr [110 x i32], [110 x i32]* @array, i32 %157
    %159 = load i32, i32* %158
    %160 = sub i32 0,1
    %161 = icmp ne i32 %159,i32 %160
    %162 = and i32 %156,%161
    %163 = call i32 @findfa(i32 0)
    %164 = load i32, i32* %25
    %165 = call i32 @findfa(i32 %164)
    %166 = icmp eq i32 %163,i32 %165
    %167 = and i32 %162,%166
    br i32 %167 %152 %151

131:
    %144 = load i32, i32* %51
    %145 = load i32, i32* %51
    %146 = load i32, i32* @n
    %147 = load i32, i32* %51
    %148 = load i32, i32* @n
    %149 = sub i32 %147,%148
    %150 = call void @mmerge(i32 %144,i32 %149)
    br %130

151:

152:
    store i32 1, i32* %15
    %168 = load i32, i32* %14
    %169 = load i32, i32* %14
    %170 = add i32 %169,1
    %171 = alloca i32
    store i32 %170, i32* %171
    %172 = load i32, i32* %171
    call void @putint(i32 %172)
    call void @putch(i32 10)
    br %151

176:
    br %7

177:
    %180 = sub i32 0,1
    call void @putint(i32 %180)
    call void @putch(i32 10)
    br %176
}

