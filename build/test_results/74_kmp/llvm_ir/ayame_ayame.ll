declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local void @get_next(i32* %0, i32* %1){
2:
    %3 = alloca i32*
    store i32* %0, i32** %3
    %4 = alloca i32*
    store i32* %1, i32** %4
    %5 = getelementptr i32*, i32** %4, i32 0
    %6 = sub i32 0,1
    store i32 %6, i32** %5
    %7 = alloca i32
    store i32 0, i32* %7
    %8 = sub i32 0,1
    %9 = alloca i32
    store i32 %8, i32* %9
    br %10

10:
    %13 = load i32, i32* %7
    %14 = getelementptr i32*, i32** %3, i32 %13
    %15 = load i32*, i32** %14
    br i32* %15 %11 %12

11:
    %19 = load i32, i32* %9
    %20 = sub i32 0,1
    %21 = icmp eq i32 %19,i32 %20
    %22 = load i32, i32* %7
    %23 = getelementptr i32*, i32** %3, i32 %22
    %24 = load i32*, i32** %23
    %25 = load i32, i32* %9
    %26 = getelementptr i32*, i32** %3, i32 %25
    %27 = load i32*, i32** %26
    %28 = icmp eq i32* %24,i32* %27
    %29 = or i32 %21,%28
    br i32 %29 %17 %18

12:

16:
    br %10

17:
    %30 = load i32, i32* %9
    %31 = load i32, i32* %9
    %32 = add i32 %31,1
    store i32 %32, i32* %9
    %33 = load i32, i32* %7
    %34 = load i32, i32* %7
    %35 = add i32 %34,1
    store i32 %35, i32* %7
    %36 = load i32, i32* %7
    %37 = getelementptr i32*, i32** %4, i32 %36
    %38 = load i32, i32* %9
    store i32 %38, i32** %37
    br %16

18:
    %39 = load i32, i32* %9
    %40 = getelementptr i32*, i32** %4, i32 %39
    %41 = load i32*, i32** %40
    store i32* %41, i32* %9
    br %16
}
define dso_local i32 @KMP(i32* %0, i32* %1){
2:
    %3 = alloca i32*
    store i32* %0, i32** %3
    %4 = alloca i32*
    store i32* %1, i32** %4
    %5 = alloca [4096 x i32]
    %6 = load i32*, i32** %3
    %7 = load [4096 x i32], [4096 x i32]* %5
    %8 = call void @get_next(i32* %6,[4096 x i32] %7)
    %9 = alloca i32
    store i32 0, i32* %9
    %10 = alloca i32
    store i32 0, i32* %10
    br %11

11:
    %14 = load i32, i32* %10
    %15 = getelementptr i32*, i32** %4, i32 %14
    %16 = load i32*, i32** %15
    br i32* %16 %12 %13

12:
    %20 = load i32, i32* %9
    %21 = getelementptr i32*, i32** %3, i32 %20
    %22 = load i32*, i32** %21
    %23 = load i32, i32* %10
    %24 = getelementptr i32*, i32** %4, i32 %23
    %25 = load i32*, i32** %24
    %26 = icmp eq i32* %22,i32* %25
    br i32* %26 %18 %19

13:
    %54 = sub i32 0,1
    ret i32 %54

17:
    br %11

18:
    %27 = load i32, i32* %9
    %28 = load i32, i32* %9
    %29 = add i32 %28,1
    store i32 %29, i32* %9
    %30 = load i32, i32* %10
    %31 = load i32, i32* %10
    %32 = add i32 %31,1
    store i32 %32, i32* %10
    %35 = load i32, i32* %9
    %36 = getelementptr i32*, i32** %3, i32 %35
    %37 = load i32*, i32** %36
    
    br void %38 %34 %33

19:
    %40 = load i32, i32* %9
    %41 = getelementptr [4096 x i32], [4096 x i32]* %5, i32 %40
    %42 = load i32, i32* %41
    store i32 %42, i32* %9
    %45 = load i32, i32* %9
    %46 = sub i32 0,1
    %47 = icmp eq i32 %45,i32 %46
    br i32 %47 %44 %43

33:

34:
    %39 = load i32, i32* %10
    ret i32 %39

43:

44:
    %48 = load i32, i32* %9
    %49 = load i32, i32* %9
    %50 = add i32 %49,1
    store i32 %50, i32* %9
    %51 = load i32, i32* %10
    %52 = load i32, i32* %10
    %53 = add i32 %52,1
    store i32 %53, i32* %10
    br %43
}
define dso_local i32 @read_str(i32* %0){
1:
    %2 = alloca i32*
    store i32* %0, i32** %2
    %3 = alloca i32
    store i32 0, i32* %3
    br %4

4:
    br i32 1 %5 %6

5:
    %7 = load i32, i32* %3
    %8 = getelementptr i32*, i32** %2, i32 %7
    %9 = call i32 @getch()
    store i32 %9, i32** %8
    %12 = load i32, i32* %3
    %13 = getelementptr i32*, i32** %2, i32 %12
    %14 = load i32*, i32** %13
    %15 = icmp eq i32* %14,i32 10
    br i32* %15 %11 %10

6:
    %19 = load i32, i32* %3
    %20 = getelementptr i32*, i32** %2, i32 %19
    store i32 0, i32** %20
    %21 = load i32, i32* %3
    ret i32 %21

10:
    %16 = load i32, i32* %3
    %17 = load i32, i32* %3
    %18 = add i32 %17,1
    store i32 %18, i32* %3
    br %4

11:
    br %6
}
define dso_local i32 @main(i32* %0){
1:
    %2 = alloca [4096 x i32]
    %3 = alloca [4096 x i32]
    %4 = load [4096 x i32], [4096 x i32]* %2
    %5 = call i32 @read_str([4096 x i32] %4)
    %6 = load [4096 x i32], [4096 x i32]* %3
    %7 = call i32 @read_str([4096 x i32] %6)
    %8 = load [4096 x i32], [4096 x i32]* %2
    %9 = load [4096 x i32], [4096 x i32]* %3
    %10 = call i32 @KMP([4096 x i32] %8,[4096 x i32] %9)
    call void @putint([4096 x i32] %8,[4096 x i32] %9,i32 %10)
    call void @putch(i32 10)
    ret i32 0
}

