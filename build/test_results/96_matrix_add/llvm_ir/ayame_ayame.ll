@M = dso_local global i32 0

@L = dso_local global i32 0

@N = dso_local global i32 0

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @add(float* %0, float* %1, float* %2, float* %3, float* %4, float* %5, float* %6, float* %7, float* %8){
9:
    %10 = alloca float*
    store float* %0, float** %10
    %11 = alloca float*
    store float* %1, float** %11
    %12 = alloca float*
    store float* %2, float** %12
    %13 = alloca float*
    store float* %3, float** %13
    %14 = alloca float*
    store float* %4, float** %14
    %15 = alloca float*
    store float* %5, float** %15
    %16 = alloca float*
    store float* %6, float** %16
    %17 = alloca float*
    store float* %7, float** %17
    %18 = alloca float*
    store float* %8, float** %18
    %19 = alloca i32
    store i32 0, i32* %19
    br %20

20:
    %23 = load i32, i32* %19
    %24 = load i32, i32* @M
    %25 = icmp lt i32 %23,i32 %24
    br i32 %25 %21 %22

21:
    %26 = load i32, i32* %19
    %27 = getelementptr float*, float** %16, i32 %26
    %28 = load i32, i32* %19
    %29 = getelementptr float*, float** %10, i32 %28
    %30 = load float*, float** %29
    %31 = load i32, i32* %19
    %32 = getelementptr float*, float** %13, i32 %31
    %33 = load float*, float** %32
    %34 = load i32, i32* %19
    %35 = getelementptr float*, float** %10, i32 %34
    %36 = load float*, float** %35
    %37 = load i32, i32* %19
    %38 = getelementptr float*, float** %13, i32 %37
    %39 = load float*, float** %38
    %40 = add i32 %36,%39
    store float* %40, float** %27
    %41 = load i32, i32* %19
    %42 = getelementptr float*, float** %17, i32 %41
    %43 = load i32, i32* %19
    %44 = getelementptr float*, float** %11, i32 %43
    %45 = load float*, float** %44
    %46 = load i32, i32* %19
    %47 = getelementptr float*, float** %14, i32 %46
    %48 = load float*, float** %47
    %49 = load i32, i32* %19
    %50 = getelementptr float*, float** %11, i32 %49
    %51 = load float*, float** %50
    %52 = load i32, i32* %19
    %53 = getelementptr float*, float** %14, i32 %52
    %54 = load float*, float** %53
    %55 = add i32 %51,%54
    store float* %55, float** %42
    %56 = load i32, i32* %19
    %57 = getelementptr float*, float** %18, i32 %56
    %58 = load i32, i32* %19
    %59 = getelementptr float*, float** %12, i32 %58
    %60 = load float*, float** %59
    %61 = load i32, i32* %19
    %62 = getelementptr float*, float** %15, i32 %61
    %63 = load float*, float** %62
    %64 = load i32, i32* %19
    %65 = getelementptr float*, float** %12, i32 %64
    %66 = load float*, float** %65
    %67 = load i32, i32* %19
    %68 = getelementptr float*, float** %15, i32 %67
    %69 = load float*, float** %68
    %70 = add i32 %66,%69
    store float* %70, float** %57
    %71 = load i32, i32* %19
    %72 = load i32, i32* %19
    %73 = add i32 %72,1
    store i32 %73, i32* %19
    br %20

22:
    ret i32 0
}
define dso_local i32 @main(float* %0, float* %1, float* %2, float* %3, float* %4, float* %5, float* %6, float* %7, float* %8){
9:
    store i32 3, i32* @N
    store i32 3, i32* @M
    store i32 3, i32* @L
    %10 = alloca [3 x float]
    %11 = alloca [3 x float]
    %12 = alloca [3 x float]
    %13 = alloca [3 x float]
    %14 = alloca [3 x float]
    %15 = alloca [3 x float]
    %16 = alloca [6 x float]
    %17 = alloca [3 x float]
    %18 = alloca [3 x float]
    %19 = alloca i32
    store i32 0, i32* %19
    br %20

20:
    %23 = load i32, i32* %19
    %24 = load i32, i32* @M
    %25 = icmp lt i32 %23,i32 %24
    br i32 %25 %21 %22

21:
    %26 = load i32, i32* %19
    %27 = getelementptr [3 x float], [3 x float]* %10, i32 %26
    %28 = load i32, i32* %19
    %29 = itof i32 %28 to float
    store float %29, float* %27
    %30 = load i32, i32* %19
    %31 = getelementptr [3 x float], [3 x float]* %11, i32 %30
    %32 = load i32, i32* %19
    %33 = itof i32 %32 to float
    store float %33, float* %31
    %34 = load i32, i32* %19
    %35 = getelementptr [3 x float], [3 x float]* %12, i32 %34
    %36 = load i32, i32* %19
    %37 = itof i32 %36 to float
    store float %37, float* %35
    %38 = load i32, i32* %19
    %39 = getelementptr [3 x float], [3 x float]* %13, i32 %38
    %40 = load i32, i32* %19
    %41 = itof i32 %40 to float
    store float %41, float* %39
    %42 = load i32, i32* %19
    %43 = getelementptr [3 x float], [3 x float]* %14, i32 %42
    %44 = load i32, i32* %19
    %45 = itof i32 %44 to float
    store float %45, float* %43
    %46 = load i32, i32* %19
    %47 = getelementptr [3 x float], [3 x float]* %15, i32 %46
    %48 = load i32, i32* %19
    %49 = itof i32 %48 to float
    store float %49, float* %47
    %50 = load i32, i32* %19
    %51 = load i32, i32* %19
    %52 = add i32 %51,1
    store i32 %52, i32* %19
    br %20

22:
    %53 = load [3 x float], [3 x float]* %10
    %54 = load [3 x float], [3 x float]* %11
    %55 = load [3 x float], [3 x float]* %12
    %56 = load [3 x float], [3 x float]* %13
    %57 = load [3 x float], [3 x float]* %14
    %58 = load [3 x float], [3 x float]* %15
    %59 = load [6 x float], [6 x float]* %16
    %60 = load [3 x float], [3 x float]* %17
    %61 = load [3 x float], [3 x float]* %18
    %62 = call i32 @add([3 x float] %53,[3 x float] %54,[3 x float] %55,[3 x float] %56,[3 x float] %57,[3 x float] %58,[6 x float] %59,[3 x float] %60,[3 x float] %61)
    store i32 %62, i32* %19
    %63 = alloca i32
    br %64

64:
    %67 = load i32, i32* %19
    %68 = load i32, i32* @N
    %69 = icmp lt i32 %67,i32 %68
    br i32 %69 %65 %66

65:
    %70 = load i32, i32* %19
    %71 = getelementptr [6 x float], [6 x float]* %16, i32 %70
    %72 = load float, float* %71
    %73 = ftoi float %72 to i32
    store i32 %73, i32* %63
    %74 = load i32, i32* %63
    call void @putint(i32 %74)
    %75 = load i32, i32* %19
    %76 = load i32, i32* %19
    %77 = add i32 %76,1
    store i32 %77, i32* %19
    br %64

66:
    store i32 10, i32* %63
    %78 = load i32, i32* %63
    call void @putch(i32 %78)
    store i32 0, i32* %19
    br %79

79:
    %82 = load i32, i32* %19
    %83 = load i32, i32* @N
    %84 = icmp lt i32 %82,i32 %83
    br i32 %84 %80 %81

80:
    %85 = load i32, i32* %19
    %86 = getelementptr [3 x float], [3 x float]* %17, i32 %85
    %87 = load float, float* %86
    %88 = ftoi float %87 to i32
    store i32 %88, i32* %63
    %89 = load i32, i32* %63
    call void @putint(i32 %89)
    %90 = load i32, i32* %19
    %91 = load i32, i32* %19
    %92 = add i32 %91,1
    store i32 %92, i32* %19
    br %79

81:
    store i32 10, i32* %63
    %93 = load i32, i32* %63
    call void @putch(i32 %93)
    store i32 0, i32* %19
    br %94

94:
    %97 = load i32, i32* %19
    %98 = load i32, i32* @N
    %99 = icmp lt i32 %97,i32 %98
    br i32 %99 %95 %96

95:
    %100 = load i32, i32* %19
    %101 = getelementptr [3 x float], [3 x float]* %18, i32 %100
    %102 = load float, float* %101
    %103 = ftoi float %102 to i32
    store i32 %103, i32* %63
    %104 = load i32, i32* %63
    call void @putint(i32 %104)
    %105 = load i32, i32* %19
    %106 = load i32, i32* %19
    %107 = add i32 %106,1
    store i32 %107, i32* %19
    br %94

96:
    store i32 10, i32* %63
    %108 = load i32, i32* %63
    call void @putch(i32 %108)
    ret i32 0
}

