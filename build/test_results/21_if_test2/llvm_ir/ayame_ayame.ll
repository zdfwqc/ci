declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @ifElseIf(){
0:
    %1 = alloca i32
    store i32 5, i32* %1
    %2 = alloca i32
    store i32 10, i32* %2
    %6 = load i32, i32* %1
    %7 = icmp eq i32 %6,i32 6
    %8 = load i32, i32* %2
    %9 = icmp eq i32 %8,i32 11
    %10 = or i32 %7,%9
    br i32 %10 %4 %5

3:
    %34 = load i32, i32* %1
    ret i32 %34

4:
    %11 = load i32, i32* %1
    ret i32 %11

5:
    %15 = load i32, i32* %2
    %16 = icmp eq i32 %15,i32 10
    %17 = load i32, i32* %1
    %18 = icmp eq i32 %17,i32 1
    %19 = and i32 %16,%18
    br i32 %19 %13 %14

12:

13:
    store i32 25, i32* %1
    br %12

14:
    %23 = load i32, i32* %2
    %24 = icmp eq i32 %23,i32 10
    %25 = load i32, i32* %1
    %26 = sub i32 0,5
    %27 = icmp eq i32 %25,i32 %26
    %28 = and i32 %24,%27
    br i32 %28 %21 %22

20:

21:
    %29 = load i32, i32* %1
    %30 = load i32, i32* %1
    %31 = add i32 %30,15
    store i32 %31, i32* %1
    br %20

22:
    %32 = load i32, i32* %1
    %33 = sub i32 0,%32
    store i32 %33, i32* %1
    br %20
}
define dso_local i32 @main(){
0:
    %1 = call i32 @ifElseIf()
    call void @putint(i32 %1)
    ret i32 0
}

