@N = dso_local global i32 0

@M = dso_local global i32 0

@L = dso_local global i32 0

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @sub(float* %0, float* %1, float* %2, float* %3, float* %4, float* %5, float* %6, float* %7, float* %8){
9:
    %10 = alloca float*
    store float* %0, float** %10
    %11 = alloca float*
    store float* %1, float** %11
    %12 = alloca float*
    store float* %2, float** %12
    %13 = alloca float*
    store float* %3, float** %13
    %14 = alloca float*
    store float* %4, float** %14
    %15 = alloca float*
    store float* %5, float** %15
    %16 = alloca float*
    store float* %6, float** %16
    %17 = alloca float*
    store float* %7, float** %17
    %18 = alloca float*
    store float* %8, float** %18
    %19 = alloca i32
    store i32 0, i32* %19
    br %20

20:
    %23 = load i32, i32* %19
    %24 = icmp lt i32 %23,i32 3
    br i32 %24 %21 %22

21:
    %25 = load i32, i32* %19
    %26 = getelementptr float*, float** %16, i32 %25
    %27 = load i32, i32* %19
    %28 = getelementptr float*, float** %10, i32 %27
    %29 = load float*, float** %28
    %30 = load i32, i32* %19
    %31 = getelementptr float*, float** %13, i32 %30
    %32 = load float*, float** %31
    %33 = load i32, i32* %19
    %34 = getelementptr float*, float** %10, i32 %33
    %35 = load float*, float** %34
    %36 = load i32, i32* %19
    %37 = getelementptr float*, float** %13, i32 %36
    %38 = load float*, float** %37
    %39 = sub i32 %35,%38
    store float* %39, float** %26
    %40 = load i32, i32* %19
    %41 = getelementptr float*, float** %17, i32 %40
    %42 = load i32, i32* %19
    %43 = getelementptr float*, float** %11, i32 %42
    %44 = load float*, float** %43
    %45 = load i32, i32* %19
    %46 = getelementptr float*, float** %14, i32 %45
    %47 = load float*, float** %46
    %48 = load i32, i32* %19
    %49 = getelementptr float*, float** %11, i32 %48
    %50 = load float*, float** %49
    %51 = load i32, i32* %19
    %52 = getelementptr float*, float** %14, i32 %51
    %53 = load float*, float** %52
    %54 = sub i32 %50,%53
    store float* %54, float** %41
    %55 = load i32, i32* %19
    %56 = getelementptr float*, float** %18, i32 %55
    %57 = load i32, i32* %19
    %58 = getelementptr float*, float** %12, i32 %57
    %59 = load float*, float** %58
    %60 = load i32, i32* %19
    %61 = getelementptr float*, float** %15, i32 %60
    %62 = load float*, float** %61
    %63 = load i32, i32* %19
    %64 = getelementptr float*, float** %12, i32 %63
    %65 = load float*, float** %64
    %66 = load i32, i32* %19
    %67 = getelementptr float*, float** %15, i32 %66
    %68 = load float*, float** %67
    %69 = sub i32 %65,%68
    store float* %69, float** %56
    %70 = load i32, i32* %19
    %71 = load i32, i32* %19
    %72 = add i32 %71,1
    store i32 %72, i32* %19
    br %20

22:
    ret i32 0
}
define dso_local i32 @main(float* %0, float* %1, float* %2, float* %3, float* %4, float* %5, float* %6, float* %7, float* %8){
9:
    store i32 3, i32* @N
    store i32 3, i32* @M
    store i32 3, i32* @L
    %10 = alloca [3 x float]
    %11 = alloca [3 x float]
    %12 = alloca [3 x float]
    %13 = alloca [3 x float]
    %14 = alloca [3 x float]
    %15 = alloca [3 x float]
    %16 = alloca [6 x float]
    %17 = alloca [3 x float]
    %18 = alloca [3 x float]
    %19 = alloca i32
    store i32 0, i32* %19
    br %20

20:
    %23 = load i32, i32* %19
    %24 = icmp lt i32 %23,i32 3
    br i32 %24 %21 %22

21:
    %25 = load i32, i32* %19
    %26 = getelementptr [3 x float], [3 x float]* %10, i32 %25
    %27 = load i32, i32* %19
    %28 = itof i32 %27 to float
    store float %28, float* %26
    %29 = load i32, i32* %19
    %30 = getelementptr [3 x float], [3 x float]* %11, i32 %29
    %31 = load i32, i32* %19
    %32 = itof i32 %31 to float
    store float %32, float* %30
    %33 = load i32, i32* %19
    %34 = getelementptr [3 x float], [3 x float]* %12, i32 %33
    %35 = load i32, i32* %19
    %36 = itof i32 %35 to float
    store float %36, float* %34
    %37 = load i32, i32* %19
    %38 = getelementptr [3 x float], [3 x float]* %13, i32 %37
    %39 = load i32, i32* %19
    %40 = itof i32 %39 to float
    store float %40, float* %38
    %41 = load i32, i32* %19
    %42 = getelementptr [3 x float], [3 x float]* %14, i32 %41
    %43 = load i32, i32* %19
    %44 = itof i32 %43 to float
    store float %44, float* %42
    %45 = load i32, i32* %19
    %46 = getelementptr [3 x float], [3 x float]* %15, i32 %45
    %47 = load i32, i32* %19
    %48 = itof i32 %47 to float
    store float %48, float* %46
    %49 = load i32, i32* %19
    %50 = load i32, i32* %19
    %51 = add i32 %50,1
    store i32 %51, i32* %19
    br %20

22:
    %52 = load [3 x float], [3 x float]* %10
    %53 = load [3 x float], [3 x float]* %11
    %54 = load [3 x float], [3 x float]* %12
    %55 = load [3 x float], [3 x float]* %13
    %56 = load [3 x float], [3 x float]* %14
    %57 = load [3 x float], [3 x float]* %15
    %58 = load [6 x float], [6 x float]* %16
    %59 = load [3 x float], [3 x float]* %17
    %60 = load [3 x float], [3 x float]* %18
    %61 = call i32 @sub([3 x float] %52,[3 x float] %53,[3 x float] %54,[3 x float] %55,[3 x float] %56,[3 x float] %57,[6 x float] %58,[3 x float] %59,[3 x float] %60)
    store i32 %61, i32* %19
    %62 = alloca i32
    br %63

63:
    %66 = load i32, i32* %19
    %67 = icmp lt i32 %66,i32 3
    br i32 %67 %64 %65

64:
    %68 = load i32, i32* %19
    %69 = getelementptr [6 x float], [6 x float]* %16, i32 %68
    %70 = load float, float* %69
    %71 = ftoi float %70 to i32
    store i32 %71, i32* %62
    %72 = load i32, i32* %62
    call void @putint(i32 %72)
    %73 = load i32, i32* %19
    %74 = load i32, i32* %19
    %75 = add i32 %74,1
    store i32 %75, i32* %19
    br %63

65:
    store i32 10, i32* %62
    store i32 0, i32* %19
    %76 = load i32, i32* %62
    call void @putch(i32 %76)
    br %77

77:
    %80 = load i32, i32* %19
    %81 = icmp lt i32 %80,i32 3
    br i32 %81 %78 %79

78:
    %82 = load i32, i32* %19
    %83 = getelementptr [3 x float], [3 x float]* %17, i32 %82
    %84 = load float, float* %83
    %85 = ftoi float %84 to i32
    store i32 %85, i32* %62
    %86 = load i32, i32* %62
    call void @putint(i32 %86)
    %87 = load i32, i32* %19
    %88 = load i32, i32* %19
    %89 = add i32 %88,1
    store i32 %89, i32* %19
    br %77

79:
    store i32 10, i32* %62
    store i32 0, i32* %19
    %90 = load i32, i32* %62
    call void @putch(i32 %90)
    br %91

91:
    %94 = load i32, i32* %19
    %95 = icmp lt i32 %94,i32 3
    br i32 %95 %92 %93

92:
    %96 = load i32, i32* %19
    %97 = getelementptr [3 x float], [3 x float]* %18, i32 %96
    %98 = load float, float* %97
    %99 = ftoi float %98 to i32
    store i32 %99, i32* %62
    %100 = load i32, i32* %62
    call void @putint(i32 %100)
    %101 = load i32, i32* %19
    %102 = load i32, i32* %19
    %103 = add i32 %102,1
    store i32 %103, i32* %19
    br %91

93:
    store i32 10, i32* %62
    %104 = load i32, i32* %62
    call void @putch(i32 %104)
    ret i32 0
}

