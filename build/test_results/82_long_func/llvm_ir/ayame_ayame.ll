@SHIFT_TABLE = dso_local constant [i32 1, i32 2, i32 4, i32 8, i32 16, i32 32, i32 64, i32 128, i32 256, i32 512, i32 1024, i32 2048, i32 4096, i32 8192, i32 16384, i32 32768]

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @long_func(){
0:
    %1 = alloca i32
    %2 = alloca i32
    %3 = alloca i32
    %4 = alloca i32
    %5 = alloca i32
    %6 = alloca i32
    store i32 2, i32* %6
    %7 = alloca i32
    store i32 0, i32* %7
    %8 = alloca i32
    store i32 1, i32* %8
    br %9

9:
    %12 = load i32, i32* %7
    %13 = icmp gt i32 %12,i32 0
    br i32 %13 %10 %11

10:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %14 = load i32, i32* %7
    store i32 %14, i32* %3
    store i32 1, i32* %4
    br %15

11:
    %807 = load i32, i32* %8
    store i32 %807, i32* %1
    %808 = load i32, i32* %1
    call void @putint(i32 %808)
    call void @putch(i32 10)
    %809 = alloca i32
    store i32 2, i32* %809
    %810 = alloca i32
    store i32 1, i32* %810
    %811 = alloca i32
    store i32 1, i32* %811
    br %812

15:
    %18 = load i32, i32* %2
    %19 = icmp lt i32 %18,i32 16
    br i32 %19 %16 %17

16:
    %22 = load i32, i32* %3
    %23 = srem i32 %22,2
    %24 = load i32, i32* %4
    %25 = srem i32 %24,2
    %26 = and i32 %23,%25
    br i32 %26 %21 %20

17:
    %47 = load i32, i32* %1
    br i32 %47 %46 %45

20:
    %38 = load i32, i32* %3
    %39 = sdiv i32 %38,2
    store i32 %39, i32* %3
    %40 = load i32, i32* %4
    %41 = sdiv i32 %40,2
    store i32 %41, i32* %4
    %42 = load i32, i32* %2
    %43 = load i32, i32* %2
    %44 = add i32 %43,1
    store i32 %44, i32* %2
    br %15

21:
    %27 = load i32, i32* %1
    %28 = load i32, i32* %2
    %29 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %28
    %30 = load i32, i32* %29
    %31 = mul i32 1,%30
    %32 = load i32, i32* %1
    %33 = load i32, i32* %2
    %34 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %33
    %35 = load i32, i32* %34
    %36 = mul i32 1,%35
    %37 = add i32 %32,%36
    store i32 %37, i32* %1
    br %20

45:
    %406 = load i32, i32* %6
    %407 = alloca i32
    store i32 %406, i32* %407
    %408 = load i32, i32* %6
    %409 = alloca i32
    store i32 %408, i32* %409
    %410 = alloca i32
    store i32 0, i32* %410
    br %411

46:
    %48 = load i32, i32* %8
    %49 = alloca i32
    store i32 %48, i32* %49
    %50 = load i32, i32* %6
    %51 = alloca i32
    store i32 %50, i32* %51
    %52 = alloca i32
    store i32 0, i32* %52
    br %53

53:
    %56 = load i32, i32* %51
    br i32 %56 %54 %55

54:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %57 = load i32, i32* %51
    store i32 %57, i32* %3
    store i32 1, i32* %4
    br %58

55:
    %404 = load i32, i32* %52
    store i32 %404, i32* %1
    %405 = load i32, i32* %1
    store i32 %405, i32* %8

58:
    %61 = load i32, i32* %2
    %62 = icmp lt i32 %61,i32 16
    br i32 %62 %59 %60

59:
    %65 = load i32, i32* %3
    %66 = srem i32 %65,2
    %67 = load i32, i32* %4
    %68 = srem i32 %67,2
    %69 = and i32 %66,%68
    br i32 %69 %64 %63

60:
    %90 = load i32, i32* %1
    br i32 %90 %89 %88

63:
    %81 = load i32, i32* %3
    %82 = sdiv i32 %81,2
    store i32 %82, i32* %3
    %83 = load i32, i32* %4
    %84 = sdiv i32 %83,2
    store i32 %84, i32* %4
    %85 = load i32, i32* %2
    %86 = load i32, i32* %2
    %87 = add i32 %86,1
    store i32 %87, i32* %2
    br %58

64:
    %70 = load i32, i32* %1
    %71 = load i32, i32* %2
    %72 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %71
    %73 = load i32, i32* %72
    %74 = mul i32 1,%73
    %75 = load i32, i32* %1
    %76 = load i32, i32* %2
    %77 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %76
    %78 = load i32, i32* %77
    %79 = mul i32 1,%78
    %80 = add i32 %75,%79
    store i32 %80, i32* %1
    br %63

88:
    %226 = load i32, i32* %49
    %227 = alloca i32
    store i32 %226, i32* %227
    %228 = load i32, i32* %49
    %229 = alloca i32
    store i32 %228, i32* %229
    %230 = alloca i32
    br %231

89:
    %91 = load i32, i32* %52
    %92 = alloca i32
    store i32 %91, i32* %92
    %93 = load i32, i32* %49
    %94 = alloca i32
    store i32 %93, i32* %94
    %95 = alloca i32
    br %96

96:
    %99 = load i32, i32* %94
    br i32 %99 %97 %98

97:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %100 = load i32, i32* %92
    store i32 %100, i32* %3
    %101 = load i32, i32* %94
    store i32 %101, i32* %4
    br %102

98:
    %224 = load i32, i32* %92
    store i32 %224, i32* %1
    %225 = load i32, i32* %1
    store i32 %225, i32* %52

102:
    %105 = load i32, i32* %2
    %106 = icmp lt i32 %105,i32 16
    br i32 %106 %103 %104

103:
    %110 = load i32, i32* %3
    %111 = srem i32 %110,2
    br i32 %111 %108 %109

104:
    %150 = load i32, i32* %1
    store i32 %150, i32* %95
    store i32 0, i32* %1
    store i32 0, i32* %2
    %151 = load i32, i32* %92
    store i32 %151, i32* %3
    %152 = load i32, i32* %94
    store i32 %152, i32* %4
    br %153

107:
    %143 = load i32, i32* %3
    %144 = sdiv i32 %143,2
    store i32 %144, i32* %3
    %145 = load i32, i32* %4
    %146 = sdiv i32 %145,2
    store i32 %146, i32* %4
    %147 = load i32, i32* %2
    %148 = load i32, i32* %2
    %149 = add i32 %148,1
    store i32 %149, i32* %2
    br %102

108:
    %114 = load i32, i32* %4
    %115 = srem i32 %114,2
    %116 = icmp eq i32 %115,i32 0
    br i32 %116 %113 %112

109:
    %130 = load i32, i32* %4
    %131 = srem i32 %130,2
    br i32 %131 %129 %128

112:

113:
    %117 = load i32, i32* %1
    %118 = load i32, i32* %2
    %119 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %118
    %120 = load i32, i32* %119
    %121 = mul i32 1,%120
    %122 = load i32, i32* %1
    %123 = load i32, i32* %2
    %124 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %123
    %125 = load i32, i32* %124
    %126 = mul i32 1,%125
    %127 = add i32 %122,%126
    store i32 %127, i32* %1
    br %112

128:

129:
    %132 = load i32, i32* %1
    %133 = load i32, i32* %2
    %134 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %133
    %135 = load i32, i32* %134
    %136 = mul i32 1,%135
    %137 = load i32, i32* %1
    %138 = load i32, i32* %2
    %139 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %138
    %140 = load i32, i32* %139
    %141 = mul i32 1,%140
    %142 = add i32 %137,%141
    store i32 %142, i32* %1
    br %128

153:
    %156 = load i32, i32* %2
    %157 = icmp lt i32 %156,i32 16
    br i32 %157 %154 %155

154:
    %160 = load i32, i32* %3
    %161 = srem i32 %160,2
    %162 = load i32, i32* %4
    %163 = srem i32 %162,2
    %164 = and i32 %161,%163
    br i32 %164 %159 %158

155:
    %183 = load i32, i32* %1
    store i32 %183, i32* %94
    %187 = icmp gt i32 1,i32 15
    br i32 %187 %185 %186

158:
    %176 = load i32, i32* %3
    %177 = sdiv i32 %176,2
    store i32 %177, i32* %3
    %178 = load i32, i32* %4
    %179 = sdiv i32 %178,2
    store i32 %179, i32* %4
    %180 = load i32, i32* %2
    %181 = load i32, i32* %2
    %182 = add i32 %181,1
    store i32 %182, i32* %2
    br %153

159:
    %165 = load i32, i32* %1
    %166 = load i32, i32* %2
    %167 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %166
    %168 = load i32, i32* %167
    %169 = mul i32 1,%168
    %170 = load i32, i32* %1
    %171 = load i32, i32* %2
    %172 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %171
    %173 = load i32, i32* %172
    %174 = mul i32 1,%173
    %175 = add i32 %170,%174
    store i32 %175, i32* %1
    br %158

184:
    %222 = load i32, i32* %1
    store i32 %222, i32* %94
    %223 = load i32, i32* %95
    store i32 %223, i32* %92
    br %96

185:
    store i32 0, i32* %1
    br %184

186:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %188 = load i32, i32* %94
    %189 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 1
    %190 = load i32, i32* %189
    %191 = mul i32 %188,%190
    store i32 %191, i32* %3
    store i32 65535, i32* %4
    br %192

192:
    %195 = load i32, i32* %2
    %196 = icmp lt i32 %195,i32 16
    br i32 %196 %193 %194

193:
    %199 = load i32, i32* %3
    %200 = srem i32 %199,2
    %201 = load i32, i32* %4
    %202 = srem i32 %201,2
    %203 = and i32 %200,%202
    br i32 %203 %198 %197

194:

197:
    %215 = load i32, i32* %3
    %216 = sdiv i32 %215,2
    store i32 %216, i32* %3
    %217 = load i32, i32* %4
    %218 = sdiv i32 %217,2
    store i32 %218, i32* %4
    %219 = load i32, i32* %2
    %220 = load i32, i32* %2
    %221 = add i32 %220,1
    store i32 %221, i32* %2
    br %192

198:
    %204 = load i32, i32* %1
    %205 = load i32, i32* %2
    %206 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %205
    %207 = load i32, i32* %206
    %208 = mul i32 1,%207
    %209 = load i32, i32* %1
    %210 = load i32, i32* %2
    %211 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %210
    %212 = load i32, i32* %211
    %213 = mul i32 1,%212
    %214 = add i32 %209,%213
    store i32 %214, i32* %1
    br %197

231:
    %234 = load i32, i32* %229
    br i32 %234 %232 %233

232:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %235 = load i32, i32* %227
    store i32 %235, i32* %3
    %236 = load i32, i32* %229
    store i32 %236, i32* %4
    br %237

233:
    %359 = load i32, i32* %227
    store i32 %359, i32* %1
    %360 = load i32, i32* %1
    store i32 %360, i32* %49
    %361 = load i32, i32* %51
    store i32 %361, i32* %3
    store i32 1, i32* %4
    %365 = load i32, i32* %4
    %366 = icmp ge i32 %365,i32 15
    br i32 %366 %363 %364

237:
    %240 = load i32, i32* %2
    %241 = icmp lt i32 %240,i32 16
    br i32 %241 %238 %239

238:
    %245 = load i32, i32* %3
    %246 = srem i32 %245,2
    br i32 %246 %243 %244

239:
    %285 = load i32, i32* %1
    store i32 %285, i32* %230
    store i32 0, i32* %1
    store i32 0, i32* %2
    %286 = load i32, i32* %227
    store i32 %286, i32* %3
    %287 = load i32, i32* %229
    store i32 %287, i32* %4
    br %288

242:
    %278 = load i32, i32* %3
    %279 = sdiv i32 %278,2
    store i32 %279, i32* %3
    %280 = load i32, i32* %4
    %281 = sdiv i32 %280,2
    store i32 %281, i32* %4
    %282 = load i32, i32* %2
    %283 = load i32, i32* %2
    %284 = add i32 %283,1
    store i32 %284, i32* %2
    br %237

243:
    %249 = load i32, i32* %4
    %250 = srem i32 %249,2
    %251 = icmp eq i32 %250,i32 0
    br i32 %251 %248 %247

244:
    %265 = load i32, i32* %4
    %266 = srem i32 %265,2
    br i32 %266 %264 %263

247:

248:
    %252 = load i32, i32* %1
    %253 = load i32, i32* %2
    %254 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %253
    %255 = load i32, i32* %254
    %256 = mul i32 1,%255
    %257 = load i32, i32* %1
    %258 = load i32, i32* %2
    %259 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %258
    %260 = load i32, i32* %259
    %261 = mul i32 1,%260
    %262 = add i32 %257,%261
    store i32 %262, i32* %1
    br %247

263:

264:
    %267 = load i32, i32* %1
    %268 = load i32, i32* %2
    %269 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %268
    %270 = load i32, i32* %269
    %271 = mul i32 1,%270
    %272 = load i32, i32* %1
    %273 = load i32, i32* %2
    %274 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %273
    %275 = load i32, i32* %274
    %276 = mul i32 1,%275
    %277 = add i32 %272,%276
    store i32 %277, i32* %1
    br %263

288:
    %291 = load i32, i32* %2
    %292 = icmp lt i32 %291,i32 16
    br i32 %292 %289 %290

289:
    %295 = load i32, i32* %3
    %296 = srem i32 %295,2
    %297 = load i32, i32* %4
    %298 = srem i32 %297,2
    %299 = and i32 %296,%298
    br i32 %299 %294 %293

290:
    %318 = load i32, i32* %1
    store i32 %318, i32* %229
    %322 = icmp gt i32 1,i32 15
    br i32 %322 %320 %321

293:
    %311 = load i32, i32* %3
    %312 = sdiv i32 %311,2
    store i32 %312, i32* %3
    %313 = load i32, i32* %4
    %314 = sdiv i32 %313,2
    store i32 %314, i32* %4
    %315 = load i32, i32* %2
    %316 = load i32, i32* %2
    %317 = add i32 %316,1
    store i32 %317, i32* %2
    br %288

294:
    %300 = load i32, i32* %1
    %301 = load i32, i32* %2
    %302 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %301
    %303 = load i32, i32* %302
    %304 = mul i32 1,%303
    %305 = load i32, i32* %1
    %306 = load i32, i32* %2
    %307 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %306
    %308 = load i32, i32* %307
    %309 = mul i32 1,%308
    %310 = add i32 %305,%309
    store i32 %310, i32* %1
    br %293

319:
    %357 = load i32, i32* %1
    store i32 %357, i32* %229
    %358 = load i32, i32* %230
    store i32 %358, i32* %227
    br %231

320:
    store i32 0, i32* %1
    br %319

321:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %323 = load i32, i32* %229
    %324 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 1
    %325 = load i32, i32* %324
    %326 = mul i32 %323,%325
    store i32 %326, i32* %3
    store i32 65535, i32* %4
    br %327

327:
    %330 = load i32, i32* %2
    %331 = icmp lt i32 %330,i32 16
    br i32 %331 %328 %329

328:
    %334 = load i32, i32* %3
    %335 = srem i32 %334,2
    %336 = load i32, i32* %4
    %337 = srem i32 %336,2
    %338 = and i32 %335,%337
    br i32 %338 %333 %332

329:

332:
    %350 = load i32, i32* %3
    %351 = sdiv i32 %350,2
    store i32 %351, i32* %3
    %352 = load i32, i32* %4
    %353 = sdiv i32 %352,2
    store i32 %353, i32* %4
    %354 = load i32, i32* %2
    %355 = load i32, i32* %2
    %356 = add i32 %355,1
    store i32 %356, i32* %2
    br %327

333:
    %339 = load i32, i32* %1
    %340 = load i32, i32* %2
    %341 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %340
    %342 = load i32, i32* %341
    %343 = mul i32 1,%342
    %344 = load i32, i32* %1
    %345 = load i32, i32* %2
    %346 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %345
    %347 = load i32, i32* %346
    %348 = mul i32 1,%347
    %349 = add i32 %344,%348
    store i32 %349, i32* %1
    br %332

362:
    %403 = load i32, i32* %1
    store i32 %403, i32* %51
    br %53

363:
    %370 = load i32, i32* %3
    %371 = icmp lt i32 %370,i32 0
    br i32 %371 %368 %369

364:
    %375 = load i32, i32* %4
    %376 = icmp gt i32 %375,i32 0
    br i32 %376 %373 %374

367:

368:
    store i32 65535, i32* %1
    br %367

369:
    store i32 0, i32* %1
    br %367

372:

373:
    %380 = load i32, i32* %3
    %381 = icmp gt i32 %380,i32 32767
    br i32 %381 %378 %379

374:
    %402 = load i32, i32* %3
    store i32 %402, i32* %1
    br %372

377:

378:
    %382 = load i32, i32* %3
    %383 = load i32, i32* %4
    %384 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %383
    %385 = load i32, i32* %384
    %386 = sdiv i32 %382,%385
    store i32 %386, i32* %3
    %387 = load i32, i32* %3
    %388 = load i32, i32* %3
    %389 = add i32 %388,65536
    %390 = load i32, i32* %4
    %391 = load i32, i32* %4
    %392 = sub i32 15,%391
    %393 = add i32 %392,1
    %394 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %393
    %395 = load i32, i32* %394
    %396 = sub i32 %389,%395
    store i32 %396, i32* %1
    br %377

379:
    %397 = load i32, i32* %3
    %398 = load i32, i32* %4
    %399 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %398
    %400 = load i32, i32* %399
    %401 = sdiv i32 %397,%400
    store i32 %401, i32* %1
    br %377

411:
    %414 = load i32, i32* %409
    br i32 %414 %412 %413

412:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %415 = load i32, i32* %409
    store i32 %415, i32* %3
    store i32 1, i32* %4
    br %416

413:
    %762 = load i32, i32* %410
    store i32 %762, i32* %1
    %763 = load i32, i32* %1
    store i32 %763, i32* %6
    %764 = load i32, i32* %7
    store i32 %764, i32* %3
    store i32 1, i32* %4
    %768 = load i32, i32* %4
    %769 = icmp ge i32 %768,i32 15
    br i32 %769 %766 %767

416:
    %419 = load i32, i32* %2
    %420 = icmp lt i32 %419,i32 16
    br i32 %420 %417 %418

417:
    %423 = load i32, i32* %3
    %424 = srem i32 %423,2
    %425 = load i32, i32* %4
    %426 = srem i32 %425,2
    %427 = and i32 %424,%426
    br i32 %427 %422 %421

418:
    %448 = load i32, i32* %1
    br i32 %448 %447 %446

421:
    %439 = load i32, i32* %3
    %440 = sdiv i32 %439,2
    store i32 %440, i32* %3
    %441 = load i32, i32* %4
    %442 = sdiv i32 %441,2
    store i32 %442, i32* %4
    %443 = load i32, i32* %2
    %444 = load i32, i32* %2
    %445 = add i32 %444,1
    store i32 %445, i32* %2
    br %416

422:
    %428 = load i32, i32* %1
    %429 = load i32, i32* %2
    %430 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %429
    %431 = load i32, i32* %430
    %432 = mul i32 1,%431
    %433 = load i32, i32* %1
    %434 = load i32, i32* %2
    %435 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %434
    %436 = load i32, i32* %435
    %437 = mul i32 1,%436
    %438 = add i32 %433,%437
    store i32 %438, i32* %1
    br %421

446:
    %584 = load i32, i32* %407
    %585 = alloca i32
    store i32 %584, i32* %585
    %586 = load i32, i32* %407
    %587 = alloca i32
    store i32 %586, i32* %587
    %588 = alloca i32
    br %589

447:
    %449 = load i32, i32* %410
    %450 = alloca i32
    store i32 %449, i32* %450
    %451 = load i32, i32* %407
    %452 = alloca i32
    store i32 %451, i32* %452
    %453 = alloca i32
    br %454

454:
    %457 = load i32, i32* %452
    br i32 %457 %455 %456

455:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %458 = load i32, i32* %450
    store i32 %458, i32* %3
    %459 = load i32, i32* %452
    store i32 %459, i32* %4
    br %460

456:
    %582 = load i32, i32* %450
    store i32 %582, i32* %1
    %583 = load i32, i32* %1
    store i32 %583, i32* %410

460:
    %463 = load i32, i32* %2
    %464 = icmp lt i32 %463,i32 16
    br i32 %464 %461 %462

461:
    %468 = load i32, i32* %3
    %469 = srem i32 %468,2
    br i32 %469 %466 %467

462:
    %508 = load i32, i32* %1
    store i32 %508, i32* %453
    store i32 0, i32* %1
    store i32 0, i32* %2
    %509 = load i32, i32* %450
    store i32 %509, i32* %3
    %510 = load i32, i32* %452
    store i32 %510, i32* %4
    br %511

465:
    %501 = load i32, i32* %3
    %502 = sdiv i32 %501,2
    store i32 %502, i32* %3
    %503 = load i32, i32* %4
    %504 = sdiv i32 %503,2
    store i32 %504, i32* %4
    %505 = load i32, i32* %2
    %506 = load i32, i32* %2
    %507 = add i32 %506,1
    store i32 %507, i32* %2
    br %460

466:
    %472 = load i32, i32* %4
    %473 = srem i32 %472,2
    %474 = icmp eq i32 %473,i32 0
    br i32 %474 %471 %470

467:
    %488 = load i32, i32* %4
    %489 = srem i32 %488,2
    br i32 %489 %487 %486

470:

471:
    %475 = load i32, i32* %1
    %476 = load i32, i32* %2
    %477 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %476
    %478 = load i32, i32* %477
    %479 = mul i32 1,%478
    %480 = load i32, i32* %1
    %481 = load i32, i32* %2
    %482 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %481
    %483 = load i32, i32* %482
    %484 = mul i32 1,%483
    %485 = add i32 %480,%484
    store i32 %485, i32* %1
    br %470

486:

487:
    %490 = load i32, i32* %1
    %491 = load i32, i32* %2
    %492 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %491
    %493 = load i32, i32* %492
    %494 = mul i32 1,%493
    %495 = load i32, i32* %1
    %496 = load i32, i32* %2
    %497 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %496
    %498 = load i32, i32* %497
    %499 = mul i32 1,%498
    %500 = add i32 %495,%499
    store i32 %500, i32* %1
    br %486

511:
    %514 = load i32, i32* %2
    %515 = icmp lt i32 %514,i32 16
    br i32 %515 %512 %513

512:
    %518 = load i32, i32* %3
    %519 = srem i32 %518,2
    %520 = load i32, i32* %4
    %521 = srem i32 %520,2
    %522 = and i32 %519,%521
    br i32 %522 %517 %516

513:
    %541 = load i32, i32* %1
    store i32 %541, i32* %452
    %545 = icmp gt i32 1,i32 15
    br i32 %545 %543 %544

516:
    %534 = load i32, i32* %3
    %535 = sdiv i32 %534,2
    store i32 %535, i32* %3
    %536 = load i32, i32* %4
    %537 = sdiv i32 %536,2
    store i32 %537, i32* %4
    %538 = load i32, i32* %2
    %539 = load i32, i32* %2
    %540 = add i32 %539,1
    store i32 %540, i32* %2
    br %511

517:
    %523 = load i32, i32* %1
    %524 = load i32, i32* %2
    %525 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %524
    %526 = load i32, i32* %525
    %527 = mul i32 1,%526
    %528 = load i32, i32* %1
    %529 = load i32, i32* %2
    %530 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %529
    %531 = load i32, i32* %530
    %532 = mul i32 1,%531
    %533 = add i32 %528,%532
    store i32 %533, i32* %1
    br %516

542:
    %580 = load i32, i32* %1
    store i32 %580, i32* %452
    %581 = load i32, i32* %453
    store i32 %581, i32* %450
    br %454

543:
    store i32 0, i32* %1
    br %542

544:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %546 = load i32, i32* %452
    %547 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 1
    %548 = load i32, i32* %547
    %549 = mul i32 %546,%548
    store i32 %549, i32* %3
    store i32 65535, i32* %4
    br %550

550:
    %553 = load i32, i32* %2
    %554 = icmp lt i32 %553,i32 16
    br i32 %554 %551 %552

551:
    %557 = load i32, i32* %3
    %558 = srem i32 %557,2
    %559 = load i32, i32* %4
    %560 = srem i32 %559,2
    %561 = and i32 %558,%560
    br i32 %561 %556 %555

552:

555:
    %573 = load i32, i32* %3
    %574 = sdiv i32 %573,2
    store i32 %574, i32* %3
    %575 = load i32, i32* %4
    %576 = sdiv i32 %575,2
    store i32 %576, i32* %4
    %577 = load i32, i32* %2
    %578 = load i32, i32* %2
    %579 = add i32 %578,1
    store i32 %579, i32* %2
    br %550

556:
    %562 = load i32, i32* %1
    %563 = load i32, i32* %2
    %564 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %563
    %565 = load i32, i32* %564
    %566 = mul i32 1,%565
    %567 = load i32, i32* %1
    %568 = load i32, i32* %2
    %569 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %568
    %570 = load i32, i32* %569
    %571 = mul i32 1,%570
    %572 = add i32 %567,%571
    store i32 %572, i32* %1
    br %555

589:
    %592 = load i32, i32* %587
    br i32 %592 %590 %591

590:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %593 = load i32, i32* %585
    store i32 %593, i32* %3
    %594 = load i32, i32* %587
    store i32 %594, i32* %4
    br %595

591:
    %717 = load i32, i32* %585
    store i32 %717, i32* %1
    %718 = load i32, i32* %1
    store i32 %718, i32* %407
    %719 = load i32, i32* %409
    store i32 %719, i32* %3
    store i32 1, i32* %4
    %723 = load i32, i32* %4
    %724 = icmp ge i32 %723,i32 15
    br i32 %724 %721 %722

595:
    %598 = load i32, i32* %2
    %599 = icmp lt i32 %598,i32 16
    br i32 %599 %596 %597

596:
    %603 = load i32, i32* %3
    %604 = srem i32 %603,2
    br i32 %604 %601 %602

597:
    %643 = load i32, i32* %1
    store i32 %643, i32* %588
    store i32 0, i32* %1
    store i32 0, i32* %2
    %644 = load i32, i32* %585
    store i32 %644, i32* %3
    %645 = load i32, i32* %587
    store i32 %645, i32* %4
    br %646

600:
    %636 = load i32, i32* %3
    %637 = sdiv i32 %636,2
    store i32 %637, i32* %3
    %638 = load i32, i32* %4
    %639 = sdiv i32 %638,2
    store i32 %639, i32* %4
    %640 = load i32, i32* %2
    %641 = load i32, i32* %2
    %642 = add i32 %641,1
    store i32 %642, i32* %2
    br %595

601:
    %607 = load i32, i32* %4
    %608 = srem i32 %607,2
    %609 = icmp eq i32 %608,i32 0
    br i32 %609 %606 %605

602:
    %623 = load i32, i32* %4
    %624 = srem i32 %623,2
    br i32 %624 %622 %621

605:

606:
    %610 = load i32, i32* %1
    %611 = load i32, i32* %2
    %612 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %611
    %613 = load i32, i32* %612
    %614 = mul i32 1,%613
    %615 = load i32, i32* %1
    %616 = load i32, i32* %2
    %617 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %616
    %618 = load i32, i32* %617
    %619 = mul i32 1,%618
    %620 = add i32 %615,%619
    store i32 %620, i32* %1
    br %605

621:

622:
    %625 = load i32, i32* %1
    %626 = load i32, i32* %2
    %627 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %626
    %628 = load i32, i32* %627
    %629 = mul i32 1,%628
    %630 = load i32, i32* %1
    %631 = load i32, i32* %2
    %632 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %631
    %633 = load i32, i32* %632
    %634 = mul i32 1,%633
    %635 = add i32 %630,%634
    store i32 %635, i32* %1
    br %621

646:
    %649 = load i32, i32* %2
    %650 = icmp lt i32 %649,i32 16
    br i32 %650 %647 %648

647:
    %653 = load i32, i32* %3
    %654 = srem i32 %653,2
    %655 = load i32, i32* %4
    %656 = srem i32 %655,2
    %657 = and i32 %654,%656
    br i32 %657 %652 %651

648:
    %676 = load i32, i32* %1
    store i32 %676, i32* %587
    %680 = icmp gt i32 1,i32 15
    br i32 %680 %678 %679

651:
    %669 = load i32, i32* %3
    %670 = sdiv i32 %669,2
    store i32 %670, i32* %3
    %671 = load i32, i32* %4
    %672 = sdiv i32 %671,2
    store i32 %672, i32* %4
    %673 = load i32, i32* %2
    %674 = load i32, i32* %2
    %675 = add i32 %674,1
    store i32 %675, i32* %2
    br %646

652:
    %658 = load i32, i32* %1
    %659 = load i32, i32* %2
    %660 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %659
    %661 = load i32, i32* %660
    %662 = mul i32 1,%661
    %663 = load i32, i32* %1
    %664 = load i32, i32* %2
    %665 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %664
    %666 = load i32, i32* %665
    %667 = mul i32 1,%666
    %668 = add i32 %663,%667
    store i32 %668, i32* %1
    br %651

677:
    %715 = load i32, i32* %1
    store i32 %715, i32* %587
    %716 = load i32, i32* %588
    store i32 %716, i32* %585
    br %589

678:
    store i32 0, i32* %1
    br %677

679:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %681 = load i32, i32* %587
    %682 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 1
    %683 = load i32, i32* %682
    %684 = mul i32 %681,%683
    store i32 %684, i32* %3
    store i32 65535, i32* %4
    br %685

685:
    %688 = load i32, i32* %2
    %689 = icmp lt i32 %688,i32 16
    br i32 %689 %686 %687

686:
    %692 = load i32, i32* %3
    %693 = srem i32 %692,2
    %694 = load i32, i32* %4
    %695 = srem i32 %694,2
    %696 = and i32 %693,%695
    br i32 %696 %691 %690

687:

690:
    %708 = load i32, i32* %3
    %709 = sdiv i32 %708,2
    store i32 %709, i32* %3
    %710 = load i32, i32* %4
    %711 = sdiv i32 %710,2
    store i32 %711, i32* %4
    %712 = load i32, i32* %2
    %713 = load i32, i32* %2
    %714 = add i32 %713,1
    store i32 %714, i32* %2
    br %685

691:
    %697 = load i32, i32* %1
    %698 = load i32, i32* %2
    %699 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %698
    %700 = load i32, i32* %699
    %701 = mul i32 1,%700
    %702 = load i32, i32* %1
    %703 = load i32, i32* %2
    %704 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %703
    %705 = load i32, i32* %704
    %706 = mul i32 1,%705
    %707 = add i32 %702,%706
    store i32 %707, i32* %1
    br %690

720:
    %761 = load i32, i32* %1
    store i32 %761, i32* %409
    br %411

721:
    %728 = load i32, i32* %3
    %729 = icmp lt i32 %728,i32 0
    br i32 %729 %726 %727

722:
    %733 = load i32, i32* %4
    %734 = icmp gt i32 %733,i32 0
    br i32 %734 %731 %732

725:

726:
    store i32 65535, i32* %1
    br %725

727:
    store i32 0, i32* %1
    br %725

730:

731:
    %738 = load i32, i32* %3
    %739 = icmp gt i32 %738,i32 32767
    br i32 %739 %736 %737

732:
    %760 = load i32, i32* %3
    store i32 %760, i32* %1
    br %730

735:

736:
    %740 = load i32, i32* %3
    %741 = load i32, i32* %4
    %742 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %741
    %743 = load i32, i32* %742
    %744 = sdiv i32 %740,%743
    store i32 %744, i32* %3
    %745 = load i32, i32* %3
    %746 = load i32, i32* %3
    %747 = add i32 %746,65536
    %748 = load i32, i32* %4
    %749 = load i32, i32* %4
    %750 = sub i32 15,%749
    %751 = add i32 %750,1
    %752 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %751
    %753 = load i32, i32* %752
    %754 = sub i32 %747,%753
    store i32 %754, i32* %1
    br %735

737:
    %755 = load i32, i32* %3
    %756 = load i32, i32* %4
    %757 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %756
    %758 = load i32, i32* %757
    %759 = sdiv i32 %755,%758
    store i32 %759, i32* %1
    br %735

765:
    %806 = load i32, i32* %1
    store i32 %806, i32* %7
    br %9

766:
    %773 = load i32, i32* %3
    %774 = icmp lt i32 %773,i32 0
    br i32 %774 %771 %772

767:
    %778 = load i32, i32* %4
    %779 = icmp gt i32 %778,i32 0
    br i32 %779 %776 %777

770:

771:
    store i32 65535, i32* %1
    br %770

772:
    store i32 0, i32* %1
    br %770

775:

776:
    %783 = load i32, i32* %3
    %784 = icmp gt i32 %783,i32 32767
    br i32 %784 %781 %782

777:
    %805 = load i32, i32* %3
    store i32 %805, i32* %1
    br %775

780:

781:
    %785 = load i32, i32* %3
    %786 = load i32, i32* %4
    %787 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %786
    %788 = load i32, i32* %787
    %789 = sdiv i32 %785,%788
    store i32 %789, i32* %3
    %790 = load i32, i32* %3
    %791 = load i32, i32* %3
    %792 = add i32 %791,65536
    %793 = load i32, i32* %4
    %794 = load i32, i32* %4
    %795 = sub i32 15,%794
    %796 = add i32 %795,1
    %797 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %796
    %798 = load i32, i32* %797
    %799 = sub i32 %792,%798
    store i32 %799, i32* %1
    br %780

782:
    %800 = load i32, i32* %3
    %801 = load i32, i32* %4
    %802 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %801
    %803 = load i32, i32* %802
    %804 = sdiv i32 %800,%803
    store i32 %804, i32* %1
    br %780

812:
    %815 = load i32, i32* %810
    %816 = icmp gt i32 %815,i32 0
    br i32 %816 %813 %814

813:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %817 = load i32, i32* %810
    store i32 %817, i32* %3
    store i32 1, i32* %4
    br %818

814:
    %1610 = load i32, i32* %811
    store i32 %1610, i32* %1
    %1611 = load i32, i32* %1
    call void @putint(i32 %1611)
    call void @putch(i32 10)
    store i32 2, i32* %5
    br %1612

818:
    %821 = load i32, i32* %2
    %822 = icmp lt i32 %821,i32 16
    br i32 %822 %819 %820

819:
    %825 = load i32, i32* %3
    %826 = srem i32 %825,2
    %827 = load i32, i32* %4
    %828 = srem i32 %827,2
    %829 = and i32 %826,%828
    br i32 %829 %824 %823

820:
    %850 = load i32, i32* %1
    br i32 %850 %849 %848

823:
    %841 = load i32, i32* %3
    %842 = sdiv i32 %841,2
    store i32 %842, i32* %3
    %843 = load i32, i32* %4
    %844 = sdiv i32 %843,2
    store i32 %844, i32* %4
    %845 = load i32, i32* %2
    %846 = load i32, i32* %2
    %847 = add i32 %846,1
    store i32 %847, i32* %2
    br %818

824:
    %830 = load i32, i32* %1
    %831 = load i32, i32* %2
    %832 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %831
    %833 = load i32, i32* %832
    %834 = mul i32 1,%833
    %835 = load i32, i32* %1
    %836 = load i32, i32* %2
    %837 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %836
    %838 = load i32, i32* %837
    %839 = mul i32 1,%838
    %840 = add i32 %835,%839
    store i32 %840, i32* %1
    br %823

848:
    %1209 = load i32, i32* %809
    %1210 = alloca i32
    store i32 %1209, i32* %1210
    %1211 = load i32, i32* %809
    %1212 = alloca i32
    store i32 %1211, i32* %1212
    %1213 = alloca i32
    store i32 0, i32* %1213
    br %1214

849:
    %851 = load i32, i32* %811
    %852 = alloca i32
    store i32 %851, i32* %852
    %853 = load i32, i32* %809
    %854 = alloca i32
    store i32 %853, i32* %854
    %855 = alloca i32
    store i32 0, i32* %855
    br %856

856:
    %859 = load i32, i32* %854
    br i32 %859 %857 %858

857:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %860 = load i32, i32* %854
    store i32 %860, i32* %3
    store i32 1, i32* %4
    br %861

858:
    %1207 = load i32, i32* %855
    store i32 %1207, i32* %1
    %1208 = load i32, i32* %1
    store i32 %1208, i32* %811

861:
    %864 = load i32, i32* %2
    %865 = icmp lt i32 %864,i32 16
    br i32 %865 %862 %863

862:
    %868 = load i32, i32* %3
    %869 = srem i32 %868,2
    %870 = load i32, i32* %4
    %871 = srem i32 %870,2
    %872 = and i32 %869,%871
    br i32 %872 %867 %866

863:
    %893 = load i32, i32* %1
    br i32 %893 %892 %891

866:
    %884 = load i32, i32* %3
    %885 = sdiv i32 %884,2
    store i32 %885, i32* %3
    %886 = load i32, i32* %4
    %887 = sdiv i32 %886,2
    store i32 %887, i32* %4
    %888 = load i32, i32* %2
    %889 = load i32, i32* %2
    %890 = add i32 %889,1
    store i32 %890, i32* %2
    br %861

867:
    %873 = load i32, i32* %1
    %874 = load i32, i32* %2
    %875 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %874
    %876 = load i32, i32* %875
    %877 = mul i32 1,%876
    %878 = load i32, i32* %1
    %879 = load i32, i32* %2
    %880 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %879
    %881 = load i32, i32* %880
    %882 = mul i32 1,%881
    %883 = add i32 %878,%882
    store i32 %883, i32* %1
    br %866

891:
    %1029 = load i32, i32* %852
    %1030 = alloca i32
    store i32 %1029, i32* %1030
    %1031 = load i32, i32* %852
    %1032 = alloca i32
    store i32 %1031, i32* %1032
    %1033 = alloca i32
    br %1034

892:
    %894 = load i32, i32* %855
    %895 = alloca i32
    store i32 %894, i32* %895
    %896 = load i32, i32* %852
    %897 = alloca i32
    store i32 %896, i32* %897
    %898 = alloca i32
    br %899

899:
    %902 = load i32, i32* %897
    br i32 %902 %900 %901

900:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %903 = load i32, i32* %895
    store i32 %903, i32* %3
    %904 = load i32, i32* %897
    store i32 %904, i32* %4
    br %905

901:
    %1027 = load i32, i32* %895
    store i32 %1027, i32* %1
    %1028 = load i32, i32* %1
    store i32 %1028, i32* %855

905:
    %908 = load i32, i32* %2
    %909 = icmp lt i32 %908,i32 16
    br i32 %909 %906 %907

906:
    %913 = load i32, i32* %3
    %914 = srem i32 %913,2
    br i32 %914 %911 %912

907:
    %953 = load i32, i32* %1
    store i32 %953, i32* %898
    store i32 0, i32* %1
    store i32 0, i32* %2
    %954 = load i32, i32* %895
    store i32 %954, i32* %3
    %955 = load i32, i32* %897
    store i32 %955, i32* %4
    br %956

910:
    %946 = load i32, i32* %3
    %947 = sdiv i32 %946,2
    store i32 %947, i32* %3
    %948 = load i32, i32* %4
    %949 = sdiv i32 %948,2
    store i32 %949, i32* %4
    %950 = load i32, i32* %2
    %951 = load i32, i32* %2
    %952 = add i32 %951,1
    store i32 %952, i32* %2
    br %905

911:
    %917 = load i32, i32* %4
    %918 = srem i32 %917,2
    %919 = icmp eq i32 %918,i32 0
    br i32 %919 %916 %915

912:
    %933 = load i32, i32* %4
    %934 = srem i32 %933,2
    br i32 %934 %932 %931

915:

916:
    %920 = load i32, i32* %1
    %921 = load i32, i32* %2
    %922 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %921
    %923 = load i32, i32* %922
    %924 = mul i32 1,%923
    %925 = load i32, i32* %1
    %926 = load i32, i32* %2
    %927 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %926
    %928 = load i32, i32* %927
    %929 = mul i32 1,%928
    %930 = add i32 %925,%929
    store i32 %930, i32* %1
    br %915

931:

932:
    %935 = load i32, i32* %1
    %936 = load i32, i32* %2
    %937 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %936
    %938 = load i32, i32* %937
    %939 = mul i32 1,%938
    %940 = load i32, i32* %1
    %941 = load i32, i32* %2
    %942 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %941
    %943 = load i32, i32* %942
    %944 = mul i32 1,%943
    %945 = add i32 %940,%944
    store i32 %945, i32* %1
    br %931

956:
    %959 = load i32, i32* %2
    %960 = icmp lt i32 %959,i32 16
    br i32 %960 %957 %958

957:
    %963 = load i32, i32* %3
    %964 = srem i32 %963,2
    %965 = load i32, i32* %4
    %966 = srem i32 %965,2
    %967 = and i32 %964,%966
    br i32 %967 %962 %961

958:
    %986 = load i32, i32* %1
    store i32 %986, i32* %897
    %990 = icmp gt i32 1,i32 15
    br i32 %990 %988 %989

961:
    %979 = load i32, i32* %3
    %980 = sdiv i32 %979,2
    store i32 %980, i32* %3
    %981 = load i32, i32* %4
    %982 = sdiv i32 %981,2
    store i32 %982, i32* %4
    %983 = load i32, i32* %2
    %984 = load i32, i32* %2
    %985 = add i32 %984,1
    store i32 %985, i32* %2
    br %956

962:
    %968 = load i32, i32* %1
    %969 = load i32, i32* %2
    %970 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %969
    %971 = load i32, i32* %970
    %972 = mul i32 1,%971
    %973 = load i32, i32* %1
    %974 = load i32, i32* %2
    %975 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %974
    %976 = load i32, i32* %975
    %977 = mul i32 1,%976
    %978 = add i32 %973,%977
    store i32 %978, i32* %1
    br %961

987:
    %1025 = load i32, i32* %1
    store i32 %1025, i32* %897
    %1026 = load i32, i32* %898
    store i32 %1026, i32* %895
    br %899

988:
    store i32 0, i32* %1
    br %987

989:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %991 = load i32, i32* %897
    %992 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 1
    %993 = load i32, i32* %992
    %994 = mul i32 %991,%993
    store i32 %994, i32* %3
    store i32 65535, i32* %4
    br %995

995:
    %998 = load i32, i32* %2
    %999 = icmp lt i32 %998,i32 16
    br i32 %999 %996 %997

996:
    %1002 = load i32, i32* %3
    %1003 = srem i32 %1002,2
    %1004 = load i32, i32* %4
    %1005 = srem i32 %1004,2
    %1006 = and i32 %1003,%1005
    br i32 %1006 %1001 %1000

997:

1000:
    %1018 = load i32, i32* %3
    %1019 = sdiv i32 %1018,2
    store i32 %1019, i32* %3
    %1020 = load i32, i32* %4
    %1021 = sdiv i32 %1020,2
    store i32 %1021, i32* %4
    %1022 = load i32, i32* %2
    %1023 = load i32, i32* %2
    %1024 = add i32 %1023,1
    store i32 %1024, i32* %2
    br %995

1001:
    %1007 = load i32, i32* %1
    %1008 = load i32, i32* %2
    %1009 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1008
    %1010 = load i32, i32* %1009
    %1011 = mul i32 1,%1010
    %1012 = load i32, i32* %1
    %1013 = load i32, i32* %2
    %1014 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1013
    %1015 = load i32, i32* %1014
    %1016 = mul i32 1,%1015
    %1017 = add i32 %1012,%1016
    store i32 %1017, i32* %1
    br %1000

1034:
    %1037 = load i32, i32* %1032
    br i32 %1037 %1035 %1036

1035:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %1038 = load i32, i32* %1030
    store i32 %1038, i32* %3
    %1039 = load i32, i32* %1032
    store i32 %1039, i32* %4
    br %1040

1036:
    %1162 = load i32, i32* %1030
    store i32 %1162, i32* %1
    %1163 = load i32, i32* %1
    store i32 %1163, i32* %852
    %1164 = load i32, i32* %854
    store i32 %1164, i32* %3
    store i32 1, i32* %4
    %1168 = load i32, i32* %4
    %1169 = icmp ge i32 %1168,i32 15
    br i32 %1169 %1166 %1167

1040:
    %1043 = load i32, i32* %2
    %1044 = icmp lt i32 %1043,i32 16
    br i32 %1044 %1041 %1042

1041:
    %1048 = load i32, i32* %3
    %1049 = srem i32 %1048,2
    br i32 %1049 %1046 %1047

1042:
    %1088 = load i32, i32* %1
    store i32 %1088, i32* %1033
    store i32 0, i32* %1
    store i32 0, i32* %2
    %1089 = load i32, i32* %1030
    store i32 %1089, i32* %3
    %1090 = load i32, i32* %1032
    store i32 %1090, i32* %4
    br %1091

1045:
    %1081 = load i32, i32* %3
    %1082 = sdiv i32 %1081,2
    store i32 %1082, i32* %3
    %1083 = load i32, i32* %4
    %1084 = sdiv i32 %1083,2
    store i32 %1084, i32* %4
    %1085 = load i32, i32* %2
    %1086 = load i32, i32* %2
    %1087 = add i32 %1086,1
    store i32 %1087, i32* %2
    br %1040

1046:
    %1052 = load i32, i32* %4
    %1053 = srem i32 %1052,2
    %1054 = icmp eq i32 %1053,i32 0
    br i32 %1054 %1051 %1050

1047:
    %1068 = load i32, i32* %4
    %1069 = srem i32 %1068,2
    br i32 %1069 %1067 %1066

1050:

1051:
    %1055 = load i32, i32* %1
    %1056 = load i32, i32* %2
    %1057 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1056
    %1058 = load i32, i32* %1057
    %1059 = mul i32 1,%1058
    %1060 = load i32, i32* %1
    %1061 = load i32, i32* %2
    %1062 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1061
    %1063 = load i32, i32* %1062
    %1064 = mul i32 1,%1063
    %1065 = add i32 %1060,%1064
    store i32 %1065, i32* %1
    br %1050

1066:

1067:
    %1070 = load i32, i32* %1
    %1071 = load i32, i32* %2
    %1072 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1071
    %1073 = load i32, i32* %1072
    %1074 = mul i32 1,%1073
    %1075 = load i32, i32* %1
    %1076 = load i32, i32* %2
    %1077 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1076
    %1078 = load i32, i32* %1077
    %1079 = mul i32 1,%1078
    %1080 = add i32 %1075,%1079
    store i32 %1080, i32* %1
    br %1066

1091:
    %1094 = load i32, i32* %2
    %1095 = icmp lt i32 %1094,i32 16
    br i32 %1095 %1092 %1093

1092:
    %1098 = load i32, i32* %3
    %1099 = srem i32 %1098,2
    %1100 = load i32, i32* %4
    %1101 = srem i32 %1100,2
    %1102 = and i32 %1099,%1101
    br i32 %1102 %1097 %1096

1093:
    %1121 = load i32, i32* %1
    store i32 %1121, i32* %1032
    %1125 = icmp gt i32 1,i32 15
    br i32 %1125 %1123 %1124

1096:
    %1114 = load i32, i32* %3
    %1115 = sdiv i32 %1114,2
    store i32 %1115, i32* %3
    %1116 = load i32, i32* %4
    %1117 = sdiv i32 %1116,2
    store i32 %1117, i32* %4
    %1118 = load i32, i32* %2
    %1119 = load i32, i32* %2
    %1120 = add i32 %1119,1
    store i32 %1120, i32* %2
    br %1091

1097:
    %1103 = load i32, i32* %1
    %1104 = load i32, i32* %2
    %1105 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1104
    %1106 = load i32, i32* %1105
    %1107 = mul i32 1,%1106
    %1108 = load i32, i32* %1
    %1109 = load i32, i32* %2
    %1110 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1109
    %1111 = load i32, i32* %1110
    %1112 = mul i32 1,%1111
    %1113 = add i32 %1108,%1112
    store i32 %1113, i32* %1
    br %1096

1122:
    %1160 = load i32, i32* %1
    store i32 %1160, i32* %1032
    %1161 = load i32, i32* %1033
    store i32 %1161, i32* %1030
    br %1034

1123:
    store i32 0, i32* %1
    br %1122

1124:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %1126 = load i32, i32* %1032
    %1127 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 1
    %1128 = load i32, i32* %1127
    %1129 = mul i32 %1126,%1128
    store i32 %1129, i32* %3
    store i32 65535, i32* %4
    br %1130

1130:
    %1133 = load i32, i32* %2
    %1134 = icmp lt i32 %1133,i32 16
    br i32 %1134 %1131 %1132

1131:
    %1137 = load i32, i32* %3
    %1138 = srem i32 %1137,2
    %1139 = load i32, i32* %4
    %1140 = srem i32 %1139,2
    %1141 = and i32 %1138,%1140
    br i32 %1141 %1136 %1135

1132:

1135:
    %1153 = load i32, i32* %3
    %1154 = sdiv i32 %1153,2
    store i32 %1154, i32* %3
    %1155 = load i32, i32* %4
    %1156 = sdiv i32 %1155,2
    store i32 %1156, i32* %4
    %1157 = load i32, i32* %2
    %1158 = load i32, i32* %2
    %1159 = add i32 %1158,1
    store i32 %1159, i32* %2
    br %1130

1136:
    %1142 = load i32, i32* %1
    %1143 = load i32, i32* %2
    %1144 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1143
    %1145 = load i32, i32* %1144
    %1146 = mul i32 1,%1145
    %1147 = load i32, i32* %1
    %1148 = load i32, i32* %2
    %1149 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1148
    %1150 = load i32, i32* %1149
    %1151 = mul i32 1,%1150
    %1152 = add i32 %1147,%1151
    store i32 %1152, i32* %1
    br %1135

1165:
    %1206 = load i32, i32* %1
    store i32 %1206, i32* %854
    br %856

1166:
    %1173 = load i32, i32* %3
    %1174 = icmp lt i32 %1173,i32 0
    br i32 %1174 %1171 %1172

1167:
    %1178 = load i32, i32* %4
    %1179 = icmp gt i32 %1178,i32 0
    br i32 %1179 %1176 %1177

1170:

1171:
    store i32 65535, i32* %1
    br %1170

1172:
    store i32 0, i32* %1
    br %1170

1175:

1176:
    %1183 = load i32, i32* %3
    %1184 = icmp gt i32 %1183,i32 32767
    br i32 %1184 %1181 %1182

1177:
    %1205 = load i32, i32* %3
    store i32 %1205, i32* %1
    br %1175

1180:

1181:
    %1185 = load i32, i32* %3
    %1186 = load i32, i32* %4
    %1187 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1186
    %1188 = load i32, i32* %1187
    %1189 = sdiv i32 %1185,%1188
    store i32 %1189, i32* %3
    %1190 = load i32, i32* %3
    %1191 = load i32, i32* %3
    %1192 = add i32 %1191,65536
    %1193 = load i32, i32* %4
    %1194 = load i32, i32* %4
    %1195 = sub i32 15,%1194
    %1196 = add i32 %1195,1
    %1197 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1196
    %1198 = load i32, i32* %1197
    %1199 = sub i32 %1192,%1198
    store i32 %1199, i32* %1
    br %1180

1182:
    %1200 = load i32, i32* %3
    %1201 = load i32, i32* %4
    %1202 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1201
    %1203 = load i32, i32* %1202
    %1204 = sdiv i32 %1200,%1203
    store i32 %1204, i32* %1
    br %1180

1214:
    %1217 = load i32, i32* %1212
    br i32 %1217 %1215 %1216

1215:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %1218 = load i32, i32* %1212
    store i32 %1218, i32* %3
    store i32 1, i32* %4
    br %1219

1216:
    %1565 = load i32, i32* %1213
    store i32 %1565, i32* %1
    %1566 = load i32, i32* %1
    store i32 %1566, i32* %809
    %1567 = load i32, i32* %810
    store i32 %1567, i32* %3
    store i32 1, i32* %4
    %1571 = load i32, i32* %4
    %1572 = icmp ge i32 %1571,i32 15
    br i32 %1572 %1569 %1570

1219:
    %1222 = load i32, i32* %2
    %1223 = icmp lt i32 %1222,i32 16
    br i32 %1223 %1220 %1221

1220:
    %1226 = load i32, i32* %3
    %1227 = srem i32 %1226,2
    %1228 = load i32, i32* %4
    %1229 = srem i32 %1228,2
    %1230 = and i32 %1227,%1229
    br i32 %1230 %1225 %1224

1221:
    %1251 = load i32, i32* %1
    br i32 %1251 %1250 %1249

1224:
    %1242 = load i32, i32* %3
    %1243 = sdiv i32 %1242,2
    store i32 %1243, i32* %3
    %1244 = load i32, i32* %4
    %1245 = sdiv i32 %1244,2
    store i32 %1245, i32* %4
    %1246 = load i32, i32* %2
    %1247 = load i32, i32* %2
    %1248 = add i32 %1247,1
    store i32 %1248, i32* %2
    br %1219

1225:
    %1231 = load i32, i32* %1
    %1232 = load i32, i32* %2
    %1233 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1232
    %1234 = load i32, i32* %1233
    %1235 = mul i32 1,%1234
    %1236 = load i32, i32* %1
    %1237 = load i32, i32* %2
    %1238 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1237
    %1239 = load i32, i32* %1238
    %1240 = mul i32 1,%1239
    %1241 = add i32 %1236,%1240
    store i32 %1241, i32* %1
    br %1224

1249:
    %1387 = load i32, i32* %1210
    %1388 = alloca i32
    store i32 %1387, i32* %1388
    %1389 = load i32, i32* %1210
    %1390 = alloca i32
    store i32 %1389, i32* %1390
    %1391 = alloca i32
    br %1392

1250:
    %1252 = load i32, i32* %1213
    %1253 = alloca i32
    store i32 %1252, i32* %1253
    %1254 = load i32, i32* %1210
    %1255 = alloca i32
    store i32 %1254, i32* %1255
    %1256 = alloca i32
    br %1257

1257:
    %1260 = load i32, i32* %1255
    br i32 %1260 %1258 %1259

1258:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %1261 = load i32, i32* %1253
    store i32 %1261, i32* %3
    %1262 = load i32, i32* %1255
    store i32 %1262, i32* %4
    br %1263

1259:
    %1385 = load i32, i32* %1253
    store i32 %1385, i32* %1
    %1386 = load i32, i32* %1
    store i32 %1386, i32* %1213

1263:
    %1266 = load i32, i32* %2
    %1267 = icmp lt i32 %1266,i32 16
    br i32 %1267 %1264 %1265

1264:
    %1271 = load i32, i32* %3
    %1272 = srem i32 %1271,2
    br i32 %1272 %1269 %1270

1265:
    %1311 = load i32, i32* %1
    store i32 %1311, i32* %1256
    store i32 0, i32* %1
    store i32 0, i32* %2
    %1312 = load i32, i32* %1253
    store i32 %1312, i32* %3
    %1313 = load i32, i32* %1255
    store i32 %1313, i32* %4
    br %1314

1268:
    %1304 = load i32, i32* %3
    %1305 = sdiv i32 %1304,2
    store i32 %1305, i32* %3
    %1306 = load i32, i32* %4
    %1307 = sdiv i32 %1306,2
    store i32 %1307, i32* %4
    %1308 = load i32, i32* %2
    %1309 = load i32, i32* %2
    %1310 = add i32 %1309,1
    store i32 %1310, i32* %2
    br %1263

1269:
    %1275 = load i32, i32* %4
    %1276 = srem i32 %1275,2
    %1277 = icmp eq i32 %1276,i32 0
    br i32 %1277 %1274 %1273

1270:
    %1291 = load i32, i32* %4
    %1292 = srem i32 %1291,2
    br i32 %1292 %1290 %1289

1273:

1274:
    %1278 = load i32, i32* %1
    %1279 = load i32, i32* %2
    %1280 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1279
    %1281 = load i32, i32* %1280
    %1282 = mul i32 1,%1281
    %1283 = load i32, i32* %1
    %1284 = load i32, i32* %2
    %1285 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1284
    %1286 = load i32, i32* %1285
    %1287 = mul i32 1,%1286
    %1288 = add i32 %1283,%1287
    store i32 %1288, i32* %1
    br %1273

1289:

1290:
    %1293 = load i32, i32* %1
    %1294 = load i32, i32* %2
    %1295 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1294
    %1296 = load i32, i32* %1295
    %1297 = mul i32 1,%1296
    %1298 = load i32, i32* %1
    %1299 = load i32, i32* %2
    %1300 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1299
    %1301 = load i32, i32* %1300
    %1302 = mul i32 1,%1301
    %1303 = add i32 %1298,%1302
    store i32 %1303, i32* %1
    br %1289

1314:
    %1317 = load i32, i32* %2
    %1318 = icmp lt i32 %1317,i32 16
    br i32 %1318 %1315 %1316

1315:
    %1321 = load i32, i32* %3
    %1322 = srem i32 %1321,2
    %1323 = load i32, i32* %4
    %1324 = srem i32 %1323,2
    %1325 = and i32 %1322,%1324
    br i32 %1325 %1320 %1319

1316:
    %1344 = load i32, i32* %1
    store i32 %1344, i32* %1255
    %1348 = icmp gt i32 1,i32 15
    br i32 %1348 %1346 %1347

1319:
    %1337 = load i32, i32* %3
    %1338 = sdiv i32 %1337,2
    store i32 %1338, i32* %3
    %1339 = load i32, i32* %4
    %1340 = sdiv i32 %1339,2
    store i32 %1340, i32* %4
    %1341 = load i32, i32* %2
    %1342 = load i32, i32* %2
    %1343 = add i32 %1342,1
    store i32 %1343, i32* %2
    br %1314

1320:
    %1326 = load i32, i32* %1
    %1327 = load i32, i32* %2
    %1328 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1327
    %1329 = load i32, i32* %1328
    %1330 = mul i32 1,%1329
    %1331 = load i32, i32* %1
    %1332 = load i32, i32* %2
    %1333 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1332
    %1334 = load i32, i32* %1333
    %1335 = mul i32 1,%1334
    %1336 = add i32 %1331,%1335
    store i32 %1336, i32* %1
    br %1319

1345:
    %1383 = load i32, i32* %1
    store i32 %1383, i32* %1255
    %1384 = load i32, i32* %1256
    store i32 %1384, i32* %1253
    br %1257

1346:
    store i32 0, i32* %1
    br %1345

1347:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %1349 = load i32, i32* %1255
    %1350 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 1
    %1351 = load i32, i32* %1350
    %1352 = mul i32 %1349,%1351
    store i32 %1352, i32* %3
    store i32 65535, i32* %4
    br %1353

1353:
    %1356 = load i32, i32* %2
    %1357 = icmp lt i32 %1356,i32 16
    br i32 %1357 %1354 %1355

1354:
    %1360 = load i32, i32* %3
    %1361 = srem i32 %1360,2
    %1362 = load i32, i32* %4
    %1363 = srem i32 %1362,2
    %1364 = and i32 %1361,%1363
    br i32 %1364 %1359 %1358

1355:

1358:
    %1376 = load i32, i32* %3
    %1377 = sdiv i32 %1376,2
    store i32 %1377, i32* %3
    %1378 = load i32, i32* %4
    %1379 = sdiv i32 %1378,2
    store i32 %1379, i32* %4
    %1380 = load i32, i32* %2
    %1381 = load i32, i32* %2
    %1382 = add i32 %1381,1
    store i32 %1382, i32* %2
    br %1353

1359:
    %1365 = load i32, i32* %1
    %1366 = load i32, i32* %2
    %1367 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1366
    %1368 = load i32, i32* %1367
    %1369 = mul i32 1,%1368
    %1370 = load i32, i32* %1
    %1371 = load i32, i32* %2
    %1372 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1371
    %1373 = load i32, i32* %1372
    %1374 = mul i32 1,%1373
    %1375 = add i32 %1370,%1374
    store i32 %1375, i32* %1
    br %1358

1392:
    %1395 = load i32, i32* %1390
    br i32 %1395 %1393 %1394

1393:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %1396 = load i32, i32* %1388
    store i32 %1396, i32* %3
    %1397 = load i32, i32* %1390
    store i32 %1397, i32* %4
    br %1398

1394:
    %1520 = load i32, i32* %1388
    store i32 %1520, i32* %1
    %1521 = load i32, i32* %1
    store i32 %1521, i32* %1210
    %1522 = load i32, i32* %1212
    store i32 %1522, i32* %3
    store i32 1, i32* %4
    %1526 = load i32, i32* %4
    %1527 = icmp ge i32 %1526,i32 15
    br i32 %1527 %1524 %1525

1398:
    %1401 = load i32, i32* %2
    %1402 = icmp lt i32 %1401,i32 16
    br i32 %1402 %1399 %1400

1399:
    %1406 = load i32, i32* %3
    %1407 = srem i32 %1406,2
    br i32 %1407 %1404 %1405

1400:
    %1446 = load i32, i32* %1
    store i32 %1446, i32* %1391
    store i32 0, i32* %1
    store i32 0, i32* %2
    %1447 = load i32, i32* %1388
    store i32 %1447, i32* %3
    %1448 = load i32, i32* %1390
    store i32 %1448, i32* %4
    br %1449

1403:
    %1439 = load i32, i32* %3
    %1440 = sdiv i32 %1439,2
    store i32 %1440, i32* %3
    %1441 = load i32, i32* %4
    %1442 = sdiv i32 %1441,2
    store i32 %1442, i32* %4
    %1443 = load i32, i32* %2
    %1444 = load i32, i32* %2
    %1445 = add i32 %1444,1
    store i32 %1445, i32* %2
    br %1398

1404:
    %1410 = load i32, i32* %4
    %1411 = srem i32 %1410,2
    %1412 = icmp eq i32 %1411,i32 0
    br i32 %1412 %1409 %1408

1405:
    %1426 = load i32, i32* %4
    %1427 = srem i32 %1426,2
    br i32 %1427 %1425 %1424

1408:

1409:
    %1413 = load i32, i32* %1
    %1414 = load i32, i32* %2
    %1415 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1414
    %1416 = load i32, i32* %1415
    %1417 = mul i32 1,%1416
    %1418 = load i32, i32* %1
    %1419 = load i32, i32* %2
    %1420 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1419
    %1421 = load i32, i32* %1420
    %1422 = mul i32 1,%1421
    %1423 = add i32 %1418,%1422
    store i32 %1423, i32* %1
    br %1408

1424:

1425:
    %1428 = load i32, i32* %1
    %1429 = load i32, i32* %2
    %1430 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1429
    %1431 = load i32, i32* %1430
    %1432 = mul i32 1,%1431
    %1433 = load i32, i32* %1
    %1434 = load i32, i32* %2
    %1435 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1434
    %1436 = load i32, i32* %1435
    %1437 = mul i32 1,%1436
    %1438 = add i32 %1433,%1437
    store i32 %1438, i32* %1
    br %1424

1449:
    %1452 = load i32, i32* %2
    %1453 = icmp lt i32 %1452,i32 16
    br i32 %1453 %1450 %1451

1450:
    %1456 = load i32, i32* %3
    %1457 = srem i32 %1456,2
    %1458 = load i32, i32* %4
    %1459 = srem i32 %1458,2
    %1460 = and i32 %1457,%1459
    br i32 %1460 %1455 %1454

1451:
    %1479 = load i32, i32* %1
    store i32 %1479, i32* %1390
    %1483 = icmp gt i32 1,i32 15
    br i32 %1483 %1481 %1482

1454:
    %1472 = load i32, i32* %3
    %1473 = sdiv i32 %1472,2
    store i32 %1473, i32* %3
    %1474 = load i32, i32* %4
    %1475 = sdiv i32 %1474,2
    store i32 %1475, i32* %4
    %1476 = load i32, i32* %2
    %1477 = load i32, i32* %2
    %1478 = add i32 %1477,1
    store i32 %1478, i32* %2
    br %1449

1455:
    %1461 = load i32, i32* %1
    %1462 = load i32, i32* %2
    %1463 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1462
    %1464 = load i32, i32* %1463
    %1465 = mul i32 1,%1464
    %1466 = load i32, i32* %1
    %1467 = load i32, i32* %2
    %1468 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1467
    %1469 = load i32, i32* %1468
    %1470 = mul i32 1,%1469
    %1471 = add i32 %1466,%1470
    store i32 %1471, i32* %1
    br %1454

1480:
    %1518 = load i32, i32* %1
    store i32 %1518, i32* %1390
    %1519 = load i32, i32* %1391
    store i32 %1519, i32* %1388
    br %1392

1481:
    store i32 0, i32* %1
    br %1480

1482:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %1484 = load i32, i32* %1390
    %1485 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 1
    %1486 = load i32, i32* %1485
    %1487 = mul i32 %1484,%1486
    store i32 %1487, i32* %3
    store i32 65535, i32* %4
    br %1488

1488:
    %1491 = load i32, i32* %2
    %1492 = icmp lt i32 %1491,i32 16
    br i32 %1492 %1489 %1490

1489:
    %1495 = load i32, i32* %3
    %1496 = srem i32 %1495,2
    %1497 = load i32, i32* %4
    %1498 = srem i32 %1497,2
    %1499 = and i32 %1496,%1498
    br i32 %1499 %1494 %1493

1490:

1493:
    %1511 = load i32, i32* %3
    %1512 = sdiv i32 %1511,2
    store i32 %1512, i32* %3
    %1513 = load i32, i32* %4
    %1514 = sdiv i32 %1513,2
    store i32 %1514, i32* %4
    %1515 = load i32, i32* %2
    %1516 = load i32, i32* %2
    %1517 = add i32 %1516,1
    store i32 %1517, i32* %2
    br %1488

1494:
    %1500 = load i32, i32* %1
    %1501 = load i32, i32* %2
    %1502 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1501
    %1503 = load i32, i32* %1502
    %1504 = mul i32 1,%1503
    %1505 = load i32, i32* %1
    %1506 = load i32, i32* %2
    %1507 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1506
    %1508 = load i32, i32* %1507
    %1509 = mul i32 1,%1508
    %1510 = add i32 %1505,%1509
    store i32 %1510, i32* %1
    br %1493

1523:
    %1564 = load i32, i32* %1
    store i32 %1564, i32* %1212
    br %1214

1524:
    %1531 = load i32, i32* %3
    %1532 = icmp lt i32 %1531,i32 0
    br i32 %1532 %1529 %1530

1525:
    %1536 = load i32, i32* %4
    %1537 = icmp gt i32 %1536,i32 0
    br i32 %1537 %1534 %1535

1528:

1529:
    store i32 65535, i32* %1
    br %1528

1530:
    store i32 0, i32* %1
    br %1528

1533:

1534:
    %1541 = load i32, i32* %3
    %1542 = icmp gt i32 %1541,i32 32767
    br i32 %1542 %1539 %1540

1535:
    %1563 = load i32, i32* %3
    store i32 %1563, i32* %1
    br %1533

1538:

1539:
    %1543 = load i32, i32* %3
    %1544 = load i32, i32* %4
    %1545 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1544
    %1546 = load i32, i32* %1545
    %1547 = sdiv i32 %1543,%1546
    store i32 %1547, i32* %3
    %1548 = load i32, i32* %3
    %1549 = load i32, i32* %3
    %1550 = add i32 %1549,65536
    %1551 = load i32, i32* %4
    %1552 = load i32, i32* %4
    %1553 = sub i32 15,%1552
    %1554 = add i32 %1553,1
    %1555 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1554
    %1556 = load i32, i32* %1555
    %1557 = sub i32 %1550,%1556
    store i32 %1557, i32* %1
    br %1538

1540:
    %1558 = load i32, i32* %3
    %1559 = load i32, i32* %4
    %1560 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1559
    %1561 = load i32, i32* %1560
    %1562 = sdiv i32 %1558,%1561
    store i32 %1562, i32* %1
    br %1538

1568:
    %1609 = load i32, i32* %1
    store i32 %1609, i32* %810
    br %812

1569:
    %1576 = load i32, i32* %3
    %1577 = icmp lt i32 %1576,i32 0
    br i32 %1577 %1574 %1575

1570:
    %1581 = load i32, i32* %4
    %1582 = icmp gt i32 %1581,i32 0
    br i32 %1582 %1579 %1580

1573:

1574:
    store i32 65535, i32* %1
    br %1573

1575:
    store i32 0, i32* %1
    br %1573

1578:

1579:
    %1586 = load i32, i32* %3
    %1587 = icmp gt i32 %1586,i32 32767
    br i32 %1587 %1584 %1585

1580:
    %1608 = load i32, i32* %3
    store i32 %1608, i32* %1
    br %1578

1583:

1584:
    %1588 = load i32, i32* %3
    %1589 = load i32, i32* %4
    %1590 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1589
    %1591 = load i32, i32* %1590
    %1592 = sdiv i32 %1588,%1591
    store i32 %1592, i32* %3
    %1593 = load i32, i32* %3
    %1594 = load i32, i32* %3
    %1595 = add i32 %1594,65536
    %1596 = load i32, i32* %4
    %1597 = load i32, i32* %4
    %1598 = sub i32 15,%1597
    %1599 = add i32 %1598,1
    %1600 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1599
    %1601 = load i32, i32* %1600
    %1602 = sub i32 %1595,%1601
    store i32 %1602, i32* %1
    br %1583

1585:
    %1603 = load i32, i32* %3
    %1604 = load i32, i32* %4
    %1605 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1604
    %1606 = load i32, i32* %1605
    %1607 = sdiv i32 %1603,%1606
    store i32 %1607, i32* %1
    br %1583

1612:
    %1615 = load i32, i32* %5
    %1616 = icmp lt i32 %1615,i32 16
    br i32 %1616 %1613 %1614

1613:
    %1617 = alloca i32
    store i32 2, i32* %1617
    %1618 = load i32, i32* %5
    %1619 = alloca i32
    store i32 %1618, i32* %1619
    %1620 = alloca i32
    store i32 1, i32* %1620
    br %1621

1614:
    store i32 0, i32* %5
    br %2424

1621:
    %1624 = load i32, i32* %1619
    %1625 = icmp gt i32 %1624,i32 0
    br i32 %1625 %1622 %1623

1622:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %1626 = load i32, i32* %1619
    store i32 %1626, i32* %3
    store i32 1, i32* %4
    br %1627

1623:
    %2419 = load i32, i32* %1620
    store i32 %2419, i32* %1
    %2420 = load i32, i32* %1
    call void @putint(i32 %2420)
    call void @putch(i32 10)
    %2421 = load i32, i32* %5
    %2422 = load i32, i32* %5
    %2423 = add i32 %2422,1
    store i32 %2423, i32* %5
    br %1612

1627:
    %1630 = load i32, i32* %2
    %1631 = icmp lt i32 %1630,i32 16
    br i32 %1631 %1628 %1629

1628:
    %1634 = load i32, i32* %3
    %1635 = srem i32 %1634,2
    %1636 = load i32, i32* %4
    %1637 = srem i32 %1636,2
    %1638 = and i32 %1635,%1637
    br i32 %1638 %1633 %1632

1629:
    %1659 = load i32, i32* %1
    br i32 %1659 %1658 %1657

1632:
    %1650 = load i32, i32* %3
    %1651 = sdiv i32 %1650,2
    store i32 %1651, i32* %3
    %1652 = load i32, i32* %4
    %1653 = sdiv i32 %1652,2
    store i32 %1653, i32* %4
    %1654 = load i32, i32* %2
    %1655 = load i32, i32* %2
    %1656 = add i32 %1655,1
    store i32 %1656, i32* %2
    br %1627

1633:
    %1639 = load i32, i32* %1
    %1640 = load i32, i32* %2
    %1641 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1640
    %1642 = load i32, i32* %1641
    %1643 = mul i32 1,%1642
    %1644 = load i32, i32* %1
    %1645 = load i32, i32* %2
    %1646 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1645
    %1647 = load i32, i32* %1646
    %1648 = mul i32 1,%1647
    %1649 = add i32 %1644,%1648
    store i32 %1649, i32* %1
    br %1632

1657:
    %2018 = load i32, i32* %1617
    %2019 = alloca i32
    store i32 %2018, i32* %2019
    %2020 = load i32, i32* %1617
    %2021 = alloca i32
    store i32 %2020, i32* %2021
    %2022 = alloca i32
    store i32 0, i32* %2022
    br %2023

1658:
    %1660 = load i32, i32* %1620
    %1661 = alloca i32
    store i32 %1660, i32* %1661
    %1662 = load i32, i32* %1617
    %1663 = alloca i32
    store i32 %1662, i32* %1663
    %1664 = alloca i32
    store i32 0, i32* %1664
    br %1665

1665:
    %1668 = load i32, i32* %1663
    br i32 %1668 %1666 %1667

1666:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %1669 = load i32, i32* %1663
    store i32 %1669, i32* %3
    store i32 1, i32* %4
    br %1670

1667:
    %2016 = load i32, i32* %1664
    store i32 %2016, i32* %1
    %2017 = load i32, i32* %1
    store i32 %2017, i32* %1620

1670:
    %1673 = load i32, i32* %2
    %1674 = icmp lt i32 %1673,i32 16
    br i32 %1674 %1671 %1672

1671:
    %1677 = load i32, i32* %3
    %1678 = srem i32 %1677,2
    %1679 = load i32, i32* %4
    %1680 = srem i32 %1679,2
    %1681 = and i32 %1678,%1680
    br i32 %1681 %1676 %1675

1672:
    %1702 = load i32, i32* %1
    br i32 %1702 %1701 %1700

1675:
    %1693 = load i32, i32* %3
    %1694 = sdiv i32 %1693,2
    store i32 %1694, i32* %3
    %1695 = load i32, i32* %4
    %1696 = sdiv i32 %1695,2
    store i32 %1696, i32* %4
    %1697 = load i32, i32* %2
    %1698 = load i32, i32* %2
    %1699 = add i32 %1698,1
    store i32 %1699, i32* %2
    br %1670

1676:
    %1682 = load i32, i32* %1
    %1683 = load i32, i32* %2
    %1684 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1683
    %1685 = load i32, i32* %1684
    %1686 = mul i32 1,%1685
    %1687 = load i32, i32* %1
    %1688 = load i32, i32* %2
    %1689 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1688
    %1690 = load i32, i32* %1689
    %1691 = mul i32 1,%1690
    %1692 = add i32 %1687,%1691
    store i32 %1692, i32* %1
    br %1675

1700:
    %1838 = load i32, i32* %1661
    %1839 = alloca i32
    store i32 %1838, i32* %1839
    %1840 = load i32, i32* %1661
    %1841 = alloca i32
    store i32 %1840, i32* %1841
    %1842 = alloca i32
    br %1843

1701:
    %1703 = load i32, i32* %1664
    %1704 = alloca i32
    store i32 %1703, i32* %1704
    %1705 = load i32, i32* %1661
    %1706 = alloca i32
    store i32 %1705, i32* %1706
    %1707 = alloca i32
    br %1708

1708:
    %1711 = load i32, i32* %1706
    br i32 %1711 %1709 %1710

1709:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %1712 = load i32, i32* %1704
    store i32 %1712, i32* %3
    %1713 = load i32, i32* %1706
    store i32 %1713, i32* %4
    br %1714

1710:
    %1836 = load i32, i32* %1704
    store i32 %1836, i32* %1
    %1837 = load i32, i32* %1
    store i32 %1837, i32* %1664

1714:
    %1717 = load i32, i32* %2
    %1718 = icmp lt i32 %1717,i32 16
    br i32 %1718 %1715 %1716

1715:
    %1722 = load i32, i32* %3
    %1723 = srem i32 %1722,2
    br i32 %1723 %1720 %1721

1716:
    %1762 = load i32, i32* %1
    store i32 %1762, i32* %1707
    store i32 0, i32* %1
    store i32 0, i32* %2
    %1763 = load i32, i32* %1704
    store i32 %1763, i32* %3
    %1764 = load i32, i32* %1706
    store i32 %1764, i32* %4
    br %1765

1719:
    %1755 = load i32, i32* %3
    %1756 = sdiv i32 %1755,2
    store i32 %1756, i32* %3
    %1757 = load i32, i32* %4
    %1758 = sdiv i32 %1757,2
    store i32 %1758, i32* %4
    %1759 = load i32, i32* %2
    %1760 = load i32, i32* %2
    %1761 = add i32 %1760,1
    store i32 %1761, i32* %2
    br %1714

1720:
    %1726 = load i32, i32* %4
    %1727 = srem i32 %1726,2
    %1728 = icmp eq i32 %1727,i32 0
    br i32 %1728 %1725 %1724

1721:
    %1742 = load i32, i32* %4
    %1743 = srem i32 %1742,2
    br i32 %1743 %1741 %1740

1724:

1725:
    %1729 = load i32, i32* %1
    %1730 = load i32, i32* %2
    %1731 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1730
    %1732 = load i32, i32* %1731
    %1733 = mul i32 1,%1732
    %1734 = load i32, i32* %1
    %1735 = load i32, i32* %2
    %1736 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1735
    %1737 = load i32, i32* %1736
    %1738 = mul i32 1,%1737
    %1739 = add i32 %1734,%1738
    store i32 %1739, i32* %1
    br %1724

1740:

1741:
    %1744 = load i32, i32* %1
    %1745 = load i32, i32* %2
    %1746 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1745
    %1747 = load i32, i32* %1746
    %1748 = mul i32 1,%1747
    %1749 = load i32, i32* %1
    %1750 = load i32, i32* %2
    %1751 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1750
    %1752 = load i32, i32* %1751
    %1753 = mul i32 1,%1752
    %1754 = add i32 %1749,%1753
    store i32 %1754, i32* %1
    br %1740

1765:
    %1768 = load i32, i32* %2
    %1769 = icmp lt i32 %1768,i32 16
    br i32 %1769 %1766 %1767

1766:
    %1772 = load i32, i32* %3
    %1773 = srem i32 %1772,2
    %1774 = load i32, i32* %4
    %1775 = srem i32 %1774,2
    %1776 = and i32 %1773,%1775
    br i32 %1776 %1771 %1770

1767:
    %1795 = load i32, i32* %1
    store i32 %1795, i32* %1706
    %1799 = icmp gt i32 1,i32 15
    br i32 %1799 %1797 %1798

1770:
    %1788 = load i32, i32* %3
    %1789 = sdiv i32 %1788,2
    store i32 %1789, i32* %3
    %1790 = load i32, i32* %4
    %1791 = sdiv i32 %1790,2
    store i32 %1791, i32* %4
    %1792 = load i32, i32* %2
    %1793 = load i32, i32* %2
    %1794 = add i32 %1793,1
    store i32 %1794, i32* %2
    br %1765

1771:
    %1777 = load i32, i32* %1
    %1778 = load i32, i32* %2
    %1779 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1778
    %1780 = load i32, i32* %1779
    %1781 = mul i32 1,%1780
    %1782 = load i32, i32* %1
    %1783 = load i32, i32* %2
    %1784 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1783
    %1785 = load i32, i32* %1784
    %1786 = mul i32 1,%1785
    %1787 = add i32 %1782,%1786
    store i32 %1787, i32* %1
    br %1770

1796:
    %1834 = load i32, i32* %1
    store i32 %1834, i32* %1706
    %1835 = load i32, i32* %1707
    store i32 %1835, i32* %1704
    br %1708

1797:
    store i32 0, i32* %1
    br %1796

1798:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %1800 = load i32, i32* %1706
    %1801 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 1
    %1802 = load i32, i32* %1801
    %1803 = mul i32 %1800,%1802
    store i32 %1803, i32* %3
    store i32 65535, i32* %4
    br %1804

1804:
    %1807 = load i32, i32* %2
    %1808 = icmp lt i32 %1807,i32 16
    br i32 %1808 %1805 %1806

1805:
    %1811 = load i32, i32* %3
    %1812 = srem i32 %1811,2
    %1813 = load i32, i32* %4
    %1814 = srem i32 %1813,2
    %1815 = and i32 %1812,%1814
    br i32 %1815 %1810 %1809

1806:

1809:
    %1827 = load i32, i32* %3
    %1828 = sdiv i32 %1827,2
    store i32 %1828, i32* %3
    %1829 = load i32, i32* %4
    %1830 = sdiv i32 %1829,2
    store i32 %1830, i32* %4
    %1831 = load i32, i32* %2
    %1832 = load i32, i32* %2
    %1833 = add i32 %1832,1
    store i32 %1833, i32* %2
    br %1804

1810:
    %1816 = load i32, i32* %1
    %1817 = load i32, i32* %2
    %1818 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1817
    %1819 = load i32, i32* %1818
    %1820 = mul i32 1,%1819
    %1821 = load i32, i32* %1
    %1822 = load i32, i32* %2
    %1823 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1822
    %1824 = load i32, i32* %1823
    %1825 = mul i32 1,%1824
    %1826 = add i32 %1821,%1825
    store i32 %1826, i32* %1
    br %1809

1843:
    %1846 = load i32, i32* %1841
    br i32 %1846 %1844 %1845

1844:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %1847 = load i32, i32* %1839
    store i32 %1847, i32* %3
    %1848 = load i32, i32* %1841
    store i32 %1848, i32* %4
    br %1849

1845:
    %1971 = load i32, i32* %1839
    store i32 %1971, i32* %1
    %1972 = load i32, i32* %1
    store i32 %1972, i32* %1661
    %1973 = load i32, i32* %1663
    store i32 %1973, i32* %3
    store i32 1, i32* %4
    %1977 = load i32, i32* %4
    %1978 = icmp ge i32 %1977,i32 15
    br i32 %1978 %1975 %1976

1849:
    %1852 = load i32, i32* %2
    %1853 = icmp lt i32 %1852,i32 16
    br i32 %1853 %1850 %1851

1850:
    %1857 = load i32, i32* %3
    %1858 = srem i32 %1857,2
    br i32 %1858 %1855 %1856

1851:
    %1897 = load i32, i32* %1
    store i32 %1897, i32* %1842
    store i32 0, i32* %1
    store i32 0, i32* %2
    %1898 = load i32, i32* %1839
    store i32 %1898, i32* %3
    %1899 = load i32, i32* %1841
    store i32 %1899, i32* %4
    br %1900

1854:
    %1890 = load i32, i32* %3
    %1891 = sdiv i32 %1890,2
    store i32 %1891, i32* %3
    %1892 = load i32, i32* %4
    %1893 = sdiv i32 %1892,2
    store i32 %1893, i32* %4
    %1894 = load i32, i32* %2
    %1895 = load i32, i32* %2
    %1896 = add i32 %1895,1
    store i32 %1896, i32* %2
    br %1849

1855:
    %1861 = load i32, i32* %4
    %1862 = srem i32 %1861,2
    %1863 = icmp eq i32 %1862,i32 0
    br i32 %1863 %1860 %1859

1856:
    %1877 = load i32, i32* %4
    %1878 = srem i32 %1877,2
    br i32 %1878 %1876 %1875

1859:

1860:
    %1864 = load i32, i32* %1
    %1865 = load i32, i32* %2
    %1866 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1865
    %1867 = load i32, i32* %1866
    %1868 = mul i32 1,%1867
    %1869 = load i32, i32* %1
    %1870 = load i32, i32* %2
    %1871 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1870
    %1872 = load i32, i32* %1871
    %1873 = mul i32 1,%1872
    %1874 = add i32 %1869,%1873
    store i32 %1874, i32* %1
    br %1859

1875:

1876:
    %1879 = load i32, i32* %1
    %1880 = load i32, i32* %2
    %1881 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1880
    %1882 = load i32, i32* %1881
    %1883 = mul i32 1,%1882
    %1884 = load i32, i32* %1
    %1885 = load i32, i32* %2
    %1886 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1885
    %1887 = load i32, i32* %1886
    %1888 = mul i32 1,%1887
    %1889 = add i32 %1884,%1888
    store i32 %1889, i32* %1
    br %1875

1900:
    %1903 = load i32, i32* %2
    %1904 = icmp lt i32 %1903,i32 16
    br i32 %1904 %1901 %1902

1901:
    %1907 = load i32, i32* %3
    %1908 = srem i32 %1907,2
    %1909 = load i32, i32* %4
    %1910 = srem i32 %1909,2
    %1911 = and i32 %1908,%1910
    br i32 %1911 %1906 %1905

1902:
    %1930 = load i32, i32* %1
    store i32 %1930, i32* %1841
    %1934 = icmp gt i32 1,i32 15
    br i32 %1934 %1932 %1933

1905:
    %1923 = load i32, i32* %3
    %1924 = sdiv i32 %1923,2
    store i32 %1924, i32* %3
    %1925 = load i32, i32* %4
    %1926 = sdiv i32 %1925,2
    store i32 %1926, i32* %4
    %1927 = load i32, i32* %2
    %1928 = load i32, i32* %2
    %1929 = add i32 %1928,1
    store i32 %1929, i32* %2
    br %1900

1906:
    %1912 = load i32, i32* %1
    %1913 = load i32, i32* %2
    %1914 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1913
    %1915 = load i32, i32* %1914
    %1916 = mul i32 1,%1915
    %1917 = load i32, i32* %1
    %1918 = load i32, i32* %2
    %1919 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1918
    %1920 = load i32, i32* %1919
    %1921 = mul i32 1,%1920
    %1922 = add i32 %1917,%1921
    store i32 %1922, i32* %1
    br %1905

1931:
    %1969 = load i32, i32* %1
    store i32 %1969, i32* %1841
    %1970 = load i32, i32* %1842
    store i32 %1970, i32* %1839
    br %1843

1932:
    store i32 0, i32* %1
    br %1931

1933:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %1935 = load i32, i32* %1841
    %1936 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 1
    %1937 = load i32, i32* %1936
    %1938 = mul i32 %1935,%1937
    store i32 %1938, i32* %3
    store i32 65535, i32* %4
    br %1939

1939:
    %1942 = load i32, i32* %2
    %1943 = icmp lt i32 %1942,i32 16
    br i32 %1943 %1940 %1941

1940:
    %1946 = load i32, i32* %3
    %1947 = srem i32 %1946,2
    %1948 = load i32, i32* %4
    %1949 = srem i32 %1948,2
    %1950 = and i32 %1947,%1949
    br i32 %1950 %1945 %1944

1941:

1944:
    %1962 = load i32, i32* %3
    %1963 = sdiv i32 %1962,2
    store i32 %1963, i32* %3
    %1964 = load i32, i32* %4
    %1965 = sdiv i32 %1964,2
    store i32 %1965, i32* %4
    %1966 = load i32, i32* %2
    %1967 = load i32, i32* %2
    %1968 = add i32 %1967,1
    store i32 %1968, i32* %2
    br %1939

1945:
    %1951 = load i32, i32* %1
    %1952 = load i32, i32* %2
    %1953 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1952
    %1954 = load i32, i32* %1953
    %1955 = mul i32 1,%1954
    %1956 = load i32, i32* %1
    %1957 = load i32, i32* %2
    %1958 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1957
    %1959 = load i32, i32* %1958
    %1960 = mul i32 1,%1959
    %1961 = add i32 %1956,%1960
    store i32 %1961, i32* %1
    br %1944

1974:
    %2015 = load i32, i32* %1
    store i32 %2015, i32* %1663
    br %1665

1975:
    %1982 = load i32, i32* %3
    %1983 = icmp lt i32 %1982,i32 0
    br i32 %1983 %1980 %1981

1976:
    %1987 = load i32, i32* %4
    %1988 = icmp gt i32 %1987,i32 0
    br i32 %1988 %1985 %1986

1979:

1980:
    store i32 65535, i32* %1
    br %1979

1981:
    store i32 0, i32* %1
    br %1979

1984:

1985:
    %1992 = load i32, i32* %3
    %1993 = icmp gt i32 %1992,i32 32767
    br i32 %1993 %1990 %1991

1986:
    %2014 = load i32, i32* %3
    store i32 %2014, i32* %1
    br %1984

1989:

1990:
    %1994 = load i32, i32* %3
    %1995 = load i32, i32* %4
    %1996 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %1995
    %1997 = load i32, i32* %1996
    %1998 = sdiv i32 %1994,%1997
    store i32 %1998, i32* %3
    %1999 = load i32, i32* %3
    %2000 = load i32, i32* %3
    %2001 = add i32 %2000,65536
    %2002 = load i32, i32* %4
    %2003 = load i32, i32* %4
    %2004 = sub i32 15,%2003
    %2005 = add i32 %2004,1
    %2006 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2005
    %2007 = load i32, i32* %2006
    %2008 = sub i32 %2001,%2007
    store i32 %2008, i32* %1
    br %1989

1991:
    %2009 = load i32, i32* %3
    %2010 = load i32, i32* %4
    %2011 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2010
    %2012 = load i32, i32* %2011
    %2013 = sdiv i32 %2009,%2012
    store i32 %2013, i32* %1
    br %1989

2023:
    %2026 = load i32, i32* %2021
    br i32 %2026 %2024 %2025

2024:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %2027 = load i32, i32* %2021
    store i32 %2027, i32* %3
    store i32 1, i32* %4
    br %2028

2025:
    %2374 = load i32, i32* %2022
    store i32 %2374, i32* %1
    %2375 = load i32, i32* %1
    store i32 %2375, i32* %1617
    %2376 = load i32, i32* %1619
    store i32 %2376, i32* %3
    store i32 1, i32* %4
    %2380 = load i32, i32* %4
    %2381 = icmp ge i32 %2380,i32 15
    br i32 %2381 %2378 %2379

2028:
    %2031 = load i32, i32* %2
    %2032 = icmp lt i32 %2031,i32 16
    br i32 %2032 %2029 %2030

2029:
    %2035 = load i32, i32* %3
    %2036 = srem i32 %2035,2
    %2037 = load i32, i32* %4
    %2038 = srem i32 %2037,2
    %2039 = and i32 %2036,%2038
    br i32 %2039 %2034 %2033

2030:
    %2060 = load i32, i32* %1
    br i32 %2060 %2059 %2058

2033:
    %2051 = load i32, i32* %3
    %2052 = sdiv i32 %2051,2
    store i32 %2052, i32* %3
    %2053 = load i32, i32* %4
    %2054 = sdiv i32 %2053,2
    store i32 %2054, i32* %4
    %2055 = load i32, i32* %2
    %2056 = load i32, i32* %2
    %2057 = add i32 %2056,1
    store i32 %2057, i32* %2
    br %2028

2034:
    %2040 = load i32, i32* %1
    %2041 = load i32, i32* %2
    %2042 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2041
    %2043 = load i32, i32* %2042
    %2044 = mul i32 1,%2043
    %2045 = load i32, i32* %1
    %2046 = load i32, i32* %2
    %2047 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2046
    %2048 = load i32, i32* %2047
    %2049 = mul i32 1,%2048
    %2050 = add i32 %2045,%2049
    store i32 %2050, i32* %1
    br %2033

2058:
    %2196 = load i32, i32* %2019
    %2197 = alloca i32
    store i32 %2196, i32* %2197
    %2198 = load i32, i32* %2019
    %2199 = alloca i32
    store i32 %2198, i32* %2199
    %2200 = alloca i32
    br %2201

2059:
    %2061 = load i32, i32* %2022
    %2062 = alloca i32
    store i32 %2061, i32* %2062
    %2063 = load i32, i32* %2019
    %2064 = alloca i32
    store i32 %2063, i32* %2064
    %2065 = alloca i32
    br %2066

2066:
    %2069 = load i32, i32* %2064
    br i32 %2069 %2067 %2068

2067:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %2070 = load i32, i32* %2062
    store i32 %2070, i32* %3
    %2071 = load i32, i32* %2064
    store i32 %2071, i32* %4
    br %2072

2068:
    %2194 = load i32, i32* %2062
    store i32 %2194, i32* %1
    %2195 = load i32, i32* %1
    store i32 %2195, i32* %2022

2072:
    %2075 = load i32, i32* %2
    %2076 = icmp lt i32 %2075,i32 16
    br i32 %2076 %2073 %2074

2073:
    %2080 = load i32, i32* %3
    %2081 = srem i32 %2080,2
    br i32 %2081 %2078 %2079

2074:
    %2120 = load i32, i32* %1
    store i32 %2120, i32* %2065
    store i32 0, i32* %1
    store i32 0, i32* %2
    %2121 = load i32, i32* %2062
    store i32 %2121, i32* %3
    %2122 = load i32, i32* %2064
    store i32 %2122, i32* %4
    br %2123

2077:
    %2113 = load i32, i32* %3
    %2114 = sdiv i32 %2113,2
    store i32 %2114, i32* %3
    %2115 = load i32, i32* %4
    %2116 = sdiv i32 %2115,2
    store i32 %2116, i32* %4
    %2117 = load i32, i32* %2
    %2118 = load i32, i32* %2
    %2119 = add i32 %2118,1
    store i32 %2119, i32* %2
    br %2072

2078:
    %2084 = load i32, i32* %4
    %2085 = srem i32 %2084,2
    %2086 = icmp eq i32 %2085,i32 0
    br i32 %2086 %2083 %2082

2079:
    %2100 = load i32, i32* %4
    %2101 = srem i32 %2100,2
    br i32 %2101 %2099 %2098

2082:

2083:
    %2087 = load i32, i32* %1
    %2088 = load i32, i32* %2
    %2089 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2088
    %2090 = load i32, i32* %2089
    %2091 = mul i32 1,%2090
    %2092 = load i32, i32* %1
    %2093 = load i32, i32* %2
    %2094 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2093
    %2095 = load i32, i32* %2094
    %2096 = mul i32 1,%2095
    %2097 = add i32 %2092,%2096
    store i32 %2097, i32* %1
    br %2082

2098:

2099:
    %2102 = load i32, i32* %1
    %2103 = load i32, i32* %2
    %2104 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2103
    %2105 = load i32, i32* %2104
    %2106 = mul i32 1,%2105
    %2107 = load i32, i32* %1
    %2108 = load i32, i32* %2
    %2109 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2108
    %2110 = load i32, i32* %2109
    %2111 = mul i32 1,%2110
    %2112 = add i32 %2107,%2111
    store i32 %2112, i32* %1
    br %2098

2123:
    %2126 = load i32, i32* %2
    %2127 = icmp lt i32 %2126,i32 16
    br i32 %2127 %2124 %2125

2124:
    %2130 = load i32, i32* %3
    %2131 = srem i32 %2130,2
    %2132 = load i32, i32* %4
    %2133 = srem i32 %2132,2
    %2134 = and i32 %2131,%2133
    br i32 %2134 %2129 %2128

2125:
    %2153 = load i32, i32* %1
    store i32 %2153, i32* %2064
    %2157 = icmp gt i32 1,i32 15
    br i32 %2157 %2155 %2156

2128:
    %2146 = load i32, i32* %3
    %2147 = sdiv i32 %2146,2
    store i32 %2147, i32* %3
    %2148 = load i32, i32* %4
    %2149 = sdiv i32 %2148,2
    store i32 %2149, i32* %4
    %2150 = load i32, i32* %2
    %2151 = load i32, i32* %2
    %2152 = add i32 %2151,1
    store i32 %2152, i32* %2
    br %2123

2129:
    %2135 = load i32, i32* %1
    %2136 = load i32, i32* %2
    %2137 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2136
    %2138 = load i32, i32* %2137
    %2139 = mul i32 1,%2138
    %2140 = load i32, i32* %1
    %2141 = load i32, i32* %2
    %2142 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2141
    %2143 = load i32, i32* %2142
    %2144 = mul i32 1,%2143
    %2145 = add i32 %2140,%2144
    store i32 %2145, i32* %1
    br %2128

2154:
    %2192 = load i32, i32* %1
    store i32 %2192, i32* %2064
    %2193 = load i32, i32* %2065
    store i32 %2193, i32* %2062
    br %2066

2155:
    store i32 0, i32* %1
    br %2154

2156:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %2158 = load i32, i32* %2064
    %2159 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 1
    %2160 = load i32, i32* %2159
    %2161 = mul i32 %2158,%2160
    store i32 %2161, i32* %3
    store i32 65535, i32* %4
    br %2162

2162:
    %2165 = load i32, i32* %2
    %2166 = icmp lt i32 %2165,i32 16
    br i32 %2166 %2163 %2164

2163:
    %2169 = load i32, i32* %3
    %2170 = srem i32 %2169,2
    %2171 = load i32, i32* %4
    %2172 = srem i32 %2171,2
    %2173 = and i32 %2170,%2172
    br i32 %2173 %2168 %2167

2164:

2167:
    %2185 = load i32, i32* %3
    %2186 = sdiv i32 %2185,2
    store i32 %2186, i32* %3
    %2187 = load i32, i32* %4
    %2188 = sdiv i32 %2187,2
    store i32 %2188, i32* %4
    %2189 = load i32, i32* %2
    %2190 = load i32, i32* %2
    %2191 = add i32 %2190,1
    store i32 %2191, i32* %2
    br %2162

2168:
    %2174 = load i32, i32* %1
    %2175 = load i32, i32* %2
    %2176 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2175
    %2177 = load i32, i32* %2176
    %2178 = mul i32 1,%2177
    %2179 = load i32, i32* %1
    %2180 = load i32, i32* %2
    %2181 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2180
    %2182 = load i32, i32* %2181
    %2183 = mul i32 1,%2182
    %2184 = add i32 %2179,%2183
    store i32 %2184, i32* %1
    br %2167

2201:
    %2204 = load i32, i32* %2199
    br i32 %2204 %2202 %2203

2202:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %2205 = load i32, i32* %2197
    store i32 %2205, i32* %3
    %2206 = load i32, i32* %2199
    store i32 %2206, i32* %4
    br %2207

2203:
    %2329 = load i32, i32* %2197
    store i32 %2329, i32* %1
    %2330 = load i32, i32* %1
    store i32 %2330, i32* %2019
    %2331 = load i32, i32* %2021
    store i32 %2331, i32* %3
    store i32 1, i32* %4
    %2335 = load i32, i32* %4
    %2336 = icmp ge i32 %2335,i32 15
    br i32 %2336 %2333 %2334

2207:
    %2210 = load i32, i32* %2
    %2211 = icmp lt i32 %2210,i32 16
    br i32 %2211 %2208 %2209

2208:
    %2215 = load i32, i32* %3
    %2216 = srem i32 %2215,2
    br i32 %2216 %2213 %2214

2209:
    %2255 = load i32, i32* %1
    store i32 %2255, i32* %2200
    store i32 0, i32* %1
    store i32 0, i32* %2
    %2256 = load i32, i32* %2197
    store i32 %2256, i32* %3
    %2257 = load i32, i32* %2199
    store i32 %2257, i32* %4
    br %2258

2212:
    %2248 = load i32, i32* %3
    %2249 = sdiv i32 %2248,2
    store i32 %2249, i32* %3
    %2250 = load i32, i32* %4
    %2251 = sdiv i32 %2250,2
    store i32 %2251, i32* %4
    %2252 = load i32, i32* %2
    %2253 = load i32, i32* %2
    %2254 = add i32 %2253,1
    store i32 %2254, i32* %2
    br %2207

2213:
    %2219 = load i32, i32* %4
    %2220 = srem i32 %2219,2
    %2221 = icmp eq i32 %2220,i32 0
    br i32 %2221 %2218 %2217

2214:
    %2235 = load i32, i32* %4
    %2236 = srem i32 %2235,2
    br i32 %2236 %2234 %2233

2217:

2218:
    %2222 = load i32, i32* %1
    %2223 = load i32, i32* %2
    %2224 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2223
    %2225 = load i32, i32* %2224
    %2226 = mul i32 1,%2225
    %2227 = load i32, i32* %1
    %2228 = load i32, i32* %2
    %2229 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2228
    %2230 = load i32, i32* %2229
    %2231 = mul i32 1,%2230
    %2232 = add i32 %2227,%2231
    store i32 %2232, i32* %1
    br %2217

2233:

2234:
    %2237 = load i32, i32* %1
    %2238 = load i32, i32* %2
    %2239 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2238
    %2240 = load i32, i32* %2239
    %2241 = mul i32 1,%2240
    %2242 = load i32, i32* %1
    %2243 = load i32, i32* %2
    %2244 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2243
    %2245 = load i32, i32* %2244
    %2246 = mul i32 1,%2245
    %2247 = add i32 %2242,%2246
    store i32 %2247, i32* %1
    br %2233

2258:
    %2261 = load i32, i32* %2
    %2262 = icmp lt i32 %2261,i32 16
    br i32 %2262 %2259 %2260

2259:
    %2265 = load i32, i32* %3
    %2266 = srem i32 %2265,2
    %2267 = load i32, i32* %4
    %2268 = srem i32 %2267,2
    %2269 = and i32 %2266,%2268
    br i32 %2269 %2264 %2263

2260:
    %2288 = load i32, i32* %1
    store i32 %2288, i32* %2199
    %2292 = icmp gt i32 1,i32 15
    br i32 %2292 %2290 %2291

2263:
    %2281 = load i32, i32* %3
    %2282 = sdiv i32 %2281,2
    store i32 %2282, i32* %3
    %2283 = load i32, i32* %4
    %2284 = sdiv i32 %2283,2
    store i32 %2284, i32* %4
    %2285 = load i32, i32* %2
    %2286 = load i32, i32* %2
    %2287 = add i32 %2286,1
    store i32 %2287, i32* %2
    br %2258

2264:
    %2270 = load i32, i32* %1
    %2271 = load i32, i32* %2
    %2272 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2271
    %2273 = load i32, i32* %2272
    %2274 = mul i32 1,%2273
    %2275 = load i32, i32* %1
    %2276 = load i32, i32* %2
    %2277 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2276
    %2278 = load i32, i32* %2277
    %2279 = mul i32 1,%2278
    %2280 = add i32 %2275,%2279
    store i32 %2280, i32* %1
    br %2263

2289:
    %2327 = load i32, i32* %1
    store i32 %2327, i32* %2199
    %2328 = load i32, i32* %2200
    store i32 %2328, i32* %2197
    br %2201

2290:
    store i32 0, i32* %1
    br %2289

2291:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %2293 = load i32, i32* %2199
    %2294 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 1
    %2295 = load i32, i32* %2294
    %2296 = mul i32 %2293,%2295
    store i32 %2296, i32* %3
    store i32 65535, i32* %4
    br %2297

2297:
    %2300 = load i32, i32* %2
    %2301 = icmp lt i32 %2300,i32 16
    br i32 %2301 %2298 %2299

2298:
    %2304 = load i32, i32* %3
    %2305 = srem i32 %2304,2
    %2306 = load i32, i32* %4
    %2307 = srem i32 %2306,2
    %2308 = and i32 %2305,%2307
    br i32 %2308 %2303 %2302

2299:

2302:
    %2320 = load i32, i32* %3
    %2321 = sdiv i32 %2320,2
    store i32 %2321, i32* %3
    %2322 = load i32, i32* %4
    %2323 = sdiv i32 %2322,2
    store i32 %2323, i32* %4
    %2324 = load i32, i32* %2
    %2325 = load i32, i32* %2
    %2326 = add i32 %2325,1
    store i32 %2326, i32* %2
    br %2297

2303:
    %2309 = load i32, i32* %1
    %2310 = load i32, i32* %2
    %2311 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2310
    %2312 = load i32, i32* %2311
    %2313 = mul i32 1,%2312
    %2314 = load i32, i32* %1
    %2315 = load i32, i32* %2
    %2316 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2315
    %2317 = load i32, i32* %2316
    %2318 = mul i32 1,%2317
    %2319 = add i32 %2314,%2318
    store i32 %2319, i32* %1
    br %2302

2332:
    %2373 = load i32, i32* %1
    store i32 %2373, i32* %2021
    br %2023

2333:
    %2340 = load i32, i32* %3
    %2341 = icmp lt i32 %2340,i32 0
    br i32 %2341 %2338 %2339

2334:
    %2345 = load i32, i32* %4
    %2346 = icmp gt i32 %2345,i32 0
    br i32 %2346 %2343 %2344

2337:

2338:
    store i32 65535, i32* %1
    br %2337

2339:
    store i32 0, i32* %1
    br %2337

2342:

2343:
    %2350 = load i32, i32* %3
    %2351 = icmp gt i32 %2350,i32 32767
    br i32 %2351 %2348 %2349

2344:
    %2372 = load i32, i32* %3
    store i32 %2372, i32* %1
    br %2342

2347:

2348:
    %2352 = load i32, i32* %3
    %2353 = load i32, i32* %4
    %2354 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2353
    %2355 = load i32, i32* %2354
    %2356 = sdiv i32 %2352,%2355
    store i32 %2356, i32* %3
    %2357 = load i32, i32* %3
    %2358 = load i32, i32* %3
    %2359 = add i32 %2358,65536
    %2360 = load i32, i32* %4
    %2361 = load i32, i32* %4
    %2362 = sub i32 15,%2361
    %2363 = add i32 %2362,1
    %2364 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2363
    %2365 = load i32, i32* %2364
    %2366 = sub i32 %2359,%2365
    store i32 %2366, i32* %1
    br %2347

2349:
    %2367 = load i32, i32* %3
    %2368 = load i32, i32* %4
    %2369 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2368
    %2370 = load i32, i32* %2369
    %2371 = sdiv i32 %2367,%2370
    store i32 %2371, i32* %1
    br %2347

2377:
    %2418 = load i32, i32* %1
    store i32 %2418, i32* %1619
    br %1621

2378:
    %2385 = load i32, i32* %3
    %2386 = icmp lt i32 %2385,i32 0
    br i32 %2386 %2383 %2384

2379:
    %2390 = load i32, i32* %4
    %2391 = icmp gt i32 %2390,i32 0
    br i32 %2391 %2388 %2389

2382:

2383:
    store i32 65535, i32* %1
    br %2382

2384:
    store i32 0, i32* %1
    br %2382

2387:

2388:
    %2395 = load i32, i32* %3
    %2396 = icmp gt i32 %2395,i32 32767
    br i32 %2396 %2393 %2394

2389:
    %2417 = load i32, i32* %3
    store i32 %2417, i32* %1
    br %2387

2392:

2393:
    %2397 = load i32, i32* %3
    %2398 = load i32, i32* %4
    %2399 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2398
    %2400 = load i32, i32* %2399
    %2401 = sdiv i32 %2397,%2400
    store i32 %2401, i32* %3
    %2402 = load i32, i32* %3
    %2403 = load i32, i32* %3
    %2404 = add i32 %2403,65536
    %2405 = load i32, i32* %4
    %2406 = load i32, i32* %4
    %2407 = sub i32 15,%2406
    %2408 = add i32 %2407,1
    %2409 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2408
    %2410 = load i32, i32* %2409
    %2411 = sub i32 %2404,%2410
    store i32 %2411, i32* %1
    br %2392

2394:
    %2412 = load i32, i32* %3
    %2413 = load i32, i32* %4
    %2414 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2413
    %2415 = load i32, i32* %2414
    %2416 = sdiv i32 %2412,%2415
    store i32 %2416, i32* %1
    br %2392

2424:
    %2427 = load i32, i32* %5
    %2428 = icmp lt i32 %2427,i32 16
    br i32 %2428 %2425 %2426

2425:
    %2429 = alloca i32
    store i32 2, i32* %2429
    %2430 = load i32, i32* %5
    %2431 = alloca i32
    store i32 %2430, i32* %2431
    %2432 = alloca i32
    store i32 1, i32* %2432
    br %2433

2426:
    ret i32 0

2433:
    %2436 = load i32, i32* %2431
    %2437 = icmp gt i32 %2436,i32 0
    br i32 %2437 %2434 %2435

2434:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %2438 = load i32, i32* %2431
    store i32 %2438, i32* %3
    store i32 1, i32* %4
    br %2439

2435:
    %3231 = load i32, i32* %2432
    store i32 %3231, i32* %1
    %3234 = load i32, i32* %5
    %3235 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %3234
    %3236 = load i32, i32* %3235
    %3237 = load i32, i32* %1
    %3238 = icmp ne i32 %3236,i32 %3237
    br i32 %3238 %3233 %3232

2439:
    %2442 = load i32, i32* %2
    %2443 = icmp lt i32 %2442,i32 16
    br i32 %2443 %2440 %2441

2440:
    %2446 = load i32, i32* %3
    %2447 = srem i32 %2446,2
    %2448 = load i32, i32* %4
    %2449 = srem i32 %2448,2
    %2450 = and i32 %2447,%2449
    br i32 %2450 %2445 %2444

2441:
    %2471 = load i32, i32* %1
    br i32 %2471 %2470 %2469

2444:
    %2462 = load i32, i32* %3
    %2463 = sdiv i32 %2462,2
    store i32 %2463, i32* %3
    %2464 = load i32, i32* %4
    %2465 = sdiv i32 %2464,2
    store i32 %2465, i32* %4
    %2466 = load i32, i32* %2
    %2467 = load i32, i32* %2
    %2468 = add i32 %2467,1
    store i32 %2468, i32* %2
    br %2439

2445:
    %2451 = load i32, i32* %1
    %2452 = load i32, i32* %2
    %2453 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2452
    %2454 = load i32, i32* %2453
    %2455 = mul i32 1,%2454
    %2456 = load i32, i32* %1
    %2457 = load i32, i32* %2
    %2458 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2457
    %2459 = load i32, i32* %2458
    %2460 = mul i32 1,%2459
    %2461 = add i32 %2456,%2460
    store i32 %2461, i32* %1
    br %2444

2469:
    %2830 = load i32, i32* %2429
    %2831 = alloca i32
    store i32 %2830, i32* %2831
    %2832 = load i32, i32* %2429
    %2833 = alloca i32
    store i32 %2832, i32* %2833
    %2834 = alloca i32
    store i32 0, i32* %2834
    br %2835

2470:
    %2472 = load i32, i32* %2432
    %2473 = alloca i32
    store i32 %2472, i32* %2473
    %2474 = load i32, i32* %2429
    %2475 = alloca i32
    store i32 %2474, i32* %2475
    %2476 = alloca i32
    store i32 0, i32* %2476
    br %2477

2477:
    %2480 = load i32, i32* %2475
    br i32 %2480 %2478 %2479

2478:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %2481 = load i32, i32* %2475
    store i32 %2481, i32* %3
    store i32 1, i32* %4
    br %2482

2479:
    %2828 = load i32, i32* %2476
    store i32 %2828, i32* %1
    %2829 = load i32, i32* %1
    store i32 %2829, i32* %2432

2482:
    %2485 = load i32, i32* %2
    %2486 = icmp lt i32 %2485,i32 16
    br i32 %2486 %2483 %2484

2483:
    %2489 = load i32, i32* %3
    %2490 = srem i32 %2489,2
    %2491 = load i32, i32* %4
    %2492 = srem i32 %2491,2
    %2493 = and i32 %2490,%2492
    br i32 %2493 %2488 %2487

2484:
    %2514 = load i32, i32* %1
    br i32 %2514 %2513 %2512

2487:
    %2505 = load i32, i32* %3
    %2506 = sdiv i32 %2505,2
    store i32 %2506, i32* %3
    %2507 = load i32, i32* %4
    %2508 = sdiv i32 %2507,2
    store i32 %2508, i32* %4
    %2509 = load i32, i32* %2
    %2510 = load i32, i32* %2
    %2511 = add i32 %2510,1
    store i32 %2511, i32* %2
    br %2482

2488:
    %2494 = load i32, i32* %1
    %2495 = load i32, i32* %2
    %2496 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2495
    %2497 = load i32, i32* %2496
    %2498 = mul i32 1,%2497
    %2499 = load i32, i32* %1
    %2500 = load i32, i32* %2
    %2501 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2500
    %2502 = load i32, i32* %2501
    %2503 = mul i32 1,%2502
    %2504 = add i32 %2499,%2503
    store i32 %2504, i32* %1
    br %2487

2512:
    %2650 = load i32, i32* %2473
    %2651 = alloca i32
    store i32 %2650, i32* %2651
    %2652 = load i32, i32* %2473
    %2653 = alloca i32
    store i32 %2652, i32* %2653
    %2654 = alloca i32
    br %2655

2513:
    %2515 = load i32, i32* %2476
    %2516 = alloca i32
    store i32 %2515, i32* %2516
    %2517 = load i32, i32* %2473
    %2518 = alloca i32
    store i32 %2517, i32* %2518
    %2519 = alloca i32
    br %2520

2520:
    %2523 = load i32, i32* %2518
    br i32 %2523 %2521 %2522

2521:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %2524 = load i32, i32* %2516
    store i32 %2524, i32* %3
    %2525 = load i32, i32* %2518
    store i32 %2525, i32* %4
    br %2526

2522:
    %2648 = load i32, i32* %2516
    store i32 %2648, i32* %1
    %2649 = load i32, i32* %1
    store i32 %2649, i32* %2476

2526:
    %2529 = load i32, i32* %2
    %2530 = icmp lt i32 %2529,i32 16
    br i32 %2530 %2527 %2528

2527:
    %2534 = load i32, i32* %3
    %2535 = srem i32 %2534,2
    br i32 %2535 %2532 %2533

2528:
    %2574 = load i32, i32* %1
    store i32 %2574, i32* %2519
    store i32 0, i32* %1
    store i32 0, i32* %2
    %2575 = load i32, i32* %2516
    store i32 %2575, i32* %3
    %2576 = load i32, i32* %2518
    store i32 %2576, i32* %4
    br %2577

2531:
    %2567 = load i32, i32* %3
    %2568 = sdiv i32 %2567,2
    store i32 %2568, i32* %3
    %2569 = load i32, i32* %4
    %2570 = sdiv i32 %2569,2
    store i32 %2570, i32* %4
    %2571 = load i32, i32* %2
    %2572 = load i32, i32* %2
    %2573 = add i32 %2572,1
    store i32 %2573, i32* %2
    br %2526

2532:
    %2538 = load i32, i32* %4
    %2539 = srem i32 %2538,2
    %2540 = icmp eq i32 %2539,i32 0
    br i32 %2540 %2537 %2536

2533:
    %2554 = load i32, i32* %4
    %2555 = srem i32 %2554,2
    br i32 %2555 %2553 %2552

2536:

2537:
    %2541 = load i32, i32* %1
    %2542 = load i32, i32* %2
    %2543 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2542
    %2544 = load i32, i32* %2543
    %2545 = mul i32 1,%2544
    %2546 = load i32, i32* %1
    %2547 = load i32, i32* %2
    %2548 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2547
    %2549 = load i32, i32* %2548
    %2550 = mul i32 1,%2549
    %2551 = add i32 %2546,%2550
    store i32 %2551, i32* %1
    br %2536

2552:

2553:
    %2556 = load i32, i32* %1
    %2557 = load i32, i32* %2
    %2558 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2557
    %2559 = load i32, i32* %2558
    %2560 = mul i32 1,%2559
    %2561 = load i32, i32* %1
    %2562 = load i32, i32* %2
    %2563 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2562
    %2564 = load i32, i32* %2563
    %2565 = mul i32 1,%2564
    %2566 = add i32 %2561,%2565
    store i32 %2566, i32* %1
    br %2552

2577:
    %2580 = load i32, i32* %2
    %2581 = icmp lt i32 %2580,i32 16
    br i32 %2581 %2578 %2579

2578:
    %2584 = load i32, i32* %3
    %2585 = srem i32 %2584,2
    %2586 = load i32, i32* %4
    %2587 = srem i32 %2586,2
    %2588 = and i32 %2585,%2587
    br i32 %2588 %2583 %2582

2579:
    %2607 = load i32, i32* %1
    store i32 %2607, i32* %2518
    %2611 = icmp gt i32 1,i32 15
    br i32 %2611 %2609 %2610

2582:
    %2600 = load i32, i32* %3
    %2601 = sdiv i32 %2600,2
    store i32 %2601, i32* %3
    %2602 = load i32, i32* %4
    %2603 = sdiv i32 %2602,2
    store i32 %2603, i32* %4
    %2604 = load i32, i32* %2
    %2605 = load i32, i32* %2
    %2606 = add i32 %2605,1
    store i32 %2606, i32* %2
    br %2577

2583:
    %2589 = load i32, i32* %1
    %2590 = load i32, i32* %2
    %2591 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2590
    %2592 = load i32, i32* %2591
    %2593 = mul i32 1,%2592
    %2594 = load i32, i32* %1
    %2595 = load i32, i32* %2
    %2596 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2595
    %2597 = load i32, i32* %2596
    %2598 = mul i32 1,%2597
    %2599 = add i32 %2594,%2598
    store i32 %2599, i32* %1
    br %2582

2608:
    %2646 = load i32, i32* %1
    store i32 %2646, i32* %2518
    %2647 = load i32, i32* %2519
    store i32 %2647, i32* %2516
    br %2520

2609:
    store i32 0, i32* %1
    br %2608

2610:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %2612 = load i32, i32* %2518
    %2613 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 1
    %2614 = load i32, i32* %2613
    %2615 = mul i32 %2612,%2614
    store i32 %2615, i32* %3
    store i32 65535, i32* %4
    br %2616

2616:
    %2619 = load i32, i32* %2
    %2620 = icmp lt i32 %2619,i32 16
    br i32 %2620 %2617 %2618

2617:
    %2623 = load i32, i32* %3
    %2624 = srem i32 %2623,2
    %2625 = load i32, i32* %4
    %2626 = srem i32 %2625,2
    %2627 = and i32 %2624,%2626
    br i32 %2627 %2622 %2621

2618:

2621:
    %2639 = load i32, i32* %3
    %2640 = sdiv i32 %2639,2
    store i32 %2640, i32* %3
    %2641 = load i32, i32* %4
    %2642 = sdiv i32 %2641,2
    store i32 %2642, i32* %4
    %2643 = load i32, i32* %2
    %2644 = load i32, i32* %2
    %2645 = add i32 %2644,1
    store i32 %2645, i32* %2
    br %2616

2622:
    %2628 = load i32, i32* %1
    %2629 = load i32, i32* %2
    %2630 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2629
    %2631 = load i32, i32* %2630
    %2632 = mul i32 1,%2631
    %2633 = load i32, i32* %1
    %2634 = load i32, i32* %2
    %2635 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2634
    %2636 = load i32, i32* %2635
    %2637 = mul i32 1,%2636
    %2638 = add i32 %2633,%2637
    store i32 %2638, i32* %1
    br %2621

2655:
    %2658 = load i32, i32* %2653
    br i32 %2658 %2656 %2657

2656:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %2659 = load i32, i32* %2651
    store i32 %2659, i32* %3
    %2660 = load i32, i32* %2653
    store i32 %2660, i32* %4
    br %2661

2657:
    %2783 = load i32, i32* %2651
    store i32 %2783, i32* %1
    %2784 = load i32, i32* %1
    store i32 %2784, i32* %2473
    %2785 = load i32, i32* %2475
    store i32 %2785, i32* %3
    store i32 1, i32* %4
    %2789 = load i32, i32* %4
    %2790 = icmp ge i32 %2789,i32 15
    br i32 %2790 %2787 %2788

2661:
    %2664 = load i32, i32* %2
    %2665 = icmp lt i32 %2664,i32 16
    br i32 %2665 %2662 %2663

2662:
    %2669 = load i32, i32* %3
    %2670 = srem i32 %2669,2
    br i32 %2670 %2667 %2668

2663:
    %2709 = load i32, i32* %1
    store i32 %2709, i32* %2654
    store i32 0, i32* %1
    store i32 0, i32* %2
    %2710 = load i32, i32* %2651
    store i32 %2710, i32* %3
    %2711 = load i32, i32* %2653
    store i32 %2711, i32* %4
    br %2712

2666:
    %2702 = load i32, i32* %3
    %2703 = sdiv i32 %2702,2
    store i32 %2703, i32* %3
    %2704 = load i32, i32* %4
    %2705 = sdiv i32 %2704,2
    store i32 %2705, i32* %4
    %2706 = load i32, i32* %2
    %2707 = load i32, i32* %2
    %2708 = add i32 %2707,1
    store i32 %2708, i32* %2
    br %2661

2667:
    %2673 = load i32, i32* %4
    %2674 = srem i32 %2673,2
    %2675 = icmp eq i32 %2674,i32 0
    br i32 %2675 %2672 %2671

2668:
    %2689 = load i32, i32* %4
    %2690 = srem i32 %2689,2
    br i32 %2690 %2688 %2687

2671:

2672:
    %2676 = load i32, i32* %1
    %2677 = load i32, i32* %2
    %2678 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2677
    %2679 = load i32, i32* %2678
    %2680 = mul i32 1,%2679
    %2681 = load i32, i32* %1
    %2682 = load i32, i32* %2
    %2683 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2682
    %2684 = load i32, i32* %2683
    %2685 = mul i32 1,%2684
    %2686 = add i32 %2681,%2685
    store i32 %2686, i32* %1
    br %2671

2687:

2688:
    %2691 = load i32, i32* %1
    %2692 = load i32, i32* %2
    %2693 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2692
    %2694 = load i32, i32* %2693
    %2695 = mul i32 1,%2694
    %2696 = load i32, i32* %1
    %2697 = load i32, i32* %2
    %2698 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2697
    %2699 = load i32, i32* %2698
    %2700 = mul i32 1,%2699
    %2701 = add i32 %2696,%2700
    store i32 %2701, i32* %1
    br %2687

2712:
    %2715 = load i32, i32* %2
    %2716 = icmp lt i32 %2715,i32 16
    br i32 %2716 %2713 %2714

2713:
    %2719 = load i32, i32* %3
    %2720 = srem i32 %2719,2
    %2721 = load i32, i32* %4
    %2722 = srem i32 %2721,2
    %2723 = and i32 %2720,%2722
    br i32 %2723 %2718 %2717

2714:
    %2742 = load i32, i32* %1
    store i32 %2742, i32* %2653
    %2746 = icmp gt i32 1,i32 15
    br i32 %2746 %2744 %2745

2717:
    %2735 = load i32, i32* %3
    %2736 = sdiv i32 %2735,2
    store i32 %2736, i32* %3
    %2737 = load i32, i32* %4
    %2738 = sdiv i32 %2737,2
    store i32 %2738, i32* %4
    %2739 = load i32, i32* %2
    %2740 = load i32, i32* %2
    %2741 = add i32 %2740,1
    store i32 %2741, i32* %2
    br %2712

2718:
    %2724 = load i32, i32* %1
    %2725 = load i32, i32* %2
    %2726 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2725
    %2727 = load i32, i32* %2726
    %2728 = mul i32 1,%2727
    %2729 = load i32, i32* %1
    %2730 = load i32, i32* %2
    %2731 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2730
    %2732 = load i32, i32* %2731
    %2733 = mul i32 1,%2732
    %2734 = add i32 %2729,%2733
    store i32 %2734, i32* %1
    br %2717

2743:
    %2781 = load i32, i32* %1
    store i32 %2781, i32* %2653
    %2782 = load i32, i32* %2654
    store i32 %2782, i32* %2651
    br %2655

2744:
    store i32 0, i32* %1
    br %2743

2745:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %2747 = load i32, i32* %2653
    %2748 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 1
    %2749 = load i32, i32* %2748
    %2750 = mul i32 %2747,%2749
    store i32 %2750, i32* %3
    store i32 65535, i32* %4
    br %2751

2751:
    %2754 = load i32, i32* %2
    %2755 = icmp lt i32 %2754,i32 16
    br i32 %2755 %2752 %2753

2752:
    %2758 = load i32, i32* %3
    %2759 = srem i32 %2758,2
    %2760 = load i32, i32* %4
    %2761 = srem i32 %2760,2
    %2762 = and i32 %2759,%2761
    br i32 %2762 %2757 %2756

2753:

2756:
    %2774 = load i32, i32* %3
    %2775 = sdiv i32 %2774,2
    store i32 %2775, i32* %3
    %2776 = load i32, i32* %4
    %2777 = sdiv i32 %2776,2
    store i32 %2777, i32* %4
    %2778 = load i32, i32* %2
    %2779 = load i32, i32* %2
    %2780 = add i32 %2779,1
    store i32 %2780, i32* %2
    br %2751

2757:
    %2763 = load i32, i32* %1
    %2764 = load i32, i32* %2
    %2765 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2764
    %2766 = load i32, i32* %2765
    %2767 = mul i32 1,%2766
    %2768 = load i32, i32* %1
    %2769 = load i32, i32* %2
    %2770 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2769
    %2771 = load i32, i32* %2770
    %2772 = mul i32 1,%2771
    %2773 = add i32 %2768,%2772
    store i32 %2773, i32* %1
    br %2756

2786:
    %2827 = load i32, i32* %1
    store i32 %2827, i32* %2475
    br %2477

2787:
    %2794 = load i32, i32* %3
    %2795 = icmp lt i32 %2794,i32 0
    br i32 %2795 %2792 %2793

2788:
    %2799 = load i32, i32* %4
    %2800 = icmp gt i32 %2799,i32 0
    br i32 %2800 %2797 %2798

2791:

2792:
    store i32 65535, i32* %1
    br %2791

2793:
    store i32 0, i32* %1
    br %2791

2796:

2797:
    %2804 = load i32, i32* %3
    %2805 = icmp gt i32 %2804,i32 32767
    br i32 %2805 %2802 %2803

2798:
    %2826 = load i32, i32* %3
    store i32 %2826, i32* %1
    br %2796

2801:

2802:
    %2806 = load i32, i32* %3
    %2807 = load i32, i32* %4
    %2808 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2807
    %2809 = load i32, i32* %2808
    %2810 = sdiv i32 %2806,%2809
    store i32 %2810, i32* %3
    %2811 = load i32, i32* %3
    %2812 = load i32, i32* %3
    %2813 = add i32 %2812,65536
    %2814 = load i32, i32* %4
    %2815 = load i32, i32* %4
    %2816 = sub i32 15,%2815
    %2817 = add i32 %2816,1
    %2818 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2817
    %2819 = load i32, i32* %2818
    %2820 = sub i32 %2813,%2819
    store i32 %2820, i32* %1
    br %2801

2803:
    %2821 = load i32, i32* %3
    %2822 = load i32, i32* %4
    %2823 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2822
    %2824 = load i32, i32* %2823
    %2825 = sdiv i32 %2821,%2824
    store i32 %2825, i32* %1
    br %2801

2835:
    %2838 = load i32, i32* %2833
    br i32 %2838 %2836 %2837

2836:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %2839 = load i32, i32* %2833
    store i32 %2839, i32* %3
    store i32 1, i32* %4
    br %2840

2837:
    %3186 = load i32, i32* %2834
    store i32 %3186, i32* %1
    %3187 = load i32, i32* %1
    store i32 %3187, i32* %2429
    %3188 = load i32, i32* %2431
    store i32 %3188, i32* %3
    store i32 1, i32* %4
    %3192 = load i32, i32* %4
    %3193 = icmp ge i32 %3192,i32 15
    br i32 %3193 %3190 %3191

2840:
    %2843 = load i32, i32* %2
    %2844 = icmp lt i32 %2843,i32 16
    br i32 %2844 %2841 %2842

2841:
    %2847 = load i32, i32* %3
    %2848 = srem i32 %2847,2
    %2849 = load i32, i32* %4
    %2850 = srem i32 %2849,2
    %2851 = and i32 %2848,%2850
    br i32 %2851 %2846 %2845

2842:
    %2872 = load i32, i32* %1
    br i32 %2872 %2871 %2870

2845:
    %2863 = load i32, i32* %3
    %2864 = sdiv i32 %2863,2
    store i32 %2864, i32* %3
    %2865 = load i32, i32* %4
    %2866 = sdiv i32 %2865,2
    store i32 %2866, i32* %4
    %2867 = load i32, i32* %2
    %2868 = load i32, i32* %2
    %2869 = add i32 %2868,1
    store i32 %2869, i32* %2
    br %2840

2846:
    %2852 = load i32, i32* %1
    %2853 = load i32, i32* %2
    %2854 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2853
    %2855 = load i32, i32* %2854
    %2856 = mul i32 1,%2855
    %2857 = load i32, i32* %1
    %2858 = load i32, i32* %2
    %2859 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2858
    %2860 = load i32, i32* %2859
    %2861 = mul i32 1,%2860
    %2862 = add i32 %2857,%2861
    store i32 %2862, i32* %1
    br %2845

2870:
    %3008 = load i32, i32* %2831
    %3009 = alloca i32
    store i32 %3008, i32* %3009
    %3010 = load i32, i32* %2831
    %3011 = alloca i32
    store i32 %3010, i32* %3011
    %3012 = alloca i32
    br %3013

2871:
    %2873 = load i32, i32* %2834
    %2874 = alloca i32
    store i32 %2873, i32* %2874
    %2875 = load i32, i32* %2831
    %2876 = alloca i32
    store i32 %2875, i32* %2876
    %2877 = alloca i32
    br %2878

2878:
    %2881 = load i32, i32* %2876
    br i32 %2881 %2879 %2880

2879:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %2882 = load i32, i32* %2874
    store i32 %2882, i32* %3
    %2883 = load i32, i32* %2876
    store i32 %2883, i32* %4
    br %2884

2880:
    %3006 = load i32, i32* %2874
    store i32 %3006, i32* %1
    %3007 = load i32, i32* %1
    store i32 %3007, i32* %2834

2884:
    %2887 = load i32, i32* %2
    %2888 = icmp lt i32 %2887,i32 16
    br i32 %2888 %2885 %2886

2885:
    %2892 = load i32, i32* %3
    %2893 = srem i32 %2892,2
    br i32 %2893 %2890 %2891

2886:
    %2932 = load i32, i32* %1
    store i32 %2932, i32* %2877
    store i32 0, i32* %1
    store i32 0, i32* %2
    %2933 = load i32, i32* %2874
    store i32 %2933, i32* %3
    %2934 = load i32, i32* %2876
    store i32 %2934, i32* %4
    br %2935

2889:
    %2925 = load i32, i32* %3
    %2926 = sdiv i32 %2925,2
    store i32 %2926, i32* %3
    %2927 = load i32, i32* %4
    %2928 = sdiv i32 %2927,2
    store i32 %2928, i32* %4
    %2929 = load i32, i32* %2
    %2930 = load i32, i32* %2
    %2931 = add i32 %2930,1
    store i32 %2931, i32* %2
    br %2884

2890:
    %2896 = load i32, i32* %4
    %2897 = srem i32 %2896,2
    %2898 = icmp eq i32 %2897,i32 0
    br i32 %2898 %2895 %2894

2891:
    %2912 = load i32, i32* %4
    %2913 = srem i32 %2912,2
    br i32 %2913 %2911 %2910

2894:

2895:
    %2899 = load i32, i32* %1
    %2900 = load i32, i32* %2
    %2901 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2900
    %2902 = load i32, i32* %2901
    %2903 = mul i32 1,%2902
    %2904 = load i32, i32* %1
    %2905 = load i32, i32* %2
    %2906 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2905
    %2907 = load i32, i32* %2906
    %2908 = mul i32 1,%2907
    %2909 = add i32 %2904,%2908
    store i32 %2909, i32* %1
    br %2894

2910:

2911:
    %2914 = load i32, i32* %1
    %2915 = load i32, i32* %2
    %2916 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2915
    %2917 = load i32, i32* %2916
    %2918 = mul i32 1,%2917
    %2919 = load i32, i32* %1
    %2920 = load i32, i32* %2
    %2921 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2920
    %2922 = load i32, i32* %2921
    %2923 = mul i32 1,%2922
    %2924 = add i32 %2919,%2923
    store i32 %2924, i32* %1
    br %2910

2935:
    %2938 = load i32, i32* %2
    %2939 = icmp lt i32 %2938,i32 16
    br i32 %2939 %2936 %2937

2936:
    %2942 = load i32, i32* %3
    %2943 = srem i32 %2942,2
    %2944 = load i32, i32* %4
    %2945 = srem i32 %2944,2
    %2946 = and i32 %2943,%2945
    br i32 %2946 %2941 %2940

2937:
    %2965 = load i32, i32* %1
    store i32 %2965, i32* %2876
    %2969 = icmp gt i32 1,i32 15
    br i32 %2969 %2967 %2968

2940:
    %2958 = load i32, i32* %3
    %2959 = sdiv i32 %2958,2
    store i32 %2959, i32* %3
    %2960 = load i32, i32* %4
    %2961 = sdiv i32 %2960,2
    store i32 %2961, i32* %4
    %2962 = load i32, i32* %2
    %2963 = load i32, i32* %2
    %2964 = add i32 %2963,1
    store i32 %2964, i32* %2
    br %2935

2941:
    %2947 = load i32, i32* %1
    %2948 = load i32, i32* %2
    %2949 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2948
    %2950 = load i32, i32* %2949
    %2951 = mul i32 1,%2950
    %2952 = load i32, i32* %1
    %2953 = load i32, i32* %2
    %2954 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2953
    %2955 = load i32, i32* %2954
    %2956 = mul i32 1,%2955
    %2957 = add i32 %2952,%2956
    store i32 %2957, i32* %1
    br %2940

2966:
    %3004 = load i32, i32* %1
    store i32 %3004, i32* %2876
    %3005 = load i32, i32* %2877
    store i32 %3005, i32* %2874
    br %2878

2967:
    store i32 0, i32* %1
    br %2966

2968:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %2970 = load i32, i32* %2876
    %2971 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 1
    %2972 = load i32, i32* %2971
    %2973 = mul i32 %2970,%2972
    store i32 %2973, i32* %3
    store i32 65535, i32* %4
    br %2974

2974:
    %2977 = load i32, i32* %2
    %2978 = icmp lt i32 %2977,i32 16
    br i32 %2978 %2975 %2976

2975:
    %2981 = load i32, i32* %3
    %2982 = srem i32 %2981,2
    %2983 = load i32, i32* %4
    %2984 = srem i32 %2983,2
    %2985 = and i32 %2982,%2984
    br i32 %2985 %2980 %2979

2976:

2979:
    %2997 = load i32, i32* %3
    %2998 = sdiv i32 %2997,2
    store i32 %2998, i32* %3
    %2999 = load i32, i32* %4
    %3000 = sdiv i32 %2999,2
    store i32 %3000, i32* %4
    %3001 = load i32, i32* %2
    %3002 = load i32, i32* %2
    %3003 = add i32 %3002,1
    store i32 %3003, i32* %2
    br %2974

2980:
    %2986 = load i32, i32* %1
    %2987 = load i32, i32* %2
    %2988 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2987
    %2989 = load i32, i32* %2988
    %2990 = mul i32 1,%2989
    %2991 = load i32, i32* %1
    %2992 = load i32, i32* %2
    %2993 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %2992
    %2994 = load i32, i32* %2993
    %2995 = mul i32 1,%2994
    %2996 = add i32 %2991,%2995
    store i32 %2996, i32* %1
    br %2979

3013:
    %3016 = load i32, i32* %3011
    br i32 %3016 %3014 %3015

3014:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %3017 = load i32, i32* %3009
    store i32 %3017, i32* %3
    %3018 = load i32, i32* %3011
    store i32 %3018, i32* %4
    br %3019

3015:
    %3141 = load i32, i32* %3009
    store i32 %3141, i32* %1
    %3142 = load i32, i32* %1
    store i32 %3142, i32* %2831
    %3143 = load i32, i32* %2833
    store i32 %3143, i32* %3
    store i32 1, i32* %4
    %3147 = load i32, i32* %4
    %3148 = icmp ge i32 %3147,i32 15
    br i32 %3148 %3145 %3146

3019:
    %3022 = load i32, i32* %2
    %3023 = icmp lt i32 %3022,i32 16
    br i32 %3023 %3020 %3021

3020:
    %3027 = load i32, i32* %3
    %3028 = srem i32 %3027,2
    br i32 %3028 %3025 %3026

3021:
    %3067 = load i32, i32* %1
    store i32 %3067, i32* %3012
    store i32 0, i32* %1
    store i32 0, i32* %2
    %3068 = load i32, i32* %3009
    store i32 %3068, i32* %3
    %3069 = load i32, i32* %3011
    store i32 %3069, i32* %4
    br %3070

3024:
    %3060 = load i32, i32* %3
    %3061 = sdiv i32 %3060,2
    store i32 %3061, i32* %3
    %3062 = load i32, i32* %4
    %3063 = sdiv i32 %3062,2
    store i32 %3063, i32* %4
    %3064 = load i32, i32* %2
    %3065 = load i32, i32* %2
    %3066 = add i32 %3065,1
    store i32 %3066, i32* %2
    br %3019

3025:
    %3031 = load i32, i32* %4
    %3032 = srem i32 %3031,2
    %3033 = icmp eq i32 %3032,i32 0
    br i32 %3033 %3030 %3029

3026:
    %3047 = load i32, i32* %4
    %3048 = srem i32 %3047,2
    br i32 %3048 %3046 %3045

3029:

3030:
    %3034 = load i32, i32* %1
    %3035 = load i32, i32* %2
    %3036 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %3035
    %3037 = load i32, i32* %3036
    %3038 = mul i32 1,%3037
    %3039 = load i32, i32* %1
    %3040 = load i32, i32* %2
    %3041 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %3040
    %3042 = load i32, i32* %3041
    %3043 = mul i32 1,%3042
    %3044 = add i32 %3039,%3043
    store i32 %3044, i32* %1
    br %3029

3045:

3046:
    %3049 = load i32, i32* %1
    %3050 = load i32, i32* %2
    %3051 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %3050
    %3052 = load i32, i32* %3051
    %3053 = mul i32 1,%3052
    %3054 = load i32, i32* %1
    %3055 = load i32, i32* %2
    %3056 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %3055
    %3057 = load i32, i32* %3056
    %3058 = mul i32 1,%3057
    %3059 = add i32 %3054,%3058
    store i32 %3059, i32* %1
    br %3045

3070:
    %3073 = load i32, i32* %2
    %3074 = icmp lt i32 %3073,i32 16
    br i32 %3074 %3071 %3072

3071:
    %3077 = load i32, i32* %3
    %3078 = srem i32 %3077,2
    %3079 = load i32, i32* %4
    %3080 = srem i32 %3079,2
    %3081 = and i32 %3078,%3080
    br i32 %3081 %3076 %3075

3072:
    %3100 = load i32, i32* %1
    store i32 %3100, i32* %3011
    %3104 = icmp gt i32 1,i32 15
    br i32 %3104 %3102 %3103

3075:
    %3093 = load i32, i32* %3
    %3094 = sdiv i32 %3093,2
    store i32 %3094, i32* %3
    %3095 = load i32, i32* %4
    %3096 = sdiv i32 %3095,2
    store i32 %3096, i32* %4
    %3097 = load i32, i32* %2
    %3098 = load i32, i32* %2
    %3099 = add i32 %3098,1
    store i32 %3099, i32* %2
    br %3070

3076:
    %3082 = load i32, i32* %1
    %3083 = load i32, i32* %2
    %3084 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %3083
    %3085 = load i32, i32* %3084
    %3086 = mul i32 1,%3085
    %3087 = load i32, i32* %1
    %3088 = load i32, i32* %2
    %3089 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %3088
    %3090 = load i32, i32* %3089
    %3091 = mul i32 1,%3090
    %3092 = add i32 %3087,%3091
    store i32 %3092, i32* %1
    br %3075

3101:
    %3139 = load i32, i32* %1
    store i32 %3139, i32* %3011
    %3140 = load i32, i32* %3012
    store i32 %3140, i32* %3009
    br %3013

3102:
    store i32 0, i32* %1
    br %3101

3103:
    store i32 0, i32* %1
    store i32 0, i32* %2
    %3105 = load i32, i32* %3011
    %3106 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 1
    %3107 = load i32, i32* %3106
    %3108 = mul i32 %3105,%3107
    store i32 %3108, i32* %3
    store i32 65535, i32* %4
    br %3109

3109:
    %3112 = load i32, i32* %2
    %3113 = icmp lt i32 %3112,i32 16
    br i32 %3113 %3110 %3111

3110:
    %3116 = load i32, i32* %3
    %3117 = srem i32 %3116,2
    %3118 = load i32, i32* %4
    %3119 = srem i32 %3118,2
    %3120 = and i32 %3117,%3119
    br i32 %3120 %3115 %3114

3111:

3114:
    %3132 = load i32, i32* %3
    %3133 = sdiv i32 %3132,2
    store i32 %3133, i32* %3
    %3134 = load i32, i32* %4
    %3135 = sdiv i32 %3134,2
    store i32 %3135, i32* %4
    %3136 = load i32, i32* %2
    %3137 = load i32, i32* %2
    %3138 = add i32 %3137,1
    store i32 %3138, i32* %2
    br %3109

3115:
    %3121 = load i32, i32* %1
    %3122 = load i32, i32* %2
    %3123 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %3122
    %3124 = load i32, i32* %3123
    %3125 = mul i32 1,%3124
    %3126 = load i32, i32* %1
    %3127 = load i32, i32* %2
    %3128 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %3127
    %3129 = load i32, i32* %3128
    %3130 = mul i32 1,%3129
    %3131 = add i32 %3126,%3130
    store i32 %3131, i32* %1
    br %3114

3144:
    %3185 = load i32, i32* %1
    store i32 %3185, i32* %2833
    br %2835

3145:
    %3152 = load i32, i32* %3
    %3153 = icmp lt i32 %3152,i32 0
    br i32 %3153 %3150 %3151

3146:
    %3157 = load i32, i32* %4
    %3158 = icmp gt i32 %3157,i32 0
    br i32 %3158 %3155 %3156

3149:

3150:
    store i32 65535, i32* %1
    br %3149

3151:
    store i32 0, i32* %1
    br %3149

3154:

3155:
    %3162 = load i32, i32* %3
    %3163 = icmp gt i32 %3162,i32 32767
    br i32 %3163 %3160 %3161

3156:
    %3184 = load i32, i32* %3
    store i32 %3184, i32* %1
    br %3154

3159:

3160:
    %3164 = load i32, i32* %3
    %3165 = load i32, i32* %4
    %3166 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %3165
    %3167 = load i32, i32* %3166
    %3168 = sdiv i32 %3164,%3167
    store i32 %3168, i32* %3
    %3169 = load i32, i32* %3
    %3170 = load i32, i32* %3
    %3171 = add i32 %3170,65536
    %3172 = load i32, i32* %4
    %3173 = load i32, i32* %4
    %3174 = sub i32 15,%3173
    %3175 = add i32 %3174,1
    %3176 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %3175
    %3177 = load i32, i32* %3176
    %3178 = sub i32 %3171,%3177
    store i32 %3178, i32* %1
    br %3159

3161:
    %3179 = load i32, i32* %3
    %3180 = load i32, i32* %4
    %3181 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %3180
    %3182 = load i32, i32* %3181
    %3183 = sdiv i32 %3179,%3182
    store i32 %3183, i32* %1
    br %3159

3189:
    %3230 = load i32, i32* %1
    store i32 %3230, i32* %2431
    br %2433

3190:
    %3197 = load i32, i32* %3
    %3198 = icmp lt i32 %3197,i32 0
    br i32 %3198 %3195 %3196

3191:
    %3202 = load i32, i32* %4
    %3203 = icmp gt i32 %3202,i32 0
    br i32 %3203 %3200 %3201

3194:

3195:
    store i32 65535, i32* %1
    br %3194

3196:
    store i32 0, i32* %1
    br %3194

3199:

3200:
    %3207 = load i32, i32* %3
    %3208 = icmp gt i32 %3207,i32 32767
    br i32 %3208 %3205 %3206

3201:
    %3229 = load i32, i32* %3
    store i32 %3229, i32* %1
    br %3199

3204:

3205:
    %3209 = load i32, i32* %3
    %3210 = load i32, i32* %4
    %3211 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %3210
    %3212 = load i32, i32* %3211
    %3213 = sdiv i32 %3209,%3212
    store i32 %3213, i32* %3
    %3214 = load i32, i32* %3
    %3215 = load i32, i32* %3
    %3216 = add i32 %3215,65536
    %3217 = load i32, i32* %4
    %3218 = load i32, i32* %4
    %3219 = sub i32 15,%3218
    %3220 = add i32 %3219,1
    %3221 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %3220
    %3222 = load i32, i32* %3221
    %3223 = sub i32 %3216,%3222
    store i32 %3223, i32* %1
    br %3204

3206:
    %3224 = load i32, i32* %3
    %3225 = load i32, i32* %4
    %3226 = getelementptr [16 x i32], [16 x i32]* @SHIFT_TABLE, i32 %3225
    %3227 = load i32, i32* %3226
    %3228 = sdiv i32 %3224,%3227
    store i32 %3228, i32* %1
    br %3204

3232:
    %3239 = load i32, i32* %5
    %3240 = load i32, i32* %5
    %3241 = add i32 %3240,1
    store i32 %3241, i32* %5
    br %2424

3233:
    ret i32 1
}
define dso_local i32 @main(){
0:
    %1 = call i32 @long_func()
    ret i32 %1
}

