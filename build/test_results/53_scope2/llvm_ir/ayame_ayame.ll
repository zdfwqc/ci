@k = dso_local global i32 0

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @main(){
0:
    store i32 3389, i32* @k
    %3 = load i32, i32* @k
    %4 = icmp lt i32 %3,i32 10000
    br i32 %4 %2 %1

1:
    %37 = load i32, i32* @k
    ret i32 %37

2:
    %5 = load i32, i32* @k
    %6 = load i32, i32* @k
    %7 = add i32 %6,1
    store i32 %7, i32* @k
    %8 = alloca i32
    store i32 112, i32* %8
    br %9

9:
    %12 = load i32, i32* %8
    %13 = icmp gt i32 %12,i32 10
    br i32 %13 %10 %11

10:
    %14 = load i32, i32* %8
    %15 = load i32, i32* %8
    %16 = sub i32 %15,88
    store i32 %16, i32* %8
    %19 = load i32, i32* %8
    %20 = icmp lt i32 %19,i32 1000
    br i32 %20 %18 %17

11:
    %36 = load i32, i32* %8
    call void @putint(i32 %36)

17:
    br %9

18:
    %21 = alloca i32
    store i32 9, i32* %21
    %22 = alloca i32
    store i32 11, i32* %22
    store i32 10, i32* %21
    %23 = load i32, i32* %8
    %24 = load i32, i32* %21
    %25 = load i32, i32* %8
    %26 = load i32, i32* %21
    %27 = sub i32 %25,%26
    store i32 %27, i32* %8
    %28 = alloca i32
    store i32 11, i32* %28
    %29 = load i32, i32* %8
    %30 = load i32, i32* %28
    %31 = load i32, i32* %8
    %32 = load i32, i32* %28
    %33 = add i32 %31,%32
    %34 = load i32, i32* %22
    %35 = add i32 %33,%34
    store i32 %35, i32* %8
    br %17
}

