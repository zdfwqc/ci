@len = dso_local constant i32 20

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @main(){
0:
    %1 = alloca i32
    %2 = alloca i32
    %3 = alloca i32
    %4 = alloca i32
    %5 = alloca i32
    %6 = alloca [20 x i32]
    %7 = getelementptr [20 x i32], [20 x i32]* %6, i32 0
    store i32 1, i32* %7
    %8 = getelementptr [20 x i32], [20 x i32]* %6, i32 1
    store i32 2, i32* %8
    %9 = getelementptr [20 x i32], [20 x i32]* %6, i32 2
    store i32 3, i32* %9
    %10 = getelementptr [20 x i32], [20 x i32]* %6, i32 3
    store i32 4, i32* %10
    %11 = getelementptr [20 x i32], [20 x i32]* %6, i32 4
    store i32 5, i32* %11
    %12 = getelementptr [20 x i32], [20 x i32]* %6, i32 5
    store i32 6, i32* %12
    %13 = getelementptr [20 x i32], [20 x i32]* %6, i32 6
    store i32 7, i32* %13
    %14 = getelementptr [20 x i32], [20 x i32]* %6, i32 7
    store i32 8, i32* %14
    %15 = getelementptr [20 x i32], [20 x i32]* %6, i32 8
    store i32 9, i32* %15
    %16 = getelementptr [20 x i32], [20 x i32]* %6, i32 9
    store i32 0, i32* %16
    %17 = getelementptr [20 x i32], [20 x i32]* %6, i32 10
    store i32 1, i32* %17
    %18 = getelementptr [20 x i32], [20 x i32]* %6, i32 11
    store i32 2, i32* %18
    %19 = getelementptr [20 x i32], [20 x i32]* %6, i32 12
    store i32 3, i32* %19
    %20 = getelementptr [20 x i32], [20 x i32]* %6, i32 13
    store i32 4, i32* %20
    %21 = getelementptr [20 x i32], [20 x i32]* %6, i32 14
    store i32 5, i32* %21
    %22 = getelementptr [20 x i32], [20 x i32]* %6, i32 15
    store i32 6, i32* %22
    %23 = getelementptr [20 x i32], [20 x i32]* %6, i32 16
    store i32 7, i32* %23
    %24 = getelementptr [20 x i32], [20 x i32]* %6, i32 17
    store i32 8, i32* %24
    %25 = getelementptr [20 x i32], [20 x i32]* %6, i32 18
    store i32 9, i32* %25
    %26 = getelementptr [20 x i32], [20 x i32]* %6, i32 19
    store i32 0, i32* %26
    %27 = alloca [20 x i32]
    %28 = getelementptr [20 x i32], [20 x i32]* %27, i32 0
    store i32 2, i32* %28
    %29 = getelementptr [20 x i32], [20 x i32]* %27, i32 1
    store i32 3, i32* %29
    %30 = getelementptr [20 x i32], [20 x i32]* %27, i32 2
    store i32 4, i32* %30
    %31 = getelementptr [20 x i32], [20 x i32]* %27, i32 3
    store i32 2, i32* %31
    %32 = getelementptr [20 x i32], [20 x i32]* %27, i32 4
    store i32 5, i32* %32
    %33 = getelementptr [20 x i32], [20 x i32]* %27, i32 5
    store i32 7, i32* %33
    %34 = getelementptr [20 x i32], [20 x i32]* %27, i32 6
    store i32 9, i32* %34
    %35 = getelementptr [20 x i32], [20 x i32]* %27, i32 7
    store i32 9, i32* %35
    %36 = getelementptr [20 x i32], [20 x i32]* %27, i32 8
    store i32 0, i32* %36
    %37 = getelementptr [20 x i32], [20 x i32]* %27, i32 9
    store i32 1, i32* %37
    %38 = getelementptr [20 x i32], [20 x i32]* %27, i32 10
    store i32 9, i32* %38
    %39 = getelementptr [20 x i32], [20 x i32]* %27, i32 11
    store i32 8, i32* %39
    %40 = getelementptr [20 x i32], [20 x i32]* %27, i32 12
    store i32 7, i32* %40
    %41 = getelementptr [20 x i32], [20 x i32]* %27, i32 13
    store i32 6, i32* %41
    %42 = getelementptr [20 x i32], [20 x i32]* %27, i32 14
    store i32 4, i32* %42
    %43 = getelementptr [20 x i32], [20 x i32]* %27, i32 15
    store i32 3, i32* %43
    %44 = getelementptr [20 x i32], [20 x i32]* %27, i32 16
    store i32 2, i32* %44
    %45 = getelementptr [20 x i32], [20 x i32]* %27, i32 17
    store i32 1, i32* %45
    %46 = getelementptr [20 x i32], [20 x i32]* %27, i32 18
    store i32 2, i32* %46
    %47 = getelementptr [20 x i32], [20 x i32]* %27, i32 19
    store i32 2, i32* %47
    %48 = load i32, i32* @len
    %49 = alloca i32
    store i32 %48, i32* %49
    %50 = load i32, i32* @len
    %51 = alloca i32
    store i32 %50, i32* %51
    %52 = alloca [25 x i32]
    %53 = alloca [25 x i32]
    %54 = alloca [40 x i32]
    store i32 0, i32* %1
    br %55

55:
    %58 = load i32, i32* %1
    %59 = load i32, i32* %49
    %60 = icmp lt i32 %58,i32 %59
    br i32 %60 %56 %57

56:
    %61 = load i32, i32* %1
    %62 = getelementptr [25 x i32], [25 x i32]* %52, i32 %61
    %63 = load i32, i32* %1
    %64 = getelementptr [20 x i32], [20 x i32]* %6, i32 %63
    %65 = load i32, i32* %64
    store i32 %65, i32* %62
    %66 = load i32, i32* %1
    %67 = load i32, i32* %1
    %68 = add i32 %67,1
    store i32 %68, i32* %1
    br %55

57:
    store i32 0, i32* %1
    br %69

69:
    %72 = load i32, i32* %1
    %73 = load i32, i32* %51
    %74 = icmp lt i32 %72,i32 %73
    br i32 %74 %70 %71

70:
    %75 = load i32, i32* %1
    %76 = getelementptr [25 x i32], [25 x i32]* %53, i32 %75
    %77 = load i32, i32* %1
    %78 = getelementptr [20 x i32], [20 x i32]* %27, i32 %77
    %79 = load i32, i32* %78
    store i32 %79, i32* %76
    %80 = load i32, i32* %1
    %81 = load i32, i32* %1
    %82 = add i32 %81,1
    store i32 %82, i32* %1
    br %69

71:
    %83 = load i32, i32* %49
    %84 = load i32, i32* %51
    %85 = load i32, i32* %49
    %86 = load i32, i32* %51
    %87 = add i32 %85,%86
    %88 = sub i32 %87,1
    store i32 %88, i32* %4
    store i32 0, i32* %1
    br %89

89:
    %92 = load i32, i32* %1
    %93 = load i32, i32* %4
    %94 = icmp le i32 %92,i32 %93
    br i32 %94 %90 %91

90:
    %95 = load i32, i32* %1
    %96 = getelementptr [40 x i32], [40 x i32]* %54, i32 %95
    store i32 0, i32* %96
    %97 = load i32, i32* %1
    %98 = load i32, i32* %1
    %99 = add i32 %98,1
    store i32 %99, i32* %1
    br %89

91:
    store i32 0, i32* %5
    %100 = load i32, i32* %51
    %101 = load i32, i32* %51
    %102 = sub i32 %101,1
    store i32 %102, i32* %1
    br %103

103:
    %106 = load i32, i32* %1
    %107 = sub i32 0,1
    %108 = icmp gt i32 %106,i32 %107
    br i32 %108 %104 %105

104:
    %109 = load i32, i32* %1
    %110 = getelementptr [25 x i32], [25 x i32]* %53, i32 %109
    %111 = load i32, i32* %110
    store i32 %111, i32* %3
    %112 = load i32, i32* %49
    %113 = load i32, i32* %49
    %114 = sub i32 %113,1
    store i32 %114, i32* %2
    br %115

105:
    %185 = getelementptr [40 x i32], [40 x i32]* %54, i32 0
    %186 = load i32, i32* %185
    %187 = icmp ne i32 %186,i32 0
    br i32 %187 %184 %183

115:
    %118 = load i32, i32* %2
    %119 = sub i32 0,1
    %120 = icmp gt i32 %118,i32 %119
    br i32 %120 %116 %117

116:
    %121 = load i32, i32* %4
    %122 = getelementptr [40 x i32], [40 x i32]* %54, i32 %121
    %123 = load i32, i32* %122
    %124 = load i32, i32* %3
    %125 = load i32, i32* %2
    %126 = getelementptr [25 x i32], [25 x i32]* %52, i32 %125
    %127 = load i32, i32* %126
    %128 = mul i32 %124,%127
    %129 = load i32, i32* %4
    %130 = getelementptr [40 x i32], [40 x i32]* %54, i32 %129
    %131 = load i32, i32* %130
    %132 = load i32, i32* %3
    %133 = load i32, i32* %2
    %134 = getelementptr [25 x i32], [25 x i32]* %52, i32 %133
    %135 = load i32, i32* %134
    %136 = mul i32 %132,%135
    %137 = add i32 %131,%136
    store i32 %137, i32* %5
    %141 = load i32, i32* %5
    %142 = icmp ge i32 %141,i32 10
    br i32 %142 %139 %140

117:
    %174 = load i32, i32* %4
    %175 = load i32, i32* %49
    %176 = load i32, i32* %4
    %177 = load i32, i32* %49
    %178 = add i32 %176,%177
    %179 = sub i32 %178,1
    store i32 %179, i32* %4
    %180 = load i32, i32* %1
    %181 = load i32, i32* %1
    %182 = sub i32 %181,1
    store i32 %182, i32* %1
    br %103

138:
    %168 = load i32, i32* %2
    %169 = load i32, i32* %2
    %170 = sub i32 %169,1
    store i32 %170, i32* %2
    %171 = load i32, i32* %4
    %172 = load i32, i32* %4
    %173 = sub i32 %172,1
    store i32 %173, i32* %4
    br %115

139:
    %143 = load i32, i32* %4
    %144 = getelementptr [40 x i32], [40 x i32]* %54, i32 %143
    %145 = load i32, i32* %5
    store i32 %145, i32* %144
    %146 = load i32, i32* %4
    %147 = load i32, i32* %4
    %148 = sub i32 %147,1
    %149 = getelementptr [40 x i32], [40 x i32]* %54, i32 %148
    %150 = load i32, i32* %4
    %151 = load i32, i32* %4
    %152 = sub i32 %151,1
    %153 = getelementptr [40 x i32], [40 x i32]* %54, i32 %152
    %154 = load i32, i32* %153
    %155 = load i32, i32* %5
    %156 = sdiv i32 %155,10
    %157 = load i32, i32* %4
    %158 = load i32, i32* %4
    %159 = sub i32 %158,1
    %160 = getelementptr [40 x i32], [40 x i32]* %54, i32 %159
    %161 = load i32, i32* %160
    %162 = load i32, i32* %5
    %163 = sdiv i32 %162,10
    %164 = add i32 %161,%163
    store i32 %164, i32* %149
    br %138

140:
    %165 = load i32, i32* %4
    %166 = getelementptr [40 x i32], [40 x i32]* %54, i32 %165
    %167 = load i32, i32* %5
    store i32 %167, i32* %166
    br %138

183:
    store i32 1, i32* %1
    br %190

184:
    %188 = getelementptr [40 x i32], [40 x i32]* %54, i32 0
    %189 = load i32, i32* %188
    call void @putint(i32 %189)
    br %183

190:
    %193 = load i32, i32* %1
    %194 = load i32, i32* %49
    %195 = load i32, i32* %51
    %196 = load i32, i32* %49
    %197 = load i32, i32* %51
    %198 = add i32 %196,%197
    %199 = sub i32 %198,1
    %200 = icmp le i32 %193,i32 %199
    br i32 %200 %191 %192

191:
    %201 = load i32, i32* %1
    %202 = getelementptr [40 x i32], [40 x i32]* %54, i32 %201
    %203 = load i32, i32* %202
    call void @putint(i32 %203)
    %204 = load i32, i32* %1
    %205 = load i32, i32* %1
    %206 = add i32 %205,1
    store i32 %206, i32* %1
    br %190

192:
    ret i32 0
}

