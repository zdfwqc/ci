@n = dso_local global i32 0

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @swap(i32* %0, i32 %1, i32 %2){
3:
    %4 = alloca i32*
    store i32* %0, i32** %4
    %5 = alloca i32
    store i32 %1, i32* %5
    %6 = alloca i32
    store i32 %2, i32* %6
    %7 = alloca i32
    %8 = load i32, i32* %5
    %9 = getelementptr i32*, i32** %4, i32 %8
    %10 = load i32*, i32** %9
    store i32* %10, i32* %7
    %11 = load i32, i32* %5
    %12 = getelementptr i32*, i32** %4, i32 %11
    %13 = load i32, i32* %6
    %14 = getelementptr i32*, i32** %4, i32 %13
    %15 = load i32*, i32** %14
    store i32* %15, i32** %12
    %16 = load i32, i32* %6
    %17 = getelementptr i32*, i32** %4, i32 %16
    %18 = load i32, i32* %7
    store i32 %18, i32** %17
    ret i32 0
}
define dso_local i32 @heap_ajust(i32* %0, i32 %1, i32 %2){
3:
    %4 = alloca i32*
    store i32* %0, i32** %4
    %5 = alloca i32
    store i32 %1, i32* %5
    %6 = alloca i32
    store i32 %2, i32* %6
    %7 = alloca i32
    %8 = load i32, i32* %5
    store i32 %8, i32* %7
    %9 = alloca i32
    %10 = load i32, i32* %7
    %11 = mul i32 %10,2
    %12 = load i32, i32* %7
    %13 = mul i32 %12,2
    %14 = add i32 %13,1
    store i32 %14, i32* %9
    br %15

15:
    %18 = load i32, i32* %9
    %19 = load i32, i32* %6
    %20 = load i32, i32* %6
    %21 = add i32 %20,1
    %22 = icmp lt i32 %18,i32 %21
    br i32 %22 %16 %17

16:
    %25 = load i32, i32* %9
    %26 = load i32, i32* %6
    %27 = icmp lt i32 %25,i32 %26
    %28 = load i32, i32* %9
    %29 = getelementptr i32*, i32** %4, i32 %28
    %30 = load i32*, i32** %29
    %31 = load i32, i32* %9
    %32 = load i32, i32* %9
    %33 = add i32 %32,1
    %34 = getelementptr i32*, i32** %4, i32 %33
    %35 = load i32*, i32** %34
    %36 = icmp lt i32* %30,i32* %35
    %37 = and i32 %27,%36
    br i32 %37 %24 %23

17:
    ret i32 0

23:
    %44 = load i32, i32* %7
    %45 = getelementptr i32*, i32** %4, i32 %44
    %46 = load i32*, i32** %45
    %47 = load i32, i32* %9
    %48 = getelementptr i32*, i32** %4, i32 %47
    %49 = load i32*, i32** %48
    %50 = icmp gt i32* %46,i32* %49
    br i32* %50 %42 %43

24:
    %38 = load i32, i32* %9
    %39 = load i32, i32* %9
    %40 = add i32 %39,1
    store i32 %40, i32* %9
    br %23

41:
    br %15

42:
    ret i32 0

43:
    %51 = load i32*, i32** %4
    %52 = load i32, i32* %7
    %53 = load i32, i32* %9
    %54 = call i32 @swap(i32* %51,i32 %52,i32 %53)
    store i32 %54, i32* %7
    %55 = load i32, i32* %9
    store i32 %55, i32* %7
    %56 = load i32, i32* %7
    %57 = mul i32 %56,2
    %58 = load i32, i32* %7
    %59 = mul i32 %58,2
    %60 = add i32 %59,1
    store i32 %60, i32* %9
    br %41
}
define dso_local i32 @heap_sort(i32* %0, i32 %1){
2:
    %3 = alloca i32*
    store i32* %0, i32** %3
    %4 = alloca i32
    store i32 %1, i32* %4
    %5 = alloca i32
    %6 = alloca i32
    %7 = load i32, i32* %4
    %8 = sdiv i32 %7,2
    %9 = load i32, i32* %4
    %10 = sdiv i32 %9,2
    %11 = sub i32 %10,1
    store i32 %11, i32* %5
    br %12

12:
    %15 = load i32, i32* %5
    %16 = sub i32 0,1
    %17 = icmp gt i32 %15,i32 %16
    br i32 %17 %13 %14

13:
    %18 = load i32, i32* %4
    %19 = load i32, i32* %4
    %20 = sub i32 %19,1
    store i32 %20, i32* %6
    %21 = load i32*, i32** %3
    %22 = load i32, i32* %5
    %23 = load i32, i32* %6
    %24 = call i32 @heap_ajust(i32* %21,i32 %22,i32 %23)
    store i32 %24, i32* %6
    %25 = load i32, i32* %5
    %26 = load i32, i32* %5
    %27 = sub i32 %26,1
    store i32 %27, i32* %5
    br %12

14:
    %28 = load i32, i32* %4
    %29 = load i32, i32* %4
    %30 = sub i32 %29,1
    store i32 %30, i32* %5
    br %31

31:
    %34 = load i32, i32* %5
    %35 = icmp gt i32 %34,i32 0
    br i32 %35 %32 %33

32:
    %36 = alloca i32
    store i32 0, i32* %36
    %37 = load i32*, i32** %3
    %38 = load i32, i32* %36
    %39 = load i32, i32* %5
    %40 = call i32 @swap(i32* %37,i32 %38,i32 %39)
    store i32 %40, i32* %6
    %41 = load i32, i32* %5
    %42 = load i32, i32* %5
    %43 = sub i32 %42,1
    store i32 %43, i32* %6
    %44 = load i32*, i32** %3
    %45 = load i32, i32* %36
    %46 = load i32, i32* %6
    %47 = call i32 @heap_ajust(i32* %44,i32 %45,i32 %46)
    store i32 %47, i32* %6
    %48 = load i32, i32* %5
    %49 = load i32, i32* %5
    %50 = sub i32 %49,1
    store i32 %50, i32* %5
    br %31

33:
    ret i32 0
}
define dso_local i32 @main(i32* %0, i32 %1){
2:
    store i32 10, i32* @n
    %3 = alloca [10 x i32]
    %4 = getelementptr [10 x i32], [10 x i32]* %3, i32 0
    store i32 4, i32* %4
    %5 = getelementptr [10 x i32], [10 x i32]* %3, i32 1
    store i32 3, i32* %5
    %6 = getelementptr [10 x i32], [10 x i32]* %3, i32 2
    store i32 9, i32* %6
    %7 = getelementptr [10 x i32], [10 x i32]* %3, i32 3
    store i32 2, i32* %7
    %8 = getelementptr [10 x i32], [10 x i32]* %3, i32 4
    store i32 0, i32* %8
    %9 = getelementptr [10 x i32], [10 x i32]* %3, i32 5
    store i32 1, i32* %9
    %10 = getelementptr [10 x i32], [10 x i32]* %3, i32 6
    store i32 6, i32* %10
    %11 = getelementptr [10 x i32], [10 x i32]* %3, i32 7
    store i32 5, i32* %11
    %12 = getelementptr [10 x i32], [10 x i32]* %3, i32 8
    store i32 7, i32* %12
    %13 = getelementptr [10 x i32], [10 x i32]* %3, i32 9
    store i32 8, i32* %13
    %14 = alloca i32
    store i32 0, i32* %14
    %15 = load [10 x i32], [10 x i32]* %3
    %16 = load i32, i32* @n
    %17 = call i32 @heap_sort([10 x i32] %15,i32 %16)
    store i32 %17, i32* %14
    br %18

18:
    %21 = load i32, i32* %14
    %22 = load i32, i32* @n
    %23 = icmp lt i32 %21,i32 %22
    br i32 %23 %19 %20

19:
    %24 = alloca i32
    %25 = load i32, i32* %14
    %26 = getelementptr [10 x i32], [10 x i32]* %3, i32 %25
    %27 = load i32, i32* %26
    store i32 %27, i32* %24
    %28 = load i32, i32* %24
    call void @putint(i32 %28)
    store i32 10, i32* %24
    %29 = load i32, i32* %24
    call void @putch(i32 %29)
    %30 = load i32, i32* %14
    %31 = load i32, i32* %14
    %32 = add i32 %31,1
    store i32 %32, i32* %14
    br %18

20:
    ret i32 0
}

