@N = dso_local constant i32 -1

@arr = dso_local global [i32 1, i32 2, i32 33, i32 4, i32 5, i32 6]

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local i32 @main(){
0:
    %1 = alloca i32
    store i32 0, i32* %1
    %2 = alloca i32
    store i32 0, i32* %2
    br %3

3:
    %6 = load i32, i32* %1
    %7 = icmp lt i32 %6,i32 6
    br i32 %7 %4 %5

4:
    %8 = load i32, i32* %2
    %9 = load i32, i32* %1
    %10 = getelementptr [6 x i32], [6 x i32]* @arr, i32 %9
    %11 = load i32, i32* %10
    %12 = load i32, i32* %2
    %13 = load i32, i32* %1
    %14 = getelementptr [6 x i32], [6 x i32]* @arr, i32 %13
    %15 = load i32, i32* %14
    %16 = add i32 %12,%15
    store i32 %16, i32* %2
    %17 = load i32, i32* %1
    %18 = load i32, i32* %1
    %19 = add i32 %18,1
    store i32 %19, i32* %1
    br %3

5:
    %20 = load i32, i32* %2
    ret i32 %20
}

