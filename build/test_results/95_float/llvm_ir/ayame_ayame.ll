@RADIUS = dso_local constant float 5.5

@PI = dso_local constant float 3.1415927

@EPS = dso_local constant float 1.0E-6

@PI_HEX = dso_local constant float 3.1415927

@HEX2 = dso_local constant float 0.078125

@FACT = dso_local constant float -33000.0

@EVAL1 = dso_local constant float 95.03318

@EVAL2 = dso_local constant float 34.55752

@EVAL3 = dso_local constant float 34.557518

@CONV1 = dso_local constant float 233.0

@CONV2 = dso_local constant float 4095.0

@MAX = dso_local constant i32 1000000000

@TWO = dso_local constant i32 2

@THREE = dso_local constant i32 3

@FIVE = dso_local constant i32 6

declare i32 @getint()

declare i32 @getch()

declare float @getfloat()

declare i32 @getarray(i32* )

declare i32 @getfarray(float* )

declare void @putint(i32 )

declare void @putch(i32 )

declare void @putfloat(float )

declare void @putarray(i32 , i32* )

declare void @putfarray(i32 , float* )

declare void @putf(i32* , i32 )

declare void @starttime()

declare void @stoptime()

define dso_local void @float_abs(float %0){
1:
    %2 = alloca float
    store float %0, float* %2
    %5 = load float, float* %2
    %7 = itof i32 0 to float
    %6 = icmp lt float %5,float %7
    br float %6 %4 %3

3:
    %11 = load float, float* %2
    ret float %11

4:
    %8 = load float, float* %2
    %10 = itof i32 0 to float
    %9 = fsub float %10,%8
    ret float %9
}
define dso_local void @circle_area(i32 %0){
1:
    %2 = alloca i32
    store i32 %0, i32* %2
    %3 = load void, void* @PI
    %4 = load i32, i32* %2
    %5 = mul i32 %3,%4
    %6 = load i32, i32* %2
    %7 = mul i32 %5,%6
    %8 = load i32, i32* %2
    %9 = load i32, i32* %2
    %10 = mul i32 %8,%9
    %11 = load void, void* @PI
    %12 = mul i32 %10,%11
    %13 = load void, void* @PI
    %14 = load i32, i32* %2
    %15 = mul i32 %13,%14
    %16 = load i32, i32* %2
    %17 = mul i32 %15,%16
    %18 = load i32, i32* %2
    %19 = load i32, i32* %2
    %20 = mul i32 %18,%19
    %21 = load void, void* @PI
    %22 = mul i32 %20,%21
    %23 = add i32 %17,%22
    %24 = sdiv i32 %23,2
    ret void %24
}
define dso_local i32 @float_eq(float %0, float %1){
2:
    %3 = alloca float
    store float %0, float* %3
    %4 = alloca float
    store float %1, float* %4
    %8 = load float, float* %3
    %9 = load float, float* %4
    %10 = load float, float* %3
    %11 = load float, float* %4
    %12 = fsub float %10,%11
    %13 = call void @float_abs(float %12)
    %14 = load void, void* @EPS
    %15 = icmp lt void %13,void %14
    br void %15 %6 %7

5:

6:
    %17 = itof i32 1 to float
    %16 = fmul float %17,2.0
    %19 = itof i32 2 to float
    %18 = fdiv float %16,%19
    ret float %18

7:
    ret i32 0
}
define dso_local void @error(float %0, float %1){
2:
    call void @putch(i32 101)
    call void @putch(i32 114)
    call void @putch(i32 114)
    call void @putch(i32 111)
    call void @putch(i32 114)
    call void @putch(i32 10)
}
define dso_local void @ok(float %0, float %1){
2:
    call void @putch(i32 111)
    call void @putch(i32 107)
    call void @putch(i32 10)
}
define dso_local void @assert(i32 %0){
1:
    %2 = alloca i32
    store i32 %0, i32* %2
    %6 = load i32, i32* %2
    
    br void %7 %4 %5

3:

4:
    %8 = call void @error()
    br %3

5:
    %9 = call void @ok()
    br %3
}
define dso_local void @assert_not(i32 %0){
1:
    %2 = alloca i32
    store i32 %0, i32* %2
    %6 = load i32, i32* %2
    br i32 %6 %4 %5

3:

4:
    %7 = call void @error()
    br %3

5:
    %8 = call void @ok()
    br %3
}
define dso_local i32 @main(i32 %0){
1:
    %2 = load void, void* @HEX2
    %3 = load void, void* @FACT
    %4 = call i32 @float_eq(void %2,void %3)
    %5 = call void @assert_not(void %2,void %3,i32 %4)
    %6 = load void, void* @EVAL1
    %7 = load void, void* @EVAL2
    %8 = call i32 @float_eq(void %6,void %7)
    %9 = call void @assert_not(void %6,void %7,i32 %8)
    %10 = load void, void* @EVAL2
    %11 = load void, void* @EVAL3
    %12 = call i32 @float_eq(void %10,void %11)
    %13 = call void @assert(void %10,void %11,i32 %12)
    %14 = load void, void* @RADIUS
    %15 = call void @circle_area(void %14)
    %16 = load i32, i32* @FIVE
    %17 = call void @circle_area(i32 %16)
    %18 = call i32 @float_eq(i32 %16,void %17)
    %19 = call void @assert(i32 %16,void %17,i32 %18)
    %20 = load void, void* @CONV1
    %21 = load void, void* @CONV2
    %22 = call i32 @float_eq(void %20,void %21)
    %23 = call void @assert_not(void %20,void %21,i32 %22)
    br float 1.5 %25 %24

24:
    
    
    br void %30 %28 %27

25:
    %26 = call void @ok()
    br %24

27:
    %35 = itof i32 3 to float
    %34 = and i32 0.0,%35
    br float %34 %33 %32

28:
    %31 = call void @ok()
    br %27

32:
    %40 = itof i32 0 to float
    %39 = or i32 %40,0.3
    br float %39 %38 %37

33:
    %36 = call void @error()
    br %32

37:
    %42 = alloca i32
    store i32 1, i32* %42
    %43 = alloca i32
    store i32 0, i32* %43
    %44 = alloca [10 x float]
    %45 = getelementptr [10 x float], [10 x float]* %44, i32 0
    store float 1.0, float* %45
    %46 = getelementptr [10 x float], [10 x float]* %44, i32 1
    %47 = itof i32 2 to float
    store float %47, float* %46
    %48 = load [10 x float], [10 x float]* %44
    %49 = call i32 @getfarray([10 x float] %48)
    %50 = alloca i32
    store i32 %49, i32* %50
    br %51

38:
    %41 = call void @ok()
    br %37

51:
    %54 = load i32, i32* %42
    %55 = load i32, i32* @MAX
    %56 = icmp lt i32 %54,i32 %55
    br i32 %56 %52 %53

52:
    %57 = call float @getfloat()
    %58 = alloca float
    store float %57, float* %58
    %59 = load void, void* @PI
    %60 = load float, float* %58
    %61 = mul i32 %59,%60
    %62 = load float, float* %58
    %63 = mul i32 %61,%62
    %64 = alloca float
    store void %63, float* %64
    %65 = load float, float* %58
    %66 = call void @circle_area(float %65)
    %67 = alloca float
    store void %66, float* %67
    %68 = load i32, i32* %43
    %69 = getelementptr [10 x float], [10 x float]* %44, i32 %68
    %70 = load i32, i32* %43
    %71 = getelementptr [10 x float], [10 x float]* %44, i32 %70
    %72 = load float, float* %71
    %73 = load float, float* %58
    %74 = load i32, i32* %43
    %75 = getelementptr [10 x float], [10 x float]* %44, i32 %74
    %76 = load float, float* %75
    %77 = load float, float* %58
    %78 = fadd float %76,%77
    store float %78, float* %69
    %79 = load float, float* %64
    call void @putfloat(float %79)
    call void @putch(i32 32)
    %80 = load float, float* %67
    call void @putint(float %80)
    call void @putch(i32 10)
    %81 = load i32, i32* %42
    %83 = itof i32 0 to float
    %82 = fsub float %83,10.0
    %85 = itof i32 0 to float
    %84 = fsub float %85,%82
    %87 = itof i32 %81 to float
    %86 = fmul float %87,%84
    %88 = ftoi float %86 to i32
    store i32 %88, i32* %42
    %89 = load i32, i32* %43
    %90 = load i32, i32* %43
    %91 = add i32 %90,1
    store i32 %91, i32* %43
    br %51

53:
    %92 = load i32, i32* %50
    %93 = load [10 x float], [10 x float]* %44
    call void @putfarray(i32 %92,[10 x float] %93)
    ret i32 0
}

