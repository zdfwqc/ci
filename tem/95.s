	.text
	.syntax unified
	.eabi_attribute	67, "2.09"	@ Tag_conformance
	.eabi_attribute	6, 1	@ Tag_CPU_arch
	.eabi_attribute	8, 1	@ Tag_ARM_ISA_use
	.eabi_attribute	34, 1	@ Tag_CPU_unaligned_access
	.eabi_attribute	17, 1	@ Tag_ABI_PCS_GOT_use
	.eabi_attribute	20, 1	@ Tag_ABI_FP_denormal
	.eabi_attribute	21, 1	@ Tag_ABI_FP_exceptions
	.eabi_attribute	23, 3	@ Tag_ABI_FP_number_model
	.eabi_attribute	24, 1	@ Tag_ABI_align_needed
	.eabi_attribute	25, 1	@ Tag_ABI_align_preserved
	.eabi_attribute	28, 1	@ Tag_ABI_VFP_args
	.eabi_attribute	38, 1	@ Tag_ABI_FP_16bit_format
	.eabi_attribute	18, 4	@ Tag_ABI_PCS_wchar_t
	.eabi_attribute	14, 0	@ Tag_ABI_PCS_R9_use
	.file	"95.c"
	.globl	float_abs                       @ -- Begin function float_abs
	.p2align	2
	.type	float_abs,%function
	.code	32                              @ @float_abs
float_abs:
	.fnstart
@ %bb.0:
	push	{r11, lr}
	mov	r11, sp
	sub	sp, sp, #8
	str	r0, [sp]
	ldr	r0, [sp]
	mov	r1, #0
	bl	__ltsf2
	cmn	r0, #1
	bgt	.LBB0_2
	b	.LBB0_1
.LBB0_1:
	ldr	r0, [sp]
	eor	r0, r0, #-2147483648
	str	r0, [sp, #4]
	b	.LBB0_3
.LBB0_2:
	ldr	r0, [sp]
	str	r0, [sp, #4]
	b	.LBB0_3
.LBB0_3:
	ldr	r0, [sp, #4]
	mov	sp, r11
	pop	{r11, lr}
	mov	pc, lr
.Lfunc_end0:
	.size	float_abs, .Lfunc_end0-float_abs
	.fnend
                                        @ -- End function
	.globl	circle_area                     @ -- Begin function circle_area
	.p2align	2
	.type	circle_area,%function
	.code	32                              @ @circle_area
circle_area:
	.fnstart
@ %bb.0:
	push	{r4, r5, r6, r7, r11, lr}
	add	r11, sp, #16
	sub	sp, sp, #8
	str	r0, [sp, #4]
	ldr	r4, [sp, #4]
	mov	r0, r4
	bl	__floatsisf
	mov	r5, r0
	ldr	r6, .LCPI1_0
	mov	r0, r5
	mov	r1, r6
	bl	__mulsf3
	mov	r7, r0
	mul	r0, r4, r4
	bl	__floatsisf
	mov	r1, r6
	bl	__mulsf3
	mov	r4, r0
	mov	r0, r7
	mov	r1, r5
	bl	__mulsf3
	mov	r1, r4
	bl	__addsf3
	mov	r1, #1073741824
	bl	__divsf3
	sub	sp, r11, #16
	pop	{r4, r5, r6, r7, r11, lr}
	mov	pc, lr
	.p2align	2
@ %bb.1:
.LCPI1_0:
	.long	1078530011                      @ 0x40490fdb
.Lfunc_end1:
	.size	circle_area, .Lfunc_end1-circle_area
	.fnend
                                        @ -- End function
	.globl	float_eq                        @ -- Begin function float_eq
	.p2align	2
	.type	float_eq,%function
	.code	32                              @ @float_eq
float_eq:
	.fnstart
@ %bb.0:
	push	{r11, lr}
	mov	r11, sp
	sub	sp, sp, #16
	str	r0, [sp, #8]
	str	r1, [sp, #4]
	ldr	r0, [sp, #8]
	ldr	r1, [sp, #4]
	bl	__subsf3
	bl	float_abs
	ldr	r1, .LCPI2_0
	bl	__ltsf2
	cmn	r0, #1
	bgt	.LBB2_2
	b	.LBB2_1
.LBB2_1:
	mov	r0, #1
	str	r0, [r11, #-4]
	b	.LBB2_3
.LBB2_2:
	mov	r0, #0
	str	r0, [r11, #-4]
	b	.LBB2_3
.LBB2_3:
	ldr	r0, [r11, #-4]
	mov	sp, r11
	pop	{r11, lr}
	mov	pc, lr
	.p2align	2
@ %bb.4:
.LCPI2_0:
	.long	897988541                       @ 0x358637bd
.Lfunc_end2:
	.size	float_eq, .Lfunc_end2-float_eq
	.fnend
                                        @ -- End function
	.globl	error                           @ -- Begin function error
	.p2align	2
	.type	error,%function
	.code	32                              @ @error
error:
	.fnstart
@ %bb.0:
	push	{r11, lr}
	mov	r11, sp
	mov	r0, #101
	bl	putch
	mov	r0, #114
	bl	putch
	mov	r0, #114
	bl	putch
	mov	r0, #111
	bl	putch
	mov	r0, #114
	bl	putch
	mov	r0, #10
	bl	putch
	pop	{r11, lr}
	mov	pc, lr
.Lfunc_end3:
	.size	error, .Lfunc_end3-error
	.fnend
                                        @ -- End function
	.globl	ok                              @ -- Begin function ok
	.p2align	2
	.type	ok,%function
	.code	32                              @ @ok
ok:
	.fnstart
@ %bb.0:
	push	{r11, lr}
	mov	r11, sp
	mov	r0, #111
	bl	putch
	mov	r0, #107
	bl	putch
	mov	r0, #10
	bl	putch
	pop	{r11, lr}
	mov	pc, lr
.Lfunc_end4:
	.size	ok, .Lfunc_end4-ok
	.fnend
                                        @ -- End function
	.globl	assert                          @ -- Begin function assert
	.p2align	2
	.type	assert,%function
	.code	32                              @ @assert
assert:
	.fnstart
@ %bb.0:
	push	{r11, lr}
	mov	r11, sp
	sub	sp, sp, #8
	str	r0, [sp, #4]
	ldr	r0, [sp, #4]
	cmp	r0, #0
	bne	.LBB5_2
	b	.LBB5_1
.LBB5_1:
	bl	error
	b	.LBB5_3
.LBB5_2:
	bl	ok
	b	.LBB5_3
.LBB5_3:
	mov	sp, r11
	pop	{r11, lr}
	mov	pc, lr
.Lfunc_end5:
	.size	assert, .Lfunc_end5-assert
	.fnend
                                        @ -- End function
	.globl	assert_not                      @ -- Begin function assert_not
	.p2align	2
	.type	assert_not,%function
	.code	32                              @ @assert_not
assert_not:
	.fnstart
@ %bb.0:
	push	{r11, lr}
	mov	r11, sp
	sub	sp, sp, #8
	str	r0, [sp, #4]
	ldr	r0, [sp, #4]
	cmp	r0, #0
	beq	.LBB6_2
	b	.LBB6_1
.LBB6_1:
	bl	error
	b	.LBB6_3
.LBB6_2:
	bl	ok
	b	.LBB6_3
.LBB6_3:
	mov	sp, r11
	pop	{r11, lr}
	mov	pc, lr
.Lfunc_end6:
	.size	assert_not, .Lfunc_end6-assert_not
	.fnend
                                        @ -- End function
	.globl	main                            @ -- Begin function main
	.p2align	2
	.type	main,%function
	.code	32                              @ @main
main:
	.fnstart
@ %bb.0:
	push	{r4, r5, r11, lr}
	add	r11, sp, #8
	sub	sp, sp, #80
	bic	sp, sp, #15
	ldr	r0, .LCPI7_5
	ldr	r0, [r0]
	str	r0, [sp, #76]
	mov	r5, #0
	str	r5, [sp, #28]
	mov	r0, #228589568
	orr	r0, r0, #805306368
	mov	r1, #59392
	orr	r1, r1, #-956301312
	bl	float_eq
	bl	assert_not
	ldr	r0, .LCPI7_0
	ldr	r4, .LCPI7_1
	mov	r1, r4
	bl	float_eq
	bl	assert_not
	mov	r0, r4
	mov	r1, r4
	bl	float_eq
	bl	assert
	mov	r0, #5
	bl	circle_area
	mov	r4, r0
	mov	r0, #5
	bl	circle_area
	mov	r1, r0
	mov	r0, r4
	bl	float_eq
	bl	assert
	mov	r0, #6881280
	orr	r0, r0, #1124073472
	ldr	r1, .LCPI7_2
	bl	float_eq
	bl	assert_not
	cmp	r5, #0
	bne	.LBB7_2
	b	.LBB7_1
.LBB7_1:
	bl	ok
	b	.LBB7_2
.LBB7_2:
	bl	ok
	bl	ok
	mov	r0, #1
	str	r0, [sp, #24]
	mov	r0, #0
	str	r0, [sp, #20]
	add	r4, sp, #32
	mov	r0, r4
	mov	r1, #0
	mov	r2, #40
	bl	memset
	mov	r0, #1065353216
	str	r0, [sp, #32]
	mov	r0, #1073741824
	str	r0, [sp, #36]
	mov	r0, r4
	bl	getfarray
	str	r0, [sp, #16]
	b	.LBB7_3
.LBB7_3:                                @ =>This Inner Loop Header: Depth=1
	ldr	r0, [sp, #24]
	ldr	r1, .LCPI7_3
	cmp	r0, r1
	bgt	.LBB7_5
	b	.LBB7_4
.LBB7_4:                                @   in Loop: Header=BB7_3 Depth=1
	bl	getfloat
	bl	__floatsisf
	str	r0, [sp, #12]
	ldr	r4, [sp, #12]
	ldr	r1, .LCPI7_4
	mov	r0, r4
	bl	__mulsf3
	mov	r1, r4
	bl	__mulsf3
	str	r0, [sp, #8]
	ldr	r0, [sp, #12]
	bl	__fixsfsi
	bl	circle_area
	str	r0, [sp, #4]
	ldr	r4, [sp, #20]
	add	r5, sp, #32
	ldr	r0, [r5, r4, lsl #2]
	ldr	r1, [sp, #12]
	bl	__addsf3
	str	r0, [r5, r4, lsl #2]
	ldr	r0, [sp, #8]
	bl	__extendsfdf2
	bl	putfloat
	mov	r0, #32
	bl	putch
	ldr	r0, [sp, #4]
	bl	__extendsfdf2
	bl	putint
	mov	r0, #10
	bl	putch
	ldr	r0, [sp, #24]
	bl	__floatsidf
	mov	r3, #2359296
	orr	r3, r3, #1073741824
	mov	r2, #0
	bl	__muldf3
	bl	__fixdfsi
	str	r0, [sp, #24]
	ldr	r0, [sp, #20]
	add	r0, r0, #1
	str	r0, [sp, #20]
	b	.LBB7_3
.LBB7_5:
	ldr	r0, [sp, #16]
	add	r1, sp, #32
	bl	putfarray
	ldr	r0, [sp, #76]
	ldr	r1, .LCPI7_5
	ldr	r1, [r1]
	cmp	r1, r0
	bne	.LBB7_6
	b	.LBB7_7
.LBB7_6:
	bl	__stack_chk_fail
.LBB7_7:
	mov	r0, #0
	sub	sp, r11, #8
	pop	{r4, r5, r11, lr}
	mov	pc, lr
	.p2align	2
@ %bb.8:
.LCPI7_0:
	.long	1119752446                      @ 0x42be10fe
.LCPI7_1:
	.long	1107966695                      @ 0x420a3ae7
.LCPI7_2:
	.long	1166012416                      @ 0x457ff000
.LCPI7_3:
	.long	999999999                       @ 0x3b9ac9ff
.LCPI7_4:
	.long	1078530011                      @ 0x40490fdb
.LCPI7_5:
	.long	__stack_chk_guard
.Lfunc_end7:
	.size	main, .Lfunc_end7-main
	.fnend
                                        @ -- End function
	.type	RADIUS,%object                  @ @RADIUS
	.section	.rodata,"a",%progbits
	.globl	RADIUS
	.p2align	2
RADIUS:
	.long	0x40b00000                      @ float 5.5
	.size	RADIUS, 4

	.type	PI,%object                      @ @PI
	.globl	PI
	.p2align	2
PI:
	.long	0x40490fdb                      @ float 3.14159274
	.size	PI, 4

	.type	EPS,%object                     @ @EPS
	.globl	EPS
	.p2align	2
EPS:
	.long	0x358637bd                      @ float 9.99999997E-7
	.size	EPS, 4

	.type	PI_HEX,%object                  @ @PI_HEX
	.globl	PI_HEX
	.p2align	2
PI_HEX:
	.long	0x40490fdb                      @ float 3.14159274
	.size	PI_HEX, 4

	.type	HEX2,%object                    @ @HEX2
	.globl	HEX2
	.p2align	2
HEX2:
	.long	0x3da00000                      @ float 0.078125
	.size	HEX2, 4

	.type	FACT,%object                    @ @FACT
	.globl	FACT
	.p2align	2
FACT:
	.long	0xc700e800                      @ float -33000
	.size	FACT, 4

	.type	EVAL1,%object                   @ @EVAL1
	.globl	EVAL1
	.p2align	2
EVAL1:
	.long	0x42be10fe                      @ float 95.0331878
	.size	EVAL1, 4

	.type	EVAL2,%object                   @ @EVAL2
	.globl	EVAL2
	.p2align	2
EVAL2:
	.long	0x420a3ae7                      @ float 34.5575218
	.size	EVAL2, 4

	.type	EVAL3,%object                   @ @EVAL3
	.globl	EVAL3
	.p2align	2
EVAL3:
	.long	0x420a3ae7                      @ float 34.5575218
	.size	EVAL3, 4

	.type	CONV1,%object                   @ @CONV1
	.globl	CONV1
	.p2align	2
CONV1:
	.long	0x43690000                      @ float 233
	.size	CONV1, 4

	.type	CONV2,%object                   @ @CONV2
	.globl	CONV2
	.p2align	2
CONV2:
	.long	0x457ff000                      @ float 4095
	.size	CONV2, 4

	.type	MAX,%object                     @ @MAX
	.globl	MAX
	.p2align	2
MAX:
	.long	1000000000                      @ 0x3b9aca00
	.size	MAX, 4

	.type	TWO,%object                     @ @TWO
	.globl	TWO
	.p2align	2
TWO:
	.long	2                               @ 0x2
	.size	TWO, 4

	.type	THREE,%object                   @ @THREE
	.globl	THREE
	.p2align	2
THREE:
	.long	3                               @ 0x3
	.size	THREE, 4

	.type	FIVE,%object                    @ @FIVE
	.globl	FIVE
	.p2align	2
FIVE:
	.long	5                               @ 0x5
	.size	FIVE, 4

	.ident	"clang version 14.0.6"
	.section	".note.GNU-stack","",%progbits
