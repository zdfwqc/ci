package VISITOR;

import IR.Values.BasicBlock;
import IR.Values.BuildFactory;
import IR.Values.*;
import IR.Values.instructions.OP;
import IR.types.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.function.ToDoubleBiFunction;

public class Visitor extends SysY2022BaseVisitor<Void> {
    private boolean allowPrint = false;


    private BasicBlock curBlock = null;
    private Function curFunc = null;
    private BasicBlock curWhileBlock = null;
    private BasicBlock curEndBlock = null;
    private Value temValue;//用于传递值
    private Type temType;//传递类型
    private boolean isGlobal = true;
    private OP saveOp = null;
    private ArrayList<Integer> temDims = null;
    private ArrayList<Value> indexStack = new ArrayList<>();
    private boolean constTag = false;
    private boolean intTag = true;
    private double saveValue = 0;
    private boolean isArray = false;
    private String temName = null;
    private Value curArray = null;
    private boolean register = false;
    private ArrayList<Value> funValueList = new ArrayList<>();
    private ArrayList<HashMap<String,Double>> constKeep = new ArrayList<>();
    private HashMap<String, Double> valueKepp = new HashMap<>();
    private ArrayList<Value> temList = new ArrayList<>();
    private ArrayList<Type> temTypeList = new ArrayList<>();
    private OP temOp;
    private ArrayList<Value> params = new ArrayList<>();
    private BuildFactory f = BuildFactory.getInstance();
    private ArrayList<HashMap<String, Value>> set = new ArrayList<>();

    public HashMap<String, Value> top() {
        return set.get(set.size() - 1);
    }

    public void pushConst(String name,Double value ){
        if (allowPrint) {
            System.out.println("push const value "+name);
        }
        constKeep.get(constKeep.size()-1).put(name,value);
    }
    public double getConstValue(String name){
        int i = constKeep.size()-1;
        if (allowPrint) {
            System.out.println("get const value "+name);
        }
        while (i>=0) {
            HashMap<String ,Double> tem =constKeep.get(i);
            if (tem.containsKey(name)) {
                return tem.get(name);
            }
            i--;
        }
        //
        return  0;
    }
    private double calculate(double a, double b, OP op) {
        if (op.equals(OP.Add)) {
            return a + b;
        } else if (op.equals(OP.Sub)) {
            return a - b;
        } else if (op.equals(OP.Mul)) {
            return a * b;
        } else if (op.equals(OP.Div)) {
            return a / b;
        } else if (op.equals(OP.Mod)) {
            return a % b;
        } else {
            if (allowPrint) {
                System.out.println("something error in calculate!");

            }
            return 0;
        }
    }


    public void push(String name, Value v) {
        top().put(name, v);
    }


    public void update() {
        HashMap<String, Value> tem = new HashMap<>();
        set.add(tem);
        HashMap<String,Double> tem2 = new HashMap<>();
        constKeep.add(tem2);
    }

    public void downDate() {

        set.remove(set.size() - 1);
        constKeep.remove(constKeep.size()-1);
    }

    public Value getValue(String name) {
        int i = set.size() - 1;
        while (i >= 0) {
            HashMap<String, Value> j = set.get(i);
            if (j.containsKey(name)) {
                return j.get(name);
            }
            i--;
        }
        System.out.println("VAR /CONST NAME NOT FOUND ERROR!!!");
        return null;
    }

    /*
    compUnit:
         (decl | funcDef)+
     */
    @Override
    public Void visitCompUnit(SysY2022Parser.CompUnitContext ctx) {
        set.add(new HashMap<>());
        constKeep.add(new HashMap<>());
        ArrayType intArrayType = new ArrayType(IntegerType.i32, -1);
        ArrayType floatArrayType = new ArrayType(FloatingType.type, -1);
        top().put("getint", f.buildLibraryFunction
                ("getint", IntegerType.i32, new ArrayList<>()));
        top().put("getch", f.buildLibraryFunction
                ("getch", IntegerType.i32, new ArrayList<>()));
        top().put("getfloat", f.buildLibraryFunction
                ("getfloat", FloatingType.type, new ArrayList<>()));
        top().put("getarray", f.buildLibraryFunction
                ("getarray", IntegerType.i32, new ArrayList<>(Collections.singleton(intArrayType))));
        top().put("getfarray", f.buildLibraryFunction
                ("getfarray", IntegerType.i32, new ArrayList<>(Collections.singleton(floatArrayType))));
        top().put("putint", f.buildLibraryFunction
                ("putint", VoidType.type, new ArrayList<>(Collections.singleton(IntegerType.i32))));
        top().put("putch", f.buildLibraryFunction
                ("putch", VoidType.type, new ArrayList<>(Collections.singleton(IntegerType.i32))));
        top().put("putfloat", f.buildLibraryFunction
                ("putfloat", VoidType.type, new ArrayList<>(Collections.singleton(FloatingType.type))));
        top().put("putarray", f.buildLibraryFunction
                ("putarray", VoidType.type, new ArrayList<>() {{
                    add(IntegerType.i32);
                    add(intArrayType);
                }}));
        top().put("putfarray", f.buildLibraryFunction
                ("putfarray", VoidType.type, new ArrayList<>() {{
                    add(IntegerType.i32);
                    add(floatArrayType);
                }}));
        top().put("putf", f.buildLibraryFunction
                ("putf", VoidType.type, new ArrayList<>() {{
                    add(intArrayType);
                    add(IntegerType.i32);
                }}));
        top().put("starttime", f.buildLibraryFunction
                ("starttime", VoidType.type, new ArrayList<>()));
        top().put("stoptime", f.buildLibraryFunction
                ("stoptime", VoidType.type, new ArrayList<>()));
        super.visitCompUnit(ctx);
        return null;
    }

    /*
    decl:
        constDecl
        | varDecl
     */
    @Override
    public Void visitDecl(SysY2022Parser.DeclContext ctx) {
        return visitChildren(ctx);
    }

    /*
    constDecl:
        CONST BType constDef (COMMA constDef)* SEMICOLON
        ;
     */
    @Override
    public Void visitConstDecl(SysY2022Parser.ConstDeclContext ctx) {
        Type type = VoidType.type;
        String typeName = ctx.getChild(1).getText();
        if (typeName.equals("int")) {
            type = IntegerType.i32;
        } else if (typeName.equals("Float")) {
            type = FloatingType.type;
        }
        temType = type;
        ctx.constDef().forEach(this::visitConstDef);
        return null;
    }

    /*
    constDef:
        Ident (LSBRACKET constExp RSBRACKET)* ASSIGN constInitVal
        ;
     */
    @Override
    public Void visitConstDef(SysY2022Parser.ConstDefContext ctx) {
        String name = ctx.Ident().getText();
        if (allowPrint) {
            System.out.println("Get Const def " + name);
        }
        //judge array
        if (ctx.constExp().isEmpty()) {
            // isn't an array
            visit(ctx.constInitVal());//要求constInitval 传递值到tem
            if (temType.isIntegerType()) {
                temValue = f.getConstantInt((int )saveValue);
            }else {
                temValue = f.getConstantFloat((float) saveValue);
            }
            pushConst(ctx.Ident().getText(),saveValue);
            if (isGlobal) {
                //全局变量
                temValue = f.buildGlobalVariable(name, temType, true, temValue);
                push(name, temValue);
            } else {
                //局部变量
                temValue = f.buildVar(curBlock, temValue, true, temType);
                push(name, temValue);
            }
        } else {
            ArrayList<Integer> dims = new ArrayList<>();
            intTag = true;
            for (SysY2022Parser.ConstExpContext i : ctx.constExp()) {
                visit(i);
                dims.add((int) saveValue);
            }
            temDims = new ArrayList<>(dims);
            int i = dims.size() - 1;
            Type keepType = null;
            while (i >= 0) {
                if (keepType == null) {
                    keepType = f.getArrayType(temType, dims.get(i));
                } else {
                    keepType = f.getArrayType(keepType, dims.get(i));
                }
                i--;
            }
            if (isGlobal) {
                temValue = f.buildGlobalArray(true, keepType, name);
            } else {
                temValue = f.buildArray(curBlock, true, keepType);
            }
            curArray = temValue;
            push(name, temValue);
            isArray = true;
            indexStack = new ArrayList<>();
            temName = ctx.Ident().getText();
            visit(ctx.constInitVal());
            isArray = false;

        }
        //  IntegerType.i32 array;

        return null;

    }

    private void dealArray() {
        if (isGlobal) {
            f.buildInitArray(curArray, indexStack, temValue);
        } else {
            Value addr = f.buildGEPInst(curBlock, curArray, indexStack);
            f.buildStoreInst(curBlock, addr, temValue);
        }
    }

    /*
constInitVal:
    constExp
        | (LBRACE (constInitVal (COMMA constInitVal)*)? RBRACE)
     */
    @Override
    public Void visitConstInitVal(SysY2022Parser.ConstInitValContext ctx) {
        if (allowPrint) {
            System.out.println("visit const initval now");
        }
        if (ctx.constExp() != null && !isArray) {
            visit(ctx.constExp());
        } else {
            if (ctx.constInitVal(0) != null) {
                int x = ctx.constInitVal().size();
                int i = 0;
                while (i < x) {
                    Value y = f.getConstantInt(i);
                    indexStack.add(y);
                    visit(ctx.constInitVal(i));
                    indexStack.remove(y);
                    i++;
                }
            } else if (ctx.constExp()!=null){
                //最底层的情况
                visit(ctx.constExp());
                Value value;
                if (temType.isIntegerType()) {
                    value = f.getConstantInt((int) saveValue);
                } else {
                    value = f.getConstantFloat((float) saveValue);
                }
                temValue = value;
                dealArray();
                //const push here
                String symbol = "";
                symbol += temName;
                while (indexStack.size()<temDims.size()) {
                    indexStack.add(f.getConstantInt(0));
                }
                for (Value i :indexStack){
                    symbol+= ((ConstantInteger ) i).getValue();
                    symbol+= ";";
                }
                pushConst(symbol,temType.isIntegerType()?(int)saveValue:saveValue);
            }else {

            }

        }
        return null;
    }

    /*
    varDecl:
        BType varDef (COMMA varDef)* SEMICOLON
        ;
    */

    @Override
    public Void visitVarDecl(SysY2022Parser.VarDeclContext ctx) {
        Type type = VoidType.type;
        String typeName = ctx.getChild(0).getText();
        if (typeName.equals("int")) {
            type = IntegerType.i32;
        } else if (typeName.equals("float")) {
            type = FloatingType.type;
        }
        temType = type;
        ctx.varDef().forEach(this::visitVarDef);
        return null;
    }

    /*
    varDef:
        (Ident (LSBRACKET constExp RSBRACKET)*)
        | (Ident (LSBRACKET constExp RSBRACKET)* ASSIGN initVal)
     */
    @Override
    public Void visitVarDef(SysY2022Parser.VarDefContext ctx) {
        String name = ctx.Ident().getText();
        if (allowPrint) {
            System.out.println("Get VAr def " + name);
        }
        //judge array
        if (ctx.constExp().isEmpty()) {
            // isn't an array
            if (ctx.initVal() != null) {
                temValue = null;
                if (isGlobal) {
                    constTag = true;
                    saveValue = 0;
                }
                visit(ctx.initVal());
                constTag = false;
            } else {
                //协助初始化
                temValue = null;
            }
            if (isGlobal) {
                Value tem;
                if (temType.isIntegerType()) {
                    tem = f.getConstantInt((int) saveValue);
                } else {
                    tem = f.getConstantFloat((float) saveValue);
                }
                temValue = f.buildGlobalVariable(name, temType, false, tem);
                push(name, temValue);
            } else {
                //局部变量
                temValue = f.buildVar(curBlock, temValue, false, temType);
                push(name, temValue);
            }
        } else {
            if (isGlobal) {
                constTag = true;
            }
            ArrayList<Integer> dims = new ArrayList<>();
            intTag = true;
            for (SysY2022Parser.ConstExpContext i : ctx.constExp()) {
                visit(i);
                dims.add((int) saveValue);
            }
            temDims = new ArrayList<>(dims);
            int i = dims.size() - 1;
            Type keepType = null;
            while (i >= 0) {
                if (keepType == null) {
                    keepType = f.getArrayType(temType, dims.get(i));
                } else {
                    keepType = f.getArrayType(keepType, dims.get(i));
                }
                i--;
            }
            if (isGlobal) {
                temValue = f.buildGlobalArray(false, keepType, name);
            } else {
                temValue = f.buildArray(curBlock, false, keepType);
            }
            curArray = temValue;
            push(name, temValue);
            if (ctx.initVal() != null) {
                isArray = true;
                indexStack = new ArrayList<>();
                visit(ctx.initVal());
                isArray = false;

            }
            constTag = false;
        }
        return null;
    }

    /*
    initVal:
            exp
            | (LBRACE (initVal (COMMA initVal)* )? RBRACE)
            ;
     */
    @Override
    public Void visitInitVal(SysY2022Parser.InitValContext ctx) {
        if (ctx.exp() != null && !isArray) {
            visit(ctx.exp());
        } else {
            if (ctx.initVal(0) != null) {
                int x = ctx.initVal().size();
                int i = 0;
                while (i < x) {
                    Value y = f.getConstantInt(i);
                    indexStack.add(y);
                    visit(ctx.initVal(i));
                    indexStack.remove(y);
                    i++;
                }
            } else if  (ctx.exp()!= null){
                //最底层的情况
                if (isGlobal) {
                    constTag = true;
                    saveValue = 0;
                    visit(ctx.exp());
                    constTag = false;
                    Value value = null;
                    if (temType.isIntegerType()) {
                        value = f.getConstantInt((int) saveValue);
                    } else {
                        value = f.getConstantFloat((float) saveValue);
                    }
                    f.buildInitArray(curArray, indexStack, value);
                } else {
                    visit(ctx.exp());
                    Value addr = f.buildGEPInst(curBlock, curArray, indexStack);
                    f.buildStoreInst(curBlock, addr, temValue);
                }
            }else {

            }

        }

        return null;
    }


    /*
    funcDef:
        (BType | VOID) Ident LPAREN (funcFParams)? RPAREN block
        ;
     */
    @Override
    public Void visitFuncDef(SysY2022Parser.FuncDefContext ctx) {
        isGlobal = false;
        String funcName = ctx.Ident().getText();
        if (allowPrint) {
            System.out.println("Start to def func " + funcName);
        }
        FunctionType functionType = new FunctionType();
        Type type = VoidType.type;
        String typeName = ctx.getChild(0).getText();
        if (typeName.equals("int")) {
            type = IntegerType.i32;
        } else if (typeName.equals("Float")) {
            type = FloatingType.type;
        }
        functionType.setRetType(type);

        if (ctx.funcFParams() != null) {
            visit(ctx.funcFParams());
        }
        Function func = f.buildFunction(funcName, functionType, temTypeList);

        curFunc = func;//change cur func
        push(funcName, func);

        //现在换区了
        update();
        push(funcName, func);
        BasicBlock bb = f.buildBasicBlock(curFunc);
        curBlock = bb;

        funValueList = f.getFuncValueList(curFunc);
        register = true;
        if (ctx.funcFParams() != null) {
            visit(ctx.funcFParams());
        }
        register = false;

        //TODO

        visit(ctx.block());
        isGlobal = true;
        downDate();
        return null;
    }

    /*
    funcFParams:
        funcFParam (COMMA funcFParam)*
        ;
    */
    @Override
    public Void visitFuncFParams(SysY2022Parser.FuncFParamsContext ctx) {
        if (!register) {
            temTypeList = new ArrayList<>();
            for (SysY2022Parser.FuncFParamContext i : ctx.funcFParam()) {
                visit(i);
                temTypeList.add(temType);
            }

        }else {
            temIndex = 0;
            for (SysY2022Parser.FuncFParamContext i :ctx.funcFParam()){
                visit(i);
                temIndex++;
            }
        }
        return  null;
    }
    private  int temIndex;

    /*
    funcFParam:
        BType Ident ( LSBRACKET RSBRACKET (LSBRACKET exp RSBRACKET)* )?
        ;
     */
    @Override
    public Void visitFuncFParam(SysY2022Parser.FuncFParamContext ctx) {
        //please return with temtype
        if (!register){
            if (ctx.LSBRACKET(0) == null) {
                if (ctx.BType().getText().equals("int")) {
                    temType = IntegerType.i32;
                } else {
                    temType = FloatingType.type;
                }
            } else {
                ArrayList<Integer> dims = new ArrayList<>();
                dims.add(-1);
                if (ctx.exp() != null) {
                    for (SysY2022Parser.ExpContext i : ctx.exp()) {
                        constTag = true;
                        visit(i);
                        dims.add((int) saveValue);
                        constTag = false;
                    }
                }
                int i = dims.size() - 1;
                temType = null;
                while (i >= 0) {
                    if (temType == null) {
                        if (ctx.BType().getText().equals("int")) {
                            temType = IntegerType.i32;
                        } else {
                            temType = FloatingType.type;
                        }
                        temType = f.getArrayType(temType, dims.get(i));
                    } else {
                        temType = f.getArrayType(temType, dims.get(i));
                    }
                    i--;
                }
            }
            return null;
        }else {
            int i = temIndex;
                Value value = f.buildVar(curBlock,funValueList.get(i),false,temTypeList.get(i));
                top().put(ctx.Ident().getText(),value);

        }
        return  null;
    }

    /*
    block:
        LBRACE (blockItem)* RBRACE
     */
    @Override
    public Void visitBlock(SysY2022Parser.BlockContext ctx) {

        ctx.blockItem().forEach(this::visit);
        return null;
    }

    /*
    blockItem:
        decl
        | stmt
        */

    @Override
    public Void visitBlockItem(SysY2022Parser.BlockItemContext ctx) {
        return visitChildren(ctx);
    }

    /*
    stmt:
        (lVal ASSIGN exp SEMICOLON)
        | ((exp)? SEMICOLON)
        | block
        | ( IF LPAREN cond RPAREN stmt (ELSE stmt)? )
        | ( WHILE LPAREN cond RPAREN stmt)
        | ( BREAK SEMICOLON)
        | ( CONTINUE SEMICOLON)
        | ( RETURN (exp)? SEMICOLON)
        ;
     */
    @Override
    public Void visitStmt(SysY2022Parser.StmtContext ctx) {
        if (ctx.getChild(0).getText().equals("return")) {
            // return
            if (ctx.exp() != null) {
                visit(ctx.exp()); //temvalue得到更新
                f.buildRet(curBlock, temValue);
            }
            if (allowPrint) {
            }
        } else if (ctx.getChild(0).getText().equals("break")) {
            f.buildBr(curBlock, curEndBlock);
        } else if (ctx.getChild(0).getText().equals("continue")) {
            f.buildBr(curBlock, curWhileBlock);
        } else if (ctx.lVal() != null) {
            //赋值
            if (ctx.lVal().exp(0) == null) {
                Value input;
                input = getValue(ctx.lVal().getText());
                visit(ctx.exp());
                temValue = f.buildStoreInst(curBlock, input, temValue);
            } else {
                ArrayList<Value> indexStack1 = new ArrayList<>();
                for (SysY2022Parser.ExpContext i : ctx.lVal().exp()) {
                    visit(i);
                    indexStack1.add(temValue);
                }
                Value addr = f.buildGEPInst(curBlock, getValue(ctx.lVal().Ident().getText()), indexStack1);
                visit(ctx.exp());
                temValue = f.buildStoreInst(curBlock, addr, temValue);
            }

        } else if (ctx.getChild(0).equals(ctx.exp())) {
            visit(ctx.exp());
        } else if (ctx.getChild(0).getText().equals("if")) {
            BasicBlock finalBlock = f.buildBasicBlock(curFunc);
            if (ctx.ELSE() != null) {
                BasicBlock trueBlock = f.buildBasicBlock(curFunc);
                BasicBlock falseBlock = f.buildBasicBlock(curFunc);
                BasicBlock keepBlock = curBlock;
                visit(ctx.cond());
                f.buildBr(keepBlock, temValue, trueBlock, falseBlock);
                curBlock = trueBlock;
                visit(ctx.stmt(0));
                f.buildBr(trueBlock, finalBlock);
                curBlock = falseBlock;
                visit(ctx.stmt(1));
                f.buildBr(falseBlock, finalBlock);
                curBlock = finalBlock;
            } else {
                BasicBlock trueBlock = f.buildBasicBlock(curFunc);
                ;
                BasicBlock keepBlock = curBlock;
                visit(ctx.cond());
                f.buildBr(keepBlock, temValue, trueBlock, finalBlock);
                curBlock = trueBlock;
                visit(ctx.stmt(0));
                f.buildBr(trueBlock, finalBlock);
                curBlock = finalBlock;
            }
        } else if (ctx.block() != null) {
            update();
            visit(ctx.block());
            downDate();
        } else if (ctx.getChild(0).getText().equals("while")) {
            // while
            BasicBlock judgeBlock = f.buildBasicBlock(curFunc);
            BasicBlock whileBlock = f.buildBasicBlock(curFunc);
            BasicBlock endBlOCK = f.buildBasicBlock(curFunc);
            f.buildBr(curBlock, judgeBlock);
            curBlock = judgeBlock;
            visit(ctx.cond());
            f.buildBr(curBlock, temValue, whileBlock, endBlOCK);
            curBlock = whileBlock;
            curEndBlock = endBlOCK;
            curWhileBlock = judgeBlock;
            visit(ctx.stmt(0));
            f.buildBr(curBlock, judgeBlock);
            curBlock = endBlOCK;

        } else {
            //ERROR

        }
        return null;
    }

    /*
    exp:
        addExp
        ;
     */
    @Override
    public Void visitExp(SysY2022Parser.ExpContext ctx) {

        temValue = null;
        visit(ctx.addExp());

        return null;
    }

    /*
     cond:
        lOrExp
        ;
     */
    @Override
    public Void visitCond(SysY2022Parser.CondContext ctx) {
        temValue = null;
        visit(ctx.lOrExp());
        return null;
    }

    /*
    Ident (LSBRACKET exp RSBRACKET)*
     */
    @Override
    public Void visitLVal(SysY2022Parser.LValContext ctx) {
        if (!constTag) {
            if (ctx.LSBRACKET(0)!=null) {
                ArrayList<Value> indexStack1 = new ArrayList<>();
                for (SysY2022Parser.ExpContext i : ctx.exp()) {
                    visit(i);
                    indexStack1.add(temValue);
                }
                temValue = getValue(ctx.Ident().getText());
                Value addr = f.buildGEPInst(curBlock, temValue, indexStack1);
                temValue = f.buildLoadInst(curBlock, addr);
            } else {
                temValue = getValue(ctx.Ident().getText());
                temValue = f.buildLoadInst(curBlock, temValue);
            }
        } else {
            String name = ctx.Ident().getText();
            if (ctx.exp() != null) {
                for (SysY2022Parser.ExpContext i : ctx.exp()) {
                    visit(i);
                    name += f.getConstantInt((int)saveValue).getValue();
                    name += ";";
                }
            }
            saveValue = getConstValue(name);
        }

        return null;
    }

    /*
    primaryExp:
        (LPAREN exp RPAREN)
        | lVal
        | number
        ;
     */
    @Override
    public Void visitPrimaryExp(SysY2022Parser.PrimaryExpContext ctx) {
        if (ctx.exp() != null) {
            visit(ctx.exp());
        } else if (ctx.number() != null) {
            visit(ctx.number());
        } else {
            visit(ctx.lVal());
        }

        return null;
    }

    /*IntConst
        | FloatConst
        ;*/
    @Override
    public Void visitNumber(SysY2022Parser.NumberContext ctx) {
        if (ctx.IntConst() != null) {
            if (constTag) {
                String s =ctx.IntConst().getText();
                if (s.length() >=2 && s.charAt(0) == '0' && (s.charAt(1) == 'x' || s.charAt(1) == 'X')){
                    saveValue = Integer.parseInt(s.substring(2),16);
                }else {
                    saveValue = Integer.parseInt(ctx.IntConst().getText());
                }
            } else {
                String s =ctx.IntConst().getText();
                if (s.length() >=2 && s.charAt(0) == '0' && (s.charAt(1) == 'x' || s.charAt(1) == 'X')){
                    temValue = f.getConstantInt(Integer.parseInt(s.substring(2),16));
                }else {
                    temValue = f.getConstantInt(Integer.parseInt(ctx.IntConst().getText()));
                }
            }
        } else {
            if (constTag) {
                saveValue = Double.parseDouble(ctx.FloatConst().getText());
            } else
                temValue = f.getConstantFloat((float) Double.parseDouble(ctx.FloatConst().getText()));
        }
        return null;
    }


    /*
     unaryExp:
        primaryExp
        | (Ident LPAREN (funcRParams)? RPAREN)
        | (unaryOp unaryExp)
     */
    @Override
    public Void visitUnaryExp(SysY2022Parser.UnaryExpContext ctx) {
        //这里漏了函数参数的情况 之后迭代
        if (ctx.primaryExp() != null) {
            visit(ctx.primaryExp());
        } else if (ctx.unaryExp()!=null) {
            if (ctx.unaryOp().PLUS() != null) {
                visit(ctx.unaryExp());
            } else if (ctx.unaryOp().SUB() != null) {
                visit(ctx.unaryExp());
                if (!constTag) {
                    temValue = f.buildBinaryInst(curBlock, OP.Sub, new ConstantInteger(0), temValue);
                }
                else {
                    saveValue = - saveValue;
                }
            } else {
                visit(ctx.unaryExp());
                temValue = f.buildNotInst(curBlock, temValue);
            }
        } else {
            temList = new ArrayList<>();
            if (ctx.funcRParams() != null) visit(ctx.funcRParams());
            temValue = f.buildCallInst(curBlock, (Function) getValue(ctx.Ident().getText()), temList);
            //得到temlist
        }
        return null;
    }


    /*
     useless
     */
    @Override
    public Void visitUnaryOp(SysY2022Parser.UnaryOpContext ctx) {
        return visitChildren(ctx);
    }

    /*
    funcRParams:
        exp (COMMA exp)*
        ;
     */
    @Override
    public Void visitFuncRParams(SysY2022Parser.FuncRParamsContext ctx) {
        temList = new ArrayList<>();
        for (SysY2022Parser.ExpContext i : ctx.exp()) {
            visit(i);
            temList.add(temValue);
        }
        return null;
    }


    /*
    mulExp:
        unaryExp
        | (unaryExp (MULT | DIV | MOD) mulExp)
        ;
     */
    @Override
    public Void visitMulExp(SysY2022Parser.MulExpContext ctx) {
        if (!constTag) {
            Value keep = temValue;
            OP keepOp = temOp;
            temValue = null;
            visit(ctx.unaryExp());
            //如果是第一个 则直接visit得到temvalue 否则用保留的temvalue和op进行计算
            if (keep != null) {
                temValue = f.buildBinaryInst(curBlock, keepOp, keep, temValue);
            }
            //tem value 更新 现在为当前结果
        } else {
            Double value = saveValue;
            OP keepOp = saveOp;
            ;
            saveValue = 0;
            visit(ctx.unaryExp());
            if (value != 0) {
                saveValue = calculate(value, saveValue, keepOp);
            }
        }
        if (ctx.mulExp() != null) {
            if (ctx.MULT() != null) {
                saveOp = OP.Mul;
                temOp =OP.Mul;
            } else if (ctx.DIV() != null) {
                saveOp = OP.Div;
                temOp =OP.Div;
            } else {
                saveOp = OP.Mod;
                temOp = OP.Mod;
            }
            visit(ctx.mulExp());//继续递归下去
        }
        return null;
    }


    private boolean check(SysY2022Parser.MulExpContext a,SysY2022Parser.MulExpContext b) {
        Value value = temValue;
        boolean keep = constTag;
        constTag = false;
        temValue = null;
        visit(a);
        Value aa = temValue;
        temValue = null;
        visit(b);
        Value bb = temValue;

        temValue = value;
        constTag = keep;
        return aa.toString().equals(bb.toString());
    }
    /*addExp:
        mulExp
        | (mulExp (PLUS | SUB) addExp)
       */
    @Override
    public Void visitAddExp(SysY2022Parser.AddExpContext ctx) {
        if (!constTag) {
            Value keep = temValue;
            OP keepOp = temOp;
            if (temValue == null && ctx.addExp()!=null) {
                if (check(ctx.mulExp(),ctx.addExp().mulExp())) {
                    int mul = 0;
                    SysY2022Parser.MulExpContext mode = ctx.mulExp();
                    SysY2022Parser.AddExpContext next = ctx.addExp();
                    SysY2022Parser.AddExpContext now =ctx;
                    while (next!=null && check(next.mulExp(),mode) && now.PLUS()!=null){
                        mul++;
                        now = next;
                        next = next.addExp();
                    }
                    temValue = null;
                    visit(mode);
                    temValue = f.buildBinaryInst(curBlock,OP.Mul,temValue,f.getConstantInt(mul));
                    temOp = now.PLUS() == null ? OP.Sub : OP.Add;
                    if (next!= null) {
                        visit(next);
                    }

                    return null;
                }
            }
            temValue = null;
            visit(ctx.mulExp());
            //如果是第一个 则直接visit得到temvalue 否则用保留的temvalue和op进行计算
            if (keep != null) {
                temValue = f.buildBinaryInst(curBlock, keepOp, keep, temValue);
            }
            //tem value 更新 现在为当前结果
            if (ctx.addExp() != null) {
                temOp = ctx.PLUS() == null ? OP.Sub : OP.Add;
                visit(ctx.addExp());//继续递归下去
            }
        } else {
            Double value = saveValue;
            OP keepOp = saveOp;
            ;
            saveValue = 0;
            visit(ctx.mulExp());
            if (value != 0) {
                saveValue = calculate(value, saveValue, keepOp);
            }
            if (ctx.addExp() != null) {
                saveOp = ctx.PLUS() == null ? OP.Sub : OP.Add;
                visit(ctx.addExp());//继续递归下去
            }
        }

        return null;
    }

    /*
     relExp:
        addExp
        | (addExp (LT | GT | LTOE | GTOE) relExp)
        ;
     */
    @Override
    public Void visitRelExp(SysY2022Parser.RelExpContext ctx) {
        Value keep = temValue;
        OP keepOp = temOp;
        temValue = null;
        visit(ctx.addExp());
        if (keep != null) {
            temValue = f.buildBinaryInst(curBlock, keepOp, keep, temValue);
        }
        if (ctx.relExp() != null) {
            if (ctx.LT() != null) {
                temOp = OP.Lt;
                visit(ctx.relExp());
            } else if (ctx.GT() != null) {
                temOp = OP.Gt;
                visit(ctx.relExp());
            } else if (ctx.LTOE() != null) {
                temOp = OP.Le;
                visit(ctx.relExp());
            } else if (ctx.GTOE() != null) {
                temOp = OP.Ge;
                visit(ctx.relExp());
            }
        }
        return null;
    }

    /*
    eqExp:
        relExp
        | (relExp (EQUALS | NEQUALS) eqExp)
        ;
     */
    @Override
    public Void visitEqExp(SysY2022Parser.EqExpContext ctx) {
        Value keep = temValue;
        OP keepOp = temOp;
        temValue = null;
        visit(ctx.relExp());
        if (keep != null) {
            temValue = f.buildBinaryInst(curBlock, keepOp, keep, temValue);
        }
        if (ctx.eqExp() != null) {
            if (ctx.EQUALS() != null) {
                temOp = OP.Eq;
                visit(ctx.eqExp());
            } else if (ctx.NEQUALS() != null) {
                temOp = OP.Ne;
                visit(ctx.eqExp());
            }
        }
        return null;
    }

    /*
    lAndExp:
        eqExp
        | (eqExp AND lAndExp)
        ;/
     */
    @Override
    public Void visitLAndExp(SysY2022Parser.LAndExpContext ctx) {
        Value keep = temValue;
        OP keepOp = temOp;
        temValue = null;
        visit(ctx.eqExp());
        if (keep != null) {
            temValue = f.buildBinaryInst(curBlock, keepOp, keep, temValue);
        }
        if (ctx.AND() != null) {
            temOp = OP.And;
            visit(ctx.lAndExp());
        }
        return null;
    }

    /*
    lOrExp:
        lAndExp
        | (lAndExp OR lOrExp)
        ;
     */
    @Override
    public Void visitLOrExp(SysY2022Parser.LOrExpContext ctx) {
        Value keep = temValue;
        OP keepOp = temOp;
        temValue = null;
        visit(ctx.lAndExp());
        if (keep != null) {
            temValue = f.buildBinaryInst(curBlock, keepOp, keep, temValue);
        }
        if (ctx.OR() != null) {
            temOp = OP.Or;
            visit(ctx.lOrExp());
        }
        return null;
    }


    /*constExp:
        addExp
        ;*/
    @Override
    public Void visitConstExp(SysY2022Parser.ConstExpContext ctx) {
        constTag = true;
        saveValue = 0;
        visitChildren(ctx);
        constTag = false;
        return null;
    }


}
