// Main.java
package VISITOR;

import IR.MyModule;
import backend.CodeGenModule;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import pass.PassModule;
import pass.ir.Mem2Reg;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        CharStream inputStream = CharStreams.fromStream(System.in); // 获取输入流
        SysY2022Lexer lexer = new SysY2022Lexer(inputStream);
        CommonTokenStream tokenStream = new CommonTokenStream(lexer); // 词法分析获取 token 流
        SysY2022Parser parser = new SysY2022Parser(tokenStream);
        ParseTree tree = parser.compUnit();
        ; // 获取语法树的根节点
        Visitor visitor = new Visitor();
        visitor.visit(tree);
        Mem2Reg mem2Reg = new Mem2Reg();
        // mem2Reg.run(MyModule.getInstance());
        // MyModule.getInstance()
        System.out.println(MyModule.getInstance().toString());
        //System.out.println(tree.toStringTree(parser)); // 打印字符串形式的语法树
        //PassModule.getInstance().runIRPasses();

        //CodeGenModule.getInstance().loadIR();
        //CodeGenModule.getInstance().run();

        //PassModule.getInstance().runMCPasses();
        //System.out.println(CodeGenModule.getInstance().genArmv7());
    }
}