package config;

import java.util.Arrays;

public class Config {
    public static String fileInPath = "test/functional/03_arr_defn2.sy";
    public static String fileOutPath = "test/stdout.txt";
    public static String fileOutPathWithOutBackend = "test/stdout.ll";
    public static boolean debug = true;

    public static boolean runMem2Reg = true;

    public static boolean runRegAllocator = false;

    public static void init(String[] args) {
        var iter = Arrays.stream(args).iterator();
        while (iter.hasNext()) {
            String arg = iter.next();
            if (arg.endsWith(".sy")) {
                fileInPath = arg;
            } else if (arg.equals("-o")) {
                fileOutPath = iter.next();
            }
        }
    }
}
