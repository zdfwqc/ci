package backend;

import IR.MyModule;
import IR.Values.BasicBlock;
import IR.Values.ConstantArray;
import IR.Values.ConstantFloat;
import IR.Values.ConstantInteger;
import IR.Values.Function;
import IR.Values.GlobalVariable;
import IR.Values.Value;
import IR.Values.instructions.Instruction;
import IR.Values.instructions.mem.AllocaInst;
import IR.Values.instructions.mem.LoadInst;
import IR.Values.instructions.mem.Phi;
import IR.types.ArrayType;
import IR.types.FloatingType;
import IR.types.IntegerType;
import IR.types.PointerType;
import backend.armv7.MCBlock;
import backend.armv7.MCFunction;
import backend.armv7.instructions.basic.InstrType;
import backend.armv7.instructions.basic.MCInstruction;
import backend.armv7.instructions.branch.Jump;
import backend.armv7.instructions.loadstore.Load;
import backend.armv7.instructions.loadstore.Move;
import backend.armv7.instructions.other.Comment;
import backend.factory.MCInstructionFactory;
import backend.factory.OperandFactory;
import backend.imm.IntImm;
import backend.reg.RegularReg;
import backend.reg.VirtualReg;
import config.Config;
import utils.IList;
import utils.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Stack;

import static backend.imm.IntImm.ableToEncode;

public class CodeGenModule {
    private static final CodeGenModule instance = new CodeGenModule();
    
    private MyModule irModule;
    private final ArrayList<MCFunction> functions = new ArrayList<>();
    private final HashMap<BasicBlock, MCBlock> blockMap = new HashMap<>();
    private final HashMap<Value, VirtualReg> irMap = new HashMap<>();
    private final HashMap<Function, MCFunction> funcMap = new HashMap<>();
    
    private final HashMap<LoadInst, AllocaInst> loadToAlloca = new HashMap<>();
    private final HashMap<AllocaInst, Operand> allocaToStore = new HashMap<>();
    private final HashMap<Pair<GlobalVariable, MCFunction>, Load> globalMap = new HashMap<>();
    
    private HashMap<Pair<MCBlock, MCBlock>, ArrayList<MCInstruction>> mvInsert =
        new HashMap<>();
    
    int mcOffset = 0;
    
    private final boolean debug = Config.debug;
    
    private CodeGenModule() {
        irModule = null;
    }
    
    public void loadIR() {
        irModule = MyModule.getInstance();
        OperandFactory.getInstance().loadModule(this);
        MCInstructionFactory.getInstance().loadModule(this);
    }
    
    public static CodeGenModule getInstance() {
        return instance;
    }
    
    public void addOffset(int off) {
        mcOffset += off;
    }
    
    public void run() {
        for (GlobalVariable gval : irModule.getGlobalVars()) {
            VirtualReg reg = new VirtualReg(gval.getName());
            reg.setGlobal(true);
            irMap.put(gval, reg);
        }
        
        // 添加所有的function
        for (IList.INode<Function, MyModule> node : irModule.getFunctions()) {
            Function f = node.getValue();
            funcMap.put(f, new MCFunction(f.getName()));
        }
        
        for (IList.INode<Function, MyModule> fnode : irModule.getFunctions()) {
            Function f = fnode.getValue();
            if (f.isLibraryFunction()) {
                continue;
            }
            
            MCFunction mf = funcMap.get(f);
            functions.add(mf);
            
            for (IList.INode<BasicBlock, Function> bnode : f.getBBList()) {
                // 添加所有的block
                blockMap.put(bnode.getValue(), new MCBlock(mf));
            }
            
            for (IList.INode<BasicBlock, Function> bnode : f.getBBList()) {
                MCBlock mb = blockMap.get(bnode.getValue());
                bnode.getValue().getPredecessors().forEach(b -> mb.addPredBlock(blockMap.get(b)));
            }
            
            // TODO: 循环处理
            
            traverseBasicBlock(f.getBBList().getHead().getValue(), f);
            handlePhi(f);
            
            analysisBlock(blockMap.get(f.getBBList().getHead().getValue()), mf, new HashSet<>());
        }
        
    }
    
    private void analysisBlock(MCBlock now, MCFunction mf, HashSet<MCBlock> vis) {
        if (vis.contains(now)) {
            return;
        }
        vis.add(now);
        mf.insertBlockAtListEnd(now);
        
        if (!now.hasSucc()) {
            return;
        }
        
        Pair<MCBlock, MCBlock> truePair = new Pair<>(now, now.getTrueBlock());
        Pair<MCBlock, MCBlock> falsePair = new Pair<>(now, now.getFalseBlock());
        
        if (!now.hasFalseSucc()) {
            MCInstruction lastInstr = now.getInstrList().getTail().getValue();
            lastInstr.getNode().removeFromList();   // 如果单一后继可以取消跳转指令
            
            if (needInsertMove(truePair)) {
                for (MCInstruction i : mvInsert.get(truePair)) {
                    i.insertBlockAtEnd(now);
                }
            }
            
            if (vis.contains(now.getTrueBlock())) {
                lastInstr.getNode().insertListEnd(now.getInstrList());
            }
            
            analysisBlock(now.getTrueBlock(), mf, vis);
        } else {
            if (!vis.contains(now.getTrueBlock()) ||
                (vis.contains(now.getTrueBlock()) && vis.contains(now.getFalseBlock()) &&
                    !needInsertMove(truePair) && needInsertMove(falsePair))) {
                now.swapSucc();
                now.getInstrList().getTail().getValue().revCond();
                
                truePair = new Pair<>(now, now.getTrueBlock());
                falsePair = new Pair<>(now, now.getFalseBlock());
            }
            
            if (mvInsert.containsKey(truePair)) {
                if (now.getTrueBlock().getPredBlocks().size() == 1) {
                    for (MCInstruction i : mvInsert.get(truePair)) {
                        i.insertBlockAtEntry(now);
                    }
                } else {
                    // 新建块处理insert
                    MCBlock newBlock = new MCBlock(mf);
                    for (MCInstruction i : mvInsert.get(truePair)) {
                        i.insertBlockAtEntry(newBlock);
                    }
                    new Jump(newBlock, now.getTrueBlock());
                    now.getTrueBlock().addPredBlock(newBlock);
                    now.getTrueBlock().removePredBlock(now);
                    newBlock.setTrueBlock(now.getTrueBlock());
                    newBlock.addPredBlock(now);
                    now.setTrueBlock(newBlock);
                    
                    Jump br = (Jump) now.getInstrList().getTail().getValue();
                    br.setTarget(newBlock);
                }
            }
            
            // 如果需要跳转到false块，那么在br后直接插入消phi指令即可
            if (needInsertMove(falsePair)) {
                for (MCInstruction i : mvInsert.get(falsePair)) {
                    i.insertBlockAtEnd(now);
                }
            }
            if (!vis.contains(now.getFalseBlock())) {
                new Jump(now, now.getFalseBlock());
            }
            
            analysisBlock(now.getFalseBlock(), mf, vis);
            analysisBlock(now.getTrueBlock(), mf, vis);
        }
        
    }
    
    private boolean needInsertMove(Pair<MCBlock, MCBlock> p) {
        return !mvInsert.getOrDefault(p, new ArrayList<>()).isEmpty();
    }
    
    public String genArmv7() {
        String text = "";
        text += ".arch armv7ve\n";
        text += ".text\n";
        
        // 函数
        for (MCFunction mf : functions) {
    
            text += "\n.global\t" + mf + "\n";
            text += mf + ":\n";
            
            // push
            if (mf.isUsedLr() || !mf.getUsedSavedRegs().isEmpty()) {
                text += "\tpush\t{";
                for (int i = 0; i < mf.getUsedSavedRegs().size(); i++) {
                    text += mf.getUsedSavedRegs().get(i);
                    if (i != mf.getUsedSavedRegs().size() - 1) {
                        text += ",\t";
                    }
                }
                text += "}\n";
                mcOffset += 4;
            }
            
            // 函数调用stack
            if (mf.getStackSize() != 0) {
                InstrType instr = ableToEncode(-mf.getStackSize()) ? InstrType.ADD : InstrType.SUB;
                IntImm imm = ableToEncode(-mf.getStackSize()) ? new IntImm(-mf.getStackSize()) :
                    new IntImm(mf.getStackSize());
                if (ableToEncode(-mf.getStackSize()) || ableToEncode(mf.getStackSize())) {
                    text += "\t" + instr + "\tsp,\tsp,\t" + imm + "\n";
                    addOffset(1);
                } else {
                    text += "\tLDR\tr5\t" + imm + "\n";
                    text += "\t" + instr + "\tsp,\tsp,\tr5\n";
                    addOffset(2);
                }
            }
            
            // 安排基本块
            for (var mbEntry : mf.getBlockList()) {
                MCBlock mb = mbEntry.getValue();
                text += mb + ":\n";
                for (var mcEnrty : mb.getInstrList()) {
                    text += mcEnrty.getValue();
                }
                text += "\n";
            }
            
            // TODO:全局寄存器?
        }
        
        // 全局变量声明
        if (irModule.getGlobalVars().size() != 0) {
            text += "\n\n.data\n";
            text += ".align 4\n";
            
            for (GlobalVariable gv : irModule.getGlobalVars()) {
                text += ".global\t" + irMap.get(gv).getName() + "\n";
                text += irMap.get(gv).getName() + ":\n";
                
                PointerType pt = (PointerType) gv.getType();
                if (pt.getTargetType() instanceof IntegerType) {
                    text += "\t.word\t" + ((ConstantInteger) gv.getValue()).getValue() + "\n";
                } else if(pt.getTargetType() instanceof FloatingType){
                    text += "\t.long\t" + ((ConstantInteger) gv.getValue()).getValue() + "\n";
                }else if (pt.getTargetType() instanceof ArrayType) {
                    // 全局变量未初始化
                    if (gv.getValue() == null) {
                        int size = 4;
                        for (var d : ((ArrayType) pt.getTargetType()).getDim()) {
                            size *= d;
                        }
                        text += "\t.zero\t" + size + "\n";
                    }
                    // 全局变量部分初始化
                    else {
                        ArrayList<Value> initValues = ((ConstantArray) gv.getValue()).getArr();
                        int pre = ((ConstantInteger) initValues.get(0)).getValue();
                        int count = 0;
    
                        for (var vEntry : initValues) {
                            int v = ((ConstantInteger) vEntry).getValue();
                            if (v == pre) {
                                count++;
                            } else {
                                if (count == 1) {
                                    text += "\t.word\t" + pre + "\n";
                                } else if (pre == 0) {
                                    text += "\t.zero\t" + count * 4 + "\n";
                                } else {
                                    text += "\t.fill\t" + count + ",\t4,\t" + pre + "\n";
                                }
                                pre = v;
                                count = 1;
                            }
                        }
    
                        if (count == 1) {
                            text += "\t.word\t" + pre + "\n";
                        } else if (pre == 0) {
                            text += "\t.zero\t" + count * 4 + "\n";
                        } else {
                            text += "\t.fill\t" + count + ",\t4,\t" + pre + "\n";
                        }
                    }
                }
            }
        }
        
        return text;
    }
    
    private void handlePhi(Function f) {
        IList<BasicBlock, Function> blockList = f.getBBList();
        mvInsert = new HashMap<>();
        for (IList.INode<BasicBlock, Function> bnode : blockList) {
            BasicBlock bb = bnode.getValue();
            if (bb.getPredecessors().size() <= 1) {
                continue;
            }
            
            // init
            MCBlock mb = blockMap.get(bb);
            for (MCBlock pre : mb.getPredBlocks()) {
                mvInsert.put(new Pair<>(pre, mb), new ArrayList<>());
            }
            
            for (int i = 0; i < bb.getPredecessors().size(); i++) {
                IList<Instruction, BasicBlock> irList = bb.getList();
                HashMap<Operand, Operand> edges = new HashMap<>();
                // map init
                for (var irEnrty : irList) {
                    Instruction ir = irEnrty.getValue();
                    if (ir instanceof Phi) {
                        Operand dst = OperandFactory.getInstance().parseValue(ir, mb, f);
                        Operand rhs =
                            OperandFactory.getInstance().parseValue(ir.getOperands().get(i), mb, f);
                        edges.put(dst, rhs);
                    } else {
                        break;
                    }
                }
                // map process
                ArrayList<MCInstruction> list =
                    mvInsert.get(new Pair<>(blockMap.get(bb.getPredecessors().get(i)), mb));
                while (!edges.isEmpty()) {
                    Stack<Operand> st = new Stack<>();
                    Operand now = edges.keySet().iterator().next();
                    
                    while (edges.containsKey(now) && !st.contains(now)) {
                        st.push(now);
                        now = edges.get(now);
                    }
                    
                    if (edges.containsKey(now)) { // 环
                        VirtualReg v = new VirtualReg(funcMap.get(f));
                        Operand last = v;
                        while (st.contains(now)) {
                            Operand rhs = st.pop();
                            Move mv = new Move(last, rhs);
                            list.add(mv);
                            edges.remove(rhs);
                            last = rhs;
                        }
    
                        Move mv = new Move(last, v);
                        list.add(mv);
                    }
                    
                    Operand rhs = now;
                    while (!st.isEmpty()) {
                        Operand dst = st.pop();
                        Move mv = new Move(dst, rhs);
                        list.add(0, mv);
                        rhs = dst;
                        edges.remove(dst);
                    }
                }
            }
        }
    }
    
    private final HashSet<BasicBlock> vis = new HashSet<>();
    
    private void traverseBasicBlock(BasicBlock now, Function f) {
        if (!vis.contains(now)) {
            vis.add(now);
            processBasicBlock(now, f);
            now.getSuccessors().forEach(b -> traverseBasicBlock(b, f));
        }
    }
    
    private void processBasicBlock(BasicBlock bblock, Function f) {
        MCBlock mb = blockMap.get(bblock);
        for (IList.INode<Instruction, BasicBlock> instrNode : bblock.getList()) {
            Instruction ir = instrNode.getValue();
            if (debug) {
                new Comment(mb, ir.toString());
            }
            MCInstructionFactory.getInstance().parseIR(ir, mb, f);
        }
    }
    
    public void updateStack(MCFunction mf) {
        var regs = mf.getAllRegId();
        for (int i = 4; i <= 12; i++) {
            for (int idx : regs) {
                if (i == idx) {
                    mf.getUsedSavedRegs().add(new RegularReg(i));
                }
            }
        }
        
        if (regs.contains(14)) {
            mf.getUsedSavedRegs().add(new RegularReg(14));
            mf.setUsedLr(true);
        }
        
        mf.getArgsMove().forEach(mv -> mv.setRhs(new IntImm(((IntImm) mv.getRhs()).getImm()
            + mf.getStackSize() + 4 * regs.size())));
    }
    
    // get
    
    public HashMap<Value, VirtualReg> getIrMap() {
        return irMap;
    }
    
    public MyModule getIrModule() {
        return irModule;
    }
    
    public HashMap<BasicBlock, MCBlock> getBlockMap() {
        return blockMap;
    }
    
    
    public HashMap<Function, MCFunction> getFuncMap() {
        return funcMap;
    }
    
    public HashMap<LoadInst, AllocaInst> getLoadToAlloca() {
        return loadToAlloca;
    }
    
    public HashMap<AllocaInst, Operand> getAllocaToStore() {
        return allocaToStore;
    }
    
    public HashMap<Pair<GlobalVariable, MCFunction>, Load> getGlobalMap() {
        return globalMap;
    }
    
    public ArrayList<MCFunction> getFunctions() {
        return functions;
    }
}
