package backend.imm;

import backend.Operand;

import java.util.Objects;

public class IntImm implements Operand {
    private final Integer imm;
    
    public IntImm(Integer imm) {
        this.imm = imm;
    }
    
    public String getName() {
        return '#' + String.valueOf(imm);
    }
    
    public Integer getImm() {
        return imm;
    }
    
    public static Boolean ableToEncode(int imm) {
        int n = imm;
        for (int ror = 0; ror < 16; ror++) {
            if ((n & (~0xff)) == 0) {
                return true;
            }
            n = n >>> 30 | n << 2;
        }
        
        return false;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        IntImm immediate = (IntImm) o;
        return Objects.equals(imm, immediate.imm);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(imm);
    }
    
    @Override
    public String toString() {
        return '#' + String.valueOf(imm);
    }
    
    @Override
    public boolean needColor() {
        return false;
    }
    
    @Override
    public boolean isPrecolored() {
        return false;
    }
    
    @Override
    public boolean isFloat() {
        return false;
    }
}
