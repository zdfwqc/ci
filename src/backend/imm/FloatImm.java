package backend.imm;

import backend.Operand;

import static java.lang.Math.abs;

public class FloatImm implements Operand {
    private final Float imm;
    
    public FloatImm(Float imm) {
        this.imm = imm;
    }
    
    public Float getImm() {
        return imm;
    }
    
    public static boolean ableToEncode(float imm) {
        float eps = 1e-14f;
        float a = imm * 128;
        
        for (int r = 0; r < 8; r++) {
            for (int n = 16; n < 32; n++) {
                if ((abs((n * (1 << (7 - r)) - a)) < eps) ||
                    (abs((n * (1 << (7 - r)) + a)) < eps)) {
                    return true;
                }
            }
        }
        return false;
    }
    
    @Override
    public String getName() {
        return '#' + String.valueOf(imm);
    }
    
    @Override
    public boolean needColor() {
        return false;
    }
    
    @Override
    public boolean isPrecolored() {
        return false;
    }
    
    @Override
    public boolean isFloat() {
        return true;
    }
    
    @Override
    public String toString() {
        return getName();
    }
}
