package backend.factory;

import IR.MyModule;
import IR.Values.BasicBlock;
import IR.Values.Function;
import IR.Values.GlobalVariable;
import IR.Values.Value;
import IR.Values.instructions.mem.AllocaInst;
import IR.Values.instructions.mem.LoadInst;
import backend.CodeGenModule;
import backend.Operand;
import backend.armv7.MCBlock;
import backend.armv7.MCFunction;
import backend.armv7.instructions.loadstore.Load;
import backend.reg.VirtualReg;
import utils.Pair;

import java.util.HashMap;

public class Factory {
    protected CodeGenModule cgm = null;
    protected MyModule irModule = null;
    protected HashMap<BasicBlock, MCBlock> blockMap = null;
    protected HashMap<Value, VirtualReg> irMap = null;
    protected HashMap<Function, MCFunction> funcMap = null;
    
    protected HashMap<LoadInst, AllocaInst> loadToAlloca = new HashMap<>();
    protected HashMap<AllocaInst, Operand> allocaToStore = new HashMap<>();
    protected HashMap<Pair<GlobalVariable, MCFunction>, Load> globalMap = new HashMap<>();
    
    
    public void loadModule(CodeGenModule codeGenModule) {
        this.cgm = codeGenModule;
        this.irMap = cgm.getIrMap();
        this.irModule = cgm.getIrModule();
        this.blockMap = cgm.getBlockMap();
        this.funcMap = cgm.getFuncMap();
        
        this.loadToAlloca = cgm.getLoadToAlloca();
        this.allocaToStore = cgm.getAllocaToStore();
        this.globalMap = cgm.getGlobalMap();
    }
}
