package backend.factory;

import IR.Values.BasicBlock;
import IR.Values.ConstantInteger;
import IR.Values.Function;
import IR.Values.Value;
import IR.Values.instructions.BinaryInst;
import IR.Values.instructions.Instruction;
import IR.Values.instructions.OP;
import IR.Values.instructions.mem.AllocaInst;
import IR.Values.instructions.mem.LoadInst;
import IR.Values.instructions.mem.StoreInst;
import IR.types.ArrayType;
import IR.types.FunctionType;
import IR.types.PointerType;
import IR.types.Type;
import backend.Operand;
import backend.armv7.MCBlock;
import backend.armv7.MCFunction;
import backend.armv7.instructions.CondType;
import backend.armv7.instructions.DataType;
import backend.armv7.instructions.Shift;
import backend.armv7.instructions.ShiftType;
import backend.armv7.instructions.basic.InstrType;
import backend.armv7.instructions.branch.Call;
import backend.armv7.instructions.branch.Compare;
import backend.armv7.instructions.branch.Jump;
import backend.armv7.instructions.branch.Return;
import backend.armv7.instructions.dataprocess.Binary;
import backend.armv7.instructions.dataprocess.LongMul;
import backend.armv7.instructions.dataprocess.Mla;
import backend.armv7.instructions.loadstore.Load;
import backend.armv7.instructions.loadstore.Move;
import backend.armv7.instructions.loadstore.Store;
import backend.armv7.instructions.vfg.VBinary;
import backend.armv7.instructions.vfg.VConvert;
import backend.imm.IntImm;
import backend.reg.RegularReg;
import backend.reg.VirtualReg;
import utils.Pair;
import utils.Triple;

import java.util.ArrayList;
import java.util.HashMap;

import static backend.imm.IntImm.ableToEncode;

public class MCInstructionFactory extends Factory {
    private static final MCInstructionFactory Instance = new MCInstructionFactory();
    
    // 防止除法重复计算
    private final HashMap<Triple<Operand, Operand, MCBlock>, VirtualReg> divMap = new HashMap<>();
    
    private MCInstructionFactory() {
    }
    
    public static MCInstructionFactory getInstance() {
        return Instance;
    }
    
    public void parseIR(Instruction ir, MCBlock mb, Function f) {
        // 数据传送
        if (ir.getOp() == OP.Alloca) {
            parseAlloca(ir, mb, f);
        } else if (ir instanceof StoreInst) {
            parseStore((StoreInst) ir, mb, f);
        } else if (ir instanceof LoadInst) {
            parseLoad((LoadInst) ir, mb, f);
        } else if (ir.getOp() == OP.GEP) {
            parseGep(ir, mb, f);
        }
        // 数据处理相关
        else if (ir instanceof BinaryInst &&
            (((BinaryInst) ir).isAdd() || ((BinaryInst) ir).isSub())) {
            parseAddOrSub((BinaryInst) ir, mb, f);
        } else if (ir instanceof BinaryInst && ((BinaryInst) ir).isMul()) {
            parseMul((BinaryInst) ir, mb, f);
        } else if (ir instanceof BinaryInst && ((BinaryInst) ir).isDiv()) {
            parseDiv((BinaryInst) ir, mb, f);
        } else if (ir instanceof BinaryInst && ((BinaryInst) ir).isMod()) {
            parseMod((BinaryInst) ir, mb, f);
        } else if (ir instanceof BinaryInst && ((BinaryInst) ir).isShl()) {
            parseShl((BinaryInst) ir, mb, f);
        } else if (ir instanceof BinaryInst && ((BinaryInst) ir).isShr()) {
            parseShr((BinaryInst) ir, mb, f);
        }
        // 分支相关
        else if (ir.getOp() == OP.Call) {
            parseCall(ir, mb, f);
        } else if (ir.getOp() == OP.Br) {
            parseBr(ir, mb, f);
        } else if (ir.getOp() == OP.Ret) {
            parseRet(ir, mb, f);
        }
        // 浮点数相关
        else if (ir.getOp() == OP.FAdd || ir.getOp() == OP.FSub || ir.getOp() == OP.FMul
            || ir.getOp() == OP.FDiv || ir.getOp() == OP.FMod) {
            parseVBinary(ir, mb, f);
        } else if (ir.getOp() == OP.Itof || ir.getOp() == OP.Ftoi) {
            parseConvert(ir, mb, f);
        }
        // 未知指令
    }
    
    private void parseStore(StoreInst ir, MCBlock mb, Function f) {
        Value v = ir.getOperands().get(1);
        //如果store的指针是个二重指针
        if (((PointerType) (v).getType()).getTargetType().isPointerType()) {
            allocaToStore.put((AllocaInst) v,
                OperandFactory.getInstance().parseValue(ir.getOperands().get(0), mb, f));
            return;
        }
        
        Operand data =
            OperandFactory.getInstance().parseValueWithoutImm(ir.getOperands().get(0), mb, f);
        Operand addr = OperandFactory.getInstance().parseValue(ir.getOperands().get(1), mb, f);
        new Store(mb, data, addr, new IntImm(0));
    }
    
    private void parseLoad(LoadInst ir, MCBlock mb, Function f) {
        Value v = ir.getOperands().get(0);
        if (((PointerType) (v).getType()).getTargetType().isPointerType()) {
            loadToAlloca.put(ir, (AllocaInst) v);
            return;
        }
        
        Operand data = OperandFactory.getInstance().parseValue(ir, mb, f);
        Operand addr = OperandFactory.getInstance().parseValue(ir.getOperands().get(0), mb, f);
        new Load(mb, data, addr, new IntImm(0));
    }
    
    private void parseAlloca(Instruction ir, MCBlock mb, Function f) {
        if (((PointerType) (ir).getType()).getTargetType() instanceof PointerType) {
            return;
        }
        Type targetType = ((PointerType) ir.getType()).getTargetType();
        int size = 4;
        
        if (targetType instanceof ArrayType) {
            for (Integer dim : ((ArrayType) targetType).getDim()) {
                size *= dim;
            }
        }
        
        MCFunction mf = funcMap.get(f);
        new Binary(InstrType.ADD, mb,
            OperandFactory.getInstance().parseValue(ir, mb, f),
            new RegularReg("sp"),
            OperandFactory.getInstance().genIntImm(mf.getStackSize(), mb)
        );
        mf.addStackSize(size);
    }
    
    private void parseGep(Instruction ir, MCBlock mb, Function f) {
        PointerType pt = (PointerType) ir.getOperands().get(0).getType();
        int offsetNum;
        ArrayList<Integer> dims;
        if (pt.getTargetType() instanceof ArrayType) {
            offsetNum = ir.getOperands().size() - 1;
            dims = ((ArrayType) pt.getTargetType()).getDim();
        } else {
            offsetNum = 1;
            dims = new ArrayList<>();
        }
        
        
        Operand arr = OperandFactory.getInstance().parseValue(ir.getOperands().get(0), mb, f);
        int lastOff = 0;
        for (int i = 1; i <= offsetNum; i++) {
            int base = 4;
            if (pt.getTargetType() instanceof ArrayType) {
                for (int j = i - 1; j < dims.size() - 1; j++) {
                    base *= dims.get(j);
                }
            }
            
            
            Operand off = OperandFactory.getInstance().parseValue(
                ir.getOperands().get(i), mb, f
            );
            
            if (off instanceof IntImm) {
                int dimOff = ((IntImm) off).getImm() * base;
                lastOff += dimOff;
                if (i == offsetNum) {
                    Operand dst = OperandFactory.getInstance().parseValue(ir, mb, f);
                    if (lastOff == 0) {
                        new Move(mb, dst, arr);
                    } else {
                        Operand imm = OperandFactory.getInstance().genIntImm(lastOff, mb);
                        new Binary(InstrType.ADD, mb, dst, arr, imm);
                    }
                }
            } else if ((base & (base - 1)) == 0) {
                if (lastOff != 0) {
                    Operand imm = OperandFactory.getInstance().genIntImm(lastOff, mb);
                    new Binary(InstrType.ADD, mb, arr, arr, imm);
                }
                
                VirtualReg newArr = new VirtualReg(funcMap.get(f));
                Pair<Operand, Shift> pair =
                    analyseShift(off, new Shift(getCTZ(base), ShiftType.LSL), mb);
                new Binary(InstrType.ADD, mb, newArr, arr, pair.getFirst(), pair.getSecond());
                
            } else {
                if (lastOff != 0) {
                    Operand imm = OperandFactory.getInstance().genIntImm(lastOff, mb);
                    new Binary(InstrType.ADD, mb, arr, arr, imm);
                }
                VirtualReg baseReg = new VirtualReg(funcMap.get(f));
                new Move(mb, baseReg, OperandFactory.getInstance().genIntImm(base, mb));
                
                VirtualReg newArr = new VirtualReg(funcMap.get(f));
                new Mla(mb, newArr, baseReg, off, arr, true, false);
                arr = newArr;
            }
        }
    }
    
    private void parseAddOrSub(BinaryInst ir, MCBlock mb, Function f) {
        boolean lhsIsConst = ir.getOperands().get(0) instanceof ConstantInteger;
        boolean rhsIsConst = ir.getOperands().get(1) instanceof ConstantInteger;
        
        Operand dst = OperandFactory.getInstance().parseValue(ir, mb, f, true);
        
        if (lhsIsConst && !rhsIsConst) {
            Operand lhs =
                OperandFactory.getInstance().parseValue(ir.getOperands().get(1), mb, f, true);
            if (ir.isSub()) {
                new Binary(InstrType.RSB, mb, dst, lhs,
                    OperandFactory.getInstance().parseValue(ir.getOperands().get(0), mb, f, true));
            } else {
                int imm = ((ConstantInteger) ir.getOperands().get(0)).getValue();
                if (ableToEncode(imm)) {
                    new Binary(InstrType.ADD, mb, dst, lhs, new IntImm(imm));
                } else if (ableToEncode(-imm)) {
                    new Binary(InstrType.SUB, mb, dst, lhs, new IntImm(-imm));
                } else {
                    new Binary(InstrType.ADD, mb, dst, lhs, OperandFactory.getInstance()
                        .parseValue(ir.getOperands().get(0), mb, f, true));
                }
            }
        } else if (!lhsIsConst && rhsIsConst) {
            Operand lhs =
                OperandFactory.getInstance().parseValue(ir.getOperands().get(0), mb, f, true);
            int imm = ((ConstantInteger) ir.getOperands().get(1)).getValue();
            if (ableToEncode(imm)) {
                InstrType op = ir.isAdd() ? InstrType.ADD : InstrType.SUB;
                new Binary(op, mb, dst, lhs, new IntImm(imm));
            } else if (ableToEncode(-imm)) {
                InstrType op = ir.isAdd() ? InstrType.SUB : InstrType.ADD;
                new Binary(op, mb, dst, lhs, new IntImm(-imm));
            } else {
                InstrType op = ir.isAdd() ? InstrType.ADD : InstrType.SUB;
                new Binary(op, mb, dst, lhs,
                    OperandFactory.getInstance().parseValue(ir.getOperands().get(1), mb, f, true));
            }
        } else {
            InstrType op = ir.isAdd() ? InstrType.ADD : InstrType.SUB;
            new Binary(op, mb, dst,
                OperandFactory.getInstance().parseValueWithoutImm(ir.getOperands().get(0), mb, f),
                OperandFactory.getInstance().parseValue(ir.getOperands().get(1), mb, f, true));
        }
    }
    
    private void parseMul(BinaryInst ir, MCBlock mb, Function f) {
        boolean lhsIsConst = ir.getOperands().get(0) instanceof ConstantInteger;
        boolean rhsIsConst = ir.getOperands().get(1) instanceof ConstantInteger;
        Operand lhs;
        Operand rhs;
        
        if (lhsIsConst || rhsIsConst) {
            int imm;
            
            if (rhsIsConst) {
                lhs = OperandFactory.getInstance()
                    .parseValueWithoutImm(ir.getOperands().get(0), mb, f);
                imm = ((ConstantInteger) ir.getOperands().get(1)).getValue();
            } else {
                lhs = OperandFactory.getInstance()
                    .parseValueWithoutImm(ir.getOperands().get(1), mb, f);
                imm = ((ConstantInteger) ir.getOperands().get(0)).getValue();
            }
            
            int abs = imm >= 0 ? imm : -imm;
            Operand dst = OperandFactory.getInstance().parseValue(ir, mb, f);
            
            // abs  = 2^n
            if (abs == 0) {
                new Move(mb, dst, new IntImm(0));
            } else if ((abs & (abs - 1)) == 0) {
                if (imm > 0) {
                    new Move(mb, dst, lhs, new Shift(new IntImm(getCTZ(imm)), ShiftType.LSL));
                } else {
                    VirtualReg vreg = new VirtualReg();
                    funcMap.get(f).addVirtualReg(vreg);
                    new Move(mb, vreg, lhs, new Shift(new IntImm(getCTZ(imm)), ShiftType.LSL));
                    new Binary(InstrType.RSB, mb, dst, vreg, new IntImm(0));
                }
            } else if (((abs - 1) & (abs - 2)) == 0) {
                if (imm > 0) {
                    new Binary(InstrType.ADD, mb, dst, lhs, lhs,
                        new Shift(new IntImm(getCTZ(imm)), ShiftType.LSL));
                } else {
                    VirtualReg vreg = new VirtualReg();
                    funcMap.get(f).addVirtualReg(vreg);
                    new Binary(InstrType.ADD, mb, vreg, lhs, lhs,
                        new Shift(new IntImm(getCTZ(imm)), ShiftType.LSL));
                    new Binary(InstrType.RSB, mb, dst, vreg, new IntImm(0));
                }
            } else if (((abs + 1) & abs) == 0) {
                if (imm > 0) {
                    new Binary(InstrType.RSB, mb, dst, lhs, lhs,
                        new Shift(new IntImm(getCTZ(imm) + 1), ShiftType.LSL));
                } else {
                    new Binary(InstrType.SUB, mb, dst, lhs, lhs,
                        new Shift(new IntImm(getCTZ(imm) + 1), ShiftType.LSL));
                }
            } else { // 退化为一般情况
                lhs = OperandFactory.getInstance()
                    .parseValueWithoutImm(ir.getOperands().get(0), mb, f);
                rhs = OperandFactory.getInstance()
                    .parseValueWithoutImm(ir.getOperands().get(1), mb, f);
                new Binary(InstrType.MUL, mb,
                    OperandFactory.getInstance().parseValue(ir, mb, f, true), lhs, rhs);
            }
        } else { // 一般情况
            lhs = OperandFactory.getInstance().parseValueWithoutImm(ir.getOperands().get(0), mb, f);
            rhs = OperandFactory.getInstance().parseValueWithoutImm(ir.getOperands().get(1), mb, f);
            new Binary(InstrType.MUL, mb, OperandFactory.getInstance().parseValue(ir, mb, f, true),
                lhs, rhs);
        }
    }
    
    private void parseDiv(BinaryInst ir, MCBlock mb, Function f) {
        boolean rhsIsConst = ir.getOperands().get(1) instanceof ConstantInteger;
        if (rhsIsConst) {
            int imm = ((ConstantInteger) ir.getOperands().get(1)).getValue();
            if (imm == 1) {
                irMap.put(ir, irMap.get(ir.getOperands().get(0)));
            } else {
                Operand lhs = OperandFactory.getInstance()
                    .parseValueWithoutImm(ir.getOperands().get(0), mb, f);
                Triple<Operand, Operand, MCBlock> div = new Triple<>(lhs, new IntImm(imm), mb);
                if (!divMap.containsKey(div)) {
                    Operand dst = OperandFactory.getInstance().parseValue(ir, mb, f);
                    divConstantOptimization(ir.getOperands().get(0), imm, (VirtualReg) dst, mb, f);
                } else {
                    irMap.put(ir, divMap.get(div));
                }
            }
        } else {
            Operand lhs =
                OperandFactory.getInstance().parseValueWithoutImm(ir.getOperands().get(0), mb, f);
            Operand rhs =
                OperandFactory.getInstance().parseValueWithoutImm(ir.getOperands().get(1), mb, f);
            
            Triple<Operand, Operand, MCBlock> div = new Triple<>(lhs, rhs, mb);
            if (!divMap.containsKey(div)) {
                Operand dst = OperandFactory.getInstance().parseValueWithoutImm(ir, mb, f);
                new Binary(InstrType.DIV, mb, dst, lhs, rhs);
            } else {
                irMap.put(ir, divMap.get(div));
            }
        }
    }
    
    // 仅处理 mod 2^n
    private void parseMod(BinaryInst ir, MCBlock mb, Function f) {
        if (ir.getOperands().get(1) instanceof ConstantInteger) {
            Operand lhs = OperandFactory.getInstance().parseValueWithoutImm(ir.getOperands().get(0),
                mb, f);
            Operand dst = OperandFactory.getInstance().parseValue(ir, mb, f);
            int imm = ((ConstantInteger) ir.getOperands().get(1)).getValue();
            int abs = imm > 0 ? imm : -imm;
            assert (abs & (abs - 1)) == 0;
            
            Triple<Operand, Operand, MCBlock> div = new Triple<>(lhs, new IntImm(imm), mb);
            if (divMap.containsKey(div)) {
                InstrType instr = imm > 0 ? InstrType.SUB : InstrType.ADD;
                new Binary(instr, mb, dst, lhs, divMap.get(div),
                    new Shift(getCTZ(abs), ShiftType.LSL));
            } else if (abs == 2) {
                MCFunction mf = funcMap.get(f);
                VirtualReg v2 = new VirtualReg(mf);
                new Binary(InstrType.ADD, mb, v2, lhs, lhs,
                    new Shift(32 - getCTZ(abs), ShiftType.LSR));
                
                VirtualReg v3 = new VirtualReg(mf);
                new Binary(InstrType.BIC, mb, v3, v2,
                    OperandFactory.getInstance().genIntImm(abs - 1, mb));
                
                new Binary(InstrType.SUB, mb, dst, lhs, v3);
            } else {
                MCFunction mf = funcMap.get(f);
                VirtualReg v1 = new VirtualReg(mf);
                new Move(mb, v1, lhs, new Shift(31, ShiftType.ASR));
                
                VirtualReg v2 = new VirtualReg(mf);
                new Binary(InstrType.ADD, mb, v2, lhs, v1,
                    new Shift(32 - getCTZ(abs), ShiftType.LSR));
                
                VirtualReg v3 = new VirtualReg(mf);
                new Binary(InstrType.BIC, mb, v3, v2,
                    OperandFactory.getInstance().genIntImm(abs - 1, mb));
                
                new Binary(InstrType.SUB, mb, dst, lhs, v3);
            }
        }
    }
    
    private void parseShl(BinaryInst ir, MCBlock mb, Function f) {
        Operand dst = OperandFactory.getInstance().parseValue(ir, mb, f);
        Operand src =
            OperandFactory.getInstance().parseValueWithoutImm(ir.getOperands().get(0), mb, f);
        Operand shiftNum =
            OperandFactory.getInstance().parseValue(ir.getOperands().get(1), mb, f, false);
        
        new Move(mb, dst, src, new Shift(shiftNum, ShiftType.LSL));
    }
    
    private void parseShr(BinaryInst ir, MCBlock mb, Function f) {
        Operand dst = OperandFactory.getInstance().parseValue(ir, mb, f);
        Operand src =
            OperandFactory.getInstance().parseValueWithoutImm(ir.getOperands().get(0), mb, f);
        
        Operand shiftNum =
            OperandFactory.getInstance().parseValue(ir.getOperands().get(1), mb, f, false);
        new Move(mb, dst, src, new Shift(shiftNum, ShiftType.ASR));
    }
    
    private void parseCall(Instruction ir, MCBlock mb, Function f) {
        ArrayList<Operand> useRegs = new ArrayList<>();
        int argNum = ir.getOperands().size() - 1;
        
        for (int i = 0; i < argNum; i++) {
            if (i <= 3) {
                Move mv = new Move(mb, new RegularReg(i), OperandFactory.getInstance()
                    .parseValue(ir.getOperands().get(i + 1), mb, f, true));
                useRegs.add(mv.getDst());
            } else {
                Operand vreg = OperandFactory.getInstance()
                    .parseValueWithoutImm(ir.getOperands().get(i + 1), mb, f);
                Operand offset = OperandFactory.getInstance().genIntImm((-(argNum - i) * 4), mb);
                new Store(mb, vreg, new RegularReg("sp"), offset);
            }
        }
        
        if (argNum > 4) {
            Operand imm = OperandFactory.getInstance().genIntImm((4 * (argNum - 4)), mb);
            new Binary(InstrType.SUB, mb, new RegularReg("sp"), new RegularReg("sp"), imm);
        }
        
        Call call = new Call(mb, funcMap.get((Function) ir.getOperands().get(0)));
        
        if (!((FunctionType) (ir.getOperands().get(0)).getType()).getRetType().isVoidType()) {
            new Move(mb,
                OperandFactory.getInstance().parseValue(ir, mb, f, true),
                new RegularReg("r0"));
        }
        
        useRegs.forEach(call::addUse);
        for (int i = 0; i < 4; i++) {
            call.addDef(new RegularReg(i));
        }
        call.addDef(new RegularReg("lr"));
    }
    
    private void parseBr(Instruction ir, MCBlock mb, Function f) {
        if (ir.getOperands().size() == 1) {
            new Jump(mb, blockMap.get((BasicBlock) ir.getOperands().get(0)));
            mb.setTrueBlock(blockMap.get((BasicBlock) ir.getOperands().get(0)));
        } else if (ir.getOperands().size() == 3) {
            MCBlock trueSucc = blockMap.get((BasicBlock) ir.getOperands().get(1));
            MCBlock falseSucc = blockMap.get((BasicBlock) ir.getOperands().get(2));
            // 常数判断
            if (ir.getOperands().get(0) instanceof ConstantInteger) {
                int cond = ((ConstantInteger) ir.getOperands().get(0)).getValue();
                if (cond != 0) {
                    new Jump(mb, trueSucc);
                } else {
                    new Jump(mb, falseSucc);
                }
            } else {
                CondType cond = analyseCond((BinaryInst) (ir.getOperands().get(0)), mb, f, false);
                
                new Jump(mb, cond, trueSucc);
                mb.setTrueBlock(trueSucc);
                mb.setFalseBlock(falseSucc);
            }
        } else {
            assert false;
        }
    }
    
    private void parseRet(Instruction ir, MCBlock mb, Function f) {
        if (ir.getOperands().size() == 1) {
            Operand rst =
                OperandFactory.getInstance().parseValue(ir.getOperands().get(0), mb, f, true);
            new Move(mb, new RegularReg("r0"), rst);
        }
        new Return(mb);
    }
    
    private void parseVBinary(Instruction ir, MCBlock mb, Function f) {
        InstrType instr = getVInstrType(ir);
        new VBinary(instr, mb,
            OperandFactory.getInstance().parseValue(ir, mb, f),
            OperandFactory.getInstance().parseValueWithoutImm(ir.getOperands().get(0), mb, f),
            OperandFactory.getInstance().parseValue(ir.getOperands().get(1), mb, f));
    }
    
    private void parseConvert(Instruction ir, MCBlock mb, Function f) {
        Operand dst = OperandFactory.getInstance().parseValue(ir, mb, f);
        Operand rhs =
            OperandFactory.getInstance().parseValueWithoutImm(ir.getOperands().get(0), mb, f);
        if (ir.getOp() == OP.Ftoi) {
            new VConvert(mb, dst, DataType.IntType, rhs, DataType.FloatType);
        } else if (ir.getOp() == OP.Itof) {
            new VConvert(mb, dst, DataType.FloatType, rhs, DataType.IntType);
        }
    }
    
    private InstrType getVInstrType(Instruction ir) {
        if (ir.getOp() == OP.FAdd) {
            return InstrType.VADD;
        } else if (ir.getOp() == OP.FSub) {
            return InstrType.VSUB;
        } else if (ir.getOp() == OP.FMul) {
            return InstrType.VMUL;
        } else if (ir.getOp() == OP.FDiv) {
            return InstrType.VDIV;
        } else if (ir.getOp() == OP.FMod) {
            return InstrType.VMOD;
        } else {
            return null;
        }
    }
    
    
    private CondType analyseCond(BinaryInst inst, MCBlock mb, Function f, boolean genMove) {
        CondType cond = parseCond(inst);
        Value l;
        Value r;
        if ((inst.getOperands().get(0) instanceof ConstantInteger)) {
            l = inst.getOperands().get(1);
            r = inst.getOperands().get(0);
            cond = CondType.mirrorReverseCond(cond);
        } else {
            l = inst.getOperands().get(0);
            r = inst.getOperands().get(1);
        }
        
        if (l instanceof BinaryInst && ((BinaryInst) l).isCond()) {
            analyseCond((BinaryInst) l, mb, f, true);
        } else if (r instanceof BinaryInst && ((BinaryInst) r).isCond()) {
            analyseCond((BinaryInst) r, mb, f, true);
        }
        
        Operand lhs = OperandFactory.getInstance().parseValueWithoutImm(l, mb, f);
        Operand rhs = OperandFactory.getInstance().parseValue(l, mb, f, true);
        new Compare(mb, cond, lhs, rhs);
        if (genMove) {
            Operand dst = OperandFactory.getInstance().parseValue(inst, mb, f, true);
            new Move(mb, cond, dst, new IntImm(1));
            new Move(mb, CondType.reverseCond(cond), dst, new IntImm(1));
        }
        return cond;
    }
    
    private Pair<Operand, Shift> analyseShift(Operand o, Shift shift, MCBlock mb) {
        if (o instanceof IntImm && !shift.isNone() && shift.getOperand() instanceof IntImm) {
            assert shift.getType() == ShiftType.LSL;
            int imm = ((IntImm) o).getImm() << ((IntImm) shift.getOperand()).getImm();
            return new Pair<>(OperandFactory.getInstance().genIntImm(imm, mb), new Shift());
        } else {
            return new Pair<>(o, shift);
        }
    }
    
    public CondType parseCond(BinaryInst inst) {
        if (inst.isLe()) {
            return CondType.LE;
        } else if (inst.isLt()) {
            return CondType.LT;
        } else if (inst.isEq()) {
            return CondType.EQ;
        } else if (inst.isGe()) {
            return CondType.GE;
        } else if (inst.isGt()) {
            return CondType.GT;
        } else if (inst.isNe()) {
            return CondType.NE;
        } else {
            return CondType.NONE;
        }
    }
    
    public int getCTZ(int num) {
        int r = 0;
        num >>>= 1;
        while (num > 0) {
            r++;
            num >>>= 1;
        }
        return r; // 0 - 31
    }
    
    public void divConstantOptimization(Value divisor, int imm, VirtualReg dst, MCBlock mb,
                                        Function f) {
        MCFunction mf = funcMap.get(f);
        Operand lhs = OperandFactory.getInstance().parseValueWithoutImm(divisor, mb, f);
        
        if (imm == -1) {
            new Binary(InstrType.RSB, mb, dst, lhs, new IntImm(0));
            return;
        }
        
        int abs = imm > 0 ? imm : -imm;
        if ((abs & (abs - 1)) == 0) {
            // 有符号除法处理 (n + ((n >> (31)) >>> (32 - l))) >> l
            VirtualReg v1 = new VirtualReg();
            mf.addVirtualReg(v1);
            new Move(mb, v1, lhs, new Shift(new IntImm(31), ShiftType.ASR));
            VirtualReg v2 = new VirtualReg();
            mf.addVirtualReg(v2);
            new Binary(InstrType.ADD, mb, v2, lhs, v1,
                new Shift(new IntImm(32 - getCTZ(abs)), ShiftType.LSR));
            
            new Move(mb, dst, v2, new Shift(new IntImm(getCTZ(abs)), ShiftType.ASR));
        } else {
            Triple<Long, Integer, Integer> multiplier = chooseMultiplier(abs, 31);
            long m = multiplier.getFirst();
            int sh = multiplier.getSecond();
            
            VirtualReg mulsh = new VirtualReg(mf);
            
            VirtualReg vm = new VirtualReg(mf);
            
            if (m < 2147483648L) {
                new Move(mb, vm, new IntImm((int) m));
                
                new LongMul(mb, mulsh, lhs, vm);
            } else {
                new Move(mb, vm, new IntImm((int) (m - (1L << 32))));
                
                Mla mla = new Mla(mb, mulsh, vm, lhs, lhs);
                mla.setAdd(true);
                mla.setSign(true);
            }
            
            VirtualReg sra = new VirtualReg();
            mf.addVirtualReg(sra);
            new Move(mb, sra, mulsh, new Shift(new IntImm(sh), ShiftType.ASR));
            new Binary(InstrType.ADD, mb, dst, sra, lhs, new Shift(new IntImm(31), ShiftType.LSR));
        }
        
        if (imm < 0) {
            new Binary(InstrType.RSB, mb, dst, dst, new IntImm(0));
        }
        divMap.put(new Triple<>(lhs, new IntImm(imm), mb), dst);
    }
    
    private Triple<Long, Integer, Integer> chooseMultiplier(int d, int prec) {
        int n = 32;
        int l = getCTZ(d - 1) + 1; // l = ceil(log2(d))
        int sh = l;
        long low = (1L << (32 + sh)) / d;
        long high = ((1L << (32 + sh)) + (1L << (n + sh - prec))) / d;
        while (low / 2 < high / 2 && sh > 0) {
            low >>= 1;
            high >>= 1;
            sh--;
        }
        return new Triple<>(high, sh, l);
    }
}
