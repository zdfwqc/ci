package backend.factory;

import IR.Values.ConstantFloat;
import IR.Values.ConstantInteger;
import IR.Values.Function;
import IR.Values.GlobalVariable;
import IR.Values.Value;
import IR.Values.instructions.mem.LoadInst;
import IR.types.FloatingType;
import backend.NameGenerate;
import backend.Operand;
import backend.armv7.MCBlock;
import backend.armv7.MCFunction;
import backend.armv7.instructions.loadstore.Load;
import backend.armv7.instructions.loadstore.Move;
import backend.imm.FloatImm;
import backend.imm.IntImm;
import backend.reg.FloatReg;
import backend.reg.RegularReg;
import backend.reg.VirtualReg;
import utils.Pair;

import java.util.HashMap;
import java.util.HashSet;

import static backend.imm.IntImm.ableToEncode;

public class OperandFactory extends Factory {
    private static final OperandFactory Instance = new OperandFactory();
    
    private OperandFactory() {
    }
    
    public static OperandFactory getInstance() {
        return Instance;
    }
    
    private final HashMap<Integer, GlobalVariable> floatMap = new HashMap<>();
    
    // isInsert: 立即数产生的寄存器是否会插入到函数块中
    public Operand parseValue(Value val, MCBlock mb, Function f, boolean isInsert) {
        MCFunction mf = funcMap.get(f);
        
        if (val instanceof ConstantInteger) {
            if (isInsert) {
                return genIntImm(((ConstantInteger) val).getValue(), mb);
            } else {
                return new IntImm(((ConstantInteger) val).getValue());
            }
        }
        
        if (val instanceof ConstantFloat) {
            if (isInsert) {
                return genFloatImm(((ConstantFloat) val).getValue(), mb);
            } else {
                return new FloatImm(((ConstantFloat) val).getValue());
            }
        }
        
        if (val instanceof Function.Arg && f.getArgList().contains(val)) {
            if (!irMap.containsKey(val)) {
                
                VirtualReg vreg = new VirtualReg(mf, val.getName());
                if (val.getType() instanceof FloatingType) {
                    vreg.setFloat(true);
                }
                irMap.put(val, vreg);
                System.out.println(f.getArgList().size());
                for (int i = 0; i < f.getArgList().size(); i++) {
                    if (f.getArgList().get(i) == val) {
                        MCBlock block = blockMap.get(f.getBBList().getHead().getValue());
                        if (i < 4) {
                            new Move(block, vreg, new RegularReg(i), true);
                        } else {
                            VirtualReg offset = new VirtualReg();
                            mf.addVirtualReg(offset);
                            
                            Move mv = new Move(block, offset, new IntImm((i - 4) * 4), true);
                            mf.addArgMove(mv);
                            
                            new Load(block, vreg, new RegularReg("sp"), offset);
                        }
                        break;
                    }
                }
                return vreg;
            } else {
                return irMap.get(val);
            }
        }
        
        if (val instanceof GlobalVariable &&
            irModule.getGlobalVars().contains(val)) {
            Pair<GlobalVariable, MCFunction> pair = new Pair<>((GlobalVariable) val, mf);
            if (!globalMap.containsKey(pair)) {
                VirtualReg vreg = new VirtualReg();
                mf.addVirtualReg(vreg);
                new Load(mb, vreg, irMap.get(val), new IntImm(0));
                return vreg;
            } else {
                return globalMap.get(pair).getDst();
            }
        }
        
        if (val instanceof LoadInst && loadToAlloca.containsKey(val)) {
            return allocaToStore.get(loadToAlloca.get(val));
        }
        
        if (!irMap.containsKey(val)) {
            VirtualReg vreg;
            if (val.getType() instanceof FloatingType) {
                vreg = new VirtualReg(mf, true);
            } else {
                vreg = new VirtualReg(mf);
            }
            irMap.put(val, vreg);
            return vreg;
        } else {
            return irMap.get(val);
        }
    }
    
    public Operand parseValue(Value val, MCBlock mb, Function f) {
        return parseValue(val, mb, f, true);
    }
    
    // without imm
    public Operand parseValueWithoutImm(Value val, MCBlock mb, Function f) {
        if (val instanceof ConstantInteger) {
            VirtualReg vr = new VirtualReg(mb.getFunction());
            new Move(mb, vr, new IntImm(((ConstantInteger) val).getValue()));
            return vr;
        } else if (val instanceof ConstantFloat) {
            int tmp = Float.floatToIntBits(((ConstantFloat) val).getValue());
            
            GlobalVariable newGv =
                new GlobalVariable(NameGenerate.getInstance().allocName(), FloatingType.type, true,
                    new ConstantInteger(tmp));
            
            VirtualReg gvreg = new VirtualReg(mb.getFunction(), newGv.getName());
            gvreg.setGlobal(true);
            
            irMap.put(newGv, gvreg);
            
            VirtualReg vreg = new VirtualReg(mb.getFunction());
            new Move(mb, vreg, gvreg);
            
            return vreg;
        } else {
            return parseValue(val, mb, f, false);
        }
    }
    
    public Operand genIntImm(int imm, MCBlock mb) {
        if (ableToEncode(imm)) {
            return new IntImm(imm);
        } else {
            VirtualReg vr = new VirtualReg(mb.getFunction());
            new Move(mb, vr, new IntImm(imm));
            return vr;
        }
    }
    
    public Operand genFloatImm(float imm, MCBlock mb) {
        MCFunction mf = mb.getFunction();
        if (FloatImm.ableToEncode(imm)) {
            return new FloatImm(imm);
        } else {
            int tmp = Float.floatToIntBits(imm);
            
            GlobalVariable newGv =
                new GlobalVariable(NameGenerate.getInstance().allocName(), FloatingType.type, true,
                    new ConstantInteger(tmp));
            
            VirtualReg gvreg = new VirtualReg(mb.getFunction(), newGv.getName());
            gvreg.setGlobal(true);
            
            irMap.put(newGv, gvreg);
            
            VirtualReg vreg = new VirtualReg(mf);
            new Move(mb, vreg, gvreg);
            
            return vreg;
        }
    }
}
