package backend.armv7.instructions;

public enum DataType {
    IntType("s32"), FloatType("f32");
    
    private final String data;
    
    DataType(String data) {
        this.data = data;
    }
    
    @Override
    public String toString() {
        return data;
    }
}
