package backend.armv7.instructions.vfg;

import backend.Operand;
import backend.armv7.MCBlock;
import backend.armv7.instructions.DataType;
import backend.armv7.instructions.basic.InstrType;
import backend.armv7.instructions.basic.MCInstruction;
import backend.imm.FloatImm;
import backend.imm.IntImm;
import backend.reg.VirtualReg;

public class VConvert extends MCInstruction {
    private Operand dst;
    private final DataType dstType;
    private Operand rhs;
    private final DataType rhsType;
    
    public VConvert(MCBlock block,
                    Operand dst, DataType dstType, Operand rhs,
                    DataType rhsType) {
        super(InstrType.VCVT, block);
        setDst(dst);
        this.dstType = dstType;
        setRhs(rhs);
        this.rhsType = rhsType;
    }
    
    public Operand getDst() {
        return dst;
    }
    
    public void setDst(Operand dst) {
        updateDef(this.dst, dst);
        this.dst = dst;
        if (dst instanceof VirtualReg) {
            ((VirtualReg) dst).setDefInstr(this);
        }
    }
    
    public Operand getRhs() {
        return rhs;
    }
    
    public void setRhs(Operand rhs) {
        updateUse(this.rhs, rhs);
        this.rhs = rhs;
    }
    
    @Override
    public void replaceReg(Operand ori, Operand target) {
        super.replaceReg(ori, target);
        if (dst.equals(ori)) {
            setDst(target);
        }
        if (rhs.equals(ori)) {
            setRhs(target);
        }
    }
    
    @Override
    public String toString() {
        return "\tcvt." + dstType + "." + rhsType + "\t" + dst + ",\t" + rhs + "\n";
    }
}
