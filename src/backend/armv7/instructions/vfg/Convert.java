package backend.armv7.instructions.vfg;

import backend.Operand;
import backend.armv7.MCBlock;
import backend.armv7.instructions.DataType;
import backend.armv7.instructions.basic.InstrType;
import backend.armv7.instructions.basic.MCInstruction;

public class Convert extends MCInstruction {
    private final Operand dst;
    private final DataType dstType;
    private final Operand rhs;
    private final DataType rhsType;
    
    public Convert(InstrType instr, MCBlock block,
                   Operand dst, DataType dstType, Operand rhs,
                   DataType rhsType) {
        super(instr, block);
        this.dst = dst;
        this.dstType = dstType;
        this.rhs = rhs;
        this.rhsType = rhsType;
    }
    
    @Override
    public String toString() {
        return "cvt." + dstType + "." + rhsType + "\t" + dst + ",\t" + rhs + "\n";
    }
}
