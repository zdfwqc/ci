package backend.armv7.instructions.vfg;

import backend.CodeGenModule;
import backend.Operand;
import backend.armv7.MCBlock;
import backend.armv7.instructions.CondType;
import backend.armv7.instructions.Shift;
import backend.armv7.instructions.basic.InstrType;
import backend.armv7.instructions.basic.MCInstruction;
import backend.imm.IntImm;
import backend.reg.VirtualReg;

import static backend.imm.IntImm.ableToEncode;

public class VMove extends MCInstruction {
    private Operand dst = null;
    private Operand rhs = null;
    
    public VMove(MCBlock block, Operand dst, Operand rhs) {
        super(InstrType.VMOV, block);
        setDst(dst);
        setRhs(rhs);
    }
    
    public VMove(MCBlock block, CondType cond, Operand dst, Operand rhs) {
        super(InstrType.VMOV, block, cond);
        setDst(dst);
        setRhs(rhs);
    }
    
    public VMove(MCBlock block, Operand dst, Operand rhs, Shift shift) {
        super(InstrType.VMOV, block, shift);
        setDst(dst);
        setRhs(rhs);
        
    }
    
    public VMove(Operand dst, Operand rhs) {
        super(InstrType.VMOV);
        setDst(dst);
        setRhs(rhs);
    }
    
    public Operand getDst() {
        return dst;
    }
    
    public void setDst(Operand dst) {
        updateDef(this.dst, dst);
        this.dst = dst;
        if (dst instanceof VirtualReg) {
            ((VirtualReg) dst).setDefInstr(this);
        }
    }
    
    public Operand getRhs() {
        return rhs;
    }
    
    public void setRhs(Operand rhs) {
        updateUse(this.rhs, rhs);
        this.rhs = rhs;
    }
    
    @Override
    public void replaceReg(Operand ori, Operand target) {
        super.replaceReg(ori, target);
        if (dst.equals(ori)) {
            setDst(target);
        }
        if (rhs.equals(ori)) {
            setRhs(target);
        }
    }
    
    @Override
    public String toString() {
        CodeGenModule.getInstance().addOffset(1);
        return "\t" + getInstr() + cond + "\t" + dst + ",\t" + rhs + shift + "\n";
    }
}
