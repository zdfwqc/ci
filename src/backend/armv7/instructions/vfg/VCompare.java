package backend.armv7.instructions.vfg;

import backend.CodeGenModule;
import backend.Operand;
import backend.armv7.MCBlock;
import backend.armv7.instructions.CondType;
import backend.armv7.instructions.basic.InstrType;
import backend.armv7.instructions.basic.MCInstruction;

public class VCompare extends MCInstruction {
    private Operand lhs;
    private Operand rhs;
    
    public VCompare(MCBlock block, Operand lhs, Operand rhs) {
        super(InstrType.VCMP, block);
        setLhs(lhs);
        setRhs(rhs);
    }
    
    public VCompare(MCBlock block, CondType cond, Operand lhs, Operand rhs) {
        super(InstrType.CMP, block, cond);
        setLhs(lhs);
        setRhs(rhs);
    }
    
    public Operand getLhs() {
        return lhs;
    }
    
    public void setLhs(Operand lhs) {
        updateUse(this.lhs, lhs);
        this.lhs = lhs;
    }
    
    public Operand getRhs() {
        return rhs;
    }
    
    public void setRhs(Operand rhs) {
        updateUse(this.rhs, rhs);
        this.rhs = rhs;
    }
    
    @Override
    public void replaceReg(Operand ori, Operand target) {
        super.replaceReg(ori, target);
        if (lhs.equals(ori)) {
            setLhs(target);
        }
        if (rhs.equals(ori)) {
            setRhs(target);
        }
    }
    
    @Override
    public String toString() {
        CodeGenModule.getInstance().addOffset(1);
        return "\t" + getInstr() + "\t" + lhs + ",\t" + rhs + "\n";
    }
}
