package backend.armv7.instructions.other;

import backend.armv7.MCBlock;
import backend.armv7.instructions.basic.InstrType;
import backend.armv7.instructions.basic.MCInstruction;

public class Comment extends MCInstruction {
    private final String text;
    
    public Comment(MCBlock block, String text) {
        super(InstrType.NONE, block);
        this.text = text;
    }
    
    @Override
    public String toString() {
        return "@" + text + "\n";
    }
}
