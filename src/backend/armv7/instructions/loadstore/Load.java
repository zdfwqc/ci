package backend.armv7.instructions.loadstore;

import backend.CodeGenModule;
import backend.Operand;
import backend.armv7.MCBlock;
import backend.armv7.instructions.basic.InstrType;
import backend.armv7.instructions.basic.MCInstruction;
import backend.reg.VirtualReg;

public class Load extends MCInstruction {
    private Operand dst = null;
    private Operand addr = null;
    private Operand offset = null;
    
    public Load(MCBlock block, Operand dst, Operand addr, Operand offset) {
        super(InstrType.LDR, block);
        setDst(dst);
        setAddr(addr);
        setOffset(offset);
    }
    
    public Load(Operand data, Operand addr, Operand offset) {
        super(InstrType.LDR);
        setDst(data);
        setAddr(addr);
        setOffset(offset);
    }
    
    
    public Operand getAddr() {
        return addr;
    }
    
    public void setAddr(Operand addr) {
        updateUse(this.addr, addr);
        this.addr = addr;
    }
    
    public Operand getOffset() {
        return offset;
    }
    
    public void setOffset(Operand offset) {
        updateUse(this.offset, offset);
        this.offset = offset;
    }
    
    public Operand getDst() {
        return dst;
    }
    
    public void setDst(Operand dst) {
        updateDef(this.dst, dst);
        this.dst = dst;
        if (dst instanceof VirtualReg) {
            ((VirtualReg) dst).setDefInstr(this);
        }
    }
    
    @Override
    public void replaceReg(Operand ori, Operand target) {
        super.replaceReg(ori, target);
        if (dst.equals(ori)) {
            setDst(target);
        }
        if (addr.equals(ori)) {
            setAddr(target);
        }
        if (offset.equals(ori)) {
            setOffset(target);
        }
    }
    
    @Override
    public String toString() {
        if (addr instanceof VirtualReg && ((VirtualReg) addr).isGlobal()) {
            CodeGenModule.getInstance().addOffset(2);
            String res = "\tmovw" + cond + "\t" + dst + ",\t:lower16:" + addr + "\n";
            res += "\tmovt" + cond + "\t" + dst + ",\t:upper16:" + addr + "\n";
            return res;
        } else {
            CodeGenModule.getInstance().addOffset(1);
            return "\t" + getInstr() + getCond() + "\t" + getDst() + ",\t[" + getAddr()
                + ",\t" + getOffset() + getShift() + "]\n";
        }
    }
}
