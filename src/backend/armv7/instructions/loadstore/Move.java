package backend.armv7.instructions.loadstore;

import backend.CodeGenModule;
import backend.Operand;
import backend.armv7.MCBlock;
import backend.armv7.instructions.CondType;
import backend.armv7.instructions.Shift;
import backend.armv7.instructions.basic.InstrType;
import backend.armv7.instructions.basic.MCInstruction;
import backend.imm.FloatImm;
import backend.imm.IntImm;
import backend.reg.VirtualReg;

import static backend.imm.IntImm.ableToEncode;

public class Move extends MCInstruction {
    private Operand dst = null;
    private Operand rhs = null;
    
    public Move(MCBlock block, Operand dst, Operand rhs) {
        super(InstrType.MV, block);
        setDst(dst);
        setRhs(rhs);
    }
    
    public Move(MCBlock block, Operand dst, Operand rhs, boolean insertEntry) {
        super(InstrType.MV, block, insertEntry);
        setDst(dst);
        setRhs(rhs);
    }
    
    public Move(MCBlock block, CondType cond, Operand dst, Operand rhs) {
        super(InstrType.MV, block, cond);
        setDst(dst);
        setRhs(rhs);
    }
    
    public Move(MCBlock block, Operand dst, Operand rhs, Shift shift) {
        super(InstrType.MV, block, shift);
        setDst(dst);
        setRhs(rhs);
        
    }
    
    public Move(Operand dst, Operand rhs) {
        super(InstrType.MV);
        setDst(dst);
        setRhs(rhs);
    }
    
    
    public Operand getDst() {
        return dst;
    }
    
    public void setDst(Operand dst) {
        updateDef(this.dst, dst);
        this.dst = dst;
        if (dst instanceof VirtualReg) {
            ((VirtualReg) dst).setDefInstr(this);
        }
    }
    
    public Operand getRhs() {
        return rhs;
    }
    
    public void setRhs(Operand rhs) {
        updateUse(this.rhs, rhs);
        this.rhs = rhs;
    }
    
    @Override
    public void replaceReg(Operand ori, Operand target) {
        super.replaceReg(ori, target);
        if (dst.equals(ori)) {
            setDst(target);
        }
        if (rhs.equals(ori)) {
            setRhs(target);
        }
    }
    
    @Override
    public String toString() {
        if (rhs instanceof IntImm) {
            if (ableToEncode(((IntImm) rhs).getImm())) {
                CodeGenModule.getInstance().addOffset(1);
                return "\tmvn" + getCond() + "\t" + dst + ",\t" + rhs + "\n";
            } else {
                int imm = ((IntImm) rhs).getImm();
                int immH = imm & 0xffff;
                int immL = imm & (~0xffff);
                String res = "";
                CodeGenModule.getInstance().addOffset(1);
                res += "\tmovw" + getCond() + "\t" + dst + ",\t#" + immL + "\n";
                if (immH != 0) {
                    CodeGenModule.getInstance().addOffset(1);
                    res +=
                        "\tmovt" + getCond() + "\t" + dst + ",\t#" + immH + "\n";
                }
                return res;
            }
        } else if (rhs.isFloat() || dst.isFloat()) {
            CodeGenModule.getInstance().addOffset(1);
            if (rhs instanceof FloatImm) {
                return "\tvmov.f32" + getCond() + "\t" + dst + ",\t" + rhs + "\n";
            } else {
                return "\tvmov" + getCond() + "\t" + dst + ",\t" + rhs + "\n";
            }
        } else {
            CodeGenModule.getInstance().addOffset(1);
            return "\tmov" + getCond() + "\t" + dst + ",\t" + rhs + shift + "\n";
        }
    }
}

