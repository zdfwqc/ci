package backend.armv7.instructions.loadstore;

import backend.CodeGenModule;
import backend.Operand;
import backend.armv7.MCBlock;
import backend.armv7.instructions.basic.InstrType;
import backend.armv7.instructions.basic.MCInstruction;
import backend.reg.VirtualReg;

public class Store extends MCInstruction {
    private Operand data;
    private Operand addr;
    private Operand offset;
    
    public Store(Operand data, Operand addr, Operand offset) {
        super(InstrType.STR);
        setData(data);
        setAddr(addr);
        setOffset(offset);
    }
    
    public Store(MCBlock block, Operand data, Operand addr, Operand offset) {
        super(InstrType.STR, block);
        setData(data);
        setAddr(addr);
        setOffset(offset);
    }
    
    public Operand getAddr() {
        return addr;
    }
    
    public void setAddr(Operand addr) {
        updateUse(this.addr, addr);
        this.addr = addr;
    }
    
    public Operand getOffset() {
        return offset;
    }
    
    public void setOffset(Operand offset) {
        updateUse(this.offset, offset);
        this.offset = offset;
    }
    
    public Operand getData() {
        return data;
    }
    
    public void setData(Operand data) {
        updateUse(this.data, data);
        this.data = data;
    }
    
    @Override
    public void replaceReg(Operand ori, Operand target) {
        super.replaceReg(ori, target);
        if (data.equals(ori)) {
            setData(target);
        }
        if (addr.equals(ori)) {
            setAddr(target);
        }
        if (offset.equals(ori)) {
            setOffset(target);
        }
    }
    
    @Override
    public String toString() {
        if (addr instanceof VirtualReg && ((VirtualReg) addr).isGlobal()) {
            CodeGenModule.getInstance().addOffset(2);
            String res =
                "\tmovw" + getCond() + "\t" + getData() + ",\t:lower16:" + getAddr() + "\n";
            res += "\tmovt" + getCond() + "\t" + getData() + ",\t:upper16:" + getAddr() + "\n";
            return res;
        } else {
            CodeGenModule.getInstance().addOffset(1);
            return "\t" + getInstr() + getCond() + "\t" + getData() + ",\t[" + getAddr()
                + ",\t" + getOffset() + getShift() + "]\n";
        }
    }
}
