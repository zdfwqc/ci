package backend.armv7.instructions.basic;

public enum InstrType {
    // 数据传送
    MOV("mov"), MVN("mvn"),
    // 算数运算
    ADD("add"), SUB("sub"), RSB("rsb"), ADC("ADC"), SBC("SBC"), RSC("RSC"),
    // 逻辑运算
    AND("AND"), ORR("ORR"), EOR("EOR"), BIC("BIC"),
    // 比较指令
    CMP("cmp"), CMN("cmn"), TST("TST"), TEQ("TEQ"),
    // 乘法/除法
    MUL("mul"), MLA("mla"), UMULL("UMULL"), UMLAL("UMLAL"), SMULL("SMULL"), SMLAL("SMLAL"),
    SMMUL("SMMUL"), SMMLA("SMMLA"), MLS("MLS"), SMMLS("SMMLS"),
    DIV("DIV"),
    // 跳转
    B("b"), BL("bl"), BX("bx"),
    // 伪指令
    ADR("ADR"), ADRL("ADRL"), NOP("NOP"),
    // LOAD/STORE
    LDR("ldr"), STR("str"), LDM("LDM"), STM("STM"), SWP("SWP"),
    // 自用伪指令;
    MV("MV"), RETURN("RETURN"), NONE(""),
    //VFP
    VADD("vadd"), VSUB("vsub"), VMUL("vmul"), VDIV("vdiv"), VMOD("vmod"),
    VLDR("vldr"), VSTR("vstr"), VMOV("vmov"), VCVT("cvt"),
    VCMP("vcmp"), VCMPE("vcmpe");
    
    private final String Instr;
    
    InstrType(String Instr) {
        this.Instr = Instr;
    }
    
    @Override
    public String toString() {
        return Instr;
    }
}
