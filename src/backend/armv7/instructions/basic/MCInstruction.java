package backend.armv7.instructions.basic;

import backend.Operand;
import backend.armv7.MCBlock;
import backend.armv7.instructions.CondType;
import backend.armv7.instructions.Shift;
import backend.reg.Reg;
import utils.IList;

import java.util.ArrayList;

import static backend.armv7.instructions.CondType.reverseCond;

public class MCInstruction {
    private InstrType instr;
    
    private MCBlock block;
    
    private final IList.INode<MCInstruction, MCBlock> node;
    
    protected Shift shift;
    protected CondType cond;
    
    private final ArrayList<Reg> useReg = new ArrayList<>();
    private final ArrayList<Reg> defReg = new ArrayList<>();
    
    public MCInstruction(InstrType instr) {
        this.instr = instr;
        this.block = null; // 未分配
        
        node = new IList.INode<>(this);
        
        shift = new Shift();
        shift.setInstr(this);
        cond = CondType.NONE;
    }
    
    public MCInstruction(InstrType instr, MCBlock block) {
        this.instr = instr;
        this.block = block;
        
        node = new IList.INode<>(this);
        node.setParent(block.getInstrList());
        block.addEnd(node);
        
        shift = new Shift();
        shift.setInstr(this);
        cond = CondType.NONE;
    }
    
    public MCInstruction(InstrType instr, MCBlock block, boolean insertEntry) {
        this.instr = instr;
        this.block = block;
        
        node = new IList.INode<>(this);
        node.setParent(block.getInstrList());
        if (insertEntry) {
            block.addEntry(node);
        } else {
            block.addEnd(node);
        }
        
        shift = new Shift();
        shift.setInstr(this);
        cond = CondType.NONE;
    }
    
    public MCInstruction(InstrType instr, MCBlock block, CondType cond) {
        this.instr = instr;
        this.block = block;
        this.cond = cond;
        
        node = new IList.INode<>(this);
        node.setParent(block.getInstrList());
        block.addEnd(node);
        
        shift = new Shift();
        shift.setInstr(this);
    }
    
    public MCInstruction(InstrType instr, MCBlock block, Shift shift) {
        this.instr = instr;
        this.block = block;
        this.shift = shift;
        shift.setInstr(this);
        
        node = new IList.INode<>(this);
        node.setParent(block.getInstrList());
        block.addEnd(node);
        
        cond = CondType.NONE;
    }
    
    public MCInstruction(InstrType instr, MCBlock block, CondType cond, Shift shift) {
        this.instr = instr;
        this.block = block;
        this.cond = cond;
        this.shift = shift;
        shift.setInstr(this);
        
        node = new IList.INode<>(this);
        node.setParent(block.getInstrList());
        block.addEnd(node);
    }
    
    public IList.INode<MCInstruction, MCBlock> getNode() {
        return node;
    }
    
    public InstrType getInstr() {
        return instr;
    }
    
    public void setInstr(InstrType instr) {
        this.instr = instr;
    }
    
    public ArrayList<Reg> getUseReg() {
        return useReg;
    }
    
    public void addUse(Operand num) {
        if (num instanceof Reg) {
            useReg.add((Reg) num);
        }
    }
    
    public void removeUse(Operand num) {
        if (num instanceof Reg) {
            useReg.remove((Reg) num);
        }
    }
    
    public ArrayList<Reg> getDefReg() {
        return defReg;
    }
    
    public void addDef(Operand num) {
        if (num instanceof Reg) {
            defReg.add((Reg) num);
        }
    }
    
    public void removeDef(Operand num) {
        if (num instanceof Reg) {
            defReg.remove((Reg) num);
        }
    }
    
    public Shift getShift() {
        return shift;
    }
    
    public void setShift(Shift shift) {
        this.shift = shift;
        this.shift.setInstr(this);
    }
    
    public CondType getCond() {
        return cond;
    }
    
    public void setCond(CondType cond) {
        this.cond = cond;
    }
    
    public void revCond() {
        this.cond = reverseCond(cond);
    }
    
    public MCBlock getBlock() {
        return block;
    }
    
    public void updateDef(Operand oldReg, Operand newReg) {
        if (oldReg instanceof Reg) {
            removeDef(oldReg);
        }
        if (newReg instanceof Reg) {
            addDef(newReg);
        }
    }
    
    public void updateUse(Operand oldReg, Operand newReg) {
        if (oldReg instanceof Reg) {
            removeUse(oldReg);
        }
        if (newReg instanceof Reg) {
            addUse(newReg);
        }
    }
    
    public void insertBlockAtEnd(MCBlock block) {
        this.block = block;
        node.setParent(block.getInstrList());
        block.addEnd(node);
    }
    
    public void insertBlockAtEntry(MCBlock block) {
        this.block = block;
        node.setParent(block.getInstrList());
        block.addEntry(node);
    }
    
    public void insertBeforeInstr(MCInstruction instr) {
        this.block = instr.getBlock();
        node.setParent(block.getInstrList());
        node.insertBefore(instr.getNode());
    }
    
    public void insertAfterInstr(MCInstruction instr) {
        this.block = instr.getBlock();
        node.setParent(block.getInstrList());
        node.insertAfter(instr.getNode());
    }
    
    public void replaceReg(Operand ori, Operand target) {
        if (shift.getOperand().equals(ori)) {
            shift.setOperand(target);
        }
    }
}
