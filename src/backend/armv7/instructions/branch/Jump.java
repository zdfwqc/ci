package backend.armv7.instructions.branch;

import backend.CodeGenModule;
import backend.armv7.MCBlock;
import backend.armv7.instructions.CondType;
import backend.armv7.instructions.basic.InstrType;
import backend.armv7.instructions.basic.MCInstruction;

public class Jump extends MCInstruction {
    private MCBlock target;
    
    public Jump(MCBlock block, MCBlock target) {
        super(InstrType.B, block);
        this.target = target;
    }
    
    public Jump(MCBlock block, CondType cond, MCBlock target) {
        super(InstrType.B, block, cond);
        this.target = target;
    }
    
    public void setTarget(MCBlock target) {
        this.target = target;
    }
    
    @Override
    public String toString() {
        CodeGenModule.getInstance().addOffset(1);
        return "\t" + getInstr() + cond+ "\t" + target + "\n";
    }
}
