package backend.armv7.instructions.branch;

import backend.CodeGenModule;
import backend.armv7.MCBlock;
import backend.armv7.MCFunction;
import backend.armv7.instructions.basic.InstrType;
import backend.armv7.instructions.basic.MCInstruction;
import backend.armv7.instructions.loadstore.Move;
import backend.imm.IntImm;
import backend.reg.RegularReg;

import java.util.ArrayList;

import static backend.imm.IntImm.ableToEncode;

public class Return extends MCInstruction {
    public Return(MCBlock block) {
        super(InstrType.RETURN, block);
    }
    
    @Override
    public String toString() {
        String res = "";
        MCFunction mf = getBlock().getFunction();
        int ssize = mf.getStackSize();
        
        // 恢复栈
        if (ssize > 0) {
            if (ableToEncode(ssize)) {
                IntImm imm = new IntImm(ssize);
                CodeGenModule.getInstance().addOffset(1);
                res += "\t" + InstrType.ADD + "\tsp,\tsp,\t" + imm + "\n";
            } else if (ableToEncode(-ssize)) {
                IntImm imm = new IntImm(-ssize);
                CodeGenModule.getInstance().addOffset(1);
                res += "\t" + InstrType.SUB + "\tsp,\tsp,\t" + imm + "\n";
            } else {
                IntImm imm = new IntImm(ssize);
                RegularReg dst = new RegularReg(5);
                Move move = new Move(getBlock(), dst, imm);
                res += move;
                CodeGenModule.getInstance().addOffset(1);
                res += "\t" + InstrType.ADD + "\tsp,\tsp,\t" + dst + "\n";
            }
        }
        
        // 恢复寄存器
        boolean usedLR = false;
        
        ArrayList<String> regs = new ArrayList<>();
        for (RegularReg reg : mf.getUsedSavedRegs()) {
            if (reg.getName().equals("lr")) {
                regs.add("pc");
                usedLR = true;
            } else {
                regs.add(reg.getName());
            }
        }
        
        if (!regs.isEmpty()) {
            CodeGenModule.getInstance().addOffset(1);
            res += "\tpop\t{";
            for (int i = 0; i < regs.size(); i++) {
                res += regs.get(i);
                if (i != regs.size() - 1) {
                    res += ",\t";
                }
            }
            res += "}\n";
        }
        
        if (!usedLR) {
            CodeGenModule.getInstance().addOffset(1);
            res += "\tbx\tlr\n";
        }
        
        return res;
    }
}
