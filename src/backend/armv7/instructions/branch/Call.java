package backend.armv7.instructions.branch;

import backend.CodeGenModule;
import backend.armv7.MCBlock;
import backend.armv7.MCFunction;
import backend.armv7.instructions.basic.InstrType;
import backend.armv7.instructions.basic.MCInstruction;

public class Call extends MCInstruction {
    private final MCFunction function;
    
    public Call(MCBlock block, MCFunction function) {
        super(InstrType.BL, block);
        this.function = function;
    }
    
    @Override
    public String toString() {
        CodeGenModule.getInstance().addOffset(1);
        return "\t" + getInstr() + "\t" + function + "\n";
    }
}
