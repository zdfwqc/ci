package backend.armv7.instructions.dataprocess;

import backend.CodeGenModule;
import backend.Operand;
import backend.armv7.MCBlock;
import backend.armv7.instructions.basic.InstrType;
import backend.armv7.instructions.basic.MCInstruction;
import backend.reg.VirtualReg;

public class LongMul extends MCInstruction {
    private Operand dst;
    private Operand lhs;
    private Operand rhs;
    
    public LongMul(MCBlock block, Operand dst, Operand lhs, Operand rhs) {
        super(InstrType.SMMUL, block);
        setDst(dst);
        setLhs(lhs);
        setRhs(rhs);
    }
    
    public Operand getDst() {
        return dst;
    }
    
    public void setDst(Operand dst) {
        updateDef(this.dst, dst);
        this.dst = dst;
        if (dst instanceof VirtualReg) {
            ((VirtualReg) dst).setDefInstr(this);
        }
    }
    
    public Operand getLhs() {
        return lhs;
    }
    
    public void setLhs(Operand lhs) {
        updateUse(this.lhs, lhs);
        this.lhs = lhs;
    }
    
    public Operand getRhs() {
        return rhs;
    }
    
    public void setRhs(Operand rhs) {
        updateUse(this.rhs, rhs);
        this.rhs = rhs;
    }
    
    @Override
    public void replaceReg(Operand ori, Operand target) {
        super.replaceReg(ori, target);
        if (dst.equals(ori)) {
            setDst(target);
        }
        if (lhs.equals(ori)) {
            setLhs(target);
        }
        if (rhs.equals(ori)) {
            setRhs(target);
        }
    }
    
    public String toString() {
        CodeGenModule.getInstance().addOffset(1);
        return "\tsmmul\t" + cond + dst + ",\t" + lhs + ",\t" + rhs + "\n";
    }
}
