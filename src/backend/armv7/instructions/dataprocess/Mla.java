package backend.armv7.instructions.dataprocess;

import backend.CodeGenModule;
import backend.Operand;
import backend.armv7.MCBlock;
import backend.armv7.instructions.basic.InstrType;
import backend.armv7.instructions.basic.MCInstruction;
import backend.reg.VirtualReg;

public class Mla extends MCInstruction {
    boolean add;
    boolean sign;
    
    private Operand dst;
    
    private Operand lhs;
    private Operand rhs;
    private Operand acc;
    
    public Mla(MCBlock block, Operand dst, Operand lhs, Operand rhs, Operand acc) {
        super(InstrType.MLA, block);
        add = true;
        sign = false;
        setDst(dst);
        setLhs(lhs);
        setRhs(rhs);
        setAcc(acc);
    }
    
    public Mla(MCBlock block, Operand dst, Operand lhs, Operand rhs, Operand acc,
               boolean add, boolean sign) {
        super(InstrType.MLA, block);
        setDst(dst);
        setLhs(lhs);
        setRhs(rhs);
        setAcc(acc);
        this.add = add;
        this.sign = sign;
        updateInstr();
    }
    
    @Override
    public String toString() {
        CodeGenModule.getInstance().addOffset(1);
        return "\t" + getInstr() + cond + "\t" + dst + ",\t" +
            lhs + ",\t" + rhs + ",\t" + acc + "\n";
    }
    
    private void updateInstr() {
        if (isAdd() && isSign()) {
            setInstr(InstrType.SMMLA);
        } else if (isAdd() && !isSign()) {
            setInstr(InstrType.MLA);
        } else if (!isAdd() && isSign()) {
            setInstr(InstrType.SMMLS);
        } else {
            setInstr(InstrType.MLS);
        }
    }
    
    public boolean isAdd() {
        return add;
    }
    
    public void setAdd(boolean add) {
        this.add = add;
        updateInstr();
    }
    
    public boolean isSign() {
        return sign;
    }
    
    public void setSign(boolean sign) {
        this.sign = sign;
        updateInstr();
    }
    
    public Operand getDst() {
        return dst;
    }
    
    public void setDst(Operand dst) {
        updateDef(this.dst, dst);
        this.dst = dst;
        if (dst instanceof VirtualReg) {
            ((VirtualReg) dst).setDefInstr(this);
        }
    }
    
    public Operand getLhs() {
        return lhs;
    }
    
    public void setLhs(Operand lhs) {
        updateUse(this.lhs, lhs);
        this.lhs = lhs;
    }
    
    public Operand getRhs() {
        return rhs;
    }
    
    public void setRhs(Operand rhs) {
        updateUse(this.rhs, rhs);
        this.rhs = rhs;
    }
    
    public Operand getAcc() {
        return acc;
    }
    
    public void setAcc(Operand acc) {
        updateUse(this.acc, acc);
        this.acc = acc;
    }
    
    @Override
    public void replaceReg(Operand ori, Operand target) {
        super.replaceReg(ori, target);
        if (dst.equals(ori)) {
            setDst(target);
        }
        if (lhs.equals(ori)) {
            setLhs(target);
        }
        if (rhs.equals(ori)) {
            setRhs(target);
        }
        if (acc.equals(ori)) {
            setAcc(target);
        }
    }
}
