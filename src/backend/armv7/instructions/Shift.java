package backend.armv7.instructions;

import backend.armv7.instructions.basic.MCInstruction;
import backend.imm.IntImm;
import backend.Operand;
import backend.reg.Reg;

import java.util.Objects;

public class Shift {
    private Operand operand;
    private ShiftType type;
    private MCInstruction instr;
    
    public Shift() {
        operand =  new IntImm(0);
        type = ShiftType.None;
    }
    
    public Shift(Operand optNumber, ShiftType type) {
        this.operand = optNumber;
        this.type = type;
    }
    
    public Shift(Integer imm, ShiftType type) {
        this.operand = new IntImm(imm);
        this.type = type;
    }
    
    public Shift(Shift shift) {
        this.operand = shift.operand;
        this.type = shift.type;
        this.instr = shift.instr;
    }
    
    public void setInstr(MCInstruction instr) {
        this.instr = instr;
    }
    
    public Operand getOperand() {
        return operand;
    }
    
    public void setOperand(Operand operand) {
        updateUse(this.operand, operand);
        this.operand = operand;
    }
    
    public void updateUse(Operand oldReg, Operand newReg) {
        if (operand instanceof Reg) {
            instr.updateUse(oldReg, newReg);
        }
    }
    
    public ShiftType getType() {
        return type;
    }
    
    public void setType(ShiftType type) {
        this.type = type;
    }
    
    public boolean isNone() {
        return (type == ShiftType.None) ||
            (operand instanceof IntImm && ((IntImm) operand).getImm() == 0);
    }
    
    @Override
    public String toString() {
        if (isNone()) {
            return "";
        }
        
        return ", " + type.getShiftType() + " " + operand;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Shift shift = (Shift) o;
        if (isNone() && shift.isNone()) {
            return true;
        }
        return operand.equals(shift.operand) && type == shift.type;
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(operand, type);
    }
}
