package backend.armv7.instructions;

public enum CondType {
    NONE(""), EQ("EQ"), NE("NE"), GE("GE"), GT("GT"), LE("LE"), LT("LT");
    
    private final String cond;
    
    CondType(String cond) {
        this.cond = cond;
    }
    
    public String getCond() {
        return cond;
    }
    
    public static CondType reverseCond(CondType cond) {
        if (cond == NE) {
            return EQ;
        } else if (cond == EQ) {
            return NE;
        } else if (cond == GT) {
            return LE;
        } else if (cond == LE) {
            return GT;
        } else if (cond == GE) {
            return LT;
        } else if (cond == LT) {
            return GE;
        } else {
            return CondType.NONE;
        }
    }
    
    public static CondType mirrorReverseCond(CondType cond) {
        if (cond == NE) {
            return EQ;
        } else if (cond == EQ) {
            return NE;
        } else if (cond == GT) {
            return LT;
        } else if (cond == LE) {
            return GE;
        } else if (cond == GE) {
            return LE;
        } else if (cond == LT) {
            return GT;
        } else {
            return CondType.NONE;
        }
    }
    
    @Override
    public String toString() {
        return cond;
    }
}
