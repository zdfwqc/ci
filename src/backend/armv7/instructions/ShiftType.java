package backend.armv7.instructions;

public enum ShiftType {
    None(""), LSL("LSL"), LSR("LSR"),
    ASR("ASR"), ROR("ROR"), ROX("ROX");
    
    private final String shiftType;
    
    ShiftType(String shiftType) {
        this.shiftType = shiftType;
    }
    
    public String getShiftType() {
        return shiftType;
    }
}
