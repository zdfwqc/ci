package backend.armv7;

import backend.armv7.instructions.basic.MCInstruction;
import utils.IList;

import java.util.ArrayList;

public class MCBlock {
    private final String name;
    private final IList.INode<MCBlock, MCFunction> node;
    private final IList<MCInstruction, MCBlock> instrList;
    private static final IdPool bidPool = new IdPool();
    
    private final MCFunction function;
    
    private MCBlock falseBlock;
    private MCBlock trueBlock;
    private final ArrayList<MCBlock> predBlocks;
    
    public String getName() {
        return name;
    }
    
    public MCBlock(MCFunction function) {
        this.function = function;
        
        name = "__Block" + bidPool.allocId();
        node = new IList.INode<>(this);
        instrList = new IList<>(this);
        node.setParent(function.getBlockList());
        
        falseBlock = null;
        trueBlock = null;
        predBlocks = new ArrayList<>();
    }
    
    public IList.INode<MCBlock, MCFunction> getNode() {
        return node;
    }
    
    public void addEntry(IList.INode<MCInstruction, MCBlock> node) {
        node.insertListHead(instrList);
    }
    
    public void addEnd(IList.INode<MCInstruction, MCBlock> node) {
        node.insertListEnd(instrList);
    }
    
    public MCBlock getFalseBlock() {
        return falseBlock;
    }
    
    public void setFalseBlock(MCBlock falseBlock) {
        this.falseBlock = falseBlock;
    }
    
    public boolean hasFalseSucc() {
        return falseBlock != null;
    }
    
    public MCBlock getTrueBlock() {
        return trueBlock;
    }
    
    public void setTrueBlock(MCBlock trueBlock) {
        this.trueBlock = trueBlock;
    }
    
    public boolean hasSucc() {
        return trueBlock != null || falseBlock != null;
    }
    
    public void swapSucc() {
        MCBlock temp = trueBlock;
        trueBlock = falseBlock;
        falseBlock = temp;
    }
    
    public ArrayList<MCBlock> getPredBlocks() {
        return predBlocks;
    }
    
    public void addPredBlock(MCBlock b) {
        this.predBlocks.add(b);
    }
    
    public void removePredBlock(MCBlock b) {
        this.predBlocks.remove(b);
    }
    
    public IList<MCInstruction, MCBlock> getInstrList() {
        return instrList;
    }
    
    public MCFunction getFunction() {
        return function;
    }
    
    @Override
    public String toString() {
        return name;
    }
}
