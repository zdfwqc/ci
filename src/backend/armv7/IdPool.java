package backend.armv7;

public class IdPool {
    private int count;
    
    public IdPool() {
        count = 0;
    }
    
    public int allocId() {
        count += 1;
        return count;
    }
}
