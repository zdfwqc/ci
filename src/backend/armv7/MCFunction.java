package backend.armv7;

import backend.CodeGenModule;
import backend.armv7.instructions.loadstore.Move;
import backend.reg.RegularReg;
import backend.reg.VirtualReg;
import utils.IList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.stream.Collectors;

public class MCFunction {
    private final String name;
    private boolean isUsedLr;
    private final ArrayList<RegularReg> usedSavedRegs;
    private final HashMap<String, VirtualReg> virtualRegMap;
    
    private final IList.INode<MCFunction, CodeGenModule> node;
    private final IList<MCBlock, MCFunction> blockList;
    
    private int stackSize;
    
    public MCFunction(String name) {
        this.name = name;
        
        node = new IList.INode<>(this);
        isUsedLr = false;
        usedSavedRegs = new ArrayList<>();
        virtualRegMap = new HashMap<>();
        blockList = new IList<>(this);
    }
    
    public String getName() {
        return name;
    }
    
    public IList.INode<MCFunction, CodeGenModule> getNode() {
        return node;
    }
    
    public IList<MCBlock, MCFunction> getBlockList() {
        return blockList;
    }
    
    public void insertBlockAtListEnd(MCBlock b) {
        b.getNode().insertListEnd(blockList);
    }
    
    // stack related
    
    public int getStackSize() {
        return stackSize;
    }
    
    public void setStackSize(int stackSize) {
        this.stackSize = stackSize;
    }
    
    public void addStackSize(int addSize) {
        this.stackSize += addSize;
    }
    
    // lr related
    
    public boolean isUsedLr() {
        return isUsedLr;
    }
    
    public void setUsedLr(boolean usedLr) {
        isUsedLr = usedLr;
    }
    
    // physical regs related
    
    public ArrayList<RegularReg> getUsedSavedRegs() {
        return usedSavedRegs;
    }
    
    public void addUsedSavedReg(RegularReg reg) {
        usedSavedRegs.add(reg);
    }
    
    // virtual regs related
    
    public void addVirtualReg(VirtualReg reg) {
        virtualRegMap.put(reg.getName(), reg);
    }
    
    public HashMap<String, VirtualReg> getVirtualRegMap() {
        return virtualRegMap;
    }
    
    public HashSet<Integer> getAllRegId() {
        HashSet<Integer> res = new HashSet<>();
        for (var mbEntry : getBlockList()) {
            for (var instrEntry : mbEntry.getValue().getInstrList()) {
                var instr = instrEntry.getValue();
                var defs = instr.getDefReg();
                var uses = instr.getUseReg();
                res.addAll(defs.stream().filter(RegularReg.class::isInstance)
                    .map(r -> ((RegularReg) r).getId())
                    .collect(Collectors.toCollection(HashSet::new)));
                
                res.addAll(uses.stream().filter(RegularReg.class::isInstance)
                    .map(r -> ((RegularReg) r).getId())
                    .collect(Collectors.toCollection(HashSet::new)));
            }
        }
        return res;
    }
    
    private final ArrayList<Move> argsMove = new ArrayList<>();
    
    public ArrayList<Move> getArgsMove() {
        return argsMove;
    }
    
    public void addArgMove(Move mv) {
        argsMove.add(mv);
    }
    
    @Override
    public String toString() {
        return name;
    }
}
