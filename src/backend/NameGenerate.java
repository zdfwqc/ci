package backend;

import backend.armv7.IdPool;

public class NameGenerate {
    private static NameGenerate Instance = new NameGenerate();
    private final IdPool idPool = new IdPool();
    
    private NameGenerate() {
    
    }
    
    public static NameGenerate getInstance() {
        return Instance;
    }
    
    public String allocName() {
        return "LLVM_FLOAT" + idPool.allocId();
    }
}
