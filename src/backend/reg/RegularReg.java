package backend.reg;

import java.util.Objects;

public class RegularReg extends PysicalReg {
    
    public RegularReg(String name) {
        super(name, RegNameMap.getInstance().getRegNum(name));
    }
    
    public RegularReg(int id) {
        super(RegNameMap.getInstance().getRegName(id), id);
    }
    
    public RegularReg(int id, boolean isAllocated) {
        super(RegNameMap.getInstance().getRegName(id), id, isAllocated);
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RegularReg reg = (RegularReg) o;
        return id == reg.id && Objects.equals(name, reg.name);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(name, id);
    }
}
