package backend.reg;

import backend.armv7.IdPool;
import backend.armv7.MCFunction;
import backend.armv7.instructions.basic.MCInstruction;

import java.util.Objects;

public class VirtualReg extends Reg {
    private boolean isGlobal;
    private static final IdPool vidPool = new IdPool();
    private final String name;
    private MCInstruction defInstr = null;
    private boolean isFloat = false;
    
    public VirtualReg(String name) {
        this.name = name;
    }
    
    public VirtualReg(MCFunction mf, String name) {
        this.name = name;
        mf.addVirtualReg(this);
    }
    
    public VirtualReg() {
        this.name = "$" + vidPool.allocId();
    }
    
    public VirtualReg(MCFunction mf) {
        this.name = "$" + vidPool.allocId();
        mf.addVirtualReg(this);
    }
    
    public VirtualReg(MCFunction mf, boolean isFloat) {
        this.name = "$F" + vidPool.allocId();
        mf.addVirtualReg(this);
        this.isFloat = isFloat;
    }
    
    @Override
    public boolean isFloat() {
        return isFloat;
    }
    
    public void setFloat(boolean aFloat) {
        isFloat = aFloat;
    }
    
    public MCInstruction getDefInstr() {
        return defInstr;
    }
    
    public void setDefInstr(MCInstruction defInstr) {
        this.defInstr = defInstr;
    }
    
    @Override
    public String getName() {
        return name;
    }
    
    public boolean isGlobal() {
        return isGlobal;
    }
    
    public void setGlobal(boolean global) {
        isGlobal = global;
    }
    
    @Override
    public boolean needColor() {
        return true;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        VirtualReg reg = (VirtualReg) o;
        return Objects.equals(name, reg.name);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
    
    @Override
    public String toString() {
        return name;
    }
}
