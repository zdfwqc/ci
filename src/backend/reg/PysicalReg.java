package backend.reg;

public class PysicalReg extends Reg{
    protected boolean isAllocated;
    protected final String name;
    protected final int id;
    
    public PysicalReg(String name, int id) {
        this.name = name;
        this.id = id;
        this.isAllocated = false;
    }
    
    public PysicalReg(String name, int id, boolean isAllocated) {
        this.name = name;
        this.id = id;
        this.isAllocated = isAllocated;
    }
    
    public boolean isPrecolored() {
        return !isAllocated;
    }
    
    public void setAllocated(boolean allocated) {
        isAllocated = allocated;
    }
    
    @Override
    public boolean needColor() {
        return isPrecolored();
    }
    
    @Override
    public String getName() {
        return name;
    }
    
    public int getId() {
        return id;
    }
    
    @Override
    public String toString() {
        return name;
    }
    
    @Override
    public boolean isFloat() {
        return false;
    }
}
