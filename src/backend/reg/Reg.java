package backend.reg;

import backend.Operand;

public class Reg implements Operand {
    public String getName() {
        return "";
    }
    
    @Override
    public boolean needColor() {
        return false;
    }
    
    @Override
    public boolean isPrecolored() {
        return false;
    }
    
    @Override
    public boolean isFloat() {
        return false;
    }
}
