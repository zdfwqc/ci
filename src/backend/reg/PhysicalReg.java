package backend.reg;

import java.util.Objects;

public class PhysicalReg extends Reg {
    private boolean isAllocated;
    private final String name;
    private final int id;
    
    public PhysicalReg(String name) {
        this.name = name;
        this.id = RegNameMap.getInstance().getRegNum(name);
        this.isAllocated = false;
    }
    
    public PhysicalReg(int id) {
        this.id = id;
        this.name = RegNameMap.getInstance().getRegName(id);
        this.isAllocated = false;
    }
    
    public PhysicalReg(int id, boolean isAllocated) {
        this.id = id;
        this.name = RegNameMap.getInstance().getRegName(id);
        this.isAllocated = isAllocated;
    }
    
    @Override
    public String getName() {
        return name;
    }
    
    public int getId() {
        return id;
    }
    
    public boolean isPrecolored() {
        return !isAllocated;
    }
    
    public void setAllocated(boolean allocated) {
        isAllocated = allocated;
    }
    
    @Override
    public boolean needColor() {
        return isPrecolored();
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PhysicalReg reg = (PhysicalReg) o;
        return id == reg.id && Objects.equals(name, reg.name);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(name, id);
    }
    
    @Override
    public String toString() {
        return name;
    }
}
