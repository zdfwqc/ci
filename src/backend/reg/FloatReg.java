package backend.reg;

import java.util.Objects;

public class FloatReg extends PysicalReg{
    
    public FloatReg(String name) {
        super(name, RegNameMap.getInstance().getFloatRegNum(name));
    }
    
    public FloatReg(int id) {
        super(RegNameMap.getInstance().getFloatRegName(id), id);
    }
    
    @Override
    public boolean isFloat() {
        return true;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FloatReg reg = (FloatReg) o;
        return id == reg.id && Objects.equals(name, reg.name);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(name, id);
    }
}
