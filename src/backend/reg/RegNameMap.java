package backend.reg;

import java.util.HashMap;

public class RegNameMap {
    private final HashMap<String, Integer> name2num = new HashMap<>();
    private final HashMap<Integer, String> num2name = new HashMap<>();
    
    private final HashMap<String, Integer> floatName2num = new HashMap<>();
    private final HashMap<Integer, String> floatNum2name = new HashMap<>();
    
    private static final RegNameMap Instance = new RegNameMap();
    
    public static RegNameMap getInstance() {
        return Instance;
    }
    
    private RegNameMap() {
        name2num.put("r0", 0);
        name2num.put("r1", 1);
        name2num.put("r2", 2);
        name2num.put("r3", 3);
        name2num.put("r4", 4);
        name2num.put("r5", 5);
        name2num.put("r6", 6);
        name2num.put("r7", 7);
        name2num.put("r8", 8);
        name2num.put("r9", 9);
        name2num.put("r10", 10);
        name2num.put("r11", 11);
        name2num.put("r12", 12);
        name2num.put("r13", 13);
        name2num.put("r14", 14);
        name2num.put("r15", 15);
        name2num.put("fp", 11);
        name2num.put("ip", 12);
        name2num.put("sp", 13);
        name2num.put("lr", 14);
        name2num.put("pc", 15);
        name2num.put("cspr", 16);
        
        num2name.put(0, "r0");
        num2name.put(1, "r1");
        num2name.put(2, "r2");
        num2name.put(3, "r3");
        num2name.put(4, "r4");
        num2name.put(5, "r5");
        num2name.put(6, "r6");
        num2name.put(7, "r7");
        num2name.put(8, "r8");
        num2name.put(9, "r9");
        num2name.put(10, "r10");
        num2name.put(11, "r11");
        num2name.put(12, "r12");
        num2name.put(13, "sp");
        num2name.put(14, "lr");
        num2name.put(15, "pc");
        num2name.put(16, "cspr");
        
        for (int i = 0; i < 32; i++) {
            floatNum2name.put(i, "s" + i);
            floatName2num.put("s" + i, i);
        }
    }
    
    public Integer getRegNum(String name) {
        return name2num.get(name);
    }
    
    public String getRegName(Integer num) {
        return num2name.get(num);
    }
    
    public Integer getFloatRegNum(String name) {
        return floatName2num.get(name);
    }
    
    public String getFloatRegName(Integer num) {
        return floatNum2name.get(num);
    }
}
