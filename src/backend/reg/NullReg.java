package backend.reg;

public class NullReg extends Reg {
    @Override
    public String toString() {
        assert (false);
        return "";
    }
}
