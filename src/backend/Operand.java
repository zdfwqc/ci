package backend;

public interface Operand {
    String getName();
    
    boolean needColor();
    
    boolean isPrecolored();
    
    boolean isFloat();
}
