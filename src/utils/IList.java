package utils;

import java.util.Iterator;

public class IList<N, L> implements Iterable<IList.INode<N, L>> {
    // N for node value type
    // L for list value type
    private INode<N, L> head;
    private INode<N, L> tail;
    private L value;
    private int size;
    
    public IList(L list_value) {
        this.value = list_value;
        this.head = null;
        this.tail = null;
        this.size = 0;
    }
    
    public INode<N, L> getHead() {
        return head;
    }
    
    public void setHead(INode<N, L> head) {
        this.head = head;
    }
    
    public INode<N, L> getTail() {
        return tail;
    }
    
    public void setTail(INode<N, L> tail) {
        this.tail = tail;
    }
    
    public L getValue() {
        return value;
    }
    
    public void setValue(L value) {
        this.value = value;
    }
    
    public int getSize() {
        return size;
    }
    
    public void setSize(int size) {
        this.size = size;
    }
    
    public void addNode() {
        this.size++;
    }
    
    public void removeNode() {
        this.size--;
    }
    
    public boolean isEmpty() {
        return (this.head == null) && (this.tail == null) && (getSize() == 0);
    }
    
    @Override
    public Iterator<INode<N, L>> iterator() {
        return new ListIterator(this.getHead());
    }
    
    class ListIterator implements Iterator<INode<N, L>> {
        INode<N, L> tmpNode = new INode<>(null);
        INode<N, L> next = null;
        
        ListIterator(INode<N, L> head) {
            tmpNode.next = head;
        }
        
        @Override
        public boolean hasNext() {
            return next != null || tmpNode.next != null;
        }
        
        @Override
        public INode<N, L> next() {
            if (next == null) {
                tmpNode = tmpNode.next;
            } else {
                tmpNode = next;
            }
            next = null;
            return tmpNode;
        }
        
        @Override
        public void remove() {
            INode<N, L> prev = tmpNode.prev;
            INode<N, L> next = tmpNode.next;
            IList<N, L> parent = tmpNode.parent;
            
            if (prev != null) {
                prev.setNext(next);
            } else {
                parent.setHead(next);
            }
            
            if (next != null) {
                next.setPrev(prev);
            } else {
                parent.setTail(prev);
            }
            
            parent.removeNode();
            
            this.next = next;
            tmpNode.next = tmpNode.prev = null;
            tmpNode.value = null;
            
            // Iterator.super.remove();
        }
    }
    
    public static class INode<N, L> {
        // N for list's node value type
        private N value;
        private INode<N, L> prev = null;
        private INode<N, L> next = null;
        
        private IList<N, L> parent;
        
        public INode(N val) {
            this.value = val;
            this.parent = null;
        }
        
        public void insertListHead(IList<N, L> parent) {
            this.parent = parent;
            if (parent.isEmpty()) {
                parent.setHead(this);
                parent.setTail(this);
                parent.addNode();
            } else {
                insertBefore(parent.getHead());
            }
        }
        
        public void insertListEnd(IList<N, L> parent) {
            this.parent = parent;
            if (parent.isEmpty()) {
                parent.setHead(this);
                parent.setTail(this);
                parent.addNode();
            } else {
                insertAfter(parent.getTail());
            }
        }
        
        public void insertAfter(INode<N, L> preNode) {
            
            this.next = preNode.next;
            preNode.next = this;
            this.prev = preNode;
            
            if (this.next != null) {
                this.next.setPrev(this);
            }
            
            this.parent = preNode.getParent();
            this.parent.addNode();
            
            if (this.parent.getTail() == preNode) {
                this.getParent().setTail(this);
            }
            
            this.parent.addNode();
        }
        
        public void insertBefore(INode<N, L> nxtNode) {
            this.next = nxtNode;
            this.prev = nxtNode.prev;
            nxtNode.prev = this;
            
            if (this.prev != null) {
                this.prev.setNext(this);
            }
            
            this.parent = nxtNode.getParent();
            this.parent.addNode();
            
            if (this.parent.getHead() == nxtNode) {
                this.parent.setHead(this);
            }
            this.parent.addNode();
        }
        
        public INode<N, L> removeFromList() {
            parent.removeNode();
            if (parent.getHead() == this) {
                parent.setHead(this.next);
            }
            if (parent.getTail() == this) {
                parent.setTail(this.prev);
            }
            
            if (this.prev != null && this.next != null) {
                this.prev.setNext(this.next);
                this.next.setPrev(this.prev);
            } else if (this.prev != null) {
                this.prev.setNext(this.next);
            } else if (this.next != null){
                this.next.setPrev(this.prev);
            }
            
            this.prev = this.next = null;
            this.parent = null;
            return this;
        }
        
        
        public N getValue() {
            return value;
        }
        
        public void setValue(N value) {
            this.value = value;
        }
        
        public INode<N, L> getPrev() {
            return prev;
        }
        
        public void setPrev(INode<N, L> prev) {
            this.prev = prev;
        }
        
        public INode<N, L> getNext() {
            return next;
        }
        
        public void setNext(INode<N, L> next) {
            this.next = next;
        }
        
        public IList<N, L> getParent() {
            return parent;
        }
        
        public void setParent(IList<N, L> parent) {
            this.parent = parent;
        }
    }
}
