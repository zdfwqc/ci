package utils;

import java.util.Objects;

public class CPair<T1 extends Comparable<T1>, T2 extends Comparable<T2>>
    implements Comparable<CPair<T1, T2>> {
    private T1 first;
    private T2 second;
    
    public CPair(T1 first, T2 second) {
        this.first = first;
        this.second = second;
    }
    
    public T1 getFirst() {
        return first;
    }
    
    public void setFirst(T1 first) {
        this.first = first;
    }
    
    public T2 getSecond() {
        return second;
    }
    
    public void setSecond(T2 second) {
        this.second = second;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CPair<?, ?> pair = (CPair<?, ?>) o;
        return Objects.equals(first, pair.first) &&
            Objects.equals(second, pair.second);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(first, second);
    }
    
    @Override
    public int compareTo(CPair<T1, T2> o) {
        if (first.compareTo(o.first) == 0) {
            return second.compareTo(o.second);
        } else {
            return first.compareTo(o.first);
        }
    }
}
