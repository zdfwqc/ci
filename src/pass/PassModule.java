package pass;

import IR.MyModule;
import backend.CodeGenModule;
import config.Config;
import pass.ir.Mem2Reg;
import pass.mc.RegAllocator;

import java.util.ArrayList;

public class PassModule {
    private static final PassModule INSTANCE = new PassModule();
    private final ArrayList<Pass.IRPass> irPasses = new ArrayList<>();
    private final ArrayList<Pass.MCPass> mcPasses = new ArrayList<>();
    
    public static PassModule getInstance() {
        return INSTANCE;
    }
    
    public PassModule() {
        
        if (Config.runMem2Reg) {
            irPasses.add(new Mem2Reg());
        }
        if (Config.runRegAllocator) {
            mcPasses.add(new RegAllocator());
        }
    }
    
    public void runIRPasses() {
        irPasses.forEach(p -> p.run(MyModule.getInstance()));
    }
    
    public void runMCPasses() {
        mcPasses.forEach(p -> p.run(CodeGenModule.getInstance()));
    }
}
