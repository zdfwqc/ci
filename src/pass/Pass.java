package pass;

import IR.MyModule;
import backend.CodeGenModule;

public interface Pass {
    String getName();
    
    interface MCPass extends Pass {
        void run(CodeGenModule codeGenManager);
    }
    
    interface IRPass extends Pass {
        void run(MyModule m);
    }
}
