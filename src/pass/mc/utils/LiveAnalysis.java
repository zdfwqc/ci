package pass.mc.utils;

import backend.Operand;
import backend.armv7.MCBlock;
import backend.armv7.MCFunction;
import backend.armv7.instructions.basic.MCInstruction;
import utils.IList;

import java.util.HashMap;
import java.util.HashSet;

public class LiveAnalysis {
    private HashMap<MCBlock, BlockLiveInfo> liveAnalysisRes;
    private final MCFunction mf;
    
    public LiveAnalysis(MCFunction mf) {
        this.mf = mf;
        liveAnalysisRes = new HashMap<>();
        run();
    }
    
    private void run() {
        for (IList.INode<MCBlock, MCFunction> mbEntry : mf.getBlockList()) {
            MCBlock mb = mbEntry.getValue();
            BlockLiveInfo info = new BlockLiveInfo();
            liveAnalysisRes.put(mb, info);
            
            for (var iEntry : mb.getInstrList()) {
                MCInstruction instr = iEntry.getValue();
                instr.getUseReg().stream()
                    .filter(Operand::needColor)
                    .filter(use -> !info.liveDef.contains(use))
                    .forEach(info.liveUse::add);
                
                instr.getDefReg().stream()
                    .filter(Operand::needColor)
                    .filter(def -> !info.liveUse.contains(def))
                    .forEach(info.liveDef::add);
            }
            
            info.liveIn.addAll(info.liveUse);
        }
        
        boolean change = true;
        while (change) {
            change = false;
            for (var mbEntry : mf.getBlockList()) {
                MCBlock mb = mbEntry.getValue();
                BlockLiveInfo info = liveAnalysisRes.get(mb);
                HashSet<Operand> newLiveOut = new HashSet<>();
                
                if (mb.hasSucc()) {
                    newLiveOut.addAll(liveAnalysisRes.get(mb.getTrueBlock()).liveIn);
                    if (mb.hasFalseSucc()) {
                        newLiveOut.addAll(liveAnalysisRes.get(mb.getFalseBlock()).liveIn);
                    }
                }
                
                if(!newLiveOut.equals(info.liveOut)){
                    change = true;
                    info.liveIn = new HashSet<>(info.liveUse);
                    info.liveOut = newLiveOut;
                    info.liveOut.stream()
                        .filter(out->!info.liveDef.contains(out))
                        .forEach(info.liveIn::add);
                }
            }
        }
    }
    
    public HashMap<MCBlock, BlockLiveInfo> getRes() {
        return liveAnalysisRes;
    }
    
    public static class BlockLiveInfo {
        private final HashSet<Operand> liveUse = new HashSet<>();
        private final HashSet<Operand> liveDef = new HashSet<>();
        private HashSet<Operand> liveIn = new HashSet<>();
        private HashSet<Operand> liveOut = new HashSet<>();
        
        public BlockLiveInfo() {
        }
    
        public HashSet<Operand> getLiveOut() {
            return liveOut;
        }
    }
}
