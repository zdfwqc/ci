package pass.mc;

import backend.CodeGenModule;
import backend.Operand;
import backend.armv7.MCBlock;
import backend.armv7.MCFunction;
import backend.armv7.instructions.CondType;
import backend.armv7.instructions.Shift;
import backend.armv7.instructions.basic.MCInstruction;
import backend.armv7.instructions.dataprocess.Binary;
import backend.armv7.instructions.loadstore.Load;
import backend.armv7.instructions.loadstore.Move;
import backend.armv7.instructions.loadstore.Store;
import backend.imm.IntImm;
import backend.reg.RegularReg;
import backend.reg.Reg;
import backend.reg.VirtualReg;
import pass.Pass;
import pass.mc.utils.LiveAnalysis;
import utils.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Stack;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class RegAllocator implements Pass.MCPass {
    @Override
    public String getName() {
        return "RegAllocator";
    }
    
    private HashMap<VirtualReg, VirtualReg> new2Old;
    private HashMap<VirtualReg, Integer> newLiveLength;
    
    @Override
    public void run(CodeGenModule codeGenManager) {
        for (MCFunction mf : codeGenManager.getFunctions()) {
            new2Old = new HashMap<>();
            newLiveLength = new HashMap<>();
            boolean done = false;
            while (!done) {
                init(mf);
                build(mf);
                mkWorklist();
                while (true) {
                    if (!simplifyWorklist.isEmpty()) {
                        simplyfy();
                    } else if (!worklistMoves.isEmpty()) {
                        coalesce();
                    } else if (!freezeWorklist.isEmpty()) {
                        freeze();
                    } else if (!spillWorklist.isEmpty()) {
                        selectSpill();
                    } else {
                        break;
                    }
                }
                assignColors(mf);
                if (!spilledNodes.isEmpty()) {
                    rewriteProgram(mf);
                } else {
                    done = true;
                }
            }
        }
        
        for (var mf : codeGenManager.getFunctions()) {
            for (var mbEntry : mf.getBlockList()) {
                for (var instrEntry : mbEntry.getValue().getInstrList()) {
                    var instr = instrEntry.getValue();
                    
                    instr.getDefReg().stream().filter(RegularReg.class::isInstance)
                        .map(RegularReg.class::cast).forEach(reg -> reg.setAllocated(false));
                    instr.getUseReg().stream().filter(RegularReg.class::isInstance)
                        .map(RegularReg.class::cast).forEach(reg -> reg.setAllocated(false));
                }
            }
        }
        
        codeGenManager.getFunctions().forEach(codeGenManager::updateStack);
    }
    
    private LiveAnalysis liveAnalysis;
    
    private final int k = 14;
    // DS
    private HashSet<Operand> initial;
    private HashSet<Operand> simplifyWorklist;
    private HashSet<Operand> freezeWorklist;
    private HashSet<Operand> spillWorklist;
    private HashSet<Operand> spilledNodes;
    private HashSet<Operand> coalescedNodes;
    private HashSet<Operand> coloredNodes;
    private Stack<Operand> selectStack;
    // Move Sets
    private HashSet<Move> coalescedMoves;
    private HashSet<Move> constrainedMoves;
    private HashSet<Move> frozenMoves;
    private HashSet<Move> worklistMoves;
    private HashSet<Move> activeMoves;
    // Others
    private HashSet<Pair<Operand, Operand>> adjSet;
    private HashMap<Operand, HashSet<Operand>> adjList;
    private HashMap<Operand, Integer> degree;
    private HashMap<Operand, HashSet<Move>> moveList;
    private HashMap<Operand, Operand> alias;
    
    private void init(MCFunction mf) {
        // 完成活跃性分析和其他结构初始化
        liveAnalysis = new LiveAnalysis(mf);
        
        initial = mf.getVirtualRegMap().values().stream().filter(vreg -> !vreg.isGlobal())
            .collect(Collectors.toCollection(HashSet::new));
        simplifyWorklist = new HashSet<>();
        freezeWorklist = new HashSet<>();
        spillWorklist = new HashSet<>();
        spilledNodes = new HashSet<>();
        coalescedNodes = new HashSet<>();
        coloredNodes = new HashSet<>();
        selectStack = new Stack<>();
        
        coalescedMoves = new HashSet<>();
        constrainedMoves = new HashSet<>();
        frozenMoves = new HashSet<>();
        worklistMoves = new HashSet<>();
        activeMoves = new HashSet<>();
        
        adjSet = new HashSet<>();
        adjList = new HashMap<>();
        degree = new HashMap<>();
        moveList = new HashMap<>();
        alias = new HashMap<>();
    }
    
    private void mapAddElement(HashMap<Operand, HashSet<Operand>> map,
                               Operand u, Operand v) {
        map.putIfAbsent(u, new HashSet<>());
        map.get(u).add(v);
    }
    
    private void mapAddElement(HashMap<Operand, HashSet<Move>> map, Operand u, Move v) {
        map.putIfAbsent(u, new HashSet<>());
        map.get(u).add(v);
    }
    
    private int getDegree(Operand u) {
        return degree.getOrDefault(u, 0);
    }
    
    private void addEdge(Operand u, Operand v) {
        Pair<Operand, Operand> edge = new Pair<>(u, v);
    
        if (!adjSet.contains(edge) && !u.equals(v)) {
            adjSet.add(edge);
            adjSet.add(new Pair<>(v, u));
            if (!u.isPrecolored()) {
                mapAddElement(adjList, u, v);
                degree.put(u, getDegree(u) + 1);
            }
            if (!v.isPrecolored()) {
                mapAddElement(adjList, v, u);
                degree.put(v, getDegree(v) + 1);
            }
        }
    }
    
    private void build(MCFunction mf) {
        for (var mbEntry : mf.getBlockList()) {
            MCBlock mb = mbEntry.getValue();
            HashSet<Operand> live = new HashSet<>(liveAnalysis.getRes().get(mb).getLiveOut());
            
            for (var instrEntry = mb.getInstrList().getTail();
                 instrEntry != null;
                 instrEntry = instrEntry.getPrev()) {
                MCInstruction instr = instrEntry.getValue();
                if (instr instanceof Move && instr.getCond() == CondType.NONE &&
                    instr.getShift().isNone()) {
                    Operand dst = ((Move) instr).getDst();
                    Operand rhs = ((Move) instr).getRhs();
                    
                    if (dst.needColor() && rhs.needColor()) {
                        live.remove(rhs);
                        mapAddElement(moveList, rhs, (Move) instr);
                        mapAddElement(moveList, dst, (Move) instr);
                        
                        worklistMoves.add((Move) instr);
                    }
                }
                
                var defs = instr.getDefReg();
                var uses = instr.getUseReg();
                defs.stream().filter(Reg::needColor).forEach(live::add);
                
                defs.stream().filter(Reg::needColor)
                    .forEach(d -> live.forEach(l -> addEdge(l, d)));
                
                defs.stream().filter(Reg::needColor).forEach(live::remove);
                uses.stream().filter(Reg::needColor).forEach(live::add);
            }
        }
    }
    
    private HashSet<Operand> adjacent(Operand n) {
        HashSet<Operand> res = new HashSet<>(adjList.getOrDefault(n, new HashSet<>()));
        selectStack.forEach(res::remove);
        coalescedNodes.forEach(res::remove);
        return res;
    }
    
    private HashSet<Move> nodeMoves(Operand n) {
        HashSet<Move> res = new HashSet<>(moveList.getOrDefault(n, new HashSet<>()));
        activeMoves.forEach(res::remove);
        worklistMoves.forEach(res::remove);
        return res;
    }
    
    private boolean moveRelate(Operand n) {
        return !moveList.getOrDefault(n, new HashSet<>()).isEmpty();
    }
    
    private void mkWorklist() {
        for (var n : initial) {
            if (getDegree(n) >= k) {
                spillWorklist.add(n);
            } else if (moveRelate(n)) {
                freezeWorklist.add(n);
            } else {
                simplifyWorklist.add(n);
            }
        }
        initial = new HashSet<>();
    }
    
    private void simplyfy() {
        var n = simplifyWorklist.iterator().next();
        simplifyWorklist.remove(n);
        selectStack.push(n);
        adjacent(n).forEach(this::decrementDegree);
    }
    
    private void decrementDegree(Operand m) {
        var d = getDegree(m);
        degree.put(m, d - 1);
        if (d == k) {
            HashSet<Operand> tmp = adjacent(m);
            tmp.add(m);
            enableMoves(tmp);
            spillWorklist.remove(m);
            if (moveRelate(m)) {
                freezeWorklist.add(m);
            } else {
                simplifyWorklist.add(m);
            }
        }
    }
    
    private void enableMoves(HashSet<Operand> nodes) {
        nodes.forEach(n -> nodeMoves(n).forEach(m -> {
            activeMoves.remove(m);
            worklistMoves.add(m);
        }));
    }
    
    private void coalesce() {
        var m = worklistMoves.iterator().next();
        var x = getAlias(m.getDst());
        var y = getAlias(m.getRhs());
        
        var u = x;
        var v = y;
        if (y.isPrecolored()) {
            u = y;
            v = x;
        }
        worklistMoves.remove(m);
        if (u.equals(v)) {
            coalescedMoves.add(m);
            addWorkList(u);
        } else if (v.isPrecolored() || adjSet.contains(new Pair<>(u, v))) {
            constrainedMoves.add(m);
            addWorkList(u);
            addWorkList(v);
        } else if ((u.isPrecolored() && adjOk(u, v)) ||
            (!u.isPrecolored() && conservative(adjacent(u), adjacent(v)))) {
            constrainedMoves.add(m);
            combine(u, v);
            addWorkList(u);
        } else {
            activeMoves.add(m);
        }
    }
    
    private void addWorkList(Operand u) {
        if (!u.isPrecolored() && !moveRelate(u) && getDegree(u) < k) {
            freezeWorklist.remove(u);
            simplifyWorklist.add(u);
        }
    }
    
    private boolean ok(Operand t, Operand r) {
        return getDegree(t) < k || t.isPrecolored() || adjSet.contains(new Pair<>(t, r));
    }
    
    private boolean adjOk(Operand u, Operand v) {
        return adjacent(v).stream().allMatch(t -> ok(t, u));
    }
    
    private boolean conservative(HashSet<Operand> u, HashSet<Operand> v) {
        HashSet<Operand> nodes = new HashSet<>(u);
        nodes.addAll(v);
        var cnt = nodes.stream().filter(n -> getDegree(n) >= k).count();
        return cnt < k;
    }
    
    private Operand getAlias(Operand n) {
        while (coalescedNodes.contains(n)) {
            n = alias.get(n);
        }
        return n;
    }
    
    private void combine(Operand u, Operand v) {
        if (freezeWorklist.contains(v)) {
            freezeWorklist.remove(v);
        } else {
            spillWorklist.remove(v);
        }
        
        coalescedNodes.add(v);
        alias.put(v, u);
        moveList.get(u).addAll(moveList.get(v));
        
        adjacent(v).forEach(t -> {
            addEdge(t, u);
            decrementDegree(t);
        });
        
        if (getDegree(u) >= k && freezeWorklist.contains(u)) {
            freezeWorklist.remove(u);
            spillWorklist.add(u);
        }
    }
    
    private void freeze() {
        var u = freezeWorklist.iterator().next();
        freezeWorklist.remove(u);
        simplifyWorklist.add(u);
        freezeMove(u);
    }
    
    private void freezeMove(Operand u) {
        for (Move m : nodeMoves(u)) {
            if (activeMoves.contains(m)) {
                activeMoves.remove(m);
            } else {
                worklistMoves.remove(m);
            }
            frozenMoves.add(m);
            Operand v = m.getDst().equals(u) ? m.getDst() : m.getRhs();
            if (nodeMoves(v).isEmpty() && getDegree(v) < k) {
                freezeWorklist.remove(v);
                simplifyWorklist.add(v);
            }
        }
    }
    
    // 可优化
    private Operand selectSpillNode() {
        return spillWorklist.iterator().next();
    }
    
    private void selectSpill() {
        Operand m = selectSpillNode();
        spillWorklist.remove(m);
        simplifyWorklist.add(m);
        freezeMove(m);
    }
    
    private void assignColors(MCFunction mf) {
        var colored = new HashMap<Operand, RegularReg>();
        while (!selectStack.isEmpty()) {
            var n = selectStack.pop();
            var okColors = IntStream.range(0, 15).filter(i -> i != 13).boxed() // 15
                .collect(Collectors.toCollection(HashSet::new));
            
            for (Operand w : adjList.getOrDefault(n, new HashSet<>())) {
                var tmp = getAlias(w);
                if (coloredNodes.contains(tmp) || tmp.isPrecolored()) {
                    if (tmp instanceof RegularReg) {
                        okColors.remove(((RegularReg) tmp).getId());
                    } else if (tmp instanceof VirtualReg) {
                        okColors.remove(colored.get(tmp).getId());
                    }
                }
            }
            
            if (okColors.isEmpty()) {
                spilledNodes.add(n);
            } else {
                coloredNodes.add(n);
                var pid = okColors.iterator().next();
                colored.put(n, new RegularReg(pid, true));
            }
        }
        
        if (!spilledNodes.isEmpty()) {
            return;
        }
        
        for (var n : coalescedNodes) {
            var tmp = getAlias(n);
            if (tmp.isPrecolored()) {
                colored.put(n, (RegularReg) tmp);
            } else {
                colored.put(n, colored.get(tmp));
            }
        }
        
        for (var mbEntry : mf.getBlockList()) {
            MCBlock mb = mbEntry.getValue();
            for (var mcEntry : mb.getInstrList()) {
                MCInstruction mc = mcEntry.getValue();
                var defs = new ArrayList<>(mc.getDefReg());
                var uses = new ArrayList<>(mc.getUseReg());
                
                defs.stream().filter(colored::containsKey)
                    .forEach(def -> mc.replaceReg(def, colored.get(def)));
                uses.stream().filter(colored::containsKey)
                    .forEach(use -> mc.replaceReg(use, colored.get(use)));
            }
        }
    }
    
    private void rewriteProgram(MCFunction mf) {
        for (var n : spilledNodes) {
            boolean storeInStack = true;
            
            for (var mbEntry : mf.getBlockList()) {
                var mb = mbEntry.getValue();
                var stSize = mf.getStackSize();
                
                Ref ref = new Ref();
                
                int cntInstr = 0;
                for (var instrEntry : mb.getInstrList()) {
                    MCInstruction instr = instrEntry.getValue();
                    HashSet<Reg> defs = new HashSet<>(instr.getDefReg());
                    HashSet<Reg> uses = new HashSet<>(instr.getUseReg());
                    
                    defs.stream().filter(def -> def.equals(n)).forEach(def -> {
                        if (ref.vreg == null) {
                            ref.vreg = cloneReg(mf, (VirtualReg) n);
                        }
                        
                        instr.replaceReg(def, ref.vreg);
                        ref.lastDef = instr;
                    });
                    
                    uses.stream().filter(use -> use.equals(n)).forEach(use -> {
                        if (ref.vreg == null) {
                            ref.vreg = cloneReg(mf, (VirtualReg) n);
                        }
                        
                        instr.replaceReg(use, ref.vreg);
                        if (ref.firstUse == null && ref.lastDef == null) {
                            ref.firstUse = instr;
                        }
                    });
                    
                    if (cntInstr > 30) {
                        checkPoint(mf, mbEntry.getValue(), storeInStack, ref, stSize, n);
                    }
                    cntInstr++;
                }
                
                checkPoint(mf, mbEntry.getValue(), storeInStack, ref, stSize, n);
            }
            
            if (storeInStack) {
                mf.addStackSize(4);
            }
        }
    }
    
    private VirtualReg cloneReg(MCFunction mf, VirtualReg reg) {
        var oldReg = reg;
        var newReg = new VirtualReg(mf);
        
        if (!reg.isGlobal()) {
            while (new2Old.containsKey(oldReg)) {
                oldReg = new2Old.get(oldReg);
            }
            new2Old.put(newReg, oldReg);
            // newVReg.setDef(oldVReg.getDefMC(), oldVReg.getCost());
        }
        return newReg;
    }
    
    private void fixOffset(MCFunction mf, MCInstruction instr, int offset) {
        Operand off = new IntImm(offset);
        if (offset < (1 << 12)) {
            if (instr instanceof Load) {
                Load load = (Load) instr;
                load.setOffset(off);
            } else if (instr instanceof Store) {
                Store store = (Store) instr;
                store.setOffset(off);
            }
        } else {
            VirtualReg vr = new VirtualReg(mf);
            Move move = new Move(vr, off);
            move.insertBeforeInstr(instr);
            if (instr instanceof Load) {
                Load load = (Load) instr;
                load.setOffset(vr);
            } else if (instr instanceof Store) {
                Store store = (Store) instr;
                store.setOffset(vr);
            }
        }
    }
    
    private void checkPoint(MCFunction mf, MCBlock mb, boolean storeInStack,
                            Ref ref, int offset, Operand n) {
        if (storeInStack) {
            if (ref.firstUse != null) {
                Load load = new Load(ref.vreg, new RegularReg("sp"), new IntImm(0));
                load.insertBeforeInstr(ref.firstUse);
                fixOffset(mf, load, offset);
            }
            
            if (ref.lastDef != null) {
                Store store = new Store(ref.vreg, new RegularReg("sp"), new IntImm(0));
                store.insertAfterInstr(ref.lastDef);
            }
        } else {
            var defInstr = ((VirtualReg) n).getDefInstr();
            var regMap = new HashMap<VirtualReg, VirtualReg>();
            var toInsertList = new LinkedList<MCInstruction>();
            var toInsertSet = new HashSet<MCInstruction>();
            var preInstr = ref.firstUse;
            
            toInsertList.addLast(defInstr);
            toInsertSet.add(defInstr);
            
            var defReg = (VirtualReg) defInstr.getDefReg().get(0);
            defReg = new2Old.getOrDefault(defReg, defReg);
            regMap.put(defReg, ref.vreg);
            
            while (!toInsertList.isEmpty()) {
                var instr = toInsertList.pollFirst();
                addDefiner(instr, toInsertList, toInsertSet);
                if (instr instanceof Binary) {
                    var oldInstr = (Binary) instr;
                    var dst = dealRegInCheckPoint(oldInstr.getDst(), regMap, mf);
                    var lhs = dealRegInCheckPoint(oldInstr.getLhs(), regMap, mf);
                    var rhs = dealRegInCheckPoint(oldInstr.getRhs(), regMap, mf);
                    Binary newInstr = new Binary(oldInstr.getInstr(), dst, lhs, rhs);
                    newInstr.insertBeforeInstr(preInstr);
                    newInstr.setShift(new Shift(oldInstr.getShift()));
                    newInstr.setCond(oldInstr.getCond());
                    preInstr = newInstr;
                } else if (instr instanceof Move) {
                    var oldInstr = (Move) instr;
                    var dst = dealRegInCheckPoint(oldInstr.getDst(), regMap, mf);
                    var rhs = dealRegInCheckPoint(oldInstr.getRhs(), regMap, mf);
                    Move newInstr = new Move(dst, rhs);
                    newInstr.insertBeforeInstr(preInstr);
                    newInstr.setShift(new Shift(oldInstr.getShift()));
                    newInstr.setCond(oldInstr.getCond());
                    preInstr = newInstr;
                    if (mf.getArgsMove().contains(instr)) {
                        mf.addArgMove(newInstr);
                    }
                } else if (instr instanceof Load) {
                    var oldInstr = (Load) instr;
                    var data = dealRegInCheckPoint(oldInstr.getDst(), regMap, mf);
                    var addr = dealRegInCheckPoint(oldInstr.getAddr(), regMap, mf);
                    var off = dealRegInCheckPoint(oldInstr.getOffset(), regMap, mf);
                    Load newInstr = new Load(data, addr, off);
                    newInstr.insertBeforeInstr(preInstr);
                    newInstr.setShift(new Shift(oldInstr.getShift()));
                    newInstr.setCond(oldInstr.getCond());
                    preInstr = newInstr;
                }
            }
        }
        
        if (ref.vreg != null) {
            int firstPos = -1;
            int lastPos = -1;
            int pos = 0;
            
            for (var instrEntry : mb.getInstrList()) {
                MCInstruction instr = instrEntry.getValue();
                pos++;
                if (instr.getDefReg().contains(ref.vreg) || instr.getUseReg().contains(ref.vreg)) {
                    if (firstPos == -1) {
                        firstPos = pos;
                    }
                    lastPos = pos;
                }
            }
            
            if (firstPos != -1) {
                newLiveLength.put(ref.vreg, lastPos - firstPos + 1);
            }
        }
        
        ref.vreg = null;
        ref.firstUse = null;
        ref.lastDef = null;
    }
    
    private static class Ref {
        VirtualReg vreg = null;
        MCInstruction firstUse = null;
        MCInstruction lastDef = null;
    }
    
    private void addDefiner(MCInstruction instr, LinkedList<MCInstruction> list,
                            HashSet<MCInstruction> set) {
        if (instr instanceof Binary) {
            var binary = (Binary) instr;
            addReg2ToInsert(list, set, binary.getLhs());
            addReg2ToInsert(list, set, binary.getRhs());
        } else if (instr instanceof Move) {
            var move = (Move) instr;
            addReg2ToInsert(list, set, move.getRhs());
        } else if (instr instanceof Load) {
            var load = (Load) instr;
            addReg2ToInsert(list, set, load.getAddr());
            addReg2ToInsert(list, set, load.getOffset());
        }
    }
    
    private void addReg2ToInsert(LinkedList<MCInstruction> list,
                                 HashSet<MCInstruction> set,
                                 Operand reg) {
        if (reg instanceof VirtualReg && !((VirtualReg) reg).isGlobal()) {
            if (!set.contains(((VirtualReg) reg).getDefInstr())) {
                list.addLast(((VirtualReg) reg).getDefInstr());
                set.add(((VirtualReg) reg).getDefInstr());
            }
        }
    }
    
    private Operand dealRegInCheckPoint(Operand reg,
                                        HashMap<VirtualReg, VirtualReg> map, MCFunction mf) {
        if (reg instanceof VirtualReg && !((VirtualReg) reg).isGlobal()) {
            var vreg = new2Old.getOrDefault(reg, (VirtualReg) reg);
            if (!map.containsKey(vreg)) {
                map.put(vreg, cloneReg(mf, vreg));
            }
            return map.get(vreg);
        } else {
            return reg;
        }
    }
}


