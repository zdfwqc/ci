package pass.ir;

import IR.MyModule;
import IR.Values.*;
import IR.Values.instructions.Instruction;
import IR.Values.instructions.OP;
import IR.Values.instructions.mem.AllocaInst;
import IR.Values.instructions.mem.LoadInst;
import IR.Values.instructions.mem.Phi;
import IR.Values.instructions.mem.StoreInst;
import pass.Pass;
import utils.IList;

import java.util.*;

import static IR.Analysis.DomAnalysis.analyzeDom;

public class Mem2Reg implements Pass.IRPass {
    @Override
    public String getName() {
        return null;
    }

    @Override
    public void run(MyModule m) {
        for (IList.INode<Function, MyModule> fun : m.getFunctions()) {
            if (!fun.getValue().isLibraryFunction()) {
                mem2reg(fun.getValue());
            }
        }
    }

    private void mem2reg(Function function) {

        // Key for allocaInst of Int and Float 's pointer
        // Value for an Arraylist of inst which assign the pointer
        HashMap<AllocaInst, ArrayList<BasicBlock>> defMap = new HashMap<>();
        ArrayList<AllocaInst> defArraylist = new ArrayList<>();

        HashMap<BasicBlock, HashSet<BasicBlock>> DFMap = analyzeDom(function);
        HashMap<Phi, AllocaInst> phiAllocaInstHashMap = new HashMap<>();
        Stack<RenameBlock> renameBlockStack = new Stack<>();
        ArrayList<Value> values = new ArrayList<>();
        boolean isInt = false;
        boolean isFloat = false;
        // initial the variables' defs
        for (IList.INode<BasicBlock, Function> basicBlockINode : function.getBBList()) {
            BasicBlock basicBlock = basicBlockINode.getValue();
            for (IList.INode<Instruction, BasicBlock> instructionINode : basicBlock.getList()) {
                Instruction instruction = instructionINode.getValue();

                if (instruction.getOp().equals(OP.Alloca)) {
                    AllocaInst allocaInst = (AllocaInst) instruction;

                    if (allocaInst.getAllocatedType().isFloatingType() ||
                            allocaInst.getAllocatedType().isIntegerType()) {
                        if (allocaInst.getAllocatedType().isFloatingType()) {
                            isInt = true;
                        } else {
                            isFloat = true;
                        }
                        defMap.put(allocaInst, new ArrayList<>());
                        defArraylist.add(allocaInst);
                    }

                }
            }
        }

        // initial the variables' store
        for (IList.INode<BasicBlock, Function> basicBlockINode : function.getBBList()) {
            BasicBlock basicBlock = basicBlockINode.getValue();
            for (IList.INode<Instruction, BasicBlock> instructionINode : basicBlock.getList()) {
                Instruction instruction = instructionINode.getValue();

                if (instruction.getOp().equals(OP.Store)) {
                    StoreInst storeInst = (StoreInst) instruction;
                    if (!(storeInst.getOperands().get(1) instanceof Instruction)) {
                        continue;
                    }
                    Instruction targetInst = (Instruction) storeInst.getOperands().get(1);

                    if (targetInst instanceof AllocaInst) {
                        if (defMap.containsKey((AllocaInst) targetInst)) {
                            defMap.get(targetInst).add(basicBlock);
                        }
                    }
                }
            }
        }

        HashMap<AllocaInst, ArrayList<BasicBlock>> tmpDefMap = new HashMap<>(defMap);

        for (AllocaInst allocaInst : defMap.keySet()) {
            if (defMap.get(allocaInst).size() == 0) {
                tmpDefMap.remove(allocaInst);
                defArraylist.remove(allocaInst);
            }
        }

        defMap = tmpDefMap;

        // get block to insert phi
        HashMap<BasicBlock, Boolean> visited = new HashMap<>();
        for (IList.INode<BasicBlock, Function> basicBlockNode : function.getBBList()) {
            visited.put(basicBlockNode.getValue(), false);
        }
        for (AllocaInst allocaInst : defArraylist) {
            Queue<BasicBlock> workList = new LinkedList<>(defMap.get(allocaInst));
            HashMap<BasicBlock, Boolean> placed = new HashMap<>();

            // initial visited and placed
            for (IList.INode<BasicBlock, Function> basicBlockNode : function.getBBList()) {
                visited.put(basicBlockNode.getValue(), false);
                placed.put(basicBlockNode.getValue(), false);
            }

            while (!workList.isEmpty()) {
                BasicBlock X = workList.remove();
                HashSet<BasicBlock> DominanceFrontier_X = DFMap.get(X);
                for (BasicBlock Y : DominanceFrontier_X) {

                    if (!placed.get(Y)) {
                        placed.replace(Y, true);
                        // Phi phiInst = new Phi(Y, allocaInst.getType());
                        // phiAllocaInstHashMap.put(phiInst, allocaInst);
                        // Phi phiInst = new Phi(TAG_.Phi, factory.getI32Ty(), y.getPredecessor_().size(), y);
                        ArrayList<Value> tmpValues = new ArrayList<>(
                                Collections.nCopies(Y.getPredecessors().size(), null));
                        Phi phiInst = new Phi(Y, allocaInst.getType(), tmpValues);
                        // Phi phiInst = null;//new Phi(Y, allocaInst.getType());
                        phiAllocaInstHashMap.put(phiInst, allocaInst);

                        if (!visited.get(Y)) {
                            visited.replace(Y, true);
                            workList.add(Y);
                        }
                    }

                }
            }

        }

        // variable renaming
        visited.replaceAll((key, value) -> false);

        // 对每一个values设置一个value值
        for (int i = 0; i < defArraylist.size(); i++) {
            if (isInt) {
                values.add(new ConstantInteger(0));
            } else {
                values.add(new ConstantFloat(0.0F));
            }
            // values.add(null);
        }
        RenameBlock entryBlock = new RenameBlock(function.getBBList().getHead().getValue(), null, values);
        renameBlockStack.push(entryBlock);

        // loop begin
        while (!renameBlockStack.isEmpty()) {
            RenameBlock loopBlock = renameBlockStack.pop();

            BasicBlock curBasicBlock = loopBlock.basicBlock;
            BasicBlock predBlock = loopBlock.predecessor;
            ArrayList<Value> tmpValues = new ArrayList<>(loopBlock.values);

            // search phi
            for (IList.INode<Instruction, BasicBlock> instructionNode : curBasicBlock.getList()) {
                if (instructionNode.getValue().getOp().equals(OP.Phi)) {
                    Phi phiInst = (Phi) instructionNode.getValue();
                    if (phiAllocaInstHashMap.containsKey(phiInst)) {
                        // TODO
                        phiInst.replaceOperands(curBasicBlock.getPredecessors().indexOf(predBlock),
                                tmpValues.get(defArraylist.indexOf(phiAllocaInstHashMap.get(phiInst))));
                    }
                }
            }

            // handle every inst
            if (visited.get(curBasicBlock)) {
                continue;
            }
            visited.replace(curBasicBlock, true);
            for (IList.INode<Instruction, BasicBlock> instructionNode
                 = curBasicBlock.getList().getHead();
                 instructionNode != null; ) {
                Instruction instruction = instructionNode.getValue();
                var next = instructionNode.getNext();

                // alloca will be removed
                switch (instruction.getOp()) {
                    case Alloca -> {
                        if (defMap.containsKey((AllocaInst) instruction)) {
                            instructionNode.removeFromList();
                        }
                    }
                    case Load -> {
                        LoadInst loadInst = (LoadInst) instruction;
                        if (!(loadInst.getOperands().get(0) instanceof AllocaInst)) {
                            instructionNode = next;
                            continue;
                        }
                        AllocaInst allocaInst = (AllocaInst) loadInst.getOperands().get(0);
                        if (!(allocaInst.getAllocatedType().isIntegerType()
                                || allocaInst.getAllocatedType().isFloatingType())) {
                            instructionNode = next;
                            continue;
                        }
                        loadInst.replaceUsedWith(tmpValues.get(defArraylist.indexOf(allocaInst)));
                        instructionNode.removeFromList();
                        instruction.removeUseFromOperands();
                    }
                    case Store -> {
                        StoreInst storeInst = (StoreInst) instruction;
                        if (!(storeInst.getOperands().get(1) instanceof AllocaInst)) {
                            instructionNode = next;
                            continue;
                        }
                        AllocaInst allocaInst = (AllocaInst) storeInst.getOperands().get(1);
                        if (!(allocaInst.getAllocatedType().isIntegerType()
                                || allocaInst.getAllocatedType().isFloatingType())) {
                            instructionNode = next;
                            continue;
                        }
                        tmpValues.set(defArraylist.indexOf(allocaInst), storeInst.getOperands().get(0));
                        storeInst.replaceUsedWith(tmpValues.get(defArraylist.indexOf(allocaInst)));
                        instructionNode.removeFromList();
                        instruction.removeUseFromOperands();
                    }
                    case Phi -> {
                        Phi phiInst = (Phi) instruction;
                        tmpValues.set(
                                defArraylist.indexOf(phiAllocaInstHashMap.get(phiInst)),
                                phiInst);
                    }
                }
                instructionNode = next;
            }

            for (BasicBlock bb : curBasicBlock.getSuccessors()) {
                renameBlockStack.push(new RenameBlock(bb, curBasicBlock, tmpValues));
            }
        }
    }

    private class RenameBlock {
        private BasicBlock basicBlock;
        private BasicBlock predecessor;
        private ArrayList<Value> values;

        public RenameBlock(BasicBlock basicBlock, BasicBlock predecessor, ArrayList<Value> values) {
            this.basicBlock = basicBlock;
            this.predecessor = predecessor;
            this.values = new ArrayList<>(values);
        }

    }


}
