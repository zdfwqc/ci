package pass.ir;

import IR.MyModule;
import IR.Values.BasicBlock;
import IR.Values.Function;
import IR.Values.instructions.Instruction;
import IR.Values.instructions.mem.Phi;
import pass.Pass;
import utils.IList;

import java.util.ArrayList;
import java.util.Stack;

/**
 * GVN: 尽可能地消除冗余的变量，同时会做常量合并、代数化简 GCM：把指令调度到支配深度尽可能深的地方
 */
public class GVNGCM implements Pass.IRPass {
    @Override
    public String getName() {
        return null;
    }

    @Override
    public void run(MyModule m) {
        for (IList.INode<Function, MyModule> funcitonNode : m.getFunctions()) {
            var function = funcitonNode.getValue();
            runGVN(function);
        }
    }

    // 逆后序遍历 basicBlock
    private void runGVN(Function function) {
        //get RPO
        ArrayList<BasicBlock> rpo = new ArrayList<>();
        Stack<BasicBlock> stack = new Stack<>();
        stack.push(function.getBBList().getHead().getValue());
        while (!stack.isEmpty()) {
            var tmp = stack.pop();
            rpo.add(tmp);
            for (var i : tmp.getSuccessors()) {
                if (!rpo.contains(i) && !stack.contains(i)) {
                    stack.push(i);
                }
            }
        }

        // 遍历
        for (var bb : rpo) {
            runGVNBasicBlock(bb);
        }
    }

    /*
    1、删除所有相同的 phi 指令
    2、遍历BB中的每一条指令
     */
    private void runGVNBasicBlock(BasicBlock basicBlock) {
        //1、删除相同 phi 指令
        for (IList.INode<Instruction, BasicBlock> instNode : basicBlock.getList()) {
            var inst = instNode.getValue();
            var next = instNode.getNext();

            for (var tmp = next; !(tmp.getValue() instanceof Phi); tmp = tmp.getNext()) {

            }
        }
    }

    /*
    1、简化指令（编译阶段即可计算出值的指令）
    2、针对不同类型的指令做出优化
     */
    private void runGVNInstruction(Instruction instruction) {

    }
}
