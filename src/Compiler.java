import VISITOR.SysY2022Lexer;
import VISITOR.SysY2022Parser;
import VISITOR.Visitor;
import backend.CodeGenModule;
import config.Config;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import pass.PassModule;

import java.io.*;

public class Compiler {
    public static void main(String[] args) throws IOException {
        Config.init(args);
        
        FileInputStream fileInputStream = new FileInputStream(Config.fileInPath);
        PrintStream printStream = new PrintStream(Config.fileOutPath);
        System.setIn(fileInputStream);
        System.setOut(printStream);
        CharStream inputStream = CharStreams.fromStream(System.in); // 获取输入流
        SysY2022Lexer lexer = new SysY2022Lexer(inputStream);
        CommonTokenStream tokenStream = new CommonTokenStream(lexer); // 词法分析获取 token 流
        SysY2022Parser parser = new SysY2022Parser(tokenStream);
        ParseTree tree = parser.compUnit();
        // 获取语法树的根节点
        Visitor visitor = new Visitor();
        visitor.visit(tree);
        //System.out.println("ready to toString:");
        
        PassModule.getInstance().runIRPasses();
        
        CodeGenModule.getInstance().loadIR();
        CodeGenModule.getInstance().run();
        
        PassModule.getInstance().runMCPasses();
        System.out.println(CodeGenModule.getInstance().genArmv7());
        
    }
}


