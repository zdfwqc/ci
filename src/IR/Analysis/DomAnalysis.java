package IR.Analysis;

import IR.Values.BasicBlock;
import IR.Values.Function;
import utils.IList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

/*
    用于分析支配信息。
 */
public class DomAnalysis {
    private HashMap<BasicBlock, ArrayList<BasicBlock>> idom = new HashMap<>();

    //计算支配边界
    public static HashMap<BasicBlock, HashSet<BasicBlock>> analyzeDom(Function function) {
        // 提取出blocks
        HashMap<BasicBlock, HashSet<BasicBlock>> dom = new HashMap<>();
        HashMap<BasicBlock, BasicBlock> idom = new HashMap<>();
        HashMap<BasicBlock, HashSet<BasicBlock>> df = new HashMap<>();
        ArrayList<BasicBlock> allBlocks = new ArrayList<>();
        LinkedList<BasicBlock> passBlock = new LinkedList<>();
        passBlock.add(function.getBBList().getHead().getValue());
        allBlocks.add(function.getBBList().getHead().getValue());
        while (!passBlock.isEmpty()) {
            BasicBlock ref = passBlock.poll();
            for (BasicBlock bb : ref.getSuccessors()) {
                if (!allBlocks.contains(bb)) {
                    allBlocks.add(bb);
                    passBlock.add(bb);
                }
            }
        }
        // 初始化dom
        for (BasicBlock basicBlock : allBlocks) {
            dom.put(basicBlock, null);
            df.put(basicBlock, new HashSet<>());
        }
        dom.replace(function.getBBList().getHead().getValue(), new HashSet<>());
        dom.get(function.getBBList().getHead().getValue())
                .add(function.getBBList().getHead().getValue());
        // 计算dom
        boolean changed = true;
        while (changed) {
            changed = false;
            for (BasicBlock basicBlock : allBlocks) {
                //得到前驱块的dom集合的交集
                HashSet<BasicBlock> temPred = null;
                for (BasicBlock bb : basicBlock.getPredecessors()) {
                    if (dom.get(bb) == null) {
                        continue;
                    }
                    if (temPred == null) {
                        temPred = new HashSet<>(dom.get(bb));
                    } else {
                        HashSet<BasicBlock> temp = new HashSet<>();
                        for (BasicBlock block : dom.get(bb)) {
                            if (temPred.contains(block)) {
                                temp.add(block);
                            }
                        }
                        temPred = temp;
                    }
                }
                if (temPred == null) {
                    temPred = new HashSet<>();
                }
                HashSet<BasicBlock> temBlocks = new HashSet<>(temPred);
                temBlocks.add(basicBlock);
                if (!temBlocks.equals(dom.get(basicBlock))) {
                    dom.replace(basicBlock, new HashSet<>(temBlocks));
                    changed = true;
                }
            }
        }
        //debug
        /*for (BasicBlock basicBlockFunctionINode : allBlocks) {
            System.out.print(basicBlockFunctionINode.getType() + " : ");
            for (BasicBlock basicBlock : dom.get(basicBlockFunctionINode)) {
                System.out.print(basicBlock.getType() + " ");
            }
            System.out.print("\n");
        }*/

        /*--------------------------------*/


        // 计算idom
        for (BasicBlock basicBlock : dom.keySet()) {
            HashSet<BasicBlock> tmpDomSet = dom.get(basicBlock);
            if (tmpDomSet.size() == 1) {
                idom.put(basicBlock, null);
            }
            for (BasicBlock mayIDom : tmpDomSet) {
                if (mayIDom.equals(basicBlock)) {
                    continue;
                }
                boolean isIDom = true;
                for (BasicBlock tmpDomBlock : tmpDomSet) {
                    if (!tmpDomBlock.equals(basicBlock) &&
                            !tmpDomBlock.equals(mayIDom) &&
                            dom.get(tmpDomBlock).contains(mayIDom)) {

                        isIDom = false;
                        break;
                    }
                }

                if (isIDom) {
                    idom.put(basicBlock, mayIDom);
                    break;
                }
            }

        }

        // test Idom
        /*for (BasicBlock bb : idom.keySet()) {
            System.out.print(bb.getType() + " : ");
            if (!(idom.get(bb) == null))
                System.out.print(idom.get(bb).getType() + " ");
            else
                System.out.print("null ");
            System.out.print("\n");
        }
        System.out.println("endddddddd\n");*/

        // 计算DF
        for (BasicBlock basicBlock : allBlocks) {
            if (basicBlock.getPredecessors().size() > 1) {

                for (BasicBlock p : basicBlock.getPredecessors()) {
                    BasicBlock runner = p;
                    while (!runner.equals(idom.get(basicBlock))
                            && !runner.equals(basicBlock)) {
                        df.get(runner).add(basicBlock);
                        runner = idom.get(runner);
                    }
                }
            }
        }
        // test df
        /*for (BasicBlock bb : df.keySet()) {
            System.out.print(bb.getType() + " : ");
            if (!df.get(bb).isEmpty()) {
                HashSet<BasicBlock> tmp = df.get(bb);
                for (BasicBlock tmpbb : tmp) {
                    System.out.print(tmpbb.getType() + " ");
                }
            } else
                System.out.print("null ");
            System.out.print("\n");
        }*/
        return df;
    }
}
