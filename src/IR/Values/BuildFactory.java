package IR.Values;

import IR.Values.instructions.BinaryInst;
import IR.Values.instructions.ConvertionInst;
import IR.Values.instructions.NotInst;
import IR.Values.instructions.OP;
import IR.Values.instructions.mem.AllocaInst;
import IR.Values.instructions.mem.GEPInst;
import IR.Values.instructions.mem.LoadInst;
import IR.Values.instructions.mem.Phi;
import IR.Values.instructions.mem.StoreInst;
import IR.Values.instructions.terminator.BrInst;
import IR.Values.instructions.terminator.CallInst;
import IR.Values.instructions.terminator.RetInst;
import IR.types.ArrayType;
import IR.types.FunctionType;
import IR.types.Type;

import java.util.ArrayList;
import java.util.Collections;

public class BuildFactory {
    private BuildFactory() {
    }

    private static final BuildFactory bf = new BuildFactory();

    public static BuildFactory getInstance() {
        return bf;
    }

    public FunctionType getFunctionType(Type retType, ArrayList<Type> parametersTypes) {
        return new FunctionType(retType, parametersTypes);
    }

    public Function buildFunction(String name, Type ret, ArrayList<Type> parametersTypes) {
        return new Function(name, getFunctionType(ret, parametersTypes), false);
    }

    public Function buildLibraryFunction(String name, Type ret, ArrayList<Type> parametersTypes) {
        return new Function(name, getFunctionType(ret, parametersTypes), true);
    }

    public BasicBlock buildBasicBlock(Function function) {
        return new BasicBlock(function);
    }

    public RetInst buildRet(BasicBlock bb) {
        return new RetInst(bb);
    }

    public RetInst buildRet(BasicBlock bb, Value ret) {
        return new RetInst(bb, ret);
    }

    public BinaryInst buildBinaryInst(BasicBlock bb, OP op, Value left, Value right) {
        return new BinaryInst(bb, op, left, right);
    }

    public ConstantInteger getConstantInt(int value) {
        return new ConstantInteger(value);
    }

    public ConstantFloat getConstantFloat(float value) {
        return new ConstantFloat(value);
    }

    public ConvertionInst buildItoF(Value val, BasicBlock basicBlock) {
        return new ConvertionInst(OP.Itof, val, basicBlock);
    }

    public ConvertionInst buildFtoI(Value val, BasicBlock basicBlock) {
        return new ConvertionInst(OP.Ftoi, val, basicBlock);
    }

    public NotInst buildNotInst(BasicBlock basicBlock, Value value) {
        return new NotInst(basicBlock, value);
    }

    public AllocaInst buildVar(BasicBlock basicBlock, Value value, boolean isConst, Type allocatedType) {
        AllocaInst a = new AllocaInst(basicBlock, isConst, allocatedType);
        if (value != null) {
            StoreInst s = new StoreInst(basicBlock, a, value);
        }
        return a;
    }

    public StoreInst buildStoreInst(BasicBlock basicBlock, Value pointer, Value value) {
        return new StoreInst(basicBlock, pointer, value);
    }

    public GlobalVariable buildGlobalVariable(String name, Type type, boolean isConst, Value value) {
        return new GlobalVariable(name, type, isConst, value);
    }

    public LoadInst buildLoadInst(BasicBlock basicBlock, Value pointer) {
        return new LoadInst(basicBlock, pointer);
    }

    public CallInst buildCallInst(BasicBlock basicBlock, Function function, ArrayList<Value> args) {
        return new CallInst(basicBlock, function, args);
    }

    public BrInst buildBr(BasicBlock basicBlock, BasicBlock trueBlock) {
        return new BrInst(basicBlock, trueBlock);
    }

    public BrInst buildBr(BasicBlock basicBlock, Value cond, BasicBlock trueBlock, BasicBlock falseBlock) {
        return new BrInst(basicBlock, cond, trueBlock, falseBlock);
    }

    public void checkBlockEnd(BasicBlock basicBlock) {
        Value inst = basicBlock.getList().getTail().getValue();
        if (inst instanceof BrInst || inst instanceof RetInst) {
            return;
        }
        this.buildRet(basicBlock);
    }

    public AllocaInst buildArray(BasicBlock basicBlock, boolean isConst, Type arrType) {
        return new AllocaInst(basicBlock, isConst, arrType);
    }

    public ArrayType getArrayType(Type elementType) {
        return new ArrayType(elementType);
    }

    public ArrayType getArrayType(Type elementType, int numOfElement) {
        return new ArrayType(elementType, numOfElement);
    }

    public GlobalVariable buildGlobalArray(boolean isConst, Type type, String name) {
        Value tmp = new ConstantArray(type, ((ArrayType) type).getElementType(),
                new ArrayList<>(Collections.nCopies(((ArrayType) type).getCapacity(),
                        new ConstantInteger(0))));
        return buildGlobalVariable(name, type, isConst, tmp);
    }

    public GEPInst buildGEPInst(BasicBlock basicBlock, Value pointer, ArrayList<Value> index) {
        return new GEPInst(basicBlock, pointer, index);
    }

    public Phi buildPhiInst(BasicBlock basicBlock, Type type, ArrayList<Value> in) {
        return new Phi(basicBlock, type, in);
    }

    public void buildInitArray(Value array, ArrayList<Value> index, Value value) {
        ((ConstantArray) ((GlobalVariable) array).getValue()).storeValue(index, value);
    }

    public ArrayList<Value> getFuncValueList(Function function) {
        return function.getArgs();
    }
}
