package IR.Values;

import IR.types.FloatingType;
import IR.types.Type;

public class ConstantFloat extends Constant {
    private float value;
    
    public ConstantFloat(float value) {
        super(String.valueOf(value), FloatingType.type);
        this.value = value;
    }
    
    public float getValue() {
        return this.value;
    }
    
    @Override
    public String toString() {
        return "float " + value;
    }
}
