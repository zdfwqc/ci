package IR.Values;

import IR.Values.instructions.Instruction;
import IR.types.LabelType;
import utils.IList;

import java.util.ArrayList;

public class BasicBlock extends Value {
    private IList.INode<BasicBlock, Function> node;
    private ArrayList<BasicBlock> predecessors;
    private ArrayList<BasicBlock> successors;
    private IList<Instruction, BasicBlock> instructionList;
    
    public BasicBlock(Function function) {
        super(String.valueOf(REG_NUMBER++), new LabelType());
        this.node = new IList.INode<>(this);
        this.predecessors = new ArrayList<>();
        this.successors = new ArrayList<>();
        this.instructionList = new IList<>(this);
        setParent(function);
    }
    
    public void setParent(Function function) {
        this.node.insertListEnd(function.getBBList());
    }
    
    public void addPred(BasicBlock basicBlock) {
        this.predecessors.add(basicBlock);
    }
    
    public void addSuc(BasicBlock basicBlock) {
        this.successors.add(basicBlock);
    }
    
    public IList.INode<BasicBlock, Function> getNode() {
        return this.node;
    }
    
    public IList<Instruction, BasicBlock> getList() {
        return this.instructionList;
    }
    
    public ArrayList<BasicBlock> getPredecessors() {
        return this.predecessors;
    }
    
    public ArrayList<BasicBlock> getSuccessors() {
        return this.successors;
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (IList.INode<Instruction, BasicBlock> instruction : instructionList) {
            sb.append("    ").append(instruction.getValue()).append("\n");
        }
        return sb.toString();
    }
}
