package IR.Values;

import IR.types.ArrayType;
import IR.types.Type;

import java.util.ArrayList;

public class ConstantArray extends Constant {
    private Type elementType;
    private ArrayList<Value> arr;

    public ConstantArray(Type type, Type elementType, ArrayList<Value> arr) {
        super("", type);
        this.elementType = elementType;
        this.arr = new ArrayList<>(arr);
    }

    public void storeValue(ArrayList<Value> index, Value value) {
        int target = 0;
        // index补零
        for (int i = index.size(); i < ((ArrayType) getType()).getDim().size(); i++) {
            index.add(new ConstantInteger(0));
        }
        // 计算位置
        int curCapacity = 1;
        for (int i = index.size() - 1; i >= 0; i--) {
            target += ((ConstantInteger) index.get(i)).getValue() * curCapacity;
            curCapacity *= ((ArrayType) getType()).getDim().get(i);
        }
        arr.set(target, value);
    }

    public ArrayList<Value> getArr() {
        return arr;
    }

    public Type getElementType() {
        return elementType;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        boolean allZero = true;
        for (var i : arr) {
            if (i instanceof ConstantInteger) {
                if (((ConstantInteger) i).getValue() != 0) {
                    allZero = false;
                    break;
                }
            } else if (i instanceof ConstantFloat) {
                if (((ConstantFloat) i).getValue() != 0.0) {
                    allZero = false;
                    break;
                }
            }
        }
        if (allZero) {
            sb.append("[").append(((ArrayType) getType()).getNumOfElement()).append(" x ").append(elementType).append("]");
            sb.append(" ").append("zeroinitializer");
        } else {
            sb.append("[");
            for (Value c : arr) {
                if (arr.indexOf(c) != 0)
                    sb.append(", ");
                sb.append(c);
            }
            sb.append("]");
        }
        return sb.toString();
    }
}
