package IR.Values;

import IR.MyModule;
import IR.Use;
import IR.types.Type;

import java.util.ArrayList;

/*
    ir一切皆value
 */
public abstract class Value {
    private MyModule module = MyModule.getInstance();//隶属的顶层容器
    private Type type;
    private String name;
    private ArrayList<Use> usesList;
    protected static int REG_NUMBER = 0;

    public Value(String name, Type type) {
        this.type = type;
        this.name = name;
        this.usesList = new ArrayList<>();
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MyModule getModule() {
        return this.module;
    }

    public void addUse(Use use) {
        this.usesList.add(use);
    }

    public void replaceUsedWith(Value value) {
        for (Use use : usesList) {
            use.getUser().setOperands(use.getPosOfOperand(), value);
        }
        this.usesList.clear();
    }

    public void removeUseByUser(User user) {
        ArrayList<Use> tmpUseList = new ArrayList<>(usesList);
        for (Use use : usesList) {
            if (use.getUser().equals(user)) {
                tmpUseList.remove(use);
            }
        }
        this.usesList = tmpUseList;
    }

}
