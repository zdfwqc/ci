package IR.Values;

import IR.types.IntegerType;
import IR.types.Type;

public class ConstantInteger extends Constant {
    private int value;
    
    public ConstantInteger() {
        super("", IntegerType.i32);
        this.value = 0;
    }
    
    public ConstantInteger(int value) {
        super(String.valueOf(value), IntegerType.i32);
        this.value = value;
    }
    
    public int getValue() {
        return value;
    }
    
    @Override
    public String toString() {
        return "i32 " + this.value;
    }
}
