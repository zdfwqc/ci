package IR.Values;

import IR.Use;
import IR.types.Type;

import java.util.ArrayList;

/*
    记录了自身op的value
 */
public abstract class User extends Value {

    private ArrayList<Value> operands;

    public User(String name, Type type) {
        super(name, type);
        this.operands = new ArrayList<>();
    }

    public ArrayList<Value> getOperands() {
        return operands;
    }

    public void addOperand(Value operand) {
        this.operands.add(operand);
        if (operand != null) {
            operand.addUse(new Use(operand, this, operands.size() - 1));
        }
    }

    public void addOperand(int pos, Value operand) {
        this.operands.add(pos, operand);
        if (operand != null) {
            operand.addUse(new Use(operand, this, operands.size() - 1));
        }
    }

    public void setOperands(int pos, Value operand) {
        if (pos >= operands.size()) {
            return;
        }
        this.operands.set(pos, operand);
        if (operand != null) {
            operand.addUse(new Use(operand, this, pos));
        }
    }

    public void removeUseFromOperands() {
        if (operands == null) {
            return;
        }
        for (Value operand : operands) {
            if (operand != null) {
                operand.removeUseByUser(this);
            }
        }
    }

    public void replaceOperands(int indexOf, Value value) {
        Value operand = operands.get(indexOf);
        this.setOperands(indexOf, value);
        if (operand != null && !this.operands.contains(value)) {
            operand.removeUseByUser(this);
        }
    }
}
