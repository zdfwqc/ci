package IR.Values;

import IR.MyModule;
import IR.types.FunctionType;
import IR.types.Type;
import utils.IList;

import java.util.ArrayList;

public class Function extends Value {
    private IList<BasicBlock, Function> list;
    private IList.INode<Function, MyModule> node;
    private ArrayList<Arg> argList;
    private ArrayList<Function> predecessors;
    private ArrayList<Function> successors;
    private boolean isLibraryFunction;

    public Function(String name, Type type, boolean isLibraryFunction) {
        super(name, type);
        REG_NUMBER = 0;
        this.isLibraryFunction = isLibraryFunction;
        this.list = new IList<>(this);
        this.node = new IList.INode<>(this);
        this.argList = new ArrayList<>();
        this.predecessors = new ArrayList<>();
        this.successors = new ArrayList<>();
        for (Type t : ((FunctionType) type).getParametersType()) {
            argList.add(new Arg(t, ((FunctionType) type).getParametersType().indexOf(t),isLibraryFunction));
        }
        this.node.insertListEnd(MyModule.getInstance().getFunctions());
    }

    public ArrayList<Value> getArgs() {
        return new ArrayList<>(argList);
    }

    public void addPred(Function function) {
        this.predecessors.add(function);
    }

    public void addSuc(Function function) {
        this.successors.add(function);
    }

    public boolean isLibraryFunction() {
        return this.isLibraryFunction;
    }

    public class Arg extends Value {
        private int rank;

        public Arg(Type type, int rank, boolean isLibraryFunction) {
            super(isLibraryFunction ? "" : "%" + REG_NUMBER++, type);
            this.rank = rank;
        }

        @Override
        public String toString() {
            return this.getType() + " " + this.getName();
        }
    }

    public IList<BasicBlock, Function> getBBList() {
        return this.list;
    }

    public ArrayList<Arg> getArgList() {
        return argList;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(((FunctionType) this.getType()).getRetType()).append(" @");
        sb.append(this.getName()).append("(");
        for (Arg arg : argList) {
            if (argList.indexOf(arg) != 0) {
                sb.append(", ");
            }
            sb.append(arg.getType()).append(" ");
            sb.append(arg.getName());
        }
        sb.append(")");
        return sb.toString();
    }
}
