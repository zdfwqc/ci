package IR.Values;

import IR.MyModule;
import IR.types.PointerType;
import IR.types.Type;

/*
全局变量类：
    属性：是否是const类型
    
 */
public class GlobalVariable extends User {
    public boolean isConst;

    private Value value;

    public GlobalVariable(String name, Type type, boolean isConst, Value value) {
        super("@" + name, new PointerType(type));
        this.isConst = isConst;
        this.value = value;
        MyModule.getInstance().addGlobalVar(this);
    }

    public Value getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.getName()).append(" = dso_local ");
        if (isConst) {
            sb.append("constant ");
        } else {
            sb.append("global ");
        }
        if (value != null) {
            sb.append(value);
        }
        return sb.toString();
    }
}
