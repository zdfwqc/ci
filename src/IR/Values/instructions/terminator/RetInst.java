package IR.Values.instructions.terminator;

import IR.Values.BasicBlock;
import IR.Values.BuildFactory;
import IR.Values.Value;
import IR.Values.instructions.OP;
import IR.types.FunctionType;
import IR.types.Type;
import IR.types.VoidType;

public class RetInst extends TerminatorInst {
    public RetInst(BasicBlock basicBlock) {
        super(VoidType.type, OP.Ret, basicBlock);
        this.addInstToBlock(basicBlock);
    }
    
    public RetInst(BasicBlock basicBlock, Value ret) {
        super(ret.getType(), OP.Ret, basicBlock);
        Type funcReType = ((FunctionType) basicBlock.getNode().getParent().getValue().getType()).getRetType();
        if (ret.getType().isIntegerType() && funcReType.isFloatingType()) {
            Value r = BuildFactory.getInstance().buildItoF(ret, basicBlock);
            this.addOperand(r);
        } else if (ret.getType().isFloatingType() && funcReType.isIntegerType()) {
            Value r = BuildFactory.getInstance().buildFtoI(ret, basicBlock);
            this.addOperand(r);
        } else {
            this.addOperand(ret);
        }
        this.addInstToBlock(basicBlock);
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ret ");
        if (getOperands().size() == 1) {
//            sb.append(((FunctionType) getNode().getParent().getValue()
//                    .getNode().getParent().getValue().getType()).getRetType()).append(", ");
            sb.append(getOperands().get(0).getType()).append(" ");
            sb.append(getOperands().get(0).getName());
        } else {
            sb.append("void");
        }
        return sb.toString();
    }
}
