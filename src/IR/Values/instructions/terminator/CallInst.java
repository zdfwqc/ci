package IR.Values.instructions.terminator;

import IR.Values.BasicBlock;
import IR.Values.Function;
import IR.Values.Value;
import IR.Values.instructions.OP;
import IR.types.FunctionType;
import IR.types.Type;
import IR.types.VoidType;

import java.util.ArrayList;

public class CallInst extends TerminatorInst {
    public CallInst(BasicBlock basicBlock, Function function, ArrayList<Value> args) {
        super(((FunctionType) function.getType()).getRetType(), OP.Call, basicBlock);
        if (!((FunctionType) function.getType()).getRetType().isVoidType()) {
            this.setName("%" + REG_NUMBER++);
        }
        this.addOperand(function);
        for (Value value : args) {
            this.addOperand(value);
        }
        this.addInstToBlock(basicBlock);
        Function curFunction = basicBlock.getNode().getParent().getValue();
        function.addPred(curFunction);
        curFunction.addSuc(function);
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        Type retType = ((FunctionType) this.getOperands().get(0).getType()).getRetType();
        if (retType.isVoidType()) {
            sb.append("call ");
        } else {
            sb.append(getName()).append(" = call ");
        }
        sb.append(retType).append(" @");
        sb.append(getOperands().get(0).getName()).append("(");
        for (Value value : getOperands()) {
            if (getOperands().indexOf(value) != 0) {
                if (getOperands().indexOf(value) != 1) {
                    sb.append(",");
                }
                sb.append(value.getType()).append(" ");
                sb.append(value.getName());
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
