package IR.Values.instructions.terminator;

import IR.Values.BasicBlock;
import IR.Values.Value;
import IR.Values.instructions.OP;
import IR.types.VoidType;

public class BrInst extends TerminatorInst {
    public BrInst(BasicBlock basicBlock, BasicBlock trueBlock) {
        super(VoidType.type, OP.Br, basicBlock);
        this.addOperand(trueBlock);
        /*
        添加BB前驱后继
         */
        if (basicBlock.getList().getTail() == null ||
                (!(basicBlock.getList().getTail().getValue() instanceof BrInst)
                        && !(basicBlock.getList().getTail().getValue() instanceof RetInst))) {
            basicBlock.addSuc(trueBlock);
            trueBlock.addPred(basicBlock);
        }
        this.addInstToBlock(basicBlock);
    }
    
    public BrInst(BasicBlock basicBlock, Value cond, BasicBlock trueBlock, BasicBlock falseBlock) {
        super(VoidType.type, OP.Br, basicBlock);
        this.addOperand(cond);
        this.addOperand(trueBlock);
        this.addOperand(falseBlock);
        /*
        添加BB前驱后继
         */
        if (basicBlock.getList().getTail() == null ||
                (!(basicBlock.getList().getTail().getValue() instanceof BrInst)
                        && !(basicBlock.getList().getTail().getValue() instanceof RetInst))) {
            basicBlock.addSuc(trueBlock);
            basicBlock.addSuc(falseBlock);
            trueBlock.addPred(basicBlock);
            falseBlock.addPred(basicBlock);
        }
        this.addInstToBlock(basicBlock);
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("br ");
        if (getOperands().size() == 1) {
            sb.append("%");
            sb.append(getOperands().get(0).getName());
        }
        if (getOperands().size() == 3) {
            sb.append(getOperands().get(0).getType()).append(" ");
            sb.append(getOperands().get(0).getName()).append(" %");
            sb.append(getOperands().get(1).getName()).append(" %");
            sb.append(getOperands().get(2).getName());
        }
        return sb.toString();
    }
}
