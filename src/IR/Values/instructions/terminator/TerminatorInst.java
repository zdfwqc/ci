package IR.Values.instructions.terminator;

import IR.Values.BasicBlock;
import IR.Values.instructions.Instruction;
import IR.Values.instructions.OP;
import IR.types.Type;

public abstract class TerminatorInst extends Instruction {
    public TerminatorInst(Type type, OP op, BasicBlock basicBlock) {
        super(type, op, basicBlock);
    }
}
