package IR.Values.instructions;

import IR.Values.BasicBlock;
import IR.Values.User;
import IR.Values.instructions.terminator.BrInst;
import IR.Values.instructions.terminator.RetInst;
import IR.types.Type;
import utils.IList;

public abstract class Instruction extends User {
    private OP op;
    private IList.INode<Instruction, BasicBlock> node;
    private int handler;
    private static int HANDLER = 0;
    
    public Instruction(Type type, OP op, BasicBlock bb) {
        super("", type);
        this.op = op;
        this.node = new IList.INode<>(this);
        this.handler = HANDLER++;
        this.getModule().addInstruction(handler, this);
    }
    
    public void setParent(BasicBlock basicBlock) {
        this.getNode().insertListEnd(basicBlock.getList());
    }
    
    public BasicBlock getParent() {
        return this.getNode().getParent().getValue();
    }
    
    public void addInstToBlock(BasicBlock basicBlock) {
        if (basicBlock.getList().getTail() == null ||
                (!(basicBlock.getList().getTail().getValue() instanceof BrInst)
                        && !(basicBlock.getList().getTail().getValue() instanceof RetInst))) {
            this.getNode().insertListEnd(basicBlock.getList());
        }
    }
    
    public void addInstToBlockHead(BasicBlock basicBlock) {
        this.getNode().insertListHead(basicBlock.getList());
    }
    
    public IList.INode<Instruction, BasicBlock> getNode() {
        return node;
    }
    
    public void setNode(IList.INode<Instruction, BasicBlock> node) {
        this.node = node;
    }
    
    public OP getOp() {
        return op;
    }
    
    public void setOp(OP op) {
        this.op = op;
    }
    
    public int getHandler() {
        return handler;
    }
    
    public void setHandler(int handler) {
        this.handler = handler;
    }
    
    public boolean isBinaryInst() {
        return this.getOp().ordinal() <= OP.Xor.ordinal();
    }
    
    public boolean isRelBinaryInst() {
        return this.getOp().ordinal() >= OP.Lt.ordinal() &&
                this.getOp().ordinal() <= OP.Ne.ordinal();
    }
    
    public boolean isConversionInst() {
        return this.getOp().ordinal() >= OP.Ftoi.ordinal() &&
                this.getOp().ordinal() <= OP.Itof.ordinal();
    }
    
    public boolean isMemInst() {
        return this.getOp().ordinal() >= OP.Alloca.ordinal() &&
                this.getOp().ordinal() <= OP.LoadDep.ordinal();
    }
    
    public boolean isVectorInst() {
        return this.getOp().ordinal() >= OP.InsertEle.ordinal() &&
                this.getOp().ordinal() <= OP.ExtractEle.ordinal();
    }
    
    public boolean isTerminatorInst() {
        return this.getOp().ordinal() >= OP.Br.ordinal() &&
                this.getOp().ordinal() <= OP.Ret.ordinal();
    }
    
    public boolean isNotInst() {
        return this.getOp().ordinal() == OP.Not.ordinal();
    }
}
