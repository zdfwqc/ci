package IR.Values.instructions;

import IR.Values.BasicBlock;
import IR.Values.BuildFactory;
import IR.Values.ConstantFloat;
import IR.Values.ConstantInteger;
import IR.Values.Value;
import IR.types.FloatingType;
import IR.types.PointerType;
import IR.types.Type;
import IR.types.VoidType;
import utils.IList;

public class BinaryInst extends Instruction {
    public BinaryInst(BasicBlock basicBlock, OP op, Value left, Value right) {
        super(VoidType.type, op, basicBlock);
        this.setName("%" + REG_NUMBER++);
        boolean isLTypeInteger = left.getType().isIntegerType() ||
                (left.getType().isPointerType() && ((PointerType) left.getType()).isIntegerType());
        boolean isRTypeInteger = right.getType().isIntegerType() ||
                (right.getType().isPointerType() && ((PointerType) right.getType()).isIntegerType());
        boolean isLTypeFloat = left.getType().isFloatingType() ||
                (left.getType().isPointerType() && ((PointerType) left.getType()).isFloatingType());
        boolean isRTypeFloat = right.getType().isFloatingType() ||
                (right.getType().isPointerType() && ((PointerType) right.getType()).isFloatingType());
        if (isLTypeFloat && isRTypeInteger) {
            Value r = BuildFactory.getInstance().buildItoF(right, basicBlock);
            addOperands(left, r);
            setOp(changeOp(op, true));
        } else if (isLTypeInteger && isRTypeFloat) {
            Value l = BuildFactory.getInstance().buildItoF(left, basicBlock);
            addOperands(l, right);
            setOp(changeOp(op, true));
        } else {
            addOperands(left, right);
            if (isLTypeFloat && isRTypeFloat) {
                setOp(changeOp(op, true));
            }
        }
        setType(getOperands().get(0).getType());
        this.addInstToBlock(basicBlock);
    }

    private OP changeOp(OP op, boolean isItoF) {
        if (op == OP.Add || op == OP.FAdd) {
            return isItoF ? OP.FAdd : OP.Add;
        }
        if (op == OP.Sub || op == OP.FSub) {
            return isItoF ? OP.FSub : OP.Sub;
        }
        if (op == OP.Mul || op == OP.FMul) {
            return isItoF ? OP.FMul : OP.Mul;
        }
        if (op == OP.Div || op == OP.FDiv) {
            return isItoF ? OP.FDiv : OP.Div;
        }
        if (op == OP.Mod || op == OP.FMod) {
            return isItoF ? OP.FMod : OP.Mod;
        }
        return op;
    }

    public void addOperands(Value left, Value right) {
        this.addOperand(left);
        this.addOperand(right);
    }

    public boolean isAdd() {
        return getOp() == OP.Add;
    }

    public boolean isSub() {
        return getOp() == OP.Sub;
    }

    public boolean isFAdd() {
        return getOp() == OP.FAdd;
    }

    public boolean isFSub() {
        return getOp() == OP.FSub;
    }

    public boolean isMul() {
        return getOp() == OP.Mul;
    }

    public boolean isFMul() {
        return getOp() == OP.FMul;
    }

    public boolean isDiv() {
        return getOp() == OP.Div;
    }

    public boolean isFDiv() {
        return getOp() == OP.FDiv;
    }

    public boolean isMod() {
        return getOp() == OP.Mod;
    }

    public boolean isFMod() {
        return getOp() == OP.FMod;
    }

    public boolean isShl() {
        return getOp() == OP.Shl;
    }

    public boolean isShr() {
        return getOp() == OP.Shr;
    }

    public boolean isAnd() {
        return getOp() == OP.And;
    }

    public boolean isOr() {
        return getOp() == OP.Or;
    }

    public boolean isXor() {
        return getOp() == OP.Xor;
    }

    public boolean isLt() {
        return getOp() == OP.Lt;
    }

    public boolean isLe() {
        return getOp() == OP.Le;
    }

    public boolean isGe() {
        return getOp() == OP.Ge;
    }

    public boolean isGt() {
        return getOp() == OP.Gt;
    }

    public boolean isEq() {
        return getOp() == OP.Eq;
    }

    public boolean isNe() {
        return getOp() == OP.Ne;
    }

    public boolean isCond() {
        return isLe() || isLt() || isGe() || isGt() || isEq() || isNe();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getName());
        sb.append(" = ");
        switch (getOp()) {
            case Add -> sb.append("add i32 ");
            case Sub -> sb.append("sub i32 ");
            case FAdd -> sb.append("fadd float ");
            case FSub -> sb.append("fsub float ");
            case Mul -> sb.append("mul i32 ");
            case FMul -> sb.append("fmul float ");
            case Div -> sb.append("sdiv i32 ");
            case FDiv -> sb.append("fdiv float ");
            case Mod -> sb.append("srem i32 ");
            case FMod -> sb.append("frem float ");
            case Shl -> sb.append("shl i32 ");
            case Shr -> sb.append("ashr i32 ");
            case And -> sb.append("and i32 ");
            case Or -> sb.append("or i32 ");
            case Xor -> sb.append("xor i32 ");
            case Lt -> sb.append("icmp lt ");
            case Le -> sb.append("icmp le ");
            case Ge -> sb.append("icmp ge ");
            case Gt -> sb.append("icmp gt ");
            case Eq -> sb.append("icmp eq ");
            case Ne -> sb.append("icmp ne ");
            default -> {
                /* exception handler */
            }
        }
        if (isCond()) {
            sb.append(getOperands().get(0).getType()).append(" ");
        }
        sb.append(getOperands().get(0).getName());
        sb.append(",");
        if (isCond()) {
            sb.append(getOperands().get(1).getType()).append(" ");
        }
        sb.append(getOperands().get(1).getName());
        return sb.toString();
    }
}
