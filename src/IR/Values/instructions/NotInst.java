package IR.Values.instructions;

import IR.Values.BasicBlock;
import IR.Values.Value;
import IR.types.VoidType;

public class NotInst extends Instruction {
    public NotInst(BasicBlock basicBlock, Value value) {
        super(VoidType.type, OP.Not, basicBlock);
        this.setName("%" + REG_NUMBER++);
        this.addOperand(value);
        this.addInstToBlock(basicBlock);
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        /*
        TODO
         */
        return sb.toString();
    }
}
