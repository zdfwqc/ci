package IR.Values.instructions;

public enum OP {
    //binary 二元运算
    Add,
    Sub,
    FAdd,   //F开头表示浮点数运算
    FSub,
    Mul,
    FMul,
    Div,
    FDiv,
    Mod,
    FMod,
    Shl,
    Shr,
    And,
    Or,
    Xor,
    
    Lt,
    Le,
    Ge,
    Gt,
    Eq,
    Ne,
    //conversion op
    Ftoi,
    Itof,
    //Mem
    Alloca,
    Load,
    Store,
    GEP, //get element ptr
    Phi,
    MemPhi,
    LoadDep,
    //vector op
    InsertEle,
    ExtractEle,
    //terminator op
    Br,
    Call,
    Ret,
    //not op
    Not
}
