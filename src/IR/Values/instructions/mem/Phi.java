package IR.Values.instructions.mem;

import IR.Values.BasicBlock;
import IR.Values.Value;
import IR.Values.instructions.OP;
import IR.types.Type;
import IR.types.VoidType;

import java.util.ArrayList;

public class Phi extends MemInst {
    public Phi(BasicBlock basicBlock, Type type, ArrayList<Value> values) {
        super(type, OP.Phi, basicBlock);
        for (Value value : values) {
            this.addOperand(value);
        }
        this.setName("%" + REG_NUMBER++);
        this.addInstToBlockHead(basicBlock);
    }
    
    private ArrayList<Value> getValues() {
        return this.getOperands();
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getName()).append(" = phi ");
        for (int i = 0; i < getOperands().size(); i++) {
            if (i != 0) {
                sb.append(",");
            }
            sb.append("[ ");
            sb.append(getOperands().get(i).getName()).append(", %");
            sb.append(this.getNode().getParent().getValue().getPredecessors().get(i).getName()).append(" ]");
        }
        return sb.toString();
    }
}
