package IR.Values.instructions.mem;

import IR.Values.BasicBlock;
import IR.Values.Value;
import IR.Values.instructions.OP;
import IR.types.PointerType;
import IR.types.Type;


public class LoadInst extends MemInst {
    public LoadInst(BasicBlock basicBlock, Value pointer) {
        super(((PointerType) pointer.getType()).getTargetType(), OP.Load, basicBlock);
        this.setName("%" + REG_NUMBER++);
        this.addOperand(pointer);
        this.addInstToBlock(basicBlock);
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getName());
        sb.append(" = load ");
        sb.append(getType());
        sb.append(", ");
        sb.append(getOperands().get(0).getType());
        sb.append(" ");
        sb.append(getOperands().get(0).getName());
        return sb.toString();
    }
}
