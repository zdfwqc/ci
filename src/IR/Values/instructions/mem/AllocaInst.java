package IR.Values.instructions.mem;

import IR.Values.BasicBlock;
import IR.Values.instructions.OP;
import IR.types.ArrayType;
import IR.types.PointerType;
import IR.types.Type;

public class AllocaInst extends MemInst {
    private boolean isConst;

    private Type allocatedType;

    public AllocaInst(BasicBlock basicBlock, boolean isConst, Type allocatedType) {
        super(new PointerType(allocatedType), OP.Alloca, basicBlock);
        this.setName("%" + REG_NUMBER++);
        this.isConst = isConst;
        this.allocatedType = allocatedType;
        this.addInstToBlock(basicBlock);
        //change -1 length array
        if (allocatedType.isArrayType()) {
            if (((ArrayType) allocatedType).getNumOfElement() == -1) {
                this.allocatedType = new PointerType(((ArrayType) allocatedType).getElementType());
                setType(new PointerType(this.allocatedType));
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getName());
        sb.append(" = alloca ");
        sb.append(allocatedType);
        return sb.toString();
    }

    public void setAllocatedType(Type allocatedType) {
        this.allocatedType = allocatedType;
    }

    public Type getAllocatedType() {
        return allocatedType;
    }

}
