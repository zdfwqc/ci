package IR.Values.instructions.mem;

import IR.Values.BasicBlock;
import IR.Values.BuildFactory;
import IR.Values.GlobalVariable;
import IR.Values.Value;
import IR.Values.instructions.OP;
import IR.types.ArrayType;
import IR.types.PointerType;
import IR.types.Type;

import java.util.ArrayList;

public class GEPInst extends MemInst {
    private Type elementType;
    private Value target;
    
    private static Type getElementType(Value pointer, ArrayList<Value> index) {
        Type type = ((PointerType) pointer.getType()).getTargetType();
        if (type.isArrayType()) {
            for (int i = 0; i < index.size(); i++) {
                type = ((ArrayType) type).getElementType();
            }
        }
        return type;
    }
    
    public GEPInst(BasicBlock basicBlock, Value pointer, ArrayList<Value> index) {
        super(new PointerType(getElementType(pointer, index)), OP.GEP, basicBlock);
        this.setName("%" + REG_NUMBER++);
        if (pointer instanceof GEPInst) {
            target = ((GEPInst) pointer).target;
        }
        if (pointer instanceof AllocaInst) {
            target = pointer;
        }
        if (pointer instanceof GlobalVariable) {
            target = pointer;
        }
        this.addOperand(pointer);
        for (Value value : index) {
            this.addOperand(value);
        }
        this.elementType = getElementType(pointer, index);
        this.addInstToBlock(basicBlock);

//        BuildFactory.getInstance().buildGEPInst()
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getName()).append(" = getelementptr ");
        sb.append(((PointerType) getOperands().get(0).getType()).getTargetType()).append(", ");
        for (int i = 0; i < getOperands().size(); i++) {
            if (i != 0) {
                sb.append(", ");
            }
            sb.append(getOperands().get(i).getType()).append(" ");
            sb.append(getOperands().get(i).getName());
        }
        return sb.toString();
    }
}
