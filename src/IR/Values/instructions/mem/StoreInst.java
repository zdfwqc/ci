package IR.Values.instructions.mem;

import IR.Values.BasicBlock;
import IR.Values.BuildFactory;
import IR.Values.Value;
import IR.Values.instructions.OP;
import IR.types.PointerType;
import IR.types.Type;

public class StoreInst extends MemInst {
    public StoreInst(BasicBlock basicBlock, Value pointer, Value value) {
        super(value.getType(), OP.Store, basicBlock);
        Type ptrType = ((PointerType) pointer.getType()).getTargetType();
        if (ptrType.isIntegerType() && value.getType().isFloatingType()) {
            Value r = BuildFactory.getInstance().buildFtoI(value, basicBlock);
            this.addOperand(r);
            this.addOperand(pointer);
        } else if (ptrType.isFloatingType() && value.getType().isIntegerType()) {
            Value r = BuildFactory.getInstance().buildItoF(value, basicBlock);
            this.addOperand(r);
            this.addOperand(pointer);
        } else {
            this.addOperand(value);
            this.addOperand(pointer);
        }
        this.addInstToBlock(basicBlock);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("store ");
        sb.append(getOperands().get(0).getType()).append(" ");
        sb.append(getOperands().get(0).getName()).append(", ");
        sb.append(getOperands().get(1).getType()).append(" ");
        sb.append(getOperands().get(1).getName());
        return sb.toString();
    }
}
