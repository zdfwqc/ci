package IR.Values.instructions.mem;

import IR.Values.BasicBlock;
import IR.Values.instructions.Instruction;
import IR.Values.instructions.OP;
import IR.types.Type;

public abstract class MemInst extends Instruction {
    public MemInst(Type type, OP op, BasicBlock basicBlock) {
        super(type, op, basicBlock);
    }
}
