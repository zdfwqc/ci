package IR.Values.instructions;

import IR.Values.BasicBlock;
import IR.Values.Value;
import IR.types.FloatingType;
import IR.types.IntegerType;
import IR.types.Type;
import IR.types.VoidType;
import utils.IList;

public class ConvertionInst extends Instruction {
    
    public ConvertionInst(OP op, Value value, BasicBlock basicBlock) {
        super(VoidType.type, op, basicBlock);
        this.setName("%" + REG_NUMBER++);
        addOperand(value);
        if (value.getType().isFloatingType()) {
            setType(IntegerType.i32);
        } else if (value.getType().isIntegerType()) {
            setType(FloatingType.type);
        }
        this.addInstToBlock(basicBlock);
    }
    
    public boolean isFtoi() {
        return getOp() == OP.Ftoi;
    }
    
    public boolean isItof() {
        return getOp() == OP.Itof;
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getName());
        sb.append(" = ");
        switch (getOp()) {
            case Ftoi -> {
                sb.append("ftoi float ");
                sb.append(getOperands().get(0).getName());
                sb.append(" to i32");
            }
            case Itof -> {
                sb.append("itof i32 ");
                sb.append(getOperands().get(0).getName());
                sb.append(" to float");
            }
            default -> {
                /* exception handler*/
            }
        }
        return sb.toString();
    }
}
