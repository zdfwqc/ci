package IR;

import IR.Values.User;
import IR.Values.Value;

/*
    用来定义use-def链
 */
public class Use {
    private Value value;
    private User user;
    private int posOfOperand;

    public Use(Value value, User user, int pos) {
        this.value = value;
        this.user = user;
        this.posOfOperand = pos;
    }

    public int getPosOfOperand() {
        return posOfOperand;
    }

    public void setPosOfOperand(int posOfOperand) {
        this.posOfOperand = posOfOperand;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    /* public Use(Value value, User user) {
        this.value = value;
        this.user = user;
    } */

}
