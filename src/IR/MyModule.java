package IR;

/*
    顶层容器（编译单元）
    单例模式
 */

import IR.Analysis.DomAnalysis;
import IR.Values.BasicBlock;
import IR.Values.Function;
import IR.Values.GlobalVariable;
import IR.Values.instructions.Instruction;
import utils.IList;

import java.util.ArrayList;
import java.util.HashMap;

public class MyModule {
    private static MyModule module;
    private ArrayList<GlobalVariable> globalVars;
    private IList<Function, MyModule> functions;
    private HashMap<Integer, Instruction> instructions;

    private MyModule() {
        this.globalVars = new ArrayList<>();
        this.functions = new IList<>(this);
        this.instructions = new HashMap<>();
    }

    public static MyModule getInstance() {
        if (module != null) {
            return module;
        }
        module = new MyModule();
        return module;
    }

    public void addInstruction(int handle, Instruction instruction) {
        this.instructions.put(handle, instruction);
    }

    public IList<Function, MyModule> getFunctions() {
        return this.functions;
    }

    public void addGlobalVar(GlobalVariable globalVariable) {
        this.globalVars.add(globalVariable);
    }

    public ArrayList<GlobalVariable> getGlobalVars() {
        return globalVars;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (GlobalVariable var : globalVars) {
            sb.append(var).append("\n\n");
        }
        for (IList.INode<Function, MyModule> function : functions) {
            /*
            test
             */
            //            if (function.getValue().isLibraryFunction()) {
            //                continue;
            //            }

            if (!function.getValue().isLibraryFunction()) {
                //DomAnalysis.analyzeDom(function.getValue());
                sb.append("define dso_local ");
            } else {
                sb.append("declare ");
            }
            sb.append(function.getValue());
            if (!function.getValue().isLibraryFunction()) {
                sb.append("{\n");
                for (IList.INode<BasicBlock, Function> bb : function.getValue().getBBList()) {
                    if (!bb.equals(function.getValue().getBBList().getHead())) {
                        sb.append("\n");
                    }
                    //sb.append(bb.getValue().getType()).append(":\n");
                    sb.append(bb.getValue().getName()).append(":\n");
                    sb.append(bb.getValue());
                }
                sb.append("}\n");
            } else {
                sb.append("\n\n");
            }
        }
        return sb.toString();
    }
}
