package IR.types;

/*
public enum Type {
    VOID,   //无返回值，无大小
    INTEGER,    //基础类型
    FLOATING_POINT,     //浮点类型
    POINTER,    //指针类型
    ARRAY,      //数组类型
    LABEL,      //标识代码块，类似于汇编的label
    TOKEN,      //与指令相关的特殊值类型
    FUNCTION_TYPE
}
 */
public abstract class Type {
    
    public boolean isArrayType() {
        return false;
    }
    
    public boolean isFloatingType() {
        return false;
    }
    
    public boolean isFunctionType() {
        return false;
    }
    
    public boolean isIntegerType() {
        return false;
    }
    
    public boolean isLabelType() {
        return false;
    }
    
    public boolean isPointerType() {
        return false;
    }
    
    public boolean isVoidType() {
        return false;
    }
    
    public abstract Type getType();
}
