package IR.types;


import java.util.ArrayList;
import java.util.Collections;

public class ArrayType extends Type {
    private Type elementType;
    private int numOfElement;
    
    public ArrayType(Type elementType) {
        this.elementType = elementType;
        this.numOfElement = 0;
    }
    
    public ArrayType(Type elementType, int numOfElement) {
        this.elementType = elementType;
        this.numOfElement = numOfElement;
    }
    
    public Type getElementType() {
        return this.elementType;
    }
    
    public int getNumOfElement() {
        return this.numOfElement;
    }
    
    public ArrayList<Integer> getDim() {
        ArrayList<Integer> dim = new ArrayList<>();
        Type arr = this;
        while (arr.isArrayType()) {
            dim.add(((ArrayType) arr).getNumOfElement());
            arr = ((ArrayType) arr).elementType;
        }
        return dim;
    }
    
    public int getCapacity() {
        ArrayList<Integer> dims = getDim();
        int capacity = 1;
        for (int i = 0; i < dims.size(); i++) {
            capacity *= dims.get(i);
        }
        return capacity;
    }
    
    @Override
    public boolean isArrayType() {
        return true;
    }
    
    public Type getType() {
        return this;
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append(numOfElement).append(" x ");
        sb.append(elementType.toString()).append("]");
        return sb.toString();
    }
}
