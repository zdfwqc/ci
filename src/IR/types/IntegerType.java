package IR.types;

public class IntegerType extends Type {
    private int bit;
    
    public static final IntegerType i1 = new IntegerType(1);
    
    public static final IntegerType i32 = new IntegerType(32);
    
    private IntegerType(int bit) {
        this.bit = bit;
    }
    
    @Override
    public Type getType() {
        return this;
    }
    
    @Override
    public boolean isIntegerType() {
        return true;
    }
    
    @Override
    public String toString() {
        return "i" + bit;
    }
}
