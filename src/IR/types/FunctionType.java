package IR.types;

import java.util.ArrayList;

public class FunctionType extends Type {
    private ArrayList<Type> parameters;
    private Type retType;

    public FunctionType() {
        this.parameters = new ArrayList<>();
    }

    public FunctionType(Type retType) {
        this.parameters = new ArrayList<>();
        this.retType = retType;
        changeNoLengthArrayType();
    }

    public FunctionType(Type retType, ArrayList<Type> parametersTypes) {
        this.parameters = new ArrayList<>(parametersTypes);
        this.retType = retType;
        changeNoLengthArrayType();
    }

    private void changeNoLengthArrayType() {
        ArrayList<Integer> target = new ArrayList<>();
        for (Type type : parameters) {
            if (type.isArrayType()) {
                if (((ArrayType) type).getNumOfElement() == -1) {
                    target.add(parameters.indexOf(type));
                }
            }
        }
        for (int index : target) {
            parameters.set(index, new PointerType(
                    ((ArrayType) parameters.get(index))
                            .getElementType()));
        }
    }

    public ArrayList<Type> getParametersType() {
        return this.parameters;
    }

    public void addParameterType(Type type) {
        this.parameters.add(type);
    }

    public void setRetType(Type type) {
        this.retType = type;
    }

    public Type getRetType() {
        return retType;
    }

    @Override
    public boolean isFunctionType() {
        return true;
    }

    @Override
    public Type getType() {
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(retType);
        //sb.append(" (");
        //for (Type type : parameters) {
        //    if (parameters.indexOf(type) != 0) {
        //        sb.append(", ");
        //    }
        //    sb.append(type);
        //}
        //sb.append(")");
        return sb.toString();
    }
}
