package IR.types;

public class PointerType extends Type {
    private Type targetType;
    
    public PointerType(Type targetType) {
        this.targetType = targetType;
    }
    
    @Override
    public boolean isPointerType() {
        return true;
    }
    
    @Override
    public String toString() {
        return targetType.toString() + "*";
    }
    
    @Override
    public Type getType() {
        return this;
    }
    
    public Type getTargetType() {
        return this.targetType;
    }
}
