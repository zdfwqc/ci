package IR.types;

public class LabelType extends Type {
    private int handler;
    private static int HANDLER = 0;
    
    public LabelType() {
        this.handler = HANDLER++;
    }
    
    @Override
    public boolean isLabelType() {
        return true;
    }
    
    @Override
    public String toString() {
        return "label_" + handler;
    }
    
    @Override
    public Type getType() {
        return this;
    }
}
