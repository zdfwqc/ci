package IR.types;

public class FloatingType extends Type {
    public static final FloatingType type = new FloatingType();
    
    private FloatingType() {
    }
    
    @Override
    public Type getType() {
        return type;
    }
    
    @Override
    public boolean isFloatingType() {
        return true;
    }
    
    @Override
    public String toString() {
        return "float";
    }
}
