import IR.MyModule;
import VISITOR.SysY2022Lexer;
import VISITOR.SysY2022Parser;
import VISITOR.Visitor;
import config.Config;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.*;

public class CompilerWithoutBackEnd {
    public static void main(String[] args) throws IOException {
        Config.init(args);
        
        FileInputStream fileInputStream = new FileInputStream(Config.fileInPath);
        PrintStream printStream = new PrintStream(Config.fileOutPathWithOutBackend);
        System.setIn(fileInputStream);
        System.setOut(printStream);
        CharStream inputStream = CharStreams.fromStream(System.in); // 获取输入流
        SysY2022Lexer lexer = new SysY2022Lexer(inputStream);
        CommonTokenStream tokenStream = new CommonTokenStream(lexer); // 词法分析获取 token 流
        SysY2022Parser parser = new SysY2022Parser(tokenStream);
        ParseTree tree = parser.compUnit();
        // 获取语法树的根节点
        Visitor visitor = new Visitor();
        visitor.visit(tree);
    
//        PassModule.getInstance().runIRPasses();
        
        System.out.println(MyModule.getInstance().toString());

    }
}
