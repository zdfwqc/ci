#include "configargs.h"

#define GCCPLUGIN_VERSION_MAJOR   11
#define GCCPLUGIN_VERSION_MINOR   2
#define GCCPLUGIN_VERSION_PATCHLEVEL   1
#define GCCPLUGIN_VERSION  (GCCPLUGIN_VERSION_MAJOR*1000 + GCCPLUGIN_VERSION_MINOR)

static char basever[] = "11.2.1";
static char datestamp[] = "20211011";
static char devphase[] = "";
static char revision[] = "[releases/gcc-11 revision b3dfc8635d26b9c3cbe7731dd7b8be8a2242eab9]";

/* FIXME plugins: We should make the version information more precise.
   One way to do is to add a checksum. */

static struct plugin_gcc_version gcc_version = {basever, datestamp,
						devphase, revision,
						configuration_arguments};
